Discuss Data
============

Open Platform for the Interactive Discussion of Research Data Quality
(on the example of area studies on the post-Soviet region)

The software is provided as a Docker image and can be obtained from the project's own repository ``docker.gitlab.gwdg.de/discuss-data``.
To get the latest image, run:

::

     docker pull docker.gitlab.gwdg.de/discuss-data/discuss-data:latest

Discuss Data is looking forward to your contributions.
To provide feedback, report bugs or propose enhancements, feel free to use our `Issue tracker`_.
For contributions to the source code, see the `documentation on contributions`_.


.. _`Issue tracker`: https://gitlab.gwdg.de/discuss-data/discuss-data/-/issues
.. _`documentation on contributions`: https://discuss-data.pages.gwdg.de/discuss-data/docs/contribution.html


Documentation
-------------

`General documentation`_

`Coverage Report`_

.. _`General documentation`: https://discuss-data.pages.gwdg.de/discuss-data/docs/
.. _`Coverage Report`: https://discuss-data.pages.gwdg.de/discuss-data/coverage/

Preparing the development environment
-------------------------------------

Provided, you have Docker, Python and Npm installed on your system, prepare your local environment by installing node
modules and python dependencies into a virtual environment. Currently, only Docker 19, Node 12 and Python 3.7 are well tested and
supported.

::

   npm install
   python -m venv venv
   . venv/bin/activate
   python -m pip install -U pip
   pip install -r requirements/local.txt

To build the docker stack and install fixtures as well as test data to fill up the database, an init script is provided for your convenience. In its last step it will ask for input on the command line to create a superuser.

::

   ./init



Settings
--------

Settings are read from environment configuration files located in ``.envs/``.
Bear in mind that the settings provided ``.envs/.local`` are not meant to be used in a production environment.
To create your own environment settings, read the documentation on `Configuring the Environment`_.

.. _`Configuring the Environment`: https://cookiecutter-django.readthedocs.io/en/latest/developing-locally-docker.html#configuring-the-environment

Testing
-------

Run type checks with mypy:

::

  mypy discuss_data

This project is configured to run pytest within a docker container. To run the tests, check your test coverage, and generate an HTML coverage report:

::

     docker-compose run --rm django ./run_pytest


Localization and Internationalization
-------------------------------------

For further information see `Localization`_.

.. _`Localization`: https://docs.djangoproject.com/en/3.0/topics/i18n/translation/#localization-how-to-create-language-files


Email Server
------------

The Compose stack comprises the SMTP server `MailHog`_ with a web interface available under ``http://127.0.0.1:8025``.

.. _mailhog: https://github.com/mailhog/MailHog

Search
------

To initialize or rebuild the elasticsearch index run:

::

   docker-compose run --rm django python manage.py search_index --rebuild

Print the whole index in development by accessing:

::

   http://localhost:9200/users/_search?pretty

Deploy to production
--------------------

For building and testing the production image, build the Dockerfile with the ``production`` target.

::

     docker build --target production -f compose/django/Dockerfile .

For an example on how to actually deploy to a production environment see the `Discuss Data Docker`_ project.

.. _`Discuss Data Docker`: https://gitlab.gwdg.de/discuss-data/discuss-data-docker

Deploy a feature branch to the dev server
--------------------

Check out the branch and build the production image:

.. code-block:: shell
    export TAG=123456
    docker build -f compose/django/Dockerfile --target=production --tag harbor.gwdg.de/sub-fe-pub/discuss_data_local_django:$TAG .
    docker push harbor.gwdg.de/sub-fe-pub/discuss_data_local_django:$TAG


On the server, change docker-compose.yml to refer to the new image

.. code-block:: yaml
    diff --git a/docker-compose.yml b/docker-compose.yml
    index 1c76b28..2a1907c 100755
    --- a/docker-compose.yml
    +++ b/docker-compose.yml
    @@ -51,7 +51,7 @@
        django:
        # overwrite user in dockerfile since this does not have a fixed UID/GID
        user: "100:101"
    -      image: docker.gitlab.gwdg.de/discuss-data/discuss-data:${ENVIRONMENT:-staging}
    +      image: harbor.gwdg.de/sub-fe-pub/discuss_data_local_django:123456
        depends_on:
            - postgres
            - redis

Pull the new image and recreate the django and ingress services

.. code-block:: shell
    docker-compose pull django && \
    docker-compose up -d django ingress --force-recreate && \
    docker-compose logs -f


Postgres Major Version Update
---------------------
Changing to a Postgres major version needs a little work as the existing database files will not be compatible to the new version.

You need to:

1. Backup the existing database  (see https://cookiecutter-django.readthedocs.io/en/latest/docker-postgres-backups.html)
2. Change the Postgres version in the postgres Dockerfile.
3. Remove the existing postgres data volume and add a new data volume for postgres data in docker-compose.override.yml. Thus the volume will be empty and new database files will be initialized on startup.
4. Restore the database using the backup dump created in step #1.
5. Start the stack with docker-compose build.

PDF Generation
--------------

Uses pdf_service as webservice. Then it uses it to create PDFs.


Badges
------

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style
.. image:: https://img.shields.io/badge/commitizen-friendly-brightgreen.svg
     :target: http://commitizen.github.io/cz-cli/
     :alt: Commitizen friendly
.. image:: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
     :target: https://github.com/semantic-release/semantic-release
     :alt: semantic-release
.. image:: https://img.shields.io/badge/License-AGPLv3-blue.svg
     :target: https://www.gnu.org/licenses/agpl-3.0
     :alt: License: AGPL v3
