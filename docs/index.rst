.. Discuss Data documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Discuss Data Project Documentation
====================================================================

Table of Contents:

.. toctree::
   :maxdepth: 2

   requirements
   contributing
   cms


Indices & Tables
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
