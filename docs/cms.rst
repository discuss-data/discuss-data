============================
Discuss Data CMS
============================

Wagtail CMS
^^^^^^^^^^^

The Discuss Data Documentation and the Landing Page are authored via the Wagtail CMS. It is accessible with a staff-Account at ``http://localhost:8000/cms/``

To create a Landing Page add a child page of type "Landing page" to the Root Page with the slug `discuss-data-landingpage` and fill in content form fields.

Documentation
^^^^^^^^^^^^^

The default way to serve the 3-level documentation at the path ``/documentation`` needs an IndexPage (as the first documentation level) connected as Root Page to the default site in Wagtail.

As child pages to this IndexPage zero to multiple ManualIndexPages (second level) can be added, which have ManualPages (third level) as their respective child pages.

To link ManualPages and content blocks to views and forms the fields `slug` and `field_slug` are used. If they are not present, the models help_text fields are shown as a fallback. If no help_text whatsoever is there nothing will be shown.


Create Pages Skeletons
^^^^^^^^^^^^^^^^^^^^^^

To create empty skeleton pages for the landing page and some documentation pages call the initial_data django runscript:

`docker-compose run --rm django python manage.py runscript initial_data`

