Requirements
============

- install python 
    `sudo apt install python3 python3-dev`

- create environment
    `python3 -m venv venv`

- upgrade pip
    `python -m pip install -U pip`

- install requirements
    `pip install -r requirements/local.txt`

- Error: pg_config executable not found.
    `sudo apt install libpq-dev` (bzw. python3-dev)

