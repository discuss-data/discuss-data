from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.documents import urls as wagtaildocs_urls
from wagtail import urls as wagtail_urls

from allauth.account.views import confirm_email

from discuss_data.core.views import (
    landing_page,
    about_page,
    contacts_page,
    security_page,
    login_page,
)
from discuss_data.ddusers.views import datasets_act_feed
from discuss_data.dddatasets.views.core import category_page, category_list
from discuss_data.api.root import api

urlpatterns = [
    # remove next 4 lines and template files!
    # path("", TemplateView.as_view(template_name="pages/home.html"), name="home"),
    # path(
    #    "about/", TemplateView.as_view(template_name="pages/about.html"), name="about"
    # ),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL + "doc/", include("django.contrib.admindocs.urls")),
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path(
        "users/", include(("discuss_data.ddusers.urls", "ddusers"), namespace="ddusers")
    ),
    # Your stuff: custom urls includes go here
    path("core/", include(("discuss_data.core.urls", "core"), namespace="core")),
    # hitcount
    path("hitcount/", include('hitcount.urls', namespace='hitcount')),
    # path(
    #    "publication/",
    #    include(
    #        ("discuss_data.ddpublications.urls", "ddpublications"),
    #        namespace="ddpublications",
    #    ),
    # ),
    path("dataset/", include("discuss_data.dddatasets.urls", namespace="dddatasets")),
    path("dhrep/", include("discuss_data.dhrep.urls", namespace="dhrep")),
    path('accounts/', include('allauth.urls')),
    # custom account confirmation email url, needs to come below accounts
    path('verify-email/<str:key>/',
        confirm_email,
        name="account_confirm_email"
    ),
    path("", landing_page, name="core.landing_page"),
    path("about/", about_page, name="core.about_page"),
    path("contacts/", contacts_page, name="core.contacts_page"),
    path("security/", security_page, name="core.security_page"),
    path("login/", login_page, name="core.login_page"),
    path("dashboard/", datasets_act_feed, name="ddusers.dashboard_page"),
    path("categories/", category_list, name="category_list"),
    path("categories/<slug:slug>/", category_page, name="category_page"),
    # API
    path("api/", api.urls)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# wagtail URL def
urlpatterns += [
    path("cms/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path("documentation/", include(wagtail_urls)),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
