"""
Base settings to build other settings files upon.
"""

from json import JSONDecodeError
import environ


ROOT_DIR = (
    environ.Path(__file__) - 3
)  # (discuss_data/config/settings/base.py - 3 = discuss_data/)
APPS_DIR = ROOT_DIR.path("discuss_data")

env = environ.Env()

# .env is currently used to configure docker-compose => REMOVE
# READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=False)
# if READ_DOT_ENV_FILE:
#     # OS environment variables take precedence over variables from .env
#     env.read_env(str(ROOT_DIR.path(".env")))


# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = "Europe/Berlin"
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = "en-us"
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = False
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
# https://docs.djangoproject.com/en/dev/ref/settings/#locale-paths
LOCALE_PATHS = [ROOT_DIR.path("locale")]

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {"default": env.db("DATABASE_URL")}
DATABASES["default"]["ATOMIC_REQUESTS"] = True

# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = "config.urls"

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",  # Handy template tags
    "django.contrib.admin",
    "django.contrib.admindocs",  # check pip dependency
]
THIRD_PARTY_APPS = [
    "actstream",
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.openid_connect',
    "crispy_forms",
    "crispy_bootstrap4",
    "django_activeurl",
    "django_bleach",
    "django_q",
    "guardian",
    "intercoolerjs",
    "rest_framework",
    "reversion",
    "taggit",
    "django_elasticsearch_dsl",
    "mptt",
    "django_sendfile",
    "request",
    "hitcount",
]

LOCAL_APPS = [
    # Your stuff: custom apps go here
    "discuss_data.ddusers.apps.DdusersConfig",
    "discuss_data.dddatasets.apps.DddatasetsConfig",
    "discuss_data.ddcomments.apps.DdcommentsConfig",
    "discuss_data.core.apps.CoreConfig",
    "discuss_data.ddpublications.apps.DdpublicationsConfig",
    "discuss_data.dhrep.apps.DhrepConfig",
    "discuss_data.pages.apps.PagesConfig",
    "discuss_data.utils.apps.UtilsConfig",
    "discuss_data.docupages.apps.DocupagesConfig",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIGRATIONS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
MIGRATION_MODULES = {"sites": "discuss_data.contrib.sites.migrations"}

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    # Needed to login by username in Django admin
    'django.contrib.auth.backends.ModelBackend',
    # `allauth` specific authentication methods
    'allauth.account.auth_backends.AuthenticationBackend',
    "guardian.backends.ObjectPermissionBackend",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = "ddusers.User"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = "ddusers.dashboard_page"
LOGOUT_REDIRECT_URL = "core.landing_page"

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "request.middleware.RequestMiddleware",
    "django.contrib.admindocs.middleware.XViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "allauth.account.middleware.AccountMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # custom middlewares
    "intercooler_helpers.middleware.HttpMethodOverride",
    "intercooler_helpers.middleware.IntercoolerData",
    "intercooler_helpers.middleware.IntercoolerRedirector",
    "discuss_data.core.middleware.IntercoolerMessageMiddleware",
]

# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR.path("staticfiles"))
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [str(APPS_DIR.path("static"))]
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(ROOT_DIR.path("media"))
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = "/media/"

# DATA
# ------------------------------------------------------------------------------
DATA_ROOT = str(ROOT_DIR.path("data"))


# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        "DIRS": [str(APPS_DIR.path("templates"))],
        "OPTIONS": {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "discuss_data.utils.context_processors.settings_context",
                # `allauth` needs this from django
                'django.template.context_processors.request',
                "wagtail.contrib.settings.context_processors.settings",
                "wagtailmenus.context_processors.wagtailmenus",
            ],
        },
    }
]
# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap4"
CRISPY_TEMPLATE_PACK = "bootstrap4"

# force form-control classes, as this seems not to be done by the bs4 template pack
CRISPY_CLASS_CONVERTERS = {
    "textinput": "textinput form-control",
    "textarea": "textarea form-control",
    "dateinput": "dateinput form-control",
    "urlinput": "urlinput form-control",
    "select": "select form-control",
    "passwordinput": "passwordinput form-control",
}


# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
X_FRAME_OPTIONS = "DENY"

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND",
    default="django.core.mail.backends.smtp.EmailBackend",
)
# https://docs.djangoproject.com/en/2.2/ref/settings/#email-timeout
EMAIL_TIMEOUT = 5
# https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = env(
    "DJANGO_DEFAULT_FROM_EMAIL",
    default="Discuss Data <noreply@discuss-data.net>",
)

ALT_FROM_EMAIL = env(
    "DJANGO_ALT_FROM_EMAIL",
    default="Discuss Data <info@discuss-data.net>",
)

EMAIL_SUBJECT_PREFIX = env("DJANGO_EMAIL_SUBJECT_PREFIX", default="[Discuss Data] ")

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL
ADMIN_URL = env("DJANGO_ADMIN_URL", default="admin/")
# Wagtail Admin URL
CMS_URL = env("DJANGO_CMS_URL", default="cms/")

# https://docs.djangoproject.com/en/dev/ref/settings/#admins
try:
    ADMINS = [(key, value) for key, value in env.json("DJANGO_ADMINS", {}).items()]
except JSONDecodeError as err:
    raise ValueError(f"The DJANGO_ADMINS environment variable must be json-parseable: {err}") from err
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {"console": {"class": "logging.StreamHandler", "formatter": "verbose"}},
    "root": {"level": "DEBUG", "handlers": ["console"]},
}

# django-compressor
# ------------------------------------------------------------------------------
# https://django-compressor.readthedocs.io/en/latest/quickstart/#installation
INSTALLED_APPS += ["compressor"]
STATICFILES_FINDERS += ["compressor.finders.CompressorFinder"]
# Your stuff...
# ------------------------------------------------------------------------------

# Wagtail
INSTALLED_APPS += [
    "wagtail.contrib.forms",
    "wagtail.contrib.redirects",
    "wagtail_modeladmin",
    "wagtail.embeds",
    "wagtail.contrib.search_promotions",
    "wagtail.sites",
    "wagtail.users",
    "wagtail.snippets",
    "wagtail.documents",
    "wagtail.images",
    "wagtail.search",
    "wagtail.admin",
    "wagtail",
    # 'condensedinlinepanel',
    "wagtailmenus",
    "modelcluster",
    "wagtail.contrib.table_block",
]


MIDDLEWARE += [
    "wagtail.contrib.redirects.middleware.RedirectMiddleware",
]

WAGTAIL_SITE_NAME = "Discuss Data"
WAGTAIL_GRAVATAR_PROVIDER_URL = None
WAGTAIL_PASSWORD_MANAGEMENT_ENABLED = False
WAGTAIL_EMAIL_MANAGEMENT_ENABLED = False

# Wagtail pages slugs
WAGTAIL_LANDING_PAGE = env("WAGTAIL_LANDING_PAGE", default="discuss-data-landingpage")
WAGTAIL_CONTACTS_PAGE = env("WAGTAIL_CONTACTS_PAGE", default="contacts")
WAGTAIL_SECURITY_PAGE = env("WAGTAIL_SECURITY_PAGE", default="security")

# If WAGTAILDOCS_SERVE_METHOD is unspecified or set to None, the default method is
# 'redirect' when a remote storage backend is in use (i.e. one that exposes a URL but
# not a local filesystem path), and 'serve_view' otherwise.
# see https://docs.wagtail.io/en/stable/reference/settings.html#documents
WAGTAILDOCS_SERVE_METHOD = "redirect"

# Discuss Data Host URL
DISCUSS_DATA_HOST = env("DISCUSS_DATA_HOST", default="https://discuss-data.net")

DARIAH_CRUD_URL = env(
    "DARIAH_CRUD_URL", default="https://dhrepworkshop.de.dariah.eu/1.0/dhcrud/"
)  # <-- staging -- testing --> https://trep.de.dariah.eu/1.0/dhcrud/
DARIAH_DOI_PREFIX = env("DARIAH_DOI_PREFIX", default="10.20375")
DARIAH_HDL_PREFIX = env("DARIAH_HDL_PREFIX", default="21.T11991")
DARIAH_STORAGE_LOCATION = env(
    "DARIAH_STORAGE_LOCATION", default="https://cdstar.de.dariah.eu/test/dariah/"
)
DARIAH_PDP_URL = env("DARIAH_PDP_URL", default="https://pdpdev.de.dariah.eu/")
DARIAH_PDP_CLIENT_ID = env("DARIAH_PDP_CLIENT_ID", default="discussdata")
DARIAH_PDP_REDIRECT_URI = env(
    "DARIAH_PDP_REDIRECT_URI", default=DISCUSS_DATA_HOST + "/dhrep/token"
)
DARIAH_PUBLISH_URL = env(
    "DARIAH_PUBLISH_URL", default="https://dhrepworkshop.de.dariah.eu/1.0/dhpublish/"
)  # <-- staging -- testing --> https://trep.de.dariah.eu/1.0/dhpublish/

ELASTICSEARCH_DSL = {
    "default": {"hosts": "elasticsearch:9200"},
}

WAGTAILSEARCH_BACKENDS = {
    "default": {
        "BACKEND": "wagtail.search.backends.elasticsearch7",
        "URLS": ["http://elasticsearch:9200"],
        "INDEX": "wagtail",
        "TIMEOUT": 5,
        "OPTIONS": {},
        "INDEX_SETTINGS": {},
    }
}


RUNSCRIPT_SCRIPT_DIR = "djangoscripts"

# basic bleach configuration
# ^= do not allow any html, delete all the tags
BLEACH_ALLOWED_TAGS = []
BLEACH_STRIP_TAGS = False
BLEACH_STRIP_COMMENTS = True

# actstream
ACTSTREAM_SETTINGS = {
    "MANAGER": "discuss_data.core.managers.DiscussDataActionManager",
    "FETCH_RELATIONS": True,
    "USE_JSONFIELD": False,
    "GFK_FETCH_DEPTH": 1,
}

# DataCite
DC_USER = env("DATACITE_USERNAME", default="")
DC_PWD = env("DATACITE_PASSWORD", default="")
DC_URL = env("DATACITE_URL", default="https://mds.test.datacite.org")
DC_PREFIX = env("DATACITE_PREFIX", default="10.5072")

# Sendfile
SENDFILE_ROOT = env("SENDFILE_ROOT", default="/app/data/")
SENDFILE_URL = env("SENDFILE_URL", default="/datasets/")


# CACHES
# ------------------------------------------------------------------------------
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": env("REDIS_URL"),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            # Mimicing memcache behavior.
            # http://niwinz.github.io/django-redis/latest/#_memcached_exceptions_behavior
            "IGNORE_EXCEPTIONS": True,
        },
    }
}
# https://docs.djangoproject.com/en/2.2/topics/http/sessions/#using-cached-sessions
SESSION_ENGINE = "django.contrib.sessions.backends.cached_db"

# django-Q
# ------------------------------------------------------------------------------
# https://django-q.readthedocs.io/en/latest/configure.html
Q_CLUSTER = {
    "name": "discuss_data",
    "workers": 8,
    "recycle": 500,
    "retry": 3650, # has to be bigger than timeour, see https://django-q2.readthedocs.io/en/master/configure.html#retry
    "timeout": 3600,  # =1h
    "compress": True,
    "save_limit": 250,
    "queue_limit": 500,
    "cpu_affinity": 1,
    "label": "Django Q",
    "django_redis": "default",
}

# Discuss Data: Matrix for Dataset licenses and access model validation
DD_LICENSE_MATRIX = {
    "OA": (
        "license-odc-by-v1-0",
        "license-odbl",
        "license-no-sharing",
        "license-individual",
    ),
    "RA": ("license-no-sharing", "license-individual"),
    "MO": ("nolicense"),
}

# Discuss Data Community Space Config
DD_TITLE = env("DJANGO_DD_TITLE", default="Discuss Data")
DD_SUBTITLE = env(
    "DJANGO_DD_SUBTITLE",
    default="Archiving, sharing and discussing research data on Eastern Europe, South Caucasus and Central Asia",
)
# already exists as DISCUSS_DATA_HOST => remove
# DD_URL = env("DJANGO_DD_URL", default="https://discuss-data.net")

# Configuration of django_request webanalytics logging
REQUEST_LOG_USER = False
REQUEST_ANONYMOUS_IP = True
REQUEST_IGNORE_PATHS = (
    r"^admin/",
    r"/media/",
)
REQUEST_TRAFFIC_MODULES = (
    "request.traffic.UniqueVisitor",
    "request.traffic.UniqueVisit",
    "request.traffic.Hit",
    "request.traffic.Error",
)

# django allauth
ACCOUNT_EMAIL_REQUIRED = True
# custom adapter is not needed; resolved by proxy config, see https://gitlab.gwdg.de/discuss-data/discuss-data-docker/-/blob/master/nginx/nginx.conf?ref_type=heads
# => REMOVE
#ACCOUNT_ADAPTER = 'discuss_data.core.utils.DDAccountAdapter'

# list of auth providers which already verify user accounts
VERIFIED_PROVIDERS = ['dariah',]

# https://docs.djangoproject.com/en/3.2/releases/3.2/#customizing-type-of-auto-created-primary-keys
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

ZENODO_ACCESS_TOKEN = env("ZENODO_ACCESS_TOKEN", default="UNDEFINED")
ZENODO_URL = env("ZENODO_URL", default="https://sandbox.zenodo.org/api")
