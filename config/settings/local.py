import socket

from .base import *  # pylint: disable=W0401,W0614
from .base import env

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="vug6D5vrVBwICIW8iqLno3I4lKAbrWmuruYpgTaHNc7k12WzS61BNswdg4w4lwI2",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ["django", "localhost", "0.0.0.0", "127.0.0.1"]


# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = env("EMAIL_HOST", default="mailhog")
# https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = 1025

# WhiteNoise
# ------------------------------------------------------------------------------
# http://whitenoise.evans.io/en/latest/django.html#using-whitenoise-in-development
INSTALLED_APPS += ["whitenoise.runserver_nostatic"]  # noqa F405


# django-debug-toolbar
# ------------------------------------------------------------------------------
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
# INSTALLED_APPS += ["debug_toolbar"]  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
# MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
# DEBUG_TOOLBAR_CONFIG = {
#    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel"],
#    "SHOW_TEMPLATE_CONTEXT": True,
# }
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ["127.0.0.1", "0.0.0.0", "10.0.2.2"]

# DOCKER
hostname, _, ips = socket.gethostbyname_ex(socket.gethostname())
INTERNAL_IPS += [ip[:-1] + "1" for ip in ips]  # type: ignore

# django-extensions
# ------------------------------------------------------------------------------
# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration
INSTALLED_APPS += ["django_extensions"]  # type: ignore # noqa F405

SENDFILE_BACKEND = "django_sendfile.backends.development"


SOCIALACCOUNT_ADAPTER = "discuss_data.ddusers.adapter.SocialAccountDebuggingAdapter"
# Authentication
SOCIALACCOUNT_PROVIDERS = {
    "openid_connect": {
        "APPS": [  # this is an array, can configure multiple apps
            {
                "provider_id": "local",
                "name": "Local Keycloak OIDC",
                "client_id": env("KC_CLIENT_ID"),
                "secret": env("KC_CLIENT_SECRET"),
                "settings": {
                    "server_url": env("KC_ISSUER"),
                    "logo_src": "images/dariah-de.svg",
                    "description": "The default way to log into Discuss Data relying on the Dariah infrastructure",
                },
            },
            {
                "provider_id": "localXXX",
                "name": "Local2 Keycloak OIDC",
                "client_id": env("KC_CLIENT_ID"),
                "secret": env("KC_CLIENT_SECRET"),
                "settings": {
                    "server_url": env("KC_ISSUER"),
                    "logo_src": "images/orcid-logo.svg",
                    "description": "Authentication via ORCID, still experimental",
                },
            },
        ]
    }
}
