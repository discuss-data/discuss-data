import os

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.redis import RedisIntegration

from .base import *  # pylint: disable=W0401,W0614
from .base import env

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env("DJANGO_SECRET_KEY")
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list("DJANGO_ALLOWED_HOSTS", default=["discuss-data.net"])
DEBUG = env.bool("DJANGO_DEBUG", default=False)

# DATABASES
# ------------------------------------------------------------------------------
DATABASES["default"] = env.db("DATABASE_URL")  # noqa F405
DATABASES["default"]["ATOMIC_REQUESTS"] = True  # noqa F405
DATABASES["default"]["CONN_MAX_AGE"] = env.int("CONN_MAX_AGE", default=60)  # noqa F405

# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-proxy-ssl-header
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-ssl-redirect
SECURE_SSL_REDIRECT = env.bool("DJANGO_SECURE_SSL_REDIRECT", default=True)
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-secure
SESSION_COOKIE_SECURE = True
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies#Cookie_prefixes
SESSION_COOKIE_NAME = "__Host-id"
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-secure
CSRF_COOKIE_SECURE = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-name
CSRF_COOKIE_NAME = "__Host-csrf"
# https://docs.djangoproject.com/en/4.2/ref/settings/#std-setting-CSRF_TRUSTED_ORIGINS
CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS", default=[])
# https://docs.djangoproject.com/en/dev/topics/security/#ssl-https
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-seconds
SECURE_HSTS_SECONDS = 31536000
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-include-subdomains
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
    "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", default=True
)
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-hsts-preload
SECURE_HSTS_PRELOAD = env.bool("DJANGO_SECURE_HSTS_PRELOAD", default=True)
# https://docs.djangoproject.com/en/dev/ref/middleware/#x-content-type-options-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
    "DJANGO_SECURE_CONTENT_TYPE_NOSNIFF", default=True
)

# DJANGO CSP
# ------------------------
# https://django-csp.readthedocs.io/en/latest/index.html
MIDDLEWARE.insert(  # noqa F405
    MIDDLEWARE.index(  # noqa F405
        "django.contrib.messages.middleware.MessageMiddleware"
    )
    + 1,
    "csp.middleware.CSPMiddleware",
)

CSP_DEFAULT_SRC = env("CSP_DEFAULT_SRC", default="'none'")
CSP_CONNECT_SRC = env("CSP_CONNECT_SRC", default="'self'")
CSP_FONT_SRC = env("CSP_FONT_SRC", default="'self'")
CSP_IMG_SRC = env("CSP_IMG_SRC", default="'self'")
CSP_SCRIPT_SRC = env("CSP_SCRIPT_SRC", default="'self'")
CSP_STYLE_SRC = env("CSP_STYLE_SRC", default="'self'")
CSP_REPORT_URI = env("CSP_REPORT_URI", default=None)
CSP_REPORT_ONLY = env("CSP_REPORT_ONLY", default=False)

# STATIC
# ------------------------
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES[0]["OPTIONS"]["loaders"] = [  # noqa F405
    (
        "django.template.loaders.cached.Loader",
        [
            "django.template.loaders.filesystem.Loader",
            "django.template.loaders.app_directories.Loader",
        ],
    )
]

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
EMAIL_HOST = env("DJANGO_EMAIL_HOST", default="localhost")
EMAIL_PORT = env("DJANGO_EMAIL_PORT", default=25)
EMAIL_USE_TLS = env("DJANGO_EMAIL_USE_TLS", default=True)

# https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = env("DJANGO_SERVER_EMAIL", default=DEFAULT_FROM_EMAIL)
# https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = env("DJANGO_EMAIL_SUBJECT_PREFIX", default="[Discuss Data] ")

# django-compressor
# ------------------------------------------------------------------------------
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_ENABLED
COMPRESS_ENABLED = env.bool("COMPRESS_ENABLED", default=True)
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_STORAGE
COMPRESS_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"
# https://django-compressor.readthedocs.io/en/latest/settings/#django.conf.settings.COMPRESS_URL
COMPRESS_URL = STATIC_URL  # noqa F405

# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {"require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}},
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
    },
    "root": {"level": f'{"DEBUG" if DEBUG else "INFO"}', "handlers": ["console"]},
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
        "django.security.DisallowedHost": {
            "level": "ERROR",
            "handlers": ["console", "mail_admins"],
            "propagate": True,
        },
    },
}

# Sentry
# ------------------------------------------------------------------------------
if sentry_dsn := os.getenv("SENTRY_DSN"):
    sentry_sdk.init(
        dsn=sentry_dsn,
        integrations=[DjangoIntegration(), RedisIntegration()],
        environment=env("ENVIRONMENT", default="local"),
        traces_sample_rate=0.5,
    )

# Sentry django-Q reporter
# ------------------------------------------------------------------------------
# https://github.com/de1ux/django-q-sentry/blob/raven-to-sentry-sdk/django_q_sentry/sentry.py
# class Sentry():
#
#    def __init__(self, dsn, **kwargs):
#        self.client = sentry_sdk.init(dsn=dsn, **kwargs)
#
#    @staticmethod
#    def report():
#        sentry_sdk.capture_exception()

# django-Q
# ------------------------------------------------------------------------------
# # https://django-q.readthedocs.io/en/latest/configure.html#django-redis
# Q_CLUSTER = {
#     "name": "discuss_data",
#     "django_redis": "default",
#     #    'error_reporter': {
#     #        'sentry': {
#     #            'https://824e439c530d423e989185abee8c5cc2@dev2.discuss-data.net/2',
#     #            'environment=env("ENVIRONMENT", default="local")',
#     #        }
#     #    }
# }

SENDFILE_BACKEND = "django_sendfile.backends.nginx"

# Add the ORCID provider to the installed apps
INSTALLED_APPS.insert(
    INSTALLED_APPS.index("allauth.socialaccount.providers.openid_connect") + 1,
    "allauth.socialaccount.providers.orcid",
)

# Require https
ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"

# use a custom adapter on debug
if DEBUG is True:
    SOCIALACCOUNT_ADAPTER = "discuss_data.ddusers.adapter.SocialAccountDebuggingAdapter"

# Provider specific configuration
SOCIALACCOUNT_PROVIDERS = {
    "openid_connect": {
        "APPS": [  # this is an array, can configure multiple apps
            {
                "provider_id": "dariah",
                "name": "DARIAH AAI",
                "client_id": env("DARIAH_CLIENT_ID"),
                "secret": env("DARIAH_CLIENT_SECRET"),
                "settings": {
                    "server_url": env("DARIAH_ISSUER"),
                    "logo_src": "images/dariah-de.svg",
                    "description": "The default way to log into Discuss Data relying on the Dariah infrastructure",
                },
            }
        ]
    },
    # "orcid": {
    #     "APPS": [
    #         {
    #             "client_id": env("ORCID_CLIENT_ID"),
    #             "secret": env("ORCID_CLIENT_SECRET"),
    #             "settings": {
    #                 "logo_src": "images/orcid-logo.svg",
    #             },
    #         },
    #     ],
    #     "BASE_DOMAIN": "sandbox.orcid.org",
    #     "MEMBER_API": True,
    # },
}
