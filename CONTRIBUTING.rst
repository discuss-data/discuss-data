============================
Contributing to Discuss Data
============================

As an open source project, Discuss Data welcomes contributions of many forms.

Examples of contributions include:

* Vulnerability reports
* Code patches
* Documentation improvements
* `Bug reports`_ and patch reviews
* Feature requests
* Merge requests

Vulnerability reports
=====================

If you need to report a vulnerability, please send an E-Mail to gitlab+discuss-data-discuss-data-9289-issue-@gwdg.de
Be aware that, by default, your E-Mail will be sent unencrypted.

Code patches
============

Discuss Data uses pre-commit hooks to ensure the compliance of contributions with our coding standards.::

  pre-commit install

Commits have to follow the conventional commit standard. This repo is configured
commitizen friendly so that you can use it to comply with our commit format.

We welcome and are likely to accept merge requests that follow the aforesaid.

.. _`Bug reports`: https://gitlab.gwdg.de/discuss-data/discuss-data/-/issues


How to fix a Bug
================

To contribute by fixing a Bug, follow these steps:

#. Write a test that
    - reproduces the wanted behaviour and
    - fails because of the bug.

#. Write a fix that
    - produces the wanted behaviour and
    - lets the previosly written test pass.
