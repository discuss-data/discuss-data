from django.contrib import admin

from discuss_data.ddpublications.models import Publication

admin.site.register(Publication)
