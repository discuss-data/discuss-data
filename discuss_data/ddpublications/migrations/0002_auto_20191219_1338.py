# Generated by Django 2.2.6 on 2019-12-19 12:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("ddpublications", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(model_name="publication", name="authors",),
        migrations.RemoveField(model_name="publication", name="date",),
        migrations.RemoveField(model_name="publication", name="editors",),
        migrations.RemoveField(model_name="publication", name="included_in",),
        migrations.RemoveField(model_name="publication", name="issn",),
        migrations.RemoveField(model_name="publication", name="issue",),
        migrations.RemoveField(model_name="publication", name="places",),
        migrations.RemoveField(model_name="publication", name="pub_type",),
        migrations.RemoveField(model_name="publication", name="publisher",),
        migrations.RemoveField(model_name="publication", name="volume",),
    ]
