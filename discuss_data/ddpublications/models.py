import uuid

from django.db import models
from django.utils.html import format_html
from django_bleach.models import BleachField

from . import validators


class Publication(models.Model):
    class Meta:
        ordering = ["authors_text", "title"]

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    title = models.CharField(
        max_length=200,
        help_text='Title (including subtitle, seperated with ":")',
        verbose_name="Title",
    )
    authors_text = models.CharField(
        max_length=200,
        help_text="Example: Andreas Heinrich, Felix Herrmann, Heiko Pleines",
        verbose_name="Authors",
    )
    places_text = models.CharField(max_length=200, blank=True, verbose_name="Place")
    publisher_text = models.CharField(
        max_length=200, blank=True, verbose_name="Publisher"
    )
    included_text = models.CharField(
        max_length=200,
        blank=True,
        help_text="Collection title, Journal issue, etc.",
        verbose_name="Included in",
    )
    year = models.CharField(max_length=20)
    url = models.URLField(blank=True)
    isbn = models.CharField(
        max_length=20,
        blank=True,
        validators=[validators.validate_isbn],
    )
    doi = models.CharField(
        max_length=200,
        blank=True,
        validators=[validators.validate_doi],
    )
    pages = models.CharField(max_length=200, blank=True)
    description = BleachField(
        blank=True,
        help_text="Additional information about the publication",
    )

    # authors = models.ManyToManyField('Person', related_name='publication_authors_person', blank=True)
    # editors = models.ManyToManyField('Person', related_name='publication_editors_person', blank=True)
    # places = models.ManyToManyField('Place', blank=True)
    # publisher = models.ForeignKey('Publisher', blank=True, null=True, on_delete=models.PROTECT)
    # date = models.DateField(blank=True, null=True)
    # issn = models.CharField(max_length=200, blank=True)
    # issue = models.CharField(max_length=200, blank=True)
    # volume = models.CharField(max_length=200, blank=True)

    # included_in = models.ForeignKey('Publication', blank=True, null=True, on_delete=models.PROTECT, related_name='publication_included_in_publication')

    def short_citation(self):
        return "{}, {}, {}".format(self.authors_text, self.title, self.year)

    def citation(self):
        citation_list = list()
        if self.included_text:
            citation_list.append("in: {}".format(self.included_text))
        if self.publisher_text and self.places_text:
            citation_list.append("{}: {}".format(self.publisher_text, self.places_text))
        elif self.places_text:
            citation_list.append(self.places_text)
        elif self.publisher_text:
            citation_list.append(self.publisher_text)
        else:
            pass
        if self.year:
            citation_list.append(self.year)
        if self.pages:
            citation_list.append(self.pages)
        if self.doi:
            doi_url = "http://doi.org/{}".format(self.doi)
            citation_list.append(doi_url)
        if self.isbn:
            isbn_str = "ISBN: {}".format(self.isbn)
            citation_list.append(isbn_str)
        if self.url:
            citation_list.append(self.url)

        return format_html(
            "<em>{}</em>, {}, {}",
            self.authors_text,
            self.title,
            ", ".join(citation_list),
        )

    def __str__(self):
        # return '{}: {} ({})'.format(self.get_authors(), self.title, self.get_pub_type_display())
        return self.title

    # def get_authors(self):
    #    alist = list()
    #    for a in self.authors.all():
    #        alist.append(a.name)
    #    return ','.join(alist)


class Person(models.Model):
    name = models.CharField(max_length=200)
    firstnames = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Place(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Publisher(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
