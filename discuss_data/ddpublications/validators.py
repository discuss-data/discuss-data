"""field validators for publications
"""

import logging

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from . import isbn


logger = logging.getLogger(__name__)


def validate_isbn(value):
    """validate an ISBN's field value

    :param value: ISBN's field value
    :type value: map

    :raises ValidationError: invalid ISBN
    """
    if not isbn.is_valid_isbn(value):
        raise ValidationError(
            _("%(value)s is not a valid ISBN"),
            params={"value": value},
        )


def validate_doi(value):
    """validate a DOI's field value

    :param value: DOI's field value
    :type value: map

    :raises ValidationError: invalid DOI
    """
    # find() returns -1 if not found
    if value.find("doi") != -1:
        raise ValidationError(_("Remove URL parts (e.g. http://doi.org/) from DOI."))
    if not value.startswith("10."):
        raise ValidationError(
            _("%(value)s is not a valid DOI"),
            params={"value": value},
        )
