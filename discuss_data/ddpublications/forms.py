from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    HTML,
    Div,
    Layout,
    Submit,
)
from django.forms import ModelForm

from discuss_data.core.helpers import get_help_texts

from .models import Publication

UPDATED_MSG = """
    {% if updated %}<div id='updated-msg' class='alert alert-success' ic-trigger-on='scrolled-into-view' ic-action='delay:2500;fadeOut;remove' role='alert'>
    <span class='spinner-sm'></span><strong>{{ target|title }} saved</strong></div>{% endif %}
    {% if err %}<div id='updated-msg' class='alert alert-danger' role='alert'>
    <span class='spinner-sm'></span><strong>Error! {{ target|title }} could not be saved. Error encountered in fields {% for field in err %} {{ field }}, {% endfor %}</strong></div> {% endif %}"""

ADD_EDIT_HEADING = (
    "<h2>{% if new %}Add{% else %}Edit{% endif %} {{ target|title }}</h2><hr>"
)
REQ_FIELD = "<p class='help-block'>[*] required field</p><hr>"


class PublicationForm(ModelForm):
    class Meta:
        model = Publication
        # fields = '__all__'
        fields = [
            "title",
            "authors_text",
            "places_text",
            "publisher_text",
            "included_text",
            "year",
            "url",
            "isbn",
            "doi",
            "pages",
            # "description",
        ]
        help_texts = get_help_texts("edit-publication")

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div("title", css_class="col-md-12",),
            Div("authors_text", css_class="col-md-6",),
            Div("publisher_text", css_class="col-md-2",),
            Div("places_text", css_class="col-md-2",),
            Div("year", css_class="col-md-2",),
            Div("included_text", css_class="col-md-10",),
            Div("pages", css_class="col-md-2",),
            Div("url", css_class="col-md-6",),
            Div("doi", css_class="col-md-3",),
            Div("isbn", css_class="col-md-3",),
            # Div("description", css_class="col-md-6",),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML("<span id='indicator'><span class='spinner-sm'></span> Saving...</span>"),
        FormActions(Submit("save", "Save {{ target }}", ic_indicator="#indicator"),),
        Div(HTML(UPDATED_MSG),),
    )
