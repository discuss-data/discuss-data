from django.apps import AppConfig


class DdpublicationsConfig(AppConfig):
    name = "discuss_data.ddpublications"
