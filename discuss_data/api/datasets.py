"""Router for Datasets API."""

import json
import logging

from django.core import serializers
from ninja import Router

from discuss_data.dddatasets.models import DataSet

logger = logging.getLogger(__name__)


router = Router()


@router.get("/")
def retrieve_datasets(request) -> list[dict[str, str | int]]:
    """Get all publicy available datasets."""
    queryset = DataSet.objects.filter(published=True)
    datasets = json.loads(
        serializers.serialize(
            "json",
            queryset,
            fields=[
                "title",
                "subtitle",
                "data_access",
                "version",
                "published_main_category",
            ],
            use_natural_foreign_keys=True,
            use_natural_primary_keys=True,
        )
    )
    serialized_datasets: list[dict[str, str | int]] = []
    for dataset in datasets:
        serialized_datasets.append(dataset["fields"])

    logger.info(serialized_datasets)
    return serialized_datasets
