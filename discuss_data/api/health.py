"""Router for health checks."""

from ninja import Router


router = Router()


@router.get("/healthz")
def get_health(request):
    """Get health status."""
    return {"status": "OK"}
