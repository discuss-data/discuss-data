"""API entrypoint."""

from ninja import NinjaAPI

from discuss_data.api.datasets import router as datasets_router
from discuss_data.api.health import router as health_router

api = NinjaAPI()

api.add_router("/datasets/", datasets_router)
api.add_router("_", health_router)
