/* Project specific Javascript goes here. */

/* Initialize Sentry
 * see https://docs.sentry.io/platforms/javascript/
 */
Sentry.init({
  dsn: 'https://db809fd6ff40b0387e9200d1ae15b38c@errs.sub.uni-goettingen.de/5',
  integrations: [new Sentry.Integrations.BrowserTracing({
    tracingOrigins: ["0.0.0.0", "localhost", "discuss-data.net", "dev.discuss-data.net", /^\//],
  })],
});

$(document).ready(function(){
  /*show sentry feedback*/
  if (document.getElementById("feedback")) {
    Sentry.showReportDialog({ eventId: '{{ sentry_event_id }}' })
  }

  /* Get token string from anchor
  * using jQuery
  */
  if ((document.getElementById("tokenizer")) && (window.location.hash)) {
    window.location = window.location.origin
            + window.location.pathname
            + window.location.hash.substr(1);
  }

});

/* Dropzone configuration options
 * see https://www.dropzonejs.com/#configuration
 */

// "userImageDropzone" is the camelized version of the HTML element's ID
// Dropzone config for user profile image
Dropzone.options.userImageDropzone = {
  paramName: "photo", // The name that will be used to transfer the file
  maxFilesize: 2, // MB
  // limit to one file per upload
  uploadMultiple: false,
  maxFiles:1,
  init: function() {
    this.on('addedfile', function(file) {
      if (this.files.length > 1) {
        this.removeFile(this.files[0]);
      }
    });
  },
  // accepted mime types
  acceptedFiles:'image/jpeg,image/png,image/gif,image/jpg',
};

// Dropzone config for dataset image
Dropzone.options.dsImageDropzone = {
  autoProcessQueue: false,
  paramName: "image", // The name that will be used to transfer the file
  maxFilesize: 2, // MB
  // limit to one file per upload
  uploadMultiple: false,
  maxFiles:1,
  init: function() {
    // remove a files if multiple files were added to list
    this.on('addedfile', function(file) {
      if (this.files.length > 1) {
        this.removeFile(this.files[0]);
      }
    });

    var myDropzone = this;
    // Update selector to match your button
    $("#ds-image-upload").click(function (e) {
        e.preventDefault();
        myDropzone.processQueue();
    });

    this.on("sending", function(file, xhr, formData) {
        // Append all form inputs to the formData Dropzone will POST
        var data = $('#ds-image-dropzone').serializeArray();
        $.each(data, function(key, el) {
            formData.append(el.name, el.value);
        });
    });

    // trigger intercooler refresh on success
    this.on("success", function(file, resp) {
      element = $("#dataset-image")
      Intercooler.refresh(element);
    });
  },
  // accepted mime types
  acceptedFiles:'image/jpeg,image/png,image/gif,image/jpg',
}

/*
 * Create dropzone objects for datafiles upload
 * programmatically to allow dynamic ids
 * Triggered by magic intercooler load request to make sure
 * that all divs have been created first
 */
$(document).on('complete.ic', '.ic-datafile-edit', function(e){
  var id = $(this).attr( "data-id" );
  var dzurl = $(this).attr( "data-url");
  var message = $(this).attr( "data-message");
  var dstype = $(this).attr( "ds-type");
  var dzid = "#dropzone-" + id;

  // allow only PDF uploads for data reviews
  if ( dstype === "DR" ) {var acc_files = "application/pdf"};

  var myDropzone = new Dropzone(dzid, {
    url: dzurl,
    autoProcessQueue: false,
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 2000, // MB
    dictDefaultMessage: message,
    // limit to one file per upload
    uploadMultiple: false,
    chunking: true,
    chunkSize: 2000000, // Bytes
    retryChunks: true,
    addRemoveLinks: true,
    maxFiles:1,
    init: function() {
      this.on('error', function(error, server_response) {
        try {
          document.querySelector("#messages").innerHTML = '<div class="alert alert-danger" role="alert" ic-action="delay:2500;fadeOut;remove">' + server_response + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        }
        catch {
          console.log(server_response + " could not be written to #messages");
        }
      });
      // remove all files but one if multiple files were added to list
      this.on('addedfile', function(file) {
        if (this.files.length > 1) {
          this.removeFile(this.files[0]);
        }
      });
      /* Filter progress values of 100 which dropzone sends periodically
       * Update custom progress bar while dz-progress is hidden by css
       * Round progress float-value for text presentation
       */
      this.on('uploadprogress', function(file, progress, bytesSent) {
        try {
          document.querySelector("#messages").innerHTML = '';
        }
        catch {
          console.log("#messages not found");
        }
        if( progress != 100 && bytesSent <= file['size']) {
          progressBar = document.querySelector(".progress .progress-bar");
          progressBar.style.width = progress + "%";
          progressBar.innerHTML = Math.round(progress) + "%";
        };
      });
      /* Update selector to match the form button
       * if no file is chosen for upload (only the file type is altered by the form)
       * do nothing here and the form will be sent by intercooler
       */
      $("#ds-datafile-upload-" + id).click(function (e) {
        if (myDropzone.files.length > 0) {
          e.preventDefault();
          e.stopPropagation();
          myDropzone.processQueue();
        }
      });


      this.on("sending", function(file, xhr, formData) {
        try {
          document.querySelector("#messages").innerHTML = '';
        }
        catch {
          console.log("#messages not found");
        }
          // Append all form inputs to the formData Dropzone will POST
          var data = $('#ds-datafile-dropzone-' + id).serializeArray();
          $.each(data, function(key, el) {
              formData.append(el.name, el.value);
          });
      });
      // trigger intercooler refresh on success
      this.on("success", function(file, resp) {
        progressBar = document.querySelector(".progress .progress-bar");
        progressBar.style.width = "100%";
        progressBar.innerHTML = "100%";
        element = $("#data-files")
        Intercooler.refresh(element);
        // try {
        //   document.querySelector("#messages").innerHTML = '<div class="alert alert-success" role="alert">Successful upload of file: ' + file.name + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        // }
        // catch {
        //   console.log("success message could not be written to #messages");
        // }
      });
    },
    acceptedFiles: acc_files
  });
});

$(document).on("archive-not-found",'body', function() {
  console.log("archive form resetted");
  formdiv = document.getElementById("archivalsignature-form");
  formdiv.reset();
  console.log(formdiv);
});

$(document).on("messages-trigger",'body', function() {
//$(document).on("complete.ic", 'body', function(evt, elt, data, status, xhr, requestId) {
  element = $("#messages")
  //Intercooler.refresh(element);
  Intercooler.triggerRequest(element);
});

/*
 * Refresh dataset image on bootstrap modal hidden

$(document).on('hidden.bs.modal','#dsImageModal', function() {
  element = $("#dataset-image")
  Intercooler.refresh(element);
});
*/


/*
* JSON lookup using twitter typeahead.js lib
* see: https://twitter.github.io/typeahead.js/examples/
*/
function tagTypeahead(tagType, urlString){
  // Constructs the suggestion engine
  var tags = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      // The url points to a json file that contains an array of tag strings
      prefetch: {
        url: urlString,
        ttl: 0
      }
  });
  // Initializing the typeahead with remote dataset without highlighting
  $('#' + tagType + '-tags .typeahead').typeahead(null, {
      name: 'tags',
      source: tags,
      limit: 10 /* Specify max number of suggestions to be displayed */
  });
};

/*
 * functions to init tagTypeahead() for specific tags with URLs
 */

$(document).on('complete.ic','#language-loader',function() {
    $(tagTypeahead("language",  "/dataset/prep/tags/language/list/" ));
})

$(document).on('complete.ic','#keyword-loader',function() {
  $(tagTypeahead("keyword",  "/dataset/prep/tags/keyword/list/" ));
})

$(document).on('complete.ic','#disciplines-loader',function() {
  $(tagTypeahead("disciplines",  "/dataset/prep/tags/disciplines/list/" ));
})

$(document).on('complete.ic','#methods_of_data_analysis-loader',function() {
  $(tagTypeahead("methods_of_data_analysis",  "/dataset/prep/tags/methods_of_data_analysis/list/" ));
})

$(document).on('complete.ic','#methods_of_data_collection-loader',function() {
  $(tagTypeahead("methods_of_data_collection",  "/dataset/prep/tags/methods_of_data_collection/list/" ));
})

$(document).on('complete.ic','#user-interests',function() {
  $(tagTypeahead("user-keyword",  "/core/tags/keywords/" ));
})

/*
 * Mirrored typing for searching
 */
$(document).on('keyup','.search-top',function() {
  $(".search-middle").val( this.value );
})


$(document).on('keyup','.search-middle',function() {
    $(".search-top").val( this.value );
})

$(document).ready(function(){
  var path = window.location.pathname;
    path = decodeURIComponent(path);
    $("#help_toc a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            if (path.length === href.length) {
              $(this).closest('a').addClass('toc_active');
            }
        }
    });
})

/*
 * Active header for filter
 */
$(document).on('click','.docucard-button',function() {
  $( ".docucard-header-active" ).removeClass('docucard-header-active');
  if( $( this ).attr('aria-expanded') === "true") {
    $( this ).parents('.card-header').addClass('docucard-header-active');
  }
  else {
    $( this ).parents('.card-header').removeClass('docucard-header-active');
  }
});

/*
 * click on discuss button scrolls down to discuss-tab
 */
$(document).on('click','#discuss-button',function() {
  $('html, body').animate({
    scrollTop: ($('#discuss-tab').offset().top)
  },500);
});


/*
 *  when comment listing is complete:
 *  if hash included in url scroll down to the element
 */
$(document).on('complete.ic', '#comments-complete', function() {
  var hash = window.location.hash.substr(1);
  if (hash.length > 0) {
    elem = $('#' + hash);
    if (elem.length > 0) {
      $('html, body').animate({
        // decrease by 60 to place beneath header
        scrollTop: ($('#' + hash).offset().top-60)
      },500);
    };
  }
});

/*
 * Remove modal manually in cases where an intercooler request conflicts
 * with bootstraps data-dismiss="modal".
 * Called like this: ic-on-beforeSend="removeModal()"
 */
function removeModal() {
  $(".modal").modal('hide');
};

/*
 * disable buttons prior to submit and show spinner
 * this should be included in future intercooler versions per default
 * https://github.com/intercoolerjs/intercooler-js/issues/220
 */
$(document).on('beforeSend.ic', function (event, el, data) {
  if (el.is('form')) {
    // disable button
    el.find('input[type="submit"], button[type="submit"]').prop('disabled');

    // ensure we don't submit if already submitted
    if (el.hasClass('form-submitted')) {
        event.preventDefault();
    } else {
      // show spinner
      const spinner = el.find('.fa-spinner');
      if (spinner) {
          spinner.show();
      }
      el.addClass('form-submitted');
    }
  }
});

var input = document.getElementById("myInput");


/*
* trigger a search, when a search phrase has been typed in and
  "enter" has been pressed
*/
$(document).on('keyup','.search-ui',function() {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    $( ".search-button").trigger('click');
  }
});

/*
* when a search filter is applied trigger a click on search button
*/
$(document).on('click','.search-filter-input', function() {
  // Trigger the button element with a click
  $( ".search-button").trigger('click');
});


/*
 * Attach to any element with .textarea or .textinput class and a maxlength
 * attribute a div which shows the number of remaining chars left to type
 * if remaining number falls under threshold
 */

$(document).on('keyup','.textarea, .textinput, .tag-input', function() {
  // get maxlength attribute
  var maxLength = $(this).attr( "maxlength" );
  // set threshold to 10% of maxLength for large lengths or to maxLength for small
  var threshold = maxLength
  if (maxLength > 10) {
    threshold = maxLength / 10
  }

  if (maxLength != null){
    var length = $(this).val().length;
    var remainingLength = maxLength-length;
    // get this elements id
    var elemId = $(this).attr('id');
    // construct new id using this elements id
    var charsLeftId = elemId + '_chars_left';

    if (remainingLength < threshold) {
      if ($('#' + charsLeftId).length) {
        // if new div exists set text
        $('#'+charsLeftId).text(remainingLength);
      }
      else {
        // create new div using new id
        $("<div class='chars-left' id='" + charsLeftId  + "'>" +  remainingLength + "</div>").insertAfter('#' + elemId);
      }
    }
    else {
      // if chars are deleted and remainingLength > 50 make new div empty
      if ($('#' + charsLeftId).length) {
        $('#'+charsLeftId).text('');
      }
    }
  }
});


$(document).on('click','.bool-button', function() {
  $(this).button('toggle')
  $(this).parent('label').button('toggle')
});

$(document).on('click','.hide-infobox', function() {
  $(".infobox").fadeOut(1500);
});

$(document).on('click','#add-creator', function() {
  // increment total forms value in management form
  total_forms = $('#id_dataset_creator-TOTAL_FORMS').attr("value");
  new_id_no = total_forms
  old_id_no = +(total_forms)-1
  $('#id_dataset_creator-TOTAL_FORMS').attr("value", +(total_forms)+1);
  last_form = $(".crispyformset").last();
  new_form = last_form.clone()
  last_form.attr("class", "row");
  // change new_form ids
  new_form.find("*").each(function () {
    if (this.id) {
      id_string = this.id.replace( old_id_no, new_id_no);
      this.id = id_string;
    }
    if (this.name) {
      name_string = this.name.replace( old_id_no, new_id_no);
      if ( this.nodeName == 'INPUT') {
        // set name to empty string
        this.value = "";
      }
      if ( this.nodeName == 'SELECT') {
        // set type to PER
        this.value = "PER";
      }
      this.name = name_string;
    }
    if (this.nodeName == 'LABEL') {
      for_string = this.getAttribute("for").replace( old_id_no, new_id_no);
      this.setAttribute("for", for_string);
    }
  });
  $(new_form).insertAfter(last_form);
});

// activate sortablejs plugin on mouseover
$(document).on('mouseover','#sortable-list',function() {
  var el = document.getElementById("sortable-list");

  Sortable.create(el, {
    filter: ".sortable-ignore", // Selectors that do not lead to dragging (String or Function)
  });
})

/*
// Element dragging ended
$(document).on('end', '#sortable-list', function (eventObject) {
  //console.log('onEnd Event');
  evt = eventObject.originalEvent;
  var itemEl = evt.item; // dragged HTMLElement
  //evt.oldIndex; // element's old index within old parent
  //evt.newIndex; // element's new index within new parent
});
*/
