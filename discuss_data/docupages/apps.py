from django.apps import AppConfig


class DocupagesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "discuss_data.docupages"
