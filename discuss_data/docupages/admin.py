from django.contrib import admin

# Register your models here.
from discuss_data.docupages.models import DocuPage


class DocuPageAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "slug", )
    search_fields = ["title", "body"]


admin.site.register(DocuPage, DocuPageAdmin)
