from django.db import models
from django.utils.translation import gettext as _
from django_bleach.models import BleachField

# Create your models here.
class DocuPage(models.Model):
    title = models.CharField(max_length=256,)
    slug = models.SlugField(blank=False, null=False)
    intro = models.CharField(max_length=256, blank=True, null=True)
    body = BleachField(verbose_name=_("Body"),)

    def __str__(self):
        return self.title
