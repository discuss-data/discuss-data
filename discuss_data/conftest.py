import logging
import pytest

from django.core.exceptions import ValidationError
from django.core.management import call_command
from wagtail.core.models import Page, Site

from discuss_data.pages.models import (
    IndexPage,
    LandingPage,
    LicensePage,
    ManualIndexPage,
)

logger = logging.getLogger(__name__)

page_structure = (
    # (page_title, page_slug, page_type, parent_slug),
    ("Discuss Data Documentation", "discuss-data-documentation", IndexPage, "root"),
    ("Data Upload", "data-upload", ManualIndexPage, "discuss-data-documentation"),
    ("Rules", "rules", ManualIndexPage, "discuss-data-documentation"),
    ("Best Practices", "best-practices", ManualIndexPage, "discuss-data-documentation"),
    ("Landing Page", "discuss-data-landingpage", LandingPage, "root"),
)


# enable database for all tests
@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


# @pytest.fixture(autouse=True)
# def run():
#     """
#     Insert initial data in to the database using the ORM (more robust than fixtures)
#     """
#     try:
#         default_home = Page.objects.get(slug="home")

#         default_home.slug = "home-old"
#         default_home.save_revision().publish()
#         default_home.save()
#     except Exception:
#         print("default home not touched")

#     def create_page(page_title, page_slug, page_type, parent_slug, page_name=None):
#         print("creating page '{}' as child of '{}'".format(page_slug, parent_slug,))

#         if page_name:
#             new_page = page_type(title=page_title, slug=page_slug, name=page_name)
#         else:
#             new_page = page_type(title=page_title, slug=page_slug,)
#         # add page to parent
#         parent = Page.objects.get(slug=parent_slug).specific
#         try:
#             parent.add_child(instance=new_page)
#             # save and publish
#             revision = new_page.save_revision()
#             revision.publish()
#             new_page.save()
#             print("page '{}' created as child of '{}'".format(page_slug, parent_slug,))

#             # Set root page for Site
#             if page_slug == "discuss-data-documentation":
#                 print("set {} as root page for site".format(page_slug,))
#                 site = Site.objects.get(is_default_site=True)
#                 site.root_page_id = new_page.id
#                 site.save()
#                 print("site saved")
#         except ValidationError:
#             print("page with slug '{}' already exists in database".format(page_slug,))

#     for elem in page_structure:
#         ptitle, pslug, ptype, parentslug = elem
#         # create new page
#         create_page(ptitle, pslug, ptype, parentslug)

#     # delete default home page
#     try:
#         default_home = Page.objects.get(slug="home-old")
#         print("delete default home page")
#         default_home.delete()
#     except Exception as e:
#         logger.error(e)

#     # Dummy pages for licenses
#     create_page("Licenses", "licenses", IndexPage, "root")
#     create_page(
#         "Attribution: Open Data Commons Attribution License (ODC-By) v1.0",
#         "license-odc-by-v1-0",
#         LicensePage,
#         "licenses",
#         "ODC-By v1.0",
#     )
#     create_page(
#         "Attribution and Share Alike: Open Data Commons Open Database License (ODbL)",
#         "license-odbl",
#         LicensePage,
#         "licenses",
#         "ODbL",
#     )
#     create_page(
#         "No Sharing", "license-no-sharing", LicensePage, "licenses", "No Sharing"
#     )
