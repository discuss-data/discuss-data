{% load static i18n %}

#table(
  stroke: none,
  columns: (auto, 1fr),
  [{% trans 'Title' %}], [{{ ds.title }}],
  [{% trans 'Subtitle' %}], [{% if subtitle %}{{ ds.subtitle }}{% else %} – {% endif %}],
  [{% trans 'Version' %}], [{{ ds.version }}],
  [{% trans 'Creators' %}],
    [{% for elem in ds.get_creators %}
      {{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
  [{% trans 'Uploaded by' %}], [{{ ds.owner.get_academic_name }}],
  [{% trans 'Main Category' %}], [{{ ds.published_main_category }}],
  [{% trans 'Additional Categories' %}],
    [{% if not ds.published_categories.all %} – {% endif %}
    {% for elem in ds.published_categories.all %}
      {{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
  [{% trans 'Institutional Affiliation' %}],
    [{% if ds.institutional_affiliation %}{{ ds.institutional_affiliation|linebreaks|safe }}{% else %} – {% endif %}],
  [{% trans 'Publication date' %}], [{{ ds.publication_date }}],
  [{% trans 'Period of data creation/gathering' %}],
    [{% if ds.date_of_data_creation_from %}
    from {{ ds.date_of_data_creation_from }}
    {% endif %}
    {% if ds.date_of_data_creation_to %}
      to {{ ds.date_of_data_creation_to }}
    {% endif %}],
  [{% trans 'Date of data creation (text)' %}], [{% if ds.date_of_data_creation_text %}{{ ds.date_of_data_creation_text }}{% else %} – {% endif %}],
  [{% trans 'Time period covered' %}],
    [{% if ds.time_period_from %}
    from {{ ds.time_period_from }}
    {% endif %}
    {% if ds.time_period_to %}
      to {{ ds.time_period_to }}
    {% endif %}],
  [{% trans 'Time period covered (text)' %}], [{% if ds.time_period_text %}{{ ds.time_period_text }}{% else %} – {% endif %}],
  [{% trans 'Sources of data' %}], [{% if ds.sources_of_data %}{{ ds.sources_of_data|linebreaks|safe }}{% else %} – {% endif %}],
  [{% trans 'Archival Record IDs' %}],
    [{% if not ds.get_archival_signatures.all %} – {% endif %}
    {% for elem in ds.get_archival_signatures.all %}
      {{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
  [{% trans 'Data types' %}],
    [{% for elem in ds.datatypes.all %}
      {{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
  [{% trans 'Data type (text)' %}], [{% if ds.datatype_text %}{{ ds.datatype_text }}{% else %} – {% endif %}],
  [{% trans 'Countries' %}],
    [{% if not ds.countries.all %} – {% endif %}
      {% for elem in ds.countries.all %}{{ elem }}{% if not forloop.last %}, {% endif %}{% endfor %}
    ],
  [{% trans 'Languages' %}],
    [{% if not ds.languages_of_data.all %} – {% endif %}
      {% for elem in ds.languages_of_data.all %}{{ elem }}{% if not forloop.last %}, {% endif %}{% endfor %}],
  [{% trans 'Disciplines' %}],
    [{% if not ds.get_disciplines_tags.all %} – {% endif %}
      {% for elem in ds.get_disciplines_tags.all %}{{ elem }}{% if not forloop.last %}, {% endif %}{% endfor %}],
  [{% trans 'Keywords' %}],
    [{% if not ds.get_keyword_tags.all %} – {% endif %}{% for elem in ds.get_keyword_tags.all %}{{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
  [{% trans 'Related datasets' %}],
    [{% if not ds.related_dataset.all %} – {% endif %}
    {% for elem in ds.related_dataset.all %}
      {{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
  [{% trans 'Related datasets (text)' %}], [{% if ds.related_dataset_text %}{{ ds.related_dataset_text|linebreaks|safe }}{% else %} – {% endif %}],
  [{% trans 'Related publications' %}],
    [{% if not ds.publications.all %} – {% endif %}
    {% for elem in ds.publications.all %}
      {{ elem.citation|urlize }}
    {% endfor %}],
  [{% trans 'Related projects' %}], [{% if ds.related_projects %}{{ ds.related_projects|linebreaks|safe }}{% else %} – {% endif %}],
  [{% trans 'Institutional affiliation' %}], [{% if ds.institutional_affiliation %}{{ ds.institutional_affiliation|linebreaks|safe }}{% else %} – {% endif %}],
  [{% trans 'Funding' %}], [{% if ds.funding %}{{ ds.funding|linebreaks|urlize|safe }}{% else %} – {% endif %}],
  [{% trans 'Methods of data collection' %}],
    [{% if not ds.get_methods_of_data_collection_tags.all %} – {% endif %}
    {% for elem in ds.get_methods_of_data_collection_tags.all %}{{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
  [{% trans 'Methods of data analysis' %}], [{% if not ds.get_methods_of_data_analysis_tags.all %} – {% endif %}
    {% for elem in ds.get_methods_of_data_analysis_tags.all %}{{ elem }}{% if not forloop.last %}, {% endif %}
    {% endfor %}],
)
