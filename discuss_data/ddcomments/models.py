import uuid

import reversion
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from django_bleach.models import BleachField
from mptt.models import MPTTModel, TreeForeignKey


class Notification(MPTTModel):
    """
    Notification with tree-traversal support for threads
    """

    PUBLIC = "PUB"
    PRIVATE = "PRI"
    CURATOR = "CUR"
    ACCESS_REQUEST = "AR"
    TRANSFER_REQUEST = "TR"
    PUB_REQUEST = "PR"

    NOTIFICATION_TYPE_CHOICES = [
        (PUBLIC, _("public")),
        (PRIVATE, _("private")),
        (CURATOR, _("curator")),
        (ACCESS_REQUEST, _("access request")),
        (PUB_REQUEST, _("publication request")),
        (TRANSFER_REQUEST, _("transfer request")),
    ]

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    notification_type = models.CharField(
        max_length=4,
        choices=NOTIFICATION_TYPE_CHOICES,
        default=PUBLIC,
    )
    text = BleachField(max_length=12000)
    date_added = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(
        "ddusers.User", related_name="notification_owner", on_delete=models.PROTECT
    )
    recipient = models.ForeignKey(
        "ddusers.User",
        related_name="notification_recipient",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    permanent = models.BooleanField(default=False)

    parent = TreeForeignKey(
        "self",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="notification_children",
    )

    # mandatory fields for generic relation
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.IntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return "[%s] %s %s (%s), %s" % (
            self.get_notification_type_display(),
            self.content_type,
            self.content_object,
            self.owner,
            self.date_added,
        )

    def class_name(self):
        return self.__class__.__name__


@reversion.register()
class Comment(MPTTModel):
    """
    Comment with tree-traversal support for threads
    """

    PUBLIC = "PUB"
    PRIVATE = "PRI"
    CURATOR = "CUR"
    PERMANENT = "PERM"

    COMMENT_TYPE_CHOICES = [
        (PUBLIC, _("public")),
        (PRIVATE, _("private")),
        (CURATOR, _("curator")),
        (PERMANENT, _("permanent")),
    ]

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    doi = models.CharField(max_length=200, blank=True)
    comment_type = models.CharField(
        max_length=4,
        choices=COMMENT_TYPE_CHOICES,
        default=PUBLIC,
    )
    text = BleachField(max_length=12000)
    date_added = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(
        "ddusers.User", related_name="comment_owner", on_delete=models.PROTECT
    )
    permanent = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    parent = TreeForeignKey(
        "self",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="comment_children",
    )

    # mandatory fields for generic relation
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.IntegerField()
    content_object = GenericForeignKey()

    def __str__(self):
        return "%s" % (
            # self.get_comment_type_display(),
            # self.content_type,
            self.content_object,
            # self.owner,
            # self.date_added.strftime("%d.%m.%Y, %H:%M:%S"),
        )

    def is_public(self):
        if (
            self.comment_type == Comment.PUBLIC
            or self.comment_type == Comment.PERMANENT
        ):
            return True

    def class_name(self):
        return self.__class__.__name__

    def set_delete(self):
        delete_date = timezone.now().strftime("%d.%m.%Y, %H:%M:%S")
        delete_text = _("This comment has been deleted at")
        self.text = "{} {}".format(delete_text, delete_date)
        self.deleted = True
        self.save()

    def get_absolute_url(self):
        return "{}discuss/#comment-{}".format(
            self.content_object.get_absolute_url(), self.uuid
        )
