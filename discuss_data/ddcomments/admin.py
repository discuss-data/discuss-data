from django.contrib import admin

from discuss_data.ddcomments.models import Comment

admin.site.register(Comment)
