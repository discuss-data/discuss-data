from django.apps import AppConfig


class DdcommentsConfig(AppConfig):
    name = "discuss_data.ddcomments"

    def ready(self):
        from actstream import registry

        registry.register(self.get_model("Comment"))
        registry.register(self.get_model("Notification"))
