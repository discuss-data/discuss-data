from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import (
    Div,
    Field,
    Layout,
    Submit,
)
from django import forms
from django.forms import ModelForm

from .models import Comment, Notification

MSG = """
{% if messages %}
    {% for message in messages %}
        {% if message.level == DEFAULT_MESSAGE_LEVELS.SUCCESS %}
            <div id='updated-msg' class='alert alert-success' ic-trigger-on='timeout:600ms' ic-action='delay:2500;fadeOut;remove' role='alert'>
            <strong>{{ message }}</strong></div>
        {% endif %}
        {% if message.level == DEFAULT_MESSAGE_LEVELS.ERROR %}
            <div id='updated-msg' class='alert alert-danger' ic-trigger-on='timeout:600ms' ic-action='delay:2500;fadeOut;remove' role='alert'>
            <strong>Error! {{ target|title }} could not be saved. Error encountered in fields {% for field in err %} {{ field }}, {% endfor %}</strong></div>
        {% endif %}
    {% endfor %}
{% endif %}
"""

ADD_EDIT_HEADING = (
    "<h2>{% if new %}Add{% else %}Edit{% endif %} {{ target|title }}</h2><hr>"
)
REQ_FIELD = "<p class='help-block'>[*] required field</p><hr>"


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            "text",
        ]

    helper = FormHelper()
    helper.form_tag = False
    helper.form_show_labels = False

    helper.layout = Layout(
        Div(Div("text", css_class="col-md-12", rows="3",), css_class="row",),
    )


class NotificationForm(ModelForm):
    recipient_uuid = forms.UUIDField(required=False)
    parent_uuid = forms.UUIDField(required=False)
    feed_type = forms.CharField(max_length=20, required=False)

    class Meta:
        model = Notification
        fields = [
            "text",
            "recipient_uuid",
            "parent_uuid",
        ]

    helper = FormHelper()
    helper.form_tag = False
    helper.form_show_labels = False

    helper.layout = Layout(
        Field("recipient_uuid", type="hidden"),
        Field("parent_uuid", type="hidden"),
        Div(
            Div("text", css_class="col-md-12 notification", rows="3",), css_class="row",
        ),
        FormActions(Submit("save", "{{ btn_text }}")),
    )
