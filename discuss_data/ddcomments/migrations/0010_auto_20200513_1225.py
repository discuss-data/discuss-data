# Generated by Django 2.2.11 on 2020-05-13 12:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ddcomments', '0009_notification'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='notification',
            name='read',
            field=models.BooleanField(default=False),
        ),
    ]
