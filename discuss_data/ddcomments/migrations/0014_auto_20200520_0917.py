# Generated by Django 2.2.11 on 2020-05-20 09:17

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ddcomments', '0013_delete_notification'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='DDComment',
            new_name='Notification',
        ),
    ]
