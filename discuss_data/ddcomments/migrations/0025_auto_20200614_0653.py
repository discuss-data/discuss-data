# Generated by Django 2.2.12 on 2020-06-14 06:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ddcomments', '0024_auto_20200602_0644'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='notification_type',
            field=models.CharField(choices=[('PUB', 'public'), ('PRI', 'private'), ('CUR', 'curator'), ('AR', 'access request'), ('PR', 'publication request')], default='PUB', max_length=4),
        ),
    ]
