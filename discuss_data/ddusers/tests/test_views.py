import logging

import pytest
from django.contrib.auth import get_user_model
from django.db import IntegrityError, transaction
from django.urls import reverse

from discuss_data.ddusers.models import Country

logger = logging.getLogger(__name__)

""" pytest-django provides the fixture `django_user_model`
    but this cannot be used conveniently here
"""
User = get_user_model()


@pytest.fixture
def country1():
    country_dict = {
        "name": "Dorne",
        "slug": "dorne",
        "public": True,
        "id": 1,
    }
    return create_country(country_dict)


@pytest.fixture
def country2():
    country_dict = {
        "name": "Riverlands",
        "slug": "riverlands",
        "public": True,
        "id": 2,
    }
    return create_country(country_dict)


def create_country(country_dict):
    try:
        country = Country.objects.get(slug=country_dict["slug"])
    except Country.DoesNotExist:
        try:
            # see https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
            with transaction.atomic():
                country = Country.objects.create(**country_dict)
        except IntegrityError as e:
            logger.error(e)
    return country


def create_user(user_dict):
    try:
        user = User.objects.get(username=user_dict["username"])
    except User.DoesNotExist:
        try:
            # see https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
            with transaction.atomic():
                user = User.objects.create_user(**user_dict)
        except IntegrityError as e:
            logger.error(e)
    return user


@pytest.fixture
def public_user():
    user_dict = {
        "username": "publicuser",
        "first_name": "public",
        "last_name": "user",
        "email": "public@user.de",
        "is_active": True,
        "uuid": "7ead2d1a-69d3-414c-80a5-8f18d879515d",
        "profile_accessibility": "PUB",
    }
    return create_user(user_dict)


@pytest.fixture
def network_user():
    user_dict = {
        "username": "networkuser",
        "first_name": "network",
        "last_name": "user",
        "email": "network@user.de",
        "is_active": True,
        "uuid": "a58edac6-8b55-404a-ac72-a7d169853981",
        "profile_accessibility": "NET",
    }
    return create_user(user_dict)


@pytest.fixture
def hidden_user():
    user_dict = {
        "username": "hiddenuser",
        "first_name": "hidden",
        "last_name": "user",
        "email": "hidden@user.de",
        "is_active": True,
        "uuid": "fa6dbaf5-4c50-4a28-9871-f82fb0dde52e",
        "profile_accessibility": "HID",
    }
    return create_user(user_dict)


@pytest.fixture
def search_user():
    user_dict = {
        "username": "searchuser",
        "first_name": "search",
        "last_name": "user",
        "email": "search@user.de",
        "is_active": True,
        "uuid": "043fb427-b2a8-4bea-b4c9-77bdc386ed7b",
        "profile_accessibility": "NET",
    }
    return create_user(user_dict)


def test_search_filters(
    client, public_user, network_user, search_user, country1, country2
):
    """check if search filters are applied correctly"""
    public_user.countries.add(country1)
    public_user.countries.add(country2)
    public_user.save()
    network_user.countries.add(country1)
    network_user.save()
    # login as searchuser to avoid false results when searching as logged in user
    client.force_login(search_user)
    # show both users when filtering for country1
    assert (
        client.get("/users/search/?q=&countries=%s/" % country1.slug).status_code == 200
    )
    assert "public" in str(
        client.get("/users/search/?q=&countries=%s" % country1.slug).content
    )
    assert "network" in str(
        client.get("/users/search/?q=&countries=%s" % country1.slug).content
    )

    # do not show network user when filtering for country2
    assert (
        client.get("/users/search/?q=&countries=%s/" % country2.slug).status_code == 200
    )
    assert "public" in str(
        client.get("/users/search/?q=&countries=%s" % country2.slug).content
    )
    assert "network" not in str(
        client.get("/users/search/?q=&countries=%s" % country2.slug).content
    )


def test_public_user_profile_no_login(client, public_user):
    """user profile MUST be visible for not logged in users"""
    # details
    assert client.get("/users/%s/" % public_user.uuid).status_code == 200
    assert "public" in str(client.get("/users/%s/" % public_user.uuid).content)
    # affiliations
    assert client.get("/users/%s/affiliations/" % public_user.uuid).status_code == 200
    assert "public" in str(
        client.get("/users/%s/affiliations/" % public_user.uuid).content
    )
    # projects
    assert client.get("/users/%s/projects/" % public_user.uuid).status_code == 200
    assert "public" in str(client.get("/users/%s/projects/" % public_user.uuid).content)
    # publications
    assert client.get("/users/%s/publications/" % public_user.uuid).status_code == 200
    assert "public" in str(
        client.get("/users/%s/publications/" % public_user.uuid).content
    )
    # datasets
    assert client.get("/users/%s/datasets/" % public_user.uuid).status_code == 200
    assert "public" in str(client.get("/users/%s/datasets/" % public_user.uuid).content)
    # datalists
    assert client.get("/users/%s/datalists/" % public_user.uuid).status_code == 200
    assert "public" in str(
        client.get("/users/%s/datalists/" % public_user.uuid).content
    )


def test_public_user_profile_login(client, public_user, network_user):
    """user profile MUST be visible for logged in users"""
    client.force_login(User.objects.get(username="networkuser"))
    # details
    assert client.get("/users/%s/" % public_user.uuid).status_code == 200
    assert "public" in str(client.get("/users/%s/" % public_user.uuid).content)
    # affiliations
    assert client.get("/users/%s/affiliations/" % public_user.uuid).status_code == 200
    assert "public" in str(
        client.get("/users/%s/affiliations/" % public_user.uuid).content
    )
    # projects
    assert client.get("/users/%s/projects/" % public_user.uuid).status_code == 200
    assert "public" in str(client.get("/users/%s/projects/" % public_user.uuid).content)
    # publications
    assert client.get("/users/%s/publications/" % public_user.uuid).status_code == 200
    assert "public" in str(
        client.get("/users/%s/publications/" % public_user.uuid).content
    )
    # datasets
    assert client.get("/users/%s/datasets/" % public_user.uuid).status_code == 200
    assert "public" in str(client.get("/users/%s/datasets/" % public_user.uuid).content)
    # datalists
    assert client.get("/users/%s/datalists/" % public_user.uuid).status_code == 200
    assert "public" in str(
        client.get("/users/%s/datalists/" % public_user.uuid).content
    )


def test_network_user_profile_no_login(client, network_user):
    """user profile MUST NOT be visible for not logged in users"""
    # details
    assert client.get("/users/%s/" % network_user.uuid).status_code == 302
    assert "network" not in str(client.get("/users/%s/" % network_user.uuid).content)
    # affiliations
    assert client.get("/users/%s/affiliations/" % network_user.uuid).status_code == 302
    assert "network" not in str(
        client.get("/users/%s/affiliations/" % network_user.uuid).content
    )
    # projects
    assert client.get("/users/%s/projects/" % network_user.uuid).status_code == 302
    assert "network" not in str(
        client.get("/users/%s/projects/" % network_user.uuid).content
    )
    # publications
    assert client.get("/users/%s/publications/" % network_user.uuid).status_code == 302
    assert "network" not in str(
        client.get("/users/%s/publications/" % network_user.uuid).content
    )
    # datasets
    assert client.get("/users/%s/datasets/" % network_user.uuid).status_code == 302
    assert "network" not in str(
        client.get("/users/%s/datasets/" % network_user.uuid).content
    )
    # datalists
    assert client.get("/users/%s/datalists/" % network_user.uuid).status_code == 302
    assert "network" not in str(
        client.get("/users/%s/datalists/" % network_user.uuid).content
    )


def test_network_user_profile_login(client, network_user, hidden_user):
    """user profile MUST be visible for logged in users"""
    client.force_login(User.objects.get(username="hiddenuser"))
    # details
    assert client.get("/users/%s/" % network_user.uuid).status_code == 200
    assert "network" in str(client.get("/users/%s/" % network_user.uuid).content)
    # affiliations
    assert client.get("/users/%s/affiliations/" % network_user.uuid).status_code == 200
    assert "network" in str(
        client.get("/users/%s/affiliations/" % network_user.uuid).content
    )
    # projects
    assert client.get("/users/%s/projects/" % network_user.uuid).status_code == 200
    assert "network" in str(
        client.get("/users/%s/projects/" % network_user.uuid).content
    )
    # publications
    assert client.get("/users/%s/publications/" % network_user.uuid).status_code == 200
    assert "network" in str(
        client.get("/users/%s/publications/" % network_user.uuid).content
    )
    # datasets
    assert client.get("/users/%s/datasets/" % network_user.uuid).status_code == 200
    assert "network" in str(
        client.get("/users/%s/datasets/" % network_user.uuid).content
    )
    # datalists
    assert client.get("/users/%s/datalists/" % network_user.uuid).status_code == 200
    assert "network" in str(
        client.get("/users/%s/datalists/" % network_user.uuid).content
    )


def test_hidden_user_profile_no_login(client, hidden_user):
    """user profile MUST NOT be visible for not logged in users"""
    # details
    assert client.get("/users/%s/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(client.get("/users/%s/" % hidden_user.uuid).content)
    # affiliations
    assert client.get("/users/%s/affiliations/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/affiliations/" % hidden_user.uuid).content
    )
    # projects
    assert client.get("/users/%s/projects/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/projects/" % hidden_user.uuid).content
    )
    # publications
    assert client.get("/users/%s/publications/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/publications/" % hidden_user.uuid).content
    )
    # datasets
    assert client.get("/users/%s/datasets/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/datasets/" % hidden_user.uuid).content
    )
    # datalists
    assert client.get("/users/%s/datalists/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/datalists/" % hidden_user.uuid).content
    )


def test_hidden_user_profile_login(client, hidden_user, public_user, network_user):
    """user profile MUST NOT be visible for logged in users"""
    client.force_login(User.objects.get(username="publicuser"))
    # details
    assert client.get("/users/%s/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(client.get("/users/%s/" % hidden_user.uuid).content)
    # affiliations
    assert client.get("/users/%s/affiliations/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/affiliations/" % hidden_user.uuid).content
    )
    # projects
    assert client.get("/users/%s/projects/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/projects/" % hidden_user.uuid).content
    )
    # publications
    assert client.get("/users/%s/publications/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/publications/" % hidden_user.uuid).content
    )
    # datasets
    assert client.get("/users/%s/datasets/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/datasets/" % hidden_user.uuid).content
    )
    # datalists
    assert client.get("/users/%s/datalists/" % hidden_user.uuid).status_code == 302
    assert "hidden" not in str(
        client.get("/users/%s/datalists/" % hidden_user.uuid).content
    )


def test_user_list_login(client, hidden_user, public_user, network_user):
    """search with login MUST NOT show hiddenuser but all others"""
    client.force_login(User.objects.get(username="publicuser"))
    response = client.get(reverse("ddusers:search"))
    assert response.status_code == 200
    assert "hidden user" not in str(response.content)
    assert "network user" in str(response.content)
    assert "public user" in str(response.content)
    # as a fallback and for hardened assurance, check uuids
    assert "fa6dbaf5-4c50-4a28-9871-f82fb0dde52e" not in str(response.content)
    assert "a58edac6-8b55-404a-ac72-a7d169853981" in str(response.content)
    assert "7ead2d1a-69d3-414c-80a5-8f18d879515d" in str(response.content)


def test_user_list_no_login(client, hidden_user, public_user, network_user):
    """search without login MUST NOT show hiddenuser but all others"""
    response = client.get(reverse("ddusers:search"))
    assert response.status_code == 200
    assert "hidden user" not in str(response.content)
    assert "network user" in str(response.content)
    assert "public user" in str(response.content)
    # as a fallback and for hardened assurance, check uuids
    assert "fa6dbaf5-4c50-4a28-9871-f82fb0dde52e" not in str(response.content)
    assert "a58edac6-8b55-404a-ac72-a7d169853981" in str(response.content)
    assert "7ead2d1a-69d3-414c-80a5-8f18d879515d" in str(response.content)


"""
[
  {
    "model":"dddatasets.datalist",
    "pk":1,
    "fields":{
      "uuid":"501576b1-28e3-4a82-82a6-5c22f93b2a6c",
      "name":"Questiones et Rationes",
      "description":"Questiones et Rationes",
      "owner":3,
      "public":false,
      "datasets":[
        3,
        2
      ]
    }
  },
  {
    "model":"dddatasets.datalist",
    "pk":2,
    "fields":{
      "uuid":"4877eb79-98ff-4042-bf80-3632f25efb64",
      "name":"Canones et Questiones",
      "description":"Canones et Questiones",
      "owner":3,
      "public":false,
      "datasets":[
        4,
        3
      ]
    }
  }
]
"""
