import uuid as uuidlib
from django.urls import reverse, resolve


def test_follow_datalist_url():
    datalist_uuid = uuidlib.UUID("501576b1-28e3-4a82-82a6-5c22f93b2a6c")
    assert (
        reverse("ddusers:datalist_follow", kwargs={"dl_uuid": datalist_uuid})
        == "/users/follow/datalist/" + str(datalist_uuid) + "/"
    )
    assert (
        resolve("/users/follow/datalist/" + str(datalist_uuid) + "/").view_name
        == "ddusers:datalist_follow"
    )
    assert resolve("/users/follow/datalist/" + str(datalist_uuid) + "/").kwargs == {
        "dl_uuid": datalist_uuid
    }


def test_datalists_url():
    user_uuid = uuidlib.UUID("7dd0691b-7df8-4167-9b32-81199e74e89e")
    assert (
        reverse("ddusers:data_lists", kwargs={"us_uuid": user_uuid})
        == "/users/" + str(user_uuid) + "/datalists/"
    )
    assert (
        resolve("/users/" + str(user_uuid) + "/datalists/").view_name
        == "ddusers:data_lists"
    )
    assert resolve("/users/" + str(user_uuid) + "/datalists/").kwargs == {
        "us_uuid": user_uuid
    }


def test_get_datalist_url():
    user_uuid = uuidlib.UUID("7dd0691b-7df8-4167-9b32-81199e74e89e")
    datalist_uuid = uuidlib.UUID("501576b1-28e3-4a82-82a6-5c22f93b2a6c")
    assert reverse(
        "ddusers:get_data_list", kwargs={"us_uuid": user_uuid, "dl_uuid": datalist_uuid}
    ) == "/users/" + str(user_uuid) + "/datalists/" + str(datalist_uuid)
    assert (
        resolve(
            "/users/" + str(user_uuid) + "/datalists/" + str(datalist_uuid)
        ).view_name
        == "ddusers:get_data_list"
    )
    assert resolve(
        "/users/" + str(user_uuid) + "/datalists/" + str(datalist_uuid)
    ).kwargs == {"us_uuid": user_uuid, "dl_uuid": datalist_uuid}
