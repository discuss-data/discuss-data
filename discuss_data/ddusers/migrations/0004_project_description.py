# Generated by Django 2.2.12 on 2020-05-30 07:20

from django.db import migrations
import django_bleach.models


class Migration(migrations.Migration):

    dependencies = [
        ('ddusers', '0003_auto_20200506_1402'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='description',
            field=django_bleach.models.BleachField(blank=True, max_length=600),
        ),
    ]
