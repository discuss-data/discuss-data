# Generated by Django 2.2.16 on 2020-09-30 07:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ddusers', '0009_auto_20200929_0956'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='access',
        ),
        migrations.AddField(
            model_name='user',
            name='profile_accessibility',
            field=models.CharField(choices=[('PUB', 'public'), ('NET', 'network'), ('HID', 'hidden')], default='NET', max_length=3),
        ),
    ]
