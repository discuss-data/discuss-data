from django.db import migrations, models

def copy_users(apps, schema_editor) -> None:
    User = apps.get_model("ddusers", "User")
    SocialAccount = apps.get_model("socialaccount", "SocialAccount")

    for user in User.objects.all():
        social_user = SocialAccount(
            provider="dariah",
            uid=user.username,
            last_login=user.last_login,
            date_joined=user.date_joined,
            user_id=user.id,
        )
        social_user.save()


def delete_users(apps, schema_editor) -> None:
    pass


class Migration(migrations.Migration):

    dependencies = [
        ("ddusers", "0016_alter_user_first_name"),
        ("socialaccount", "0006_alter_socialaccount_extra_data"),
    ]

    operations = [
        migrations.RunPython(copy_users, delete_users),
    ]
