import logging

from actstream import action
from actstream.actions import follow, unfollow
from actstream.models import Action, followers, following
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import Paginator
from django.http import (
    HttpResponse,
    HttpResponseRedirect,
)
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _
from django.views.generic import TemplateView
from django.views.generic.list import ListView

from discuss_data.core.decorators import dd_tou_accepted, dd_profile_accessible, dd_user_verified
from discuss_data.core.models import KeywordTagged
from discuss_data.core.views import core_search_view
from discuss_data.core.helpers import index_search, get_oep_parameters

from discuss_data.ddcomments.forms import NotificationForm
from discuss_data.ddcomments.models import Notification
from discuss_data.dddatasets.forms import DataListForm, DataSetToListForm
from discuss_data.dddatasets.models import DataList, DataSet
from discuss_data.dddatasets.utils import get_man_page, get_docu_page
from discuss_data.ddpublications.forms import PublicationForm
from discuss_data.ddpublications.models import Publication
from discuss_data.ddusers.forms import (
    AffiliationForm,
    ProjectForm,
    UserForm,
    UserImageForm,
    UserVerificationRequestForm,
)
from discuss_data.ddusers.models import Affiliation, Country, Project, User, UserVerificationRequest

logger = logging.getLogger(__name__)


class UsersListView(ListView):
    """a class based implementation of `ddusers.views.user_list` (deprecated)"""

    model = User
    context_object_name = "users"
    paginate_by = 25
    template_name = "ddusers/_search_results.html"

    def get_queryset(self):
        filtered = (
            self.model.objects.exclude(profile_accessibility="HID")
            .exclude(is_active=False)
            .exclude(first_name="")
            .exclude(last_name="")
        )

        if self.request.user.is_authenticated:
            return filtered

        return filtered.exclude(profile_accessibility="NET")


class UserDetailsView(TemplateView):
    """show user details"""

    template_name = "ddusers/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = get_object_or_404(User, uuid=context["us_uuid"])

        initial = {
            "recipient_uuid": user.uuid,
            "parent_uuid": None,
        }
        additional_context = {
            "profile_accessibility": user.profile_accessibility,
            "form": NotificationForm(initial=initial),
            "add_url": "ddusers:notification",
            "object": user,
            "ictarget": "#ds-content",
            "btn_text": _("Send message"),
            "user": user,
        }
        return {**context, **additional_context}

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if (context["user"].profile_accessibility == "PUB") or (
            context["user"].profile_accessibility == "NET"
            and request.user.is_authenticated
        ):
            return render(request, "ddusers/detail.html", context)

        messages.error(request, "The user has chosen not to share his profile details.")
        return HttpResponseRedirect("/users/search/")  # better: redirect to last page


def user_search(query):
    """
    Search view to lookup User objects on prep collaboration page
    """
    return index_search("user_index", query)


def search(request):
    """
    Main search view for users, used as the listing view for the people page
    """
    return core_search_view(request, "user_index", 10)


def first_steps(request):
    page = get_man_page(slug="first-steps-overview")
    if request.method == "POST":
        request.user.show_first_steps = False
        request.user.save()
        return HttpResponse()

    return render(request, "ddusers/_first_steps.html", {"page": page},)


def accept_tou(request):
    page = get_man_page(slug="terms-and-conditions")
    if request.method == "POST":
        if not request.user.accepted_dd_tou:
            request.user.accepted_dd_tou = True
            request.user.save()
        next_url = request.POST.get("nexturl")
        logger.debug("next url %s", next_url)
        messages.success(request, _("Terms of use accepted, redirecting"))

        # if next_url:
        #    return HttpResponseRedirect(next_url)
        # else:
        return HttpResponseRedirect("/dashboard")
    else:
        next_url = request.GET.get("next")
    return render(
        request, "ddusers/accept_tou.html", {"page": page, "next_url": next_url},
    )


@login_required
@dd_tou_accepted
def verification_info(request):
    page = get_docu_page(slug="user-verification-process")
    user = request.user
    new = False
    pending = False
    logger.debug(request.user)
    if hasattr(user, "verification_request"):
        veri_pro = user.verification_request
        messages.info(request, _("Verification is pending."))
        pending = True
    else:
        veri_pro = UserVerificationRequest(user=user)
    updated = False
    err = ""
    if request.method == "POST":
        logger.debug("user verification post")
        form = UserVerificationRequestForm(request.POST, request.FILES, instance=veri_pro)
        if form.is_valid():
            veri_pro = form.save()
            updated = True
        else:
            err = form.errors.as_data()
    else:
        logger.debug("user verification get")
        form = UserVerificationRequestForm(instance=veri_pro)

    return render(
        request, "ddusers/verification_info.html", {"page": page, "form":form, "user": user, "err": err, "updated": updated, "pending": pending },
    )



@login_required
@dd_tou_accepted
@dd_user_verified
def format_act_feed(request, feed, object_list=None):
    action_list = list()

    for act in feed:
        try:
            if act.target.class_name() == "User":
                action_list.append(act)
            if act.target.class_name() == "DataSet" and act.target.published:
                action_list.append(act)
            if act.target.class_name() == "DataList":
                action_list.append(act)
            if act.target.class_name() == "Comment":
                action_list.append(act)
            if (
                act.target.class_name() == "DataSetManagementObject"
                and act.target.published
            ):
                action_list.append(act)
        except AttributeError as e:
            logger.error("act {}, target {}, error {}".format(act, act.target, e))
    paginator = Paginator(action_list, 5)
    page = request.GET.get("page")
    feed_actions = paginator.get_page(page)
    paginator_range = range(1, paginator.num_pages + 1)
    return feed_actions, paginator_range, paginator.num_pages + 1


@login_required
@dd_tou_accepted
@dd_user_verified
def datasets_act_feed(request):
    if request.user.is_authenticated:
        feed_actions, paginator_range, paginator_last_page = format_act_feed(
            request, request.user.get_datasets_actions()
        )
        feed_type = "datasets-act-feed"

        return render(
            request,
            "ddusers/dashboard.html",
            {
                "feed_actions": feed_actions,
                "paginator_range": paginator_range,
                "paginator_last_page": paginator_last_page,
                "feed_type": feed_type,
            },
        )


@login_required
@dd_tou_accepted
@dd_user_verified
def following_act_feed(request):
    if request.user.is_authenticated:
        feed_actions, paginator_range, paginator_last_page = format_act_feed(
            request, request.user.get_following_actions()
        )
        feed_type = "following-act-feed"

        return render(
            request,
            "ddusers/dashboard.html",
            {
                "feed_actions": feed_actions,
                "paginator_range": paginator_range,
                "paginator_last_page": paginator_last_page,
                "feed_type": feed_type,
            },
        )


@login_required
@dd_tou_accepted
@dd_user_verified
def notification_act_feed(request):
    """
    Deprecated, to be replaced by user_notifications()
    """
    # list of ids of datasets where user has admin rights
    user_admin_datasets = list(
        request.user.get_published_admin_datasets().values_list("id", flat=True)
    )

    # queries for actions with notification as action object and one of the users datasets as target
    actions_ar = Action.objects.access_requests_received(user_admin_datasets)

    notification_content_type = ContentType.objects.get(
        app_label="ddcomments", model="comment"
    )
    actions_notifications = Action.objects.any_everything(request.user).filter(
        action_object_content_type=notification_content_type.id,
    )

    actions = actions_ar | actions_notifications
    feed_actions, paginator_range, paginator_last_page = format_act_feed(
        request, actions
    )
    feed_type = "notification-act-feed"

    return render(
        request,
        "ddusers/dashboard.html",
        {
            "feed_actions": feed_actions,
            "paginator_range": paginator_range,
            "paginator_last_page": paginator_last_page,
            "feed_type": feed_type,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_notifications(request):
    user_notifications_to = Notification.objects.root_nodes().filter(
        recipient=request.user
    )
    user_notifications_from = Notification.objects.root_nodes().filter(
        owner=request.user
    )

    user_admin_datasets = list(
        request.user.get_published_admin_datasets().values_list("id", flat=True)
    )

    dataset_content_type = ContentType.objects.get(
        app_label="dddatasets", model="dataset"
    )
    datasets_notifications = Notification.objects.root_nodes().filter(
        content_type=dataset_content_type.id, object_id__in=user_admin_datasets
    )

    notifications = (
        user_notifications_to | user_notifications_from | datasets_notifications
    )

    feed_type = "notifications"

    paginator = Paginator(notifications.order_by("-date_added"), 5)
    page = request.GET.get("page")
    feed_actions = paginator.get_page(page)
    paginator_range = range(1, paginator.num_pages + 1)
    paginator_last_page = paginator.num_pages + 1

    form = NotificationForm()

    return render(
        request,
        "ddusers/dashboard.html",
        {
            "feed_actions": feed_actions,
            "paginator_range": paginator_range,
            "paginator_last_page": paginator_last_page,
            "feed_type": feed_type,
            "form": form,
            "add_url": "ddusers:notification",
            "btn_text": "Reply",
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_curation(request):
    dataset_content_type = ContentType.objects.get(
        app_label="dddatasets", model="dataset"
    )
    user_curated_datasets = list(
        request.user.get_curated_datasets().values_list("id", flat=True)
    )

    datasets_notifications_curators = Notification.objects.root_nodes().filter(
        content_type=dataset_content_type.id, object_id__in=user_curated_datasets
    )

    notifications = datasets_notifications_curators
    feed_type = "curation"

    paginator = Paginator(notifications.order_by("-date_added"), 5)
    page = request.GET.get("page")
    feed_actions = paginator.get_page(page)
    paginator_range = range(1, paginator.num_pages + 1)
    paginator_last_page = paginator.num_pages + 1

    form = NotificationForm()

    return render(
        request,
        "ddusers/dashboard.html",
        {
            "feed_actions": feed_actions,
            "paginator_range": paginator_range,
            "paginator_last_page": paginator_last_page,
            "feed_type": feed_type,
            "form": form,
            "add_url": "ddusers:notification",
            "btn_text": "Reply",
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_act_feed(request):
    feed_actions, paginator_range, paginator_last_page = format_act_feed(
        request, request.user.get_user_actions()
    )

    feed_type = "user-act-feed"
    return render(
        request,
        "ddusers/dashboard.html",
        {
            "feed_actions": feed_actions,
            "paginator_range": paginator_range,
            "paginator_last_page": paginator_last_page,
            "feed_type": feed_type,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def following_users(request):
    feed_type = "following-users"
    object_list = following(request.user, User)

    return render(
        request,
        "ddusers/dashboard.html",
        {
            "object_list": object_list,
            # "paginator_range": paginator_range,
            # "paginator_last_page": paginator_last_page,
            "feed_type": feed_type,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def followers_users(request):
    feed_type = "followers"
    object_list = followers(request.user)

    return render(
        request,
        "ddusers/dashboard.html",
        {
            "object_list": object_list,
            # "paginator_range": paginator_range,
            # "paginator_last_page": paginator_last_page,
            "feed_type": feed_type,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def list_following_datasets(request):
    object_list = following(request.user, DataSet)
    pagetitle = _("Followed Datasets")
    follow_type = "datasets"

    return render(
        request,
        "ddusers/list_follow.html",
        {
            "object_list": object_list,
            "object_type": follow_type + "_followed",
            "pagetitle": pagetitle,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def list_following_datalists(request):
    object_list = following(request.user, DataList)
    pagetitle = _("Followed Data Lists")
    follow_type = "datalists"

    return render(
        request,
        "ddusers/list_follow.html",
        {
            "object_list": object_list,
            "object_type": follow_type + "_followed",
            "pagetitle": pagetitle,
        },
    )


# TODO: not used anymore
# @login_required
# @dd_tou_accepted
@dd_user_verified
# def list_following(request, follow_type):
#     if request.user.is_authenticated:
#         if follow_type == "datasets":
#             object_list = following(request.user, DataSet)
#             pagetitle = _("Followed Datasets")
#         elif follow_type == "users":
#             object_list = following(request.user, User)
#             pagetitle = _("Followed Users")
#         elif follow_type == "data_lists":
#             object_list = following(request.user, DataList)
#             pagetitle = _("Followed Data Lists")
#         else:
#             return Http404()

#         return render(
#             request,
#             "ddusers/list_follow.html",
#             {
#                 "object_list": object_list,
#                 "object_type": follow_type + "_followed",
#                 "pagetitle": pagetitle,
#             },
#         )


@login_required
@dd_tou_accepted
@dd_user_verified
def list_followed(request, follow_type):
    if request.user.is_authenticated:
        if follow_type == "users":
            object_list = followers(request.user)
            pagetitle = _("Users who are following you")
        else:
            object_list = None
            pagetitle = None

        return render(
            request,
            "ddusers/list_follow.html",
            {
                "object_list": object_list,
                "object_type": follow_type + "_following",
                "pagetitle": pagetitle,
            },
        )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_follow(request, us_uuid):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit")
    user = get_object_or_404(User, uuid=us_uuid)
    if request.user in followers(user):
        unfollow(request.user, user)
    else:
        follow(request.user, user)
    return render(
        request,
        "ddusers/_follow_button.html",
        {"object": user, "follow_url": "ddusers:follow"},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_follow_status(request):
    return render(request, "ddusers/_user_sidebar_followers.html", {},)


@login_required
@dd_tou_accepted
@dd_user_verified
def user_notification(request):
    if request.method == "POST":
        form = NotificationForm(request.POST)
        if form.is_valid():
            notification = form.save(commit=False)
            notification.owner = request.user
            feed_type = form.cleaned_data.get("feed_type")
            parent_uuid = form.cleaned_data.get("parent_uuid")
            if parent_uuid:
                notification.parent = Notification.objects.get(uuid=parent_uuid)

            recipient_uuid = form.cleaned_data.get("recipient_uuid")
            user = User.objects.get(uuid=recipient_uuid)
            notification.recipient = user
            notification.notification_type = Notification.PRIVATE
            notification.content_type = ContentType.objects.get_for_model(User)
            notification.object_id = user.id
            notification.save()

            messages.success(request, _("Message sent"))

            if feed_type == "notifications":
                return user_notifications(request)
            elif feed_type == "curation":
                return user_curation(request)
            else:
                return user_notifications(request)
        else:
            messages.error(request, form.errors.as_data())
    else:
        form = NotificationForm()
    return render(
        request,
        "ddcomments/_notification_add.html",
        {"form": form, "add_url": "ddusers:notification", "ictarget": "#ds-content"},
    )


@dd_profile_accessible
def user_affiliations(request, us_uuid):
    user = get_object_or_404(User, uuid=us_uuid)
    # if (user.profile_accessibility == "PUB") or (user.profile_accessibility == "NET" and request.user.is_authenticated):
    return render(request, "ddusers/affiliations.html", {"user": user})

    # messages.error(request, "The user has chosen not to share his profile details.")
    # return HttpResponseRedirect("/users/search/")  # better: redirect to last page


@login_required
@dd_tou_accepted
@dd_user_verified
def datalist_follow(request, dl_uuid):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("dddatasets:search")

    data_list = get_object_or_404(DataList, uuid=dl_uuid)

    if request.user in followers(data_list):
        unfollow(request.user, data_list)
    else:
        follow(request.user, data_list)
    return render(
        request,
        "ddusers/_follow_button.html",
        {
            "object": data_list,
            "follow_url": "ddusers:datalist_follow",
            "follow_text": "Follow this Data List",
        },
    )


@dd_profile_accessible
def user_data_lists(request, us_uuid):
    user = get_object_or_404(User, uuid=us_uuid)
    return render(request, "ddusers/data_lists.html", {"user": user})


@login_required
@dd_tou_accepted
@dd_user_verified
def user_get_data_list(request, us_uuid, dl_uuid):
    user = get_object_or_404(User, uuid=us_uuid)
    data_list = get_object_or_404(DataList, uuid=dl_uuid, public=True)

    return render(
        request, "ddusers/get_data_list.html", {"user": user, "data_list": data_list}
    )


@dd_profile_accessible
def user_projects(request, us_uuid):
    user = get_object_or_404(User, uuid=us_uuid)
    return render(request, "ddusers/projects.html", {"user": user})


@dd_profile_accessible
def user_publications(request, us_uuid):
    user = get_object_or_404(User, uuid=us_uuid)
    return render(request, "ddusers/publications.html", {"user": user})


@login_required
@dd_tou_accepted
@dd_user_verified
def dataset_to_list(request):
    user = request.user
    if request.method == "DELETE":
        form = DataSetToListForm(request.DELETE)
        if form.is_valid():
            ds_uuid = form.cleaned_data["ds_uuid"]
            dl_uuid = form.cleaned_data["dl_uuid"]
            form_type = form.cleaned_data["form_type"]

            dataset = DataSet.objects.get(uuid=ds_uuid)
            datalist = DataList.objects.get(uuid=dl_uuid)

            if datalist.owner == user:
                datalist.datasets.remove(dataset)
                datalist.save()
                if form_type == "table":
                    response = HttpResponse()
                    response["X-IC-Remove"] = "10ms"
                    return response
                else:
                    return render(
                        request,
                        "ddusers/_datalist_button.html",
                        {
                            "datalist": datalist,
                            "dataset": dataset,
                            "btn_mode": "delete",
                        },
                    )
    if request.method == "POST":
        form = DataSetToListForm(request.POST)
        if form.is_valid():
            ds_uuid = form.cleaned_data["ds_uuid"]
            dl_uuid = form.cleaned_data["dl_uuid"]
            dataset = DataSet.objects.get(uuid=ds_uuid)
            datalist = DataList.objects.get(uuid=dl_uuid)
            if datalist.owner == user:
                datalist.datasets.add(dataset)
                datalist.save()
                action.send(
                    request.user,
                    verb="added dataset",
                    action_object=dataset,
                    target=datalist,
                    public=datalist.public,
                )
                return render(
                    request,
                    "ddusers/_datalist_button.html",
                    {"datalist": datalist, "dataset": dataset, "btn_mode": "add"},
                )
        else:
            logger.error(form.errors.as_data())
            messages.error(request, _("Dataset not added to list"))

    return render(
        request,
        "ddusers/_datalist_button.html",
        {"user": user, "datalist": datalist, "dataset": dataset, "btn_mode": "error"},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_lists_all(request):
    user = request.user
    lists = user.get_lists_all()
    return render(request, "ddusers/data_lists.html", {"user": user, "lists": lists})


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_lists(request):
    user = request.user
    edit_url = "ddusers:edit_list"
    return render(
        request,
        "ddusers/_edit_data_lists.html",
        {
            "user": user,
            "objects": user.get_lists_all(),
            "edit_url": edit_url,
            "add_url": edit_url,
            "delete_url": edit_url,
            "object_type": "data_list",
            "object_text": "Data List",
            "ictarget": "#ds-content",
            "edit_mode": True,
        },
    )


@dd_profile_accessible
def user_lists_public(request, us_uuid):
    user = get_object_or_404(User, uuid=us_uuid)
    lists = user.get_lists_public()
    return render(request, "ddusers/data_lists.html", {"user": user, "lists": lists})


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_data_list(request, dl_uuid=None):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit_lists")
    user = request.user
    if request.method == "DELETE":  # remove
        dl_uuid = request.DELETE["objectid"]
        data_list = DataList.objects.get(uuid=dl_uuid)
        data_list.delete()
        return redirect("ddusers:edit_lists")
    if request.method == "POST":  # Edit + Add
        if dl_uuid:  # Edit
            data_list = DataList.objects.get(uuid=dl_uuid)
            form = DataListForm(request.POST, instance=data_list)
            verb = "edited description of data list"
        else:  # Add
            data_list = DataList()
            form = DataListForm(request.POST)
            verb = "created data list"
        if form.is_valid():
            data_list = form.save(commit=False)
            data_list.owner = user
            data_list.save()
            updated = True
            form_err = None
            logger.error("send action {} for {}".format(verb, data_list))
            action.send(
                request.user,
                verb=verb,
                # action_object=datalist,
                target=data_list,
                public=data_list.public,
            )
            return user_edit_lists(request)
        else:
            updated = False
            form_err = form.errors.as_data()
    if request.method == "GET":  # get
        if dl_uuid:
            data_list = DataList.objects.get(uuid=dl_uuid)
            form = DataListForm(instance=data_list)
        else:
            form = DataListForm()
        updated = False
        form_err = None

    return render(
        request,
        "core/_edit_form.html",
        {
            "target": "Data List",
            "ictarget": "#ds-content",
            "form": form,
            "user": user,
            "error_msg": form_err,
            "updated": updated,
            "editmode": True,
        },
    )


@dd_profile_accessible
def user_datasets(request, us_uuid):
    user = get_object_or_404(User, uuid=us_uuid)
    return render(request, "ddusers/datasets.html", {"user": user})


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit(request):
    oep_publications = get_oep_parameters(
        "publication", True, "Add new Discuss Data related publication..."
    )
    oep_projects = get_oep_parameters(
        "project", True, "Add new Discuss Data related project..."
    )
    oep_affiliations = get_oep_parameters("affiliation", True, "Add new affiliation...")
    updated = False
    user = request.user
    if request.method == "POST":
        form = UserImageForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data["photo"]:
                user.photo = form.cleaned_data["photo"]
            user.save()
            logger.debug(user.__dict__)
            updated = True
        else:
            logger.debug("not valid")
            logger.debug(form.errors.as_data())
    else:
        form = UserImageForm(instance=user)

    return render(
        request,
        "ddusers/edit.html",
        {
            "user_form": form,
            "user": user,
            "updated": updated,
            "oep_affiliations": oep_affiliations,
            "oep_projects": oep_projects,
            "oep_publications": oep_publications,
            "editmode": True,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_profile(request):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit")
    user = request.user
    updated = False
    err = None
    if request.method == "POST":
        form = UserForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            user = form.save()
            updated = True
        else:
            updated = False
            err = form.errors.as_data()
    else:
        form = UserForm(instance=user)
    return render(
        request,
        "core/_edit_form.html",
        {
            "form": form,
            "user": user,
            "updated": updated,
            "err": err,
            "target": "User Profile",
            "editmode": True,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_affiliations(request):
    user = request.user
    edit_url = "ddusers:edit_affiliation"
    template = "ddusers/_edit_user_objects.html"
    if request.method == "POST":
        template = "ddusers/_edit_user_objects_sort.html"
        object_list = request.POST.getlist("object_uuid")
        for i, elem_uuid in enumerate(object_list, start=0):
            aff = Affiliation.objects.get(uuid=elem_uuid)
            aff.list_position = i
            aff.save()

    return render(
        request,
        template,
        {
            "user": user,
            "objects": user.get_affiliations(),
            "edit_url": edit_url,
            "add_url": edit_url,
            "sort_url": "ddusers:edit_affiliations",
            "delete_url": edit_url,
            "object_type": "affiliation",
            "object_text": "Add new affiliation...",
            "ictarget": "#ds-content",
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_affiliation(request, af_uuid=None):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit_affiliations")
    user = request.user
    if request.method == "DELETE":  # remove
        af_uuid = request.DELETE["objectid"]
        affiliation = Affiliation.objects.get(uuid=af_uuid)
        affiliation.delete()
        return redirect("ddusers:edit_affiliations")
    if request.method == "POST":
        if af_uuid:  # Edit
            affiliation = Affiliation.objects.get(uuid=af_uuid)
            form = AffiliationForm(request.POST, prefix="aff", instance=affiliation)
        else:
            affiliation = Affiliation()  # Add
            form = AffiliationForm(request.POST, prefix="aff")
        if form.is_valid():
            affiliation = form.save(commit=False)
            affiliation.user = user
            affiliation.save()
            updated = True
            form_err = None
            return redirect("ddusers:edit_affiliations")
        else:
            updated = False
            form_err = form.errors.as_data()

    if request.method == "GET":  # get
        if af_uuid:
            affiliation = Affiliation.objects.get(uuid=af_uuid)
            form = AffiliationForm(prefix="aff", instance=affiliation)
        else:
            form = AffiliationForm(prefix="aff")
        updated = False
        form_err = None
    return render(
        request,
        "core/_edit_form.html",
        {
            "target": "affiliation",
            "ictarget": "#ds-content",
            "form": form,
            "user": user,
            "error_msg": form_err,
            "updated": updated,
            "editmode": True,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_projects(request):
    user = request.user
    edit_url = "ddusers:edit_project"
    return render(
        request,
        "ddusers/_edit_user_objects.html",
        {
            "user": user,
            "objects": user.get_projects(),
            "edit_url": edit_url,
            "add_url": edit_url,
            "delete_url": edit_url,
            "object_type": "project",
            "object_text": "Add new project...",
            "ictarget": "#ds-content",
            "editmode": True,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_project(request, pr_uuid=None):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit_projects")
    user = request.user
    if request.method == "DELETE":  # remove
        pr_uuid = request.DELETE["objectid"]
        project = Project.objects.get(uuid=pr_uuid)
        project.delete()
        return redirect("ddusers:edit_projects")
    if request.method == "POST":  # Edit + Add
        if pr_uuid:  # Edit
            project = Project.objects.get(uuid=pr_uuid)
            form = ProjectForm(request.POST, instance=project)
        else:  # Add
            project = Project()
            form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.user = user
            project.save()
            updated = True
            form_err = None
            return user_edit_projects(request)
        else:
            updated = False
            form_err = form.errors.as_data()
    if request.method == "GET":  # get
        if pr_uuid:
            project = Project.objects.get(uuid=pr_uuid)
            form = ProjectForm(instance=project)
        else:
            form = ProjectForm()
        updated = False
        form_err = None

    return render(
        request,
        "core/_edit_form.html",
        {
            "target": "project",
            "ictarget": "#ds-content",
            "form": form,
            "user": user,
            "error_msg": form_err,
            "updated": updated,
            "editmode": True,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_publications(request):
    user = request.user
    edit_url = "ddusers:edit_publication"
    return render(
        request,
        "ddusers/_edit_user_objects.html",
        {
            "user": user,
            "objects": user.get_publications(),
            "edit_url": edit_url,
            "add_url": edit_url,
            "delete_url": edit_url,
            "object_type": "publication",
            "object_text": "Add new publication...",
            "ictarget": "#ds-content",
            "editmode": True,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_edit_publication(request, pub_uuid=None):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit_publications")
    user = request.user
    if request.method == "DELETE":  # remove
        pub_uuid = request.DELETE["objectid"]
        publication = Publication.objects.get(uuid=pub_uuid)
        publication.delete()
        return redirect("ddusers:edit_publications")
    if request.method == "POST":  # Edit/Add
        if pub_uuid:  # Edit
            publication = Publication.objects.get(uuid=pub_uuid)
            form = PublicationForm(request.POST, instance=publication)
        else:  # Add
            publication = Publication()
            form = PublicationForm(request.POST)
        if form.is_valid():
            publication = form.save()
            user.publications.add(publication)
            updated = True
            form_err = None
            return user_edit_publications(request)
        else:
            updated = False
            form_err = form.errors.as_data()
    if request.method == "GET":  # get
        if pub_uuid:
            publication = Publication.objects.get(uuid=pub_uuid)
            form = PublicationForm(instance=publication)
        else:
            form = PublicationForm()
        updated = False
        form_err = None
    return render(
        request,
        "core/_edit_form.html",
        {
            "target": "publication",
            "ictarget": "#ds-content",
            "form": form,
            "user": user,
            "error_msg": form_err,
            "updated": updated,
            "editmode": True,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_countries(request):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit")
    user = request.user
    all_countries = Country.objects.filter(public=True)
    countries = list()
    # POST: add or remove a country
    if request.method == "POST":
        country_slug = request.POST["countryslug"]
        country = Country.objects.get(slug=country_slug)
        if country in user.get_countries():
            user.countries.remove(country)
        else:
            user.countries.add(country)
        user.save()

    # GET & POST: return a list of all countries, with countries attached to user marked
    for ac in all_countries:
        if ac in user.get_countries():
            countries.append((ac, True))
        else:
            countries.append((ac, False))
    return render(
        request, "ddusers/_countries.html", {"user": user, "countries": countries},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def user_tags(request):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("ddusers:edit")

    user = request.user
    if request.method == "DELETE":
        tagid = request.POST.get("tag-keyword")
        tag = KeywordTagged.objects.get(id=tagid)
        user.interests.remove(tag)

    if request.method == "POST":
        add = request.POST["interest-tag"].title()
        if len(add) > 0:
            user.interests.add(add)

    control_args = {
        "div_id": "user-interests",
        "input_id": "interest-tag",
        "objects_get": "user.get_interests",
        "url_objects_all": "core:tags",
        "url_object_add": "ddusers:tags",
        "url_object_remove": "ddusers:tags",
        "text": "user interests",
    }
    return render(request, "ddusers/_tags.html", {"user": user, "ca": control_args})
