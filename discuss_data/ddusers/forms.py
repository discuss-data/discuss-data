from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Layout, Submit
from django import forms
from django.forms import ModelForm

from discuss_data.ddusers.models import Affiliation, Project, User, UserVerificationRequest

UPDATED_MSG = """
    {% if updated %}<div id='updated-msg' class='alert alert-success' role='alert'>
    <span class='spinner-sm'></span><strong>{{ target|title }} saved</strong></div>{% endif %}
    {% if err %}<div id='updated-msg' class='alert alert-danger' role='alert'>
    <span class='spinner-sm'></span><strong>Error! {{ target|title }} could not be saved. Error encountered in fields {% for field in err %} {{ field }}, {% endfor %}</strong></div> {% endif %}"""

ADD_EDIT_HEADING = (
    "<h2>{% if new %}Add{% else %}Edit{% endif %} {{ target|title }}</h2><hr>"
)
REQ_FIELD = "<p class='help-block'>[*] required field</p>"

ACCESSIBILTY_TEXT = """
    <div class="col-md-6">
        <div class="card border-primary mb-3">
            <div class="card-body text-primary">
                <p class="card-text">
                    Set <b>Profile accessibility</b> to <b>public</b> if you want your profile to be found by everyone. Set to <b>internal</b> to limit your profile views to registered Discuss Data users and to <b>hidden</b> if you don't want your profile to be viewed and found at all.
                </p>
            </div>
        </div>
    </div>"""


class UserVerificationRequestForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     # first call parent's constructor
    #     super(UserVerificationRequestForm, self).__init__(*args, **kwargs)
    #     # there's a `fields` property now
    #     self.fields['verification_info'].required = True
    #     self.fields['verification_url'].required = True
    #     self.fields['first_name'].required = True
    #     self.fields['last_name'].required = True

    class Meta:
        model = UserVerificationRequest
        fields = [
            "first_name",
            "last_name",
            "verification_info",
            "verification_url",
        ]
        required_fields = fields

        helper = FormHelper()
        helper.form_tag = True
        helper.layout = Layout(
            # HTML(ADD_EDIT_HEADING),
            Div(
                # Div("name_prefix", css_class="col-md-2",),
                Div(
                    "first_name",
                    css_class="col-md-4",
                ),
                # Div("middle_name", css_class="col-md-2",),
                Div(
                    "last_name",
                    css_class="col-md-4",
                ),
                Div(
                    "verification_info",
                    css_class="col-md-12",
                ),
                Div(
                    "verification_url",
                    css_class="col-md-12",
                ),
                # Div("name_suffix", css_class="col-md-2",),
                css_class="row",
            ),
            HTML(REQ_FIELD),
            HTML("<span id='indicator'><span class='spinner-sm'></span> Saving...</span>"),
            Div(
                HTML(UPDATED_MSG),
            ),
        )




class UserForm(ModelForm):
    class Meta:
        model = User
        fields = [
            # "name_prefix",
            # "name_suffix",
            "first_name",
            # "middle_name",
            "last_name",
            "email",
            "email_alternative",
            "research_profile_short",
            "research_profile_full",
            "external_profile",
            "profile_accessibility",
        ]

    first_name = forms.CharField(disabled=True)
    last_name = forms.CharField(disabled=True)
    email = forms.EmailField(disabled=True)

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        # HTML(ADD_EDIT_HEADING),
        Div(
            # Div("name_prefix", css_class="col-md-2",),
            Div(
                "first_name",
                css_class="col-md-4",
            ),
            # Div("middle_name", css_class="col-md-2",),
            Div(
                "last_name",
                css_class="col-md-4",
            ),
            Div(
                "email",
                css_class="col-md-4",
            ),
            # Div("name_suffix", css_class="col-md-2",),
            css_class="row",
        ),
        Div(
            Div(
                "email_alternative",
                css_class="col-md-4",
            ),
            css_class="row",
        ),
        Div(
            Div(
                "research_profile_short",
                css_class="col-md-6",
            ),
            Div(
                "research_profile_full",
                css_class="col-md-6",
            ),
            css_class="row",
        ),
        Div(
            Div(
                "external_profile",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        Div(
            Div(
                "profile_accessibility",
                css_class="col-md-6",
            ),
            # HTML(ACCESSIBILTY_TEXT),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML("<span id='indicator'><span class='spinner-sm'></span> Saving...</span>"),
        FormActions(
            Submit(
                "save",
                "Save {{ target }}",
                data_toggle="collapse",
                data_target="#collapse-userdata-edit",
            ),
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )


class UserImageForm(ModelForm):
    class Meta:
        model = User
        fields = [
            "photo",
        ]


class AffiliationForm(ModelForm):
    class Meta:
        model = Affiliation
        fields = [
            "name_of_institution",
            "website_of_institution",
            "place_of_institution",
            "country_of_institution",
            "position",
            "main",
        ]

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div(
                "name_of_institution",
                css_class="col-md-12",
            ),
            Div(
                "website_of_institution",
                css_class="col-md-12",
            ),
            Div(
                "place_of_institution",
                css_class="col-md-6",
            ),
            Div(
                "country_of_institution",
                css_class="col-md-6",
            ),
            Div(
                "position",
                css_class="col-md-12",
            ),
            Field(
                "main", css_class="custom-control-input", template="core/_checkbox.html"
            ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML("<span id='indicator'><span class='spinner-sm'></span> Saving...</span>"),
        FormActions(
            Submit("save", "Save {{ target }}", ic_indicator="#indicator"),
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "url",
            "description",
        ]

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div(
                "name",
                css_class="col-md-12",
            ),
            Div(
                "url",
                css_class="col-md-12",
            ),
            Div(
                "description",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        FormActions(
            Submit("save", "Save {{ target }}"),
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )


class DariahTermsOfUse(ModelForm):
    class Meta:
        model = User
        fields = [
            "accepted_dariah_tou",
        ]

    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            Div(
                "accepted_dariah_tou",
                css_class="col-md-8",
            ),
            css_class="row",
        ),
        FormActions(
            Submit("save", "Save {{ target }}"),
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )
