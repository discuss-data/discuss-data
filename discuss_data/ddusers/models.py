import logging
import uuid
from datetime import timedelta

from actstream.models import followers, following, user_stream
from django.apps import apps
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _
from django_bleach.models import BleachField
from django.template.loader import render_to_string
from guardian.shortcuts import get_objects_for_user
from PIL import Image
from taggit.managers import TaggableManager
from actstream.models import Action

from discuss_data.core.models import AffiliationTags, EnglishTags, KeywordTags, Topic
from discuss_data.core.utils import weekly_td, daily_td, send_update_email
from discuss_data.ddcomments.models import Notification
from discuss_data.utils.cropped_thumbnail import cropped_thumbnail

logger = logging.getLogger(__name__)


ACADEMIC_TITLE_CHOICES = (
    ("PHD", "PhD"),
    ("DR", "Dr."),
    ("PROF", "Prof. Dr."),
    ("MA", "MA",),
    ("BA", "BA",),
    ("MAG", "Magister"),
    ("M.A.", "M.A."),
)

ACADEMIC_TITLE_CHOICES_ORDER = {
    "PHD": "b",
    "DR": "f",
    "PROF": "f",
    "MA": "b",
    "BA": "b",
    "MAG": "f",
    "M.A.": "b",
}


class User(AbstractUser):
    """
    User profile accessibility
    """
    PUBLIC = "PUB" # accessible for all website visitors
    NETWORK = "NET" # accessible for logged-in users
    HIDDEN = "HID" # accessible for owner only

    ACCESS_RULES_CHOICES = (
        (PUBLIC, _("public")),
        (NETWORK, _("internal")),
        (HIDDEN, _("hidden")),
    )

    """
    Login types
    """
    DD = "DD" # Discuss Data login
    DARIAH = "DAR" # Dariah login
    ORCID = "ORC" # Orcid login
    NONE = "NON" # None

    LOGIN_TYPE_CHOICES = (
        (DD, _("Discuss Data")),
        (DARIAH, _("DARIAH")),
        (ORCID, _("ORCID")),
        (NONE, _("None")),
    )

    id = models.AutoField(primary_key=True)
    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False
    )  # uuid _not_ as pk as this disturbs django 3rd party apps
    middle_name = models.CharField(max_length=200, blank=True)
    academic_title = models.CharField(
        max_length=5, choices=ACADEMIC_TITLE_CHOICES, blank="None"
    )  # deprecated
    name_prefix = models.CharField(
        max_length=200, blank=True, verbose_name="prefix", help_text='for example "Dr"',
    )
    name_suffix = models.CharField(
        max_length=200,
        blank=True,
        verbose_name="suffix",
        help_text='for example "PhD"',
    )
    # position = models.CharField(max_length=600)
    # institution = models.ForeignKey('Institution', blank=True, null=True, related_name='user_profile_institution')
    # institutions = models.ManyToManyField('Institution', blank=True,)
    photo = models.ImageField(blank=True, null=True)
    # fields_of_interest = models.ManyToManyField('FOI', blank=True)
    topics = models.ManyToManyField(Topic, blank=True)
    interests = TaggableManager(blank=True, through=KeywordTags)
    receptive_tos = models.ManyToManyField("ReceptiveTo", blank=True)
    countries = models.ManyToManyField(
        "Country", related_name="user_country", blank=True
    )
    research_profile_full = BleachField(
        blank=True, max_length=12000, verbose_name="Research Profile (full)"
    )
    research_profile_short = BleachField(
        max_length=280,
        blank=True,
        verbose_name="Research Profile (brief)",
        help_text="brief version, max. 280 characters",
    )
    profile_accessibility = models.CharField(
        max_length=3, choices=ACCESS_RULES_CHOICES, default=PUBLIC
    )
    publications = models.ManyToManyField(
        "ddpublications.Publication", blank=True, related_name="user_publication",
    )
    en_tags = TaggableManager(blank=True, through=EnglishTags)
    affiliation_tags = TaggableManager(
        "affiliations", blank=True, through=AffiliationTags
    )
    external_profile = models.URLField(blank=True)

    accepted_dariah_tou = models.BooleanField(
        verbose_name="Accept DARIAH TOU", default=False
    )

    accepted_dd_tou = models.BooleanField(
        verbose_name="Accept Discuss Data TOU", default=False
    )

    show_first_steps = models.BooleanField(
        verbose_name="Show First Steps", default=True
    )
    email_alternative = models.EmailField(
        verbose_name="Alternative email address",
        help_text="Add an other email address to use in your profile and to receive Discuss Data emails ",
        blank=True,
    )
    verified = models.BooleanField(
        verbose_name=_("Verified?"), default=False
    )
    login_type = models.CharField(
        max_length=3, choices=LOGIN_TYPE_CHOICES, default=NONE
    )

    class Meta:
        ordering = ["last_name"]

    def get_academic_name(self):
        a_n = list()
        if self.name_prefix:
            a_n.append(self.name_prefix)

        if self.first_name:
            a_n.append(self.first_name)

        if self.middle_name:
            a_n.append(self.middle_name)

        if self.last_name:
            a_n.append(self.last_name)

        academic_name = " ".join(a_n)

        if self.name_suffix:
            academic_name = academic_name + ", " + self.name_suffix

        return academic_name

    def get_email(self):
        if self.email_alternative:
            return self.email_alternative
        else:
            return self.email

    def get_publications(self):
        return self.publications.all().order_by("-year")

    def get_countries(self):
        return self.countries.all().order_by("name")

    def get_interests(self):
        return self.interests.all().order_by("name")

    def get_main_affiliation(self):
        return Affiliation.objects.filter(user=self).filter(main=True)

    def get_main_affiliation_text(self):
        affiliation_list = [affiliation.get_institution() for affiliation in self.get_main_affiliation()]
        return "; ".join(affiliation_list)

    def get_affiliations(self):
        return self.affiliation_user.all().order_by("list_position")

    def get_lists_public(self):
        model = apps.get_model("dddatasets", "datalist")
        return model.objects.filter(public=True, owner=self)

    def get_lists_all(self):
        model = apps.get_model("dddatasets", "datalist")
        return model.objects.filter(owner=self)

    def get_published_admin_datasets(self):
        model = apps.get_model("dddatasets", "dataset")
        return get_objects_for_user(self, "admin_dsmo", model).filter(published=True)

    def get_published_datasets(self):
        # use get model to avoid circular import
        model = apps.get_model("dddatasets", "datasetmanagementobject")
        dsmos = model.objects.filter(owner=self).filter(published=True)
        datasets = list()
        for dsmo in dsmos:
            if dsmo.get_top_version_published_dataset():
                datasets.append(dsmo.get_top_version_published_dataset())
        return datasets

    def get_published_datasets_count(self):
        return len(self.get_published_datasets())

    def curated_categories(self):
        return self.category_curators_user.all()

    def get_curated_datasets(self):
        model = apps.get_model("dddatasets", "dataset")
        datasets = model.objects.filter(
            dataset_management_object__main_category__in=self.curated_categories()
        )
        return datasets

    def get_all_datasets(self):
        return self.dataset_set.all()

    def get_projects(self):
        return Project.objects.filter(user=self).order_by("name")
        # projects_list = list()
        # for project in Project.objects.filter(user=self):
        #    projects_list.append(project)
        # return projects_list

    def __str__(self):
        academic_name = self.get_academic_name()
        if academic_name:
            return self.get_academic_name()
        else:
            return self.username

    def get_notifications_curators(self, dataset_content_type):
        user_curated_datasets = list(
            self.get_curated_datasets().values_list("id", flat=True)
        )
        notifications_curators = Notification.objects.filter(
            content_type=dataset_content_type.id, object_id__in=user_curated_datasets
        )
        return notifications_curators

    def get_notifications(self):
        user_all_datasets = list(self.get_all_datasets().values_list("id", flat=True))

        user_admin_datasets = list(
            self.get_published_admin_datasets().values_list("id", flat=True)
        )

        user_notifications_to = Notification.objects.filter(recipient=self)

        dataset_content_type = ContentType.objects.get(
            app_label="dddatasets", model="dataset"
        )

        datasets_notifications = Notification.objects.filter(
            content_type=dataset_content_type.id, object_id__in=user_all_datasets
        )

        datasets_notifications_admins = Notification.objects.filter(
            content_type=dataset_content_type.id, object_id__in=user_admin_datasets
        )

        datasets_notifications_curators = self.get_notifications_curators(
            dataset_content_type
        )

        notifications = (
            user_notifications_to
            | datasets_notifications
            | datasets_notifications_curators
            | datasets_notifications_admins
        )
        return notifications

    def get_notifications_count(self):
        return self.get_notifications().count()

    def get_notifications_recent(self):
        startdate = timezone.now()
        enddate = startdate - timedelta(days=7)
        return (
            self.get_notifications()
            .filter(date_added__lte=startdate)
            .filter(date_added__gte=enddate)
        )

    def get_notifications_recent_count(self):
        return self.get_notifications_recent().count()

    def get_following_actions(self):
        return user_stream(self)

    def get_following_actions_recent_weekly(self):
        startdate, enddate = weekly_td()
        return (
            self.get_following_actions()
            .filter(timestamp__lte=startdate)
            .filter(timestamp__gte=enddate)
        )

    def get_following_actions_recent_daily(self):
        startdate, enddate = daily_td()
        return (
            self.get_following_actions()
            .filter(timestamp__lte=startdate)
            .filter(timestamp__gte=enddate)
        )

    def get_datasets_actions(self):
        user_admin_datasets = list(
            self.get_published_admin_datasets().values_list("id", flat=True)
        )
        actor_ctype = ContentType.objects.get(app_label="ddusers", model="user")
        return Action.objects.datasets_actions(user_admin_datasets).exclude(
            actor_content_type=actor_ctype, actor_object_id=self.id
        )

    def get_datasets_actions_recent_weekly(self):
        startdate, enddate = weekly_td()
        return (
            self.get_datasets_actions()
            .filter(timestamp__lte=startdate)
            .filter(timestamp__gte=enddate)
        )

    def get_datasets_actions_recent_daily(self):
        startdate, enddate = daily_td()
        return (
            self.get_datasets_actions()
            .filter(timestamp__lte=startdate)
            .filter(timestamp__gte=enddate)
        )

    def get_user_actions(self):
        return Action.objects.any_everything(self)

    def get_user_actions_recent_weekly(self):
        startdate, enddate = weekly_td()
        return (
            self.get_user_actions()
            .filter(timestamp__lte=startdate)
            .filter(timestamp__gte=enddate)
        )

    def get_user_actions_recent_daily(self):
        startdate, enddate = daily_td()
        return (
            self.get_user_actions()
            .filter(timestamp__lte=startdate)
            .filter(timestamp__gte=enddate)
        )

    def compose_status_email_daily(self):
        # daily status email
        feed_actions = self.get_user_actions_recent_daily()
        return render_to_string(
            "ddusers/status_email.html", {"user": self, "feed_actions": feed_actions}
        )

    def compose_status_email_weekly(self):
        # weekly status email
        feed_actions = self.get_user_actions_recent_weekly()
        return render_to_string(
            "ddusers/status_email.html", {"user": self, "feed_actions": feed_actions}
        )

    def get_following(self):
        return following(self, User)

    def get_followers(self):
        return followers(self)

    def get_following_count(self):
        return len(following(self, User))

    def get_followers_count(self):
        return len(followers(self))

    def group_member(self, group):
        return self.groups.filter(id=group.id).exists()

    @property
    def image_url(self):
        try:
            return self.photo.path
        except Exception:
            return "/static/images/user_default.png"

    def get_activities(self):
        return user_stream(self, with_user_activity=True)

    def get_absolute_url(self):
        return reverse("ddusers:detail", args=[str(self.uuid)])

    def class_name(self):
        return self.__class__.__name__

    def generate_thumbnail(self):
        # TODO: move into upload view?
        if self.photo and hasattr(self.photo, "url"):
            # for in situ thumbnail generation at upload
            im = Image.open(self.photo.path)
            thumbnail = cropped_thumbnail(im, [250, 250])
            thumbnail.save(self.photo.path)

    def get_socialaccount_provider(self):
        from allauth.socialaccount.models import SocialAccount
        try:
            sa = SocialAccount.objects.get(user=self)
            return sa.provider
        except SocialAccount.DoesNotExist:
            return None


class UserVerificationRequest(models.Model):
    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False
    )
    user = models.OneToOneField("User", related_name="verification_request", on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, help_text=_("Please provide your real name"),)
    last_name = models.CharField(max_length=100, help_text=_("Please provide your real name"))
    verification_info = BleachField(
        max_length=280,
        verbose_name=_("Information for verification"),
        help_text=_("Information for verification purposes only"),
    )
    verification_url = models.URLField(blank=True, null=True, help_text=_("An URL to information about you at a research insitution can speed up the verification process considerably"))
    verified = models.BooleanField(
        verbose_name=_("Verified?"), default=False
    )
    verified_by = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    verification_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.user.username

    def verify_user(self):
        if self.verified:
            self.user.first_name = self.first_name
            self.user.last_name = self.last_name
            self.user.verified = True
            self.user.login_type = "DD"
            self.user.save()
            message = f"""
Dear {self.user.first_name} {self.user.last_name},

your account has been verified. You can now log into Discuss Data at https://discuss-data.net/login.

Best regard,
Discuss Data Team
            """
            # TODO: messages should be defined in db
            send_update_email(
                subject=_("Your Discuss Data account has been verified"),
                message=message,
                email_to=[self.user.email,]
            )


    def inform_verifiers(self, verified=False):
        to_list = User.objects.filter(groups__name='User verification').values_list('email', flat=True)
        if to_list.count() < 1:
            logger.error("Group 'User verification' is empty or non existant. Needed for user verification.")
        if not verified:
            send_update_email(
                subject=_("User verification: new user signed up"),
                message=_(f"A new user signed up to Discuss Data and needs to be verified."),
                email_to=to_list
            )
        else:
            send_update_email(
                subject=_("New Discuss Data account has been verified"),
                message=f"User account for {self.user.first_name} {self.user.last_name} has been verified by {self.verified_by.username}",
                email_to=to_list
            )

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.verified:
            pass
            # self.verify_user()
        else:
            self.inform_verifiers()



class ReceptiveTo(models.Model):
    text = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.text


class City(models.Model):
    name = models.CharField(max_length=400)
    slug = models.SlugField()
    geo_lat = models.CharField(max_length=200, blank=True)
    geo_lng = models.CharField(max_length=200, blank=True)
    country = models.ForeignKey(
        "Country", related_name="city_country", on_delete=models.CASCADE
    )
    public = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField()
    geo_lat = models.CharField(max_length=200)
    geo_lng = models.CharField(max_length=200)
    public = models.BooleanField(default=True)

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class FOI(models.Model):
    name = models.CharField(max_length=200)
    description = BleachField(max_length=1200, blank=True)
    is_country = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Affiliation(models.Model):
    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False
    )  # uuid _not_ as pk as this disturbs django 3rd party apps
    user = models.ForeignKey(
        "User", related_name="affiliation_user", on_delete=models.CASCADE
    )
    institution = models.ForeignKey(
        "Institution",
        related_name="affiliation_institution",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    name_of_institution = models.CharField(max_length=200)
    place_of_institution = models.CharField(max_length=200)
    country_of_institution = models.CharField(max_length=200)
    position = models.CharField(max_length=200)
    website_of_institution = models.URLField(blank=True)
    main = models.BooleanField(default=True)
    list_position = models.IntegerField(null=True, blank=True)

    def get_institution(self):
        return "{}, {}, {}".format(
            self.name_of_institution,
            self.place_of_institution,
            self.country_of_institution,
        )

    def __str__(self):
        return "%s, %s, %s, %s" % (
            self.position,
            self.name_of_institution,
            self.place_of_institution,
            self.country_of_institution,
        )


class Institution(models.Model):
    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False
    )  # uuid _not_ as pk as this disturbs django 3rd party apps
    name = models.CharField(max_length=600)
    original_name = models.CharField(max_length=600, blank=True)
    short_name = models.CharField(max_length=60, blank=True)
    department = models.CharField(max_length=600, blank=True)
    address = models.CharField(max_length=600)
    postcode = models.CharField(max_length=40, blank=True, null=True)
    image = models.ImageField(blank=True, null=True)
    image_large = models.ImageField(blank=True, null=True)
    website = models.URLField(blank=True)
    description = BleachField(max_length=1200, blank=True)
    short_description = models.CharField(max_length=400, blank=True)
    location = models.CharField(max_length=20, blank=True)
    city = models.CharField(max_length=120)
    country = models.ForeignKey(
        "Country",
        related_name="institution_country",
        blank=True,
        on_delete=models.CASCADE,
    )
    public = models.BooleanField(default=True)
    admin = models.ForeignKey(
        "User",
        related_name="institution_admin_user",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    affiliated_users = models.ManyToManyField("User", blank=True,)

    def __str__(self):
        return self.name


class Project(models.Model):
    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False
    )  # uuid _not_ as pk as this disturbs django 3rd party apps
    user = models.ForeignKey(
        "User", related_name="project_user", on_delete=models.CASCADE
    )
    # group = models.ForeignKey(
    #     Group,
    #     blank=True,
    #     null=True,
    #     related_name="project_group",
    #     on_delete=models.CASCADE,
    # )
    name = models.CharField(max_length=200)
    url = models.URLField(blank=True)
    description = BleachField(blank=True, max_length=2400)

    def __str__(self):
        return self.name
