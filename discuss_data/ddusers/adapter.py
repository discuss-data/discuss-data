import json
import logging
from wsgiref.simple_server import WSGIRequestHandler

import allauth
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter


logger = logging.getLogger(__name__)


class SocialAccountDebuggingAdapter(DefaultSocialAccountAdapter):
    """A custom SocialAccountAdapter that hooks into the process of social
    authentication and logs the user attributes."""

    def pre_social_login(
        self,
        request: WSGIRequestHandler,
        sociallogin: allauth.socialaccount.models.SocialLogin,
    ):
        logger.debug("SOCIALLOGIN: %s", json.dumps(sociallogin.serialize()))
