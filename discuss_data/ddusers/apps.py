from django.apps import AppConfig


class DdusersConfig(AppConfig):
    name = "discuss_data.ddusers"

    def ready(self):
        from actstream import registry

        registry.register(self.get_model("User"))
