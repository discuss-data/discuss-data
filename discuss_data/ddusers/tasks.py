from django.utils.translation import gettext as _

from django_q.tasks import async_task

from discuss_data.ddusers.models import User


def send_status_emails_daily():
    """
        Task function for sending status emails to all users
        Called by a django-q Schedule
        Uses a django-q async_task
        user.compose_status_email_daily() generates text
    """
    for user in User.objects.all():
        email = user.get_email().split(";")[0]
        subject = _("[Discuss Data] Daily Status")
        text = user.compose_status_email_daily()
        sender = "info@discuss-data.net"
        to = [email]
        async_task(
            "django.core.mail.send_mail",
            subject,
            text,
            sender,
            to,
            html_message=text,
            fail_silently=False,
        )
