from django.contrib import admin
from django.utils import timezone

from discuss_data.ddusers.models import Country, User, UserVerificationRequest

@admin.action(description="Set selected users to verified")
def make_verified(modeladmin, request, queryset):
    queryset.update(verified=True)


class UserVerificationRequestAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "last_name", "first_name", "verified", "verified_by", "verification_date")
    readonly_fields = ("verified_by", "verification_date")

    def username(self, obj):
        return obj.user.username

    def save_model(self, request, obj, form, change):
        if obj.verified:
            obj.verify_user()
            obj.verified_by = request.user
            obj.verification_date = timezone.now()
            obj.inform_verifiers(verified=True)
        super().save_model(request, obj, form, change)


class UserAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "login_type", "verified", "name", "profile_accessibility")
    search_fields = ["first_name", "last_name", "username"]
    actions = [make_verified]

    def name(self, obj):
        return "%s, %s" % (obj.last_name, obj.first_name)

    # name.short_description = "Name" #commented out because of mypy error


admin.site.register(UserVerificationRequest, UserVerificationRequestAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(Country)
