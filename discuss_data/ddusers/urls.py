from django.urls import path

from discuss_data.ddusers import views


app_name = "ddusers"


urlpatterns = [
    # public URLs
    # path("search/", views.UsersListView.as_view(), name="search"), # this line breaks user search
    path("search/", views.search, name="search"),  # use this for searching
    path("verification/", views.verification_info, name="verification_info"),
    path("terms-of-use/", views.accept_tou, name="accept_tou"),
    path("first-steps/", views.first_steps, name="first_steps"),
    path("<uuid:us_uuid>/", views.UserDetailsView.as_view(), name="detail"),
    path("follow/<uuid:us_uuid>/", views.user_follow, name="follow"),
    path(
        "follow/datalist/<uuid:dl_uuid>/", views.datalist_follow, name="datalist_follow"
    ),
    path("follow/", views.user_follow_status, name="follow_status"),
    path("feed/users/", views.user_act_feed, name="user_feed"),
    path("feed/following/", views.following_act_feed, name="following_feed"),
    path("following/", views.following_users, name="following_users"),
    path("followers/", views.followers_users, name="followers_users"),
    path(
        "following/datasets/",
        views.list_following_datasets,
        name="list_following_datasets",
    ),
    path(
        "following/datalists/",
        views.list_following_datalists,
        name="list_following_datalists",
    ),
    path("followed/<str:follow_type>/", views.list_followed, name="list_followed"),
    # path("feed/notifications", views.notification_act_feed, name="notification_feed"),
    # path("follow/otherfeed/", views.other_feed, name="other_feed"),
    path("<uuid:us_uuid>/affiliations/", views.user_affiliations, name="affiliations",),
    path("<uuid:us_uuid>/datalists/", views.user_data_lists, name="data_lists",),
    path(
        "<uuid:us_uuid>/datalists/<uuid:dl_uuid>",
        views.user_get_data_list,
        name="get_data_list",
    ),
    path("<uuid:us_uuid>/projects/", views.user_projects, name="projects"),
    path("<uuid:us_uuid>/publications/", views.user_publications, name="publications",),
    path("<uuid:us_uuid>/datasets/", views.user_datasets, name="datasets"),
    path("<uuid:us_uuid>/lists/", views.user_lists_public, name="lists_public",),
    path("notifications/", views.user_notifications, name="notifications"),
    path("notifications/add/", views.user_notification, name="notification"),
    # edit urls
    # users search for dataset access control
    path("edit/search/", views.user_search, name="user_search"),
    # user profile: edit pages
    path("edit/", views.user_edit, name="edit"),
    path("edit/profile/", views.user_edit_profile, name="profile_edit"),
    # user profile: data lists
    path("follow/lists/add/dataset", views.dataset_to_list, name="dataset_to_list",),
    path("edit/lists/", views.user_edit_lists, name="edit_lists",),
    path("edit/lists/add/", views.user_edit_data_list, name="edit_list"),
    path("edit/lists/<uuid:dl_uuid>", views.user_edit_data_list, name="edit_list",),
    # user profile: affiliations
    path("edit/affiliations/", views.user_edit_affiliations, name="edit_affiliations",),
    path(
        "edit/affiliations/add/", views.user_edit_affiliation, name="edit_affiliation"
    ),
    path(
        "edit/affiliations/<uuid:af_uuid>/",
        views.user_edit_affiliation,
        name="edit_affiliation",
    ),
    # user profile: projects
    path("edit/projects/", views.user_edit_projects, name="edit_projects"),
    path("edit/projects/add/", views.user_edit_project, name="edit_project"),
    path("edit/projects/<uuid:pr_uuid>/", views.user_edit_project, name="edit_project"),
    # user profile: publications
    path("edit/publications/", views.user_edit_publications, name="edit_publications",),
    path(
        "edit/publications/add/", views.user_edit_publication, name="edit_publication"
    ),
    path(
        "edit/publications/<uuid:pub_uuid>/",
        views.user_edit_publication,
        name="edit_publication",
    ),
    # TODO: integrate ddpublications
    path("edit/countries/", views.user_countries, name="countries"),
    path("edit/tags/", views.user_tags, name="tags"),
    path("curation/", views.user_curation, name="curation"),
]
