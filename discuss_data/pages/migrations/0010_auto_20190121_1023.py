# Generated by Django 2.0.9 on 2019-01-21 10:23

from django.db import migrations, models
import django.db.models.deletion
import wagtail.fields


class Migration(migrations.Migration):

    dependencies = [
        ("wagtailcore", "0041_group_collection_permissions_verbose_name_plural"),
        ("pages", "0009_auto_20190118_1427"),
    ]

    operations = [
        migrations.CreateModel(
            name="ManualIndexPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="wagtailcore.Page",
                    ),
                ),
                ("intro", models.CharField(blank=True, max_length=256, null=True)),
                ("text", wagtail.fields.RichTextField(blank=True)),
            ],
            options={"abstract": False,},
            bases=("wagtailcore.page",),
        ),
        migrations.AddField(
            model_name="indexpage",
            name="text",
            field=wagtail.fields.RichTextField(blank=True),
        ),
        migrations.AlterField(
            model_name="indexpage",
            name="intro",
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
