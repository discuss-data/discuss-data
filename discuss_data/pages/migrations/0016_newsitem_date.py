# Generated by Django 2.2.10 on 2020-04-03 11:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0015_newsitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='newsitem',
            name='date',
            field=models.DateField(blank=True, null=True, verbose_name='Post date'),
        ),
    ]
