# Generated by Django 2.2.14 on 2020-09-03 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0022_auto_20200902_1111'),
    ]

    operations = [
        migrations.AddField(
            model_name='manualpage',
            name='search_index_exclude',
            field=models.BooleanField(default=False),
        ),
    ]
