# Generated by Django 2.2.16 on 2020-10-22 17:38

from django.db import migrations
import wagtail.contrib.table_block.blocks
import wagtail.blocks
import wagtail.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0027_data_migration_generate_toctree'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manualpage',
            name='body',
            field=wagtail.fields.StreamField([('topic', wagtail.blocks.StructBlock([('heading', wagtail.blocks.CharBlock(required=False)), ('field_slug', wagtail.blocks.CharBlock(required=True)), ('paragraph', wagtail.blocks.RichTextBlock(required=False)), ('image', wagtail.images.blocks.ImageChooserBlock(required=False)), ('table', wagtail.contrib.table_block.blocks.TableBlock(required=False, template='pages/blocks/table.html'))]))], blank=True, null=True),
        ),
    ]
