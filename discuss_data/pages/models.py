from django.db import models
from django.template.loader import render_to_string
from modelcluster.fields import ParentalKey
from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail import blocks
from wagtail.fields import RichTextField, StreamField
from wagtail.models import Orderable, Page
from wagtail.images.blocks import ImageChooserBlock
from wagtail.search import index
from wagtail.contrib.table_block.blocks import TableBlock


def generate_toc_tree(slug):
    """
    Generate a _toc.html template on ManualPage and
    ManualIndexPage save to avoid full page tree rendering
    on every page load
    """
    toc_tree, created = TocTree.objects.get_or_create(slug=slug)
    toc_tree.html = render_to_string("pages/_toc.html", {},)
    toc_tree.save()


class TocTree(models.Model):
    html = models.TextField(blank=True, null=True)
    slug = models.SlugField(blank=True, null=True, max_length=256)


class ManualTopicBlock(blocks.StructBlock):
    heading = blocks.CharBlock(required=False)
    field_slug = blocks.CharBlock(required=True)
    paragraph = blocks.RichTextBlock(required=False)
    image = ImageChooserBlock(required=False)
    table = TableBlock(required=False, template="pages/blocks/table.html")

    class Meta:
        template = "pages/blocks/manual_topic_block.html"


class IndexPage(Page):
    intro = models.CharField(max_length=256, blank=True, null=True)
    text = RichTextField(blank=True)

    # exclude from search
    search_fields = []

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        FieldPanel("text", classname="full"),
    ]


class IndexPageSearch(Page):
    intro = models.CharField(max_length=256, blank=True, null=True)
    text = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        FieldPanel("text", classname="full"),
    ]


class ManualIndexPage(Page):
    intro = models.CharField(max_length=256, blank=True, null=True)
    text = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel("intro", classname="full"),
        FieldPanel("text", classname="full"),
    ]

    def class_name(self):
        return self.__class__.__name__

    def save(self, *args, **kwargs):
        if not kwargs.get("update_fields"):
            # Generate new table of contents tree
            generate_toc_tree(slug="help")

        return super().save(*args, **kwargs)


class ManualPage(Page):
    """
    query in django views using:
    p = ManualPage.objects.get(id=7)
    p.body[0].__dict__

    """

    intro = models.CharField(max_length=256, blank=True, null=True)
    body = StreamField([("topic", ManualTopicBlock())], blank=True, null=True, use_json_field=True)
    search_index_exclude = models.BooleanField(default=False)

    content_panels = Page.content_panels + [
        FieldPanel("intro"),
        FieldPanel("search_index_exclude"),
        FieldPanel("body"),
    ]

    search_fields = Page.search_fields + [  # Inherit search_fields from Page
        index.SearchField("intro"),
        index.SearchField("body"),
    ]

    @classmethod
    def get_indexed_objects(cls):
        return cls.objects.filter(search_index_exclude=False)

    def class_name(self):
        return self.__class__.__name__

    def save(self, *args, **kwargs):
        if not kwargs.get("update_fields"):
            # Generate new table of contents tree
            generate_toc_tree(slug="help")

        return super().save(*args, **kwargs)


class TeamMemberIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [FieldPanel("intro", classname="full")]


class LongTextBlock(blocks.StructBlock):
    heading = blocks.CharBlock(required=False)
    paragraph = blocks.RichTextBlock(required=False)
    image = ImageChooserBlock(required=False)
    slogan = blocks.CharBlock(required=False)

    class Meta:
        template = "pages/blocks/long_text_block.html"


class LandingPage(Page):
    template = 'landingpage/landing_page.html'
    subtitle = models.CharField(max_length=256, blank=True, null=True)
    short_description = RichTextField(blank=True)
    project_description = StreamField(
        [("project_description", LongTextBlock())], blank=True, null=True, use_json_field=True
    )
    advisory_board = RichTextField(blank=True)

    # exclude from search
    search_fields = []

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        FieldPanel("short_description"),
        InlinePanel("slogans_item"),
        FieldPanel("project_description"),
        FieldPanel("advisory_board"),
        InlinePanel("team_member"),
        InlinePanel("news_item"),
    ]


class SlogansItem(Orderable):
    page = ParentalKey(
        LandingPage, on_delete=models.CASCADE, related_name="slogans_item"
    )
    title = models.CharField(max_length=256, blank=True, null=True)
    text = RichTextField(blank=True)
    panels = [
        FieldPanel("title"),
        FieldPanel("text"),
    ]


class NewsItem(Orderable):
    class Meta:
        ordering = ["-date"]

    page = ParentalKey(LandingPage, on_delete=models.CASCADE, related_name="news_item")
    title = models.CharField(max_length=256, blank=True, null=True)
    text = RichTextField(blank=True)
    date = models.DateField("Post date", blank=True, null=True)

    panels = [
        FieldPanel("date"),
        FieldPanel("title"),
        FieldPanel("text"),
    ]


class TeamMemberEntry(Orderable):
    page = ParentalKey(
        LandingPage, on_delete=models.CASCADE, related_name="team_member"
    )
    name = models.CharField(max_length=256, blank=True, null=True)
    role = models.CharField(max_length=256, blank=True, null=True)
    text = RichTextField(blank=True)
    link = models.URLField(blank=True)
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        related_name="+",
        blank=True,
        null=True,
    )

    panels = [
        FieldPanel("name"),
        FieldPanel("role"),
        FieldPanel("page"),
        FieldPanel("text"),
        FieldPanel("link"),
        FieldPanel("image"),
    ]


class TeamMemberPage(Page):
    text = RichTextField(blank=True)
    link = models.URLField(blank=True)
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        related_name="+",
        blank=True,
        null=True,
    )

    # exclude from search
    search_fields = []

    content_panels = Page.content_panels + [
        FieldPanel("text", classname="full"),
        FieldPanel("link"),
        FieldPanel("image"),
    ]


class LicensePage(Page):
    name = models.CharField(max_length=256, blank=True, null=True)
    text = RichTextField(blank=True)
    link = models.URLField(blank=True)
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.PROTECT,
        related_name="+",
        blank=True,
        null=True,
    )

    # exclude from search
    search_fields = []

    content_panels = Page.content_panels + [
        FieldPanel("name"),
        FieldPanel("text", classname="full"),
        FieldPanel("link"),
        FieldPanel("image"),
    ]


class TextPage(Page):
    text = RichTextField(blank=True)

    # exclude from search
    search_fields = []

    content_panels = Page.content_panels + [
        FieldPanel("text", classname="full"),
    ]
