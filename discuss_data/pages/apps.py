from django.apps import AppConfig


class PagesConfig(AppConfig):
    name = "discuss_data.pages"
