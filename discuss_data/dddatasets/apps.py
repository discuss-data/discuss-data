from django.apps import AppConfig


class DddatasetsConfig(AppConfig):
    name = "discuss_data.dddatasets"

    def ready(self):
        from actstream import registry

        registry.register(self.get_model("DataSet"))
        registry.register(self.get_model("DataSetManagementObject"))
        registry.register(self.get_model("DataList"))
