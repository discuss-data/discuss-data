from wagtail_modeladmin.options import (
    ModelAdmin,
    ModelAdminGroup,
    modeladmin_register,
)
from discuss_data.dddatasets.models import (
    DataType,
    Category,
    CollectionMethodsTagged,
    AnalysisMethodsTagged,
    DisciplinesTagged,
)

from discuss_data.core.models import (
    KeywordTagged,
    LanguageTagged,
)

"""
Register Django models for administration in wagtail.
Needs also FieldPanel definitions in the Django models

Models from core are added here as they also belong into
the TagsGroup
"""


class DataTypeAdmin(ModelAdmin):
    model = DataType
    menu_label = "Data types"  # ditch this to use verbose_name_plural from model
    menu_icon = "cogs"  # change as required
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",)


class CategoryAdmin(ModelAdmin):
    model = Category
    menu_label = "Categories"  # ditch this to use verbose_name_plural from model
    menu_icon = "cogs"  # change as required
    menu_order = 300  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name", "curators__last_name", "curators__first_name")


class CuratedItemsGroup(ModelAdminGroup):
    menu_label = "DD Curated Items"
    menu_icon = "cogs"  # change as required
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    items = (DataTypeAdmin, CategoryAdmin)


class CollectionMethodsTaggedAdmin(ModelAdmin):
    model = CollectionMethodsTagged
    menu_icon = "tag"  # change as required
    menu_order = 300  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",)


class AnalysisMethodsTaggedAdmin(ModelAdmin):
    model = AnalysisMethodsTagged
    menu_icon = "tag"  # change as required
    menu_order = 400  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",)


class DisciplinesTaggedAdmin(ModelAdmin):
    model = DisciplinesTagged
    menu_icon = "tag"  # change as required
    menu_order = 500  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",)


class KeywordTaggedAdmin(ModelAdmin):
    model = KeywordTagged
    menu_icon = "tag"  # change as required
    menu_order = 600  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",)


class LanguageTaggedAdmin(ModelAdmin):
    model = LanguageTagged
    menu_icon = "tag"  # change as required
    menu_order = 700  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = (
        False  # or True to exclude pages of this type from Wagtail's explorer view
    )
    list_display = ("name",)
    list_filter = ("name",)
    search_fields = ("name",)


class TagsGroup(ModelAdminGroup):
    menu_label = "DD Tags"
    menu_icon = "tag"  # change as required
    menu_order = 300  # will put in 3rd place (000 being 1st, 100 2nd)
    items = (
        CollectionMethodsTaggedAdmin,
        AnalysisMethodsTaggedAdmin,
        DisciplinesTaggedAdmin,
        KeywordTaggedAdmin,
        LanguageTaggedAdmin,
    )


# register as a single menu entry for every admin
# modeladmin_register(DataTypeAdmin)
# modeladmin_register(CategoryAdmin)

# register as a admin group
modeladmin_register(CuratedItemsGroup)
modeladmin_register(TagsGroup)
