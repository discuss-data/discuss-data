from django.contrib.auth.decorators import login_required

from discuss_data.dddatasets.utils import dataset_comments, dataset_edit_comment

from discuss_data.core.decorators import dd_tou_accepted, dd_user_verified


# ************
# auth check in template to allow comment listing for everyone
# ************
def comments(request, ds_uuid):
    """ calls dataset_comments for "public" comments
    """
    response = dataset_comments(request, ds_uuid, "public")
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_comment(request, ds_uuid, co_uuid=None):
    """ calls dataset_edit_comment for a "public" comment
    """
    response = dataset_edit_comment(request, ds_uuid, "public", co_uuid)
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_comment_reply(request, ds_uuid, co_uuid):
    """ calls dataset_edit_comment_reply for a "public" comment
    """
    response = dataset_edit_comment(request, ds_uuid, "public", co_uuid, reply=True)
    return response
