import logging
import os
import uuid as uuidlib
from datetime import date

from actstream import action
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import (
    ImproperlyConfigured,
    PermissionDenied,
    ValidationError,
)
from django.forms.models import inlineformset_factory
from django.http import FileResponse, Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _
from django_q.tasks import fetch
from django.db.models import Q
from django.views.generic.edit import FormView, UpdateView
from django.views.generic.base import TemplateView
from django.urls import reverse

from guardian.shortcuts import get_objects_for_user

from discuss_data.core.decorators import (
    dd_tou_accepted,
    dd_dataset_access,
    dd_user_verified,
)
from discuss_data.core.errors import MetaDataError
from discuss_data.core.helpers import check_perms, check_perms_403
from discuss_data.core.models import KeywordTagged, LanguageTagged
from discuss_data.core.views import (
    handler403,
    response403,
)
from discuss_data.core.helpers import get_help_texts
from discuss_data.ddcomments.models import Notification
from discuss_data.dddatasets.forms import (
    DataFileUploadForm,
    DataSetCopyrightForm,
    DataSetPrivacyRightsForm,
    DataSetCreatorFormSetHelper,
    DataSetEditForm,
    DataSetForm,
    DataSetImageForm,
    DataSetIndividualLicenseForm,
    DataSetPublicationForm,
    DataSetPublishAccept,
    DataSubmissionAgreementAcceptForm,
    DataSubmissionAgreementForm,
    ArchivalSignatureForm,
    ArchiveForm,
    DataReviewAddForm,
    DataReviewEditForm,
    DataReviewFileUploadForm,
)
from discuss_data.dddatasets.models import (
    AnalysisMethodsTagged,
    Category,
    CollectionMethodsTagged,
    DataFile,
    DataSet,
    DataSetCreator,
    DataSetManagementObject,
    DataSetPublication,
    DataSubmissionAgreement,
    DisciplinesTagged,
    License,
    ArchivalSignature,
    Archive,
    DataSetOwnerTransfer,
)
from discuss_data.dddatasets.utils import (
    dataset_comments,
    dataset_edit_comment,
    get_dataset_fields_from_form,
    get_man_page,
)
from discuss_data.ddpublications.forms import PublicationForm
from discuss_data.ddpublications.models import Publication
from discuss_data.ddusers.forms import DariahTermsOfUse
from discuss_data.ddusers.models import Country, User
from discuss_data.ddusers.views import user_notifications, user_search
from discuss_data.dhrep.publisher import Publisher, PublisherError
from discuss_data.dhrep.token import TokenHelper
from discuss_data.pages.models import LicensePage
from discuss_data.core.utils import send_update_email

logger = logging.getLogger(__name__)


""" prep views
    TODO: split up further
"""


@login_required
@dd_tou_accepted
@dd_user_verified
def listing(request):
    context = dict()
    user = request.user
    context["user"] = user
    dsmo_admin = set(
        get_objects_for_user(user, "admin_dsmo", DataSetManagementObject).exclude(
            owner=user
        )
    )
    dsmo_edit = set(
        get_objects_for_user(user, "edit_dsmo", DataSetManagementObject).exclude(
            owner=user
        )
    )
    dsmo_view = set(
        get_objects_for_user(user, "view_dsmo", DataSetManagementObject).exclude(
            owner=user
        )
    )

    context["dsmo_own"] = DataSetManagementObject.objects.filter(owner=user).order_by("-created_at")
    context["dsmo_admin"] = dsmo_admin
    context["dsmo_edit"] = dsmo_edit.difference(dsmo_admin)
    context["dsmo_view"] = dsmo_view.difference(dsmo_edit)

    # logger.debug(objects_own)
    # logger.debug(objects_view)
    context["manpage"] = get_man_page("dataset-list")  # ddataset-edit-description")
    context["object_type"] = "dataset management object"
    context["edit_url"] = "dataset_edit"
    context["add_url"] = "dataset_add"
    context["delete_url"] = "ic_ds_remove"

    return render(request, "dddatasets/prep/listing.html", context)


def get_data_from_doi_url(doi_url):
    import requests

    # doi_url = "https://doi.org/10.48320/992AB407-6E2F-43EC-B7B1-D4856734B6BD

    headers = {"accept": "application/vnd.citationstyles.csl+json;q=1.0"}
    dd_data = {}
    creators = list()
    r = requests.get(doi_url, headers=headers, timeout=10)
    try:
        doi_data = r.json()
        for entry in doi_data.get("author"):
            dd_creator = {}
            if entry.get("family"):
                dd_creator["name"] = entry.get("family")
            if entry.get("given"):
                dd_creator["first_name"] = entry.get("given")
            if not dd_creator.get("name"):
                if entry.get("literal"):
                    dd_creator["name"] = entry.get("literal")
            creators.append(dd_creator)
        dd_data["creators"] = creators

        dd_data["year"] = doi_data.get("issued").get("date-parts")[0][0]
        dd_data["title"] = doi_data.get("title")
        dd_data["doi"] = doi_data.get("DOI")
        dd_data["version"] = doi_data.get("version")
        dd_data["publisher"] = doi_data.get("publisher")
        dd_data["url"] = doi_data.get("url")
        return dd_data
    except requests.exceptions.JSONDecodeError:
        logger.error("DOI could not be parsed")
        return None


@method_decorator(login_required, name="dispatch")
@method_decorator(dd_tou_accepted, name="dispatch")
class DataSetAddFormView(FormView):
    """
    FormView to add a dataset
    """
    template_name = "dddatasets/prep/dataset_add.html"
    form_class = DataSetEditForm
    heading = _("Add a New Dataset")
    btn_text = _("Add Dataset")
    ds_type = "DS"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["categories_all"] = Category.objects.all().order_by("name")
        context["btn_text"] = self.btn_text
        context["heading"] = self.heading
        return context

    def form_valid(self, form):
        ds = form.save(commit=False)
        ds.owner = self.request.user

        if self.ds_type == "DR":
            # data review
            ds.dataset_type = self.ds_type
            ds.save()
            link = form.cleaned_data["link"]
            self.set_license(ds)
            self.set_meta_data_from_doi(ds, link)
        if self.ds_type == "EX":
            # external dataset
            ds.dataset_type = self.ds_type
            ds.save()
            link = form.cleaned_data["link"]
            self.set_meta_data_from_doi(ds, link)
        try:
            ds.save()
        except LicensePage.DoesNotExist as e:
            messages.error(self.get_form_classrequest, e)
            logger.error(e)
            raise Http404(e) from e
        form.save_m2m()
        main_category_slug = form.cleaned_data.get("main_category")
        try:
            main_category = Category.objects.get(slug=main_category_slug)
            ds.dataset_management_object.main_category = main_category
            ds.dataset_management_object.save()
            self.ds = ds
            return redirect("dddatasets:prep_edit", ds_uuid=ds.uuid)
        except:
            messages.warning(self.request, _("Please add a main category"))
            self.ds = ds
            return self.form_invalid(form)
        # return super().form_valid(form)

    def set_license(self, ds):
        ds.data_access = "OA"
        # set license to ODbL TODO: move to settings
        license_name = 'Attribution and Share Alike: Open Data Commons Open Database License (ODbL) ("share-alike")'
        ds.license = License.objects.get(license_name=license_name)
        standard_license = LicensePage.objects.get(name=license_name)
        ds.license.license_type = License.STANDARD
        ds.license.standard_license = standard_license
        ds.license.save()


    def set_creator(self, name, first_name, ds):
        # set an author
        dscreator = DataSetCreator(
            dataset=ds,
            name=name,
        )
        if first_name:
            dscreator.first_name = first_name
        dscreator.save()


    def set_meta_data_from_doi(self, ds, link):
        title_prefix = ""
        shorttitle_prefix = ""
        if ds.dataset_type == "DR":
            title_prefix = "Review of"
            shorttitle_prefix = "Review of"

        if "doi.org" in link:
            # extract metadata if link_url points to doi.org
            creators = list()
            data = get_data_from_doi_url(link)
            short_creators = list()
            for entry in data["creators"]:
                short_creators.append(entry.get("name"))
                if entry.get("first_name"):
                    creators.append(
                        f"{entry.get('name')}, {entry.get('first_name')}"
                    )
                    self.set_creator(entry.get('name'), entry.get('first_name'), ds)
                else:
                    creators.append(entry.get("name"))
                    self.set_creator(entry.get('name'), None, ds)

            title = f"{title_prefix} {'; '.join(creators)} ({data['year']}): {data['title']}"
            if title.endswith('.') or title.endswith(','):
                title = title.strip()[:-1]
            ds.title = title[:200] # hard cut field length
            ds.external_publisher = data['publisher'][:400]
            ds.shorttitle = (
                f"{shorttitle_prefix} {'; '.join(short_creators)} ({data['year']})"[:50]
            )
            ds.save()
        else:
            ds.title = f"{title_prefix} <authors> (<year>): <title>"
            ds.shorttitle = f"{shorttitle_prefix} <authors> (<year>)"
            ds.save()



class DataReviewAddFormView(DataSetAddFormView):
    """
    FormView to add a data review
    """
    form_class = DataReviewAddForm
    heading = _("Add a Data Review")
    btn_text = _("Add data review")
    ds_type = "DR"


class ExternalDataSetAddFormView(DataSetAddFormView):
    """
    FormView to add an external dataset
    """
    form_class = DataReviewAddForm
    heading = _("Add an External Dataset")
    btn_text = _("Add external dataset")
    ds_type = "EX"


@method_decorator(login_required, name="dispatch")
@method_decorator(dd_tou_accepted, name="dispatch")
@method_decorator(dd_user_verified, name="dispatch")
@method_decorator(dd_dataset_access, name="dispatch")
class DataSetEditFormView(UpdateView):
    """
    Prep area tab to edit dataset description
    """
    btn_text = _("Save Description")
    template_name = "core/_edit_form.html"

    def get_form_class(self):
        ds = self.get_object()
        # for reviews and external
        if ds.dataset_type == "DR" or ds.dataset_type == "EX":
            return DataReviewEditForm
        # for datasets
        return DataSetEditForm

    def get_context_data(self, **kwargs):
        ds = self.get_object()
        context = super().get_context_data(**kwargs)
        context["categories_all"] = Category.objects.all().order_by("name")
        context["btn_text"] = self.btn_text
        context["ds"] = ds
        context["target"] = "Description"
        return context

    def get_object(self):
        return get_object_or_404(DataSet, uuid=self.kwargs["ds_uuid"])

    def render_template(self, context):
        return render(
            self.request, self.template_name, context
        )

    def form_invalid(self, form):
        context = self.get_context_data()
        context["updated"] = False
        context["err"] = form.errors.as_data()
        return self.render_template(context)

    def form_valid(self, form):
        ds = form.save(commit=False)
        ds.uuid = self.kwargs["ds_uuid"]
        ds.save()
        form.save_m2m()
        main_category_slug = form.cleaned_data.get("main_category")
        categories = form.cleaned_data.get("categories")
        main_category = Category.objects.get(slug=main_category_slug)
        ds.dataset_management_object.main_category = main_category
        ds.dataset_management_object.categories.set(categories)
        ds.dataset_management_object.save()
        ds.save()
        context = self.get_context_data()
        context["updated"] = True

        # send an action
        action.send(
            self.request.user, verb="edited description of", target=ds, public=False
        )
        return self.render_template(context)
        # return super().form_valid(form)


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_datafiles(request, ds_uuid):
    """
    List datafiles
    """
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("manage-datafiles")
    next_url = request.session.get(str(ds_uuid) + "wp")

    return render(
        request,
        "dddatasets/prep/dataset_edit_datafiles.html",
        {"ds": ds, "manpage": manpage, "next_url": next_url},
    )


@method_decorator(login_required, name="dispatch")
@method_decorator(dd_tou_accepted, name="dispatch")
@method_decorator(dd_user_verified, name="dispatch")
@method_decorator(dd_dataset_access, name="dispatch")
class EditDataFileView(TemplateView):
    """
    Formview to add, edit, remove a datafile
    """
    btn_text = _("Save Description")
    template_name = "dddatasets/prep/_dataset_edit_datafile.html"

    def get_form_class(self, request, *args, **kwargs):
        ds = self.get_dataset(request, *args, **kwargs)
        if ds.dataset_type == "DR":
            return DataReviewFileUploadForm
        return DataFileUploadForm

    def get_next_url(self, request, *args, **kwargs):
        return self.request.session.get(str(self.kwargs["ds_uuid"]) + "wp")

    def get_dataset(self, request, *args, **kwargs):
        return get_object_or_404(DataSet, uuid=self.kwargs["ds_uuid"])

    def get_dataset_type(self, request, *args, **kwargs):
        ds = self.get_dataset(request, *args, **kwargs)
        return ds.dataset_type

    def delete(self, request, *args, **kwargs):
        ds = self.get_dataset(request, *args, **kwargs)
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            raise PermissionDenied

        df_uuid = request.DELETE["objectid"]
        datafile = DataFile.objects.get(uuid=df_uuid)
        datafile.delete()
        # needs a full http get to close the modal
        return redirect("dddatasets:prep_edit_datafiles", ds_uuid=self.kwargs["ds_uuid"])

    def get(self, request, *args, **kwargs):
        ds = self.get_dataset(request, *args, **kwargs)
        df_uuid = self.kwargs.get("df_uuid")
        form_class = self.get_form_class(request, *args, **kwargs)
        if df_uuid:
            # Edit
            datafile = DataFile.objects.get(uuid=df_uuid, dataset=ds)
            form = form_class(instance=datafile)
            #form = DataFileUploadForm(instance=datafile)
        else:
            # generate datafile uuid to add to form
            df_uuid = uuidlib.uuid4()
            form = form_class()

        context = self.get_context_data()
        context["form"] = form
        context["form_err"] = None
        context["updated"] = False
        context["ds_uuid"] = ds.uuid
        context["df_uuid"] = df_uuid
        context["user"] = request.user
        context["next_url"] = self.get_next_url(request, *args, **kwargs)
        context["ds_type"] = self.get_dataset_type(request)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        logger.debug("EditDataFileView post")
        ds = self.get_dataset(request, *args, **kwargs)
        df_uuid = self.kwargs["df_uuid"]
        logger.debug(f"EditDataFileView df_uuid {df_uuid}")
        form_class = self.get_form_class(request, *args, **kwargs)
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            # not raising permission denied to avoid intercooler page load
            return HttpResponse(_("Access denied"))

        if df_uuid:
            try:
                datafile = DataFile.objects.get(uuid=df_uuid, dataset=ds)
                logger.debug("EditDataFileView try")
            except DataFile.DoesNotExist:
                datafile = DataFile(uuid=df_uuid, dataset=ds)
                logger.debug("EditDataFileView except")

            form = form_class(request.POST, request.FILES, instance=datafile)
            context = self.get_context_data()
            context["form"] = form
            context["ds_uuid"] = ds.uuid
            context["df_uuid"] = df_uuid
            context["user"] = request.user
            context["next_url"] = self.get_next_url(request, *args, **kwargs)

            if form.is_valid():
                if request.FILES:
                    logger.debug("EditDataFileView if request.FILES")
                    orig_name = datafile.file.name
                    filePath = self.handle_uploaded_file(datafile.file, df_uuid)
                    datafile.file = filePath

                    if ds.dataset_type == "DR":
                        try:
                            old_datafile = ds.get_review_file()
                            old_datafile.delete()
                        except DataFile.DoesNotExist:
                            pass
                        datafile.data_file_type = "REV"
                        datafile.name = datafile.get_download_file_name()
                        datafile.save()
                    else:
                        datafile.data_file_type = form.cleaned_data["data_file_type"]
                        datafile.name = orig_name
                        datafile.save()
                context["form_err"] = None
                context["updated"] = True
                return self.render_to_response(context)
            else:
                context["updated"] = False
                return HttpResponse(
                    form.errors.as_text(),
                    status=500,
                )
        else:
            # POST should always have an df_uuid, trigger reload
            return edit_datafiles(request, ds_uuid=ds.uuid)

    def handle_uploaded_file(self, file_obj, df_uuid):
        # construct filePath using DATA_ROOT setting and datasets prefix
        filePath = settings.DATA_ROOT + os.sep + "datasets" + os.sep + str(df_uuid)
        with open(filePath, "ab") as destination:
            for chunk in file_obj.chunks():
                destination.write(chunk)
        return filePath

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context["target"] = "datafile"
        context["ictarget"] = "#ds-content"
        return context


# @login_required
# @dd_tou_accepted
# @dd_user_verified
# @dd_dataset_access
# def edit_datafile(request, ds_uuid, df_uuid=None):
#     """
#     Add, edit, remove datafiles
#     """
#     ds = get_object_or_404(DataSet, uuid=ds_uuid)
#     next_url = request.session.get(str(ds_uuid) + "wp")

#     # DELETE
#     if request.method == "DELETE":
#         if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
#             raise PermissionDenied

#         df_uuid = request.DELETE["objectid"]
#         datafile = DataFile.objects.get(uuid=df_uuid)
#         datafile.delete()
#         # TODO: needs a full http get to close the modal
#         return redirect("dddatasets:prep_edit_datafiles", ds_uuid=ds.uuid)

#     if request.method == "POST":
#         if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
#             # not raising permission denied to avoid intercooler page load
#             return HttpResponse(_("Access denied"))

#         if df_uuid:
#             # Edit
#             logger.debug(f"in if df_uuid")
#             try:
#                 datafile = DataFile.objects.get(uuid=df_uuid, dataset=ds)
#             except DataFile.DoesNotExist:
#                 datafile = DataFile(uuid=df_uuid, dataset=ds)

#             form = DataFileUploadForm(request.POST, request.FILES, instance=datafile)
#             if form.is_valid():
#                 if request.FILES:
#                     orig_name = datafile.file.name
#                     filePath = handle_uploaded_file(datafile.file, df_uuid)
#                     datafile.file = filePath
#                     datafile.name = orig_name

#                 datafile.data_file_type = form.cleaned_data["data_file_type"]
#                 datafile.save()
#                 form_err = None
#                 updated = True
#             else:
#                 updated = False
#                 return HttpResponse(
#                     form.errors.as_text(),
#                     status=500,
#                 )
#         else:
#             # POST should always have an df_uuid
#             return edit_datafiles(request, ds_uuid=ds_uuid)
#     else:
#         # Get
#         if df_uuid:
#             # Edit
#             datafile = DataFile.objects.get(uuid=df_uuid, dataset=ds)
#             form = DataFileUploadForm(instance=datafile)
#         else:
#             # generate datafile uuid to add to form
#             df_uuid = uuidlib.uuid4()
#             form = DataFileUploadForm()
#             print(form.fields['data_file_type'].__dict__)

#         updated = False
#         form_err = None

#     return render(
#         request,
#         "dddatasets/prep/_dataset_edit_datafile.html",
#         {
#             "target": "datafile",
#             "ictarget": "#ds-content",
#             "form": form,
#             "user": request.user,
#             "form_err": form_err,
#             "updated": updated,
#             "ds_uuid": ds_uuid,
#             "df_uuid": df_uuid,
#             "next_url": next_url,
#         },
#     )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_metadata(request, ds_uuid):
    """
    Prep area tab to edit metadata
    """
    updated = False
    target = "Dataset Metadata"
    manpage = get_man_page("edit-dataset-metadata")
    ds = get_object_or_404(DataSet, uuid=ds_uuid)

    ds_creators = DataSetCreator.objects.filter(dataset=ds)
    next_url = request.session.get(str(ds_uuid) + "wp")

    if ds_creators.count() == 0:
        extra = 1
    else:
        extra = 0

    DataSetCreatorFormSet = inlineformset_factory(
        DataSet,
        DataSetCreator,
        fields=["name", "creator_type", "url", "first_name", "list_position"],
        extra=extra,
        can_delete=True,
    )

    if request.method == "POST":
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            return HttpResponse(_("Access denied"))

        formset = DataSetCreatorFormSet(request.POST, queryset=ds_creators, instance=ds)
        helper = DataSetCreatorFormSetHelper()
        form = DataSetForm(request.POST, instance=ds)
        dsmo = ds.dataset_management_object
        if form.is_valid():
            ds = form.save(commit=False)
            ds.dataset_management_object = dsmo
            ds.save()
            form.save_m2m()
            err = None
            updated = True

            if formset.is_valid():
                formset.save()
            else:
                err = formset.errors
            action.send(
                request.user, verb="edited metadata of", target=ds, public=False
            )
            if next_url:
                return redirect(next_url)

        else:
            err = form.errors.as_data()

    else:
        err = None
    formset = DataSetCreatorFormSet(queryset=ds_creators, instance=ds)
    helper = DataSetCreatorFormSetHelper()
    form = DataSetForm(instance=ds)

    help_texts = get_help_texts(
        "edit-metadata"
    )  # needed to populate creators help in form

    return render(
        request,
        "dddatasets/prep/dataset_edit_metadata.html",
        {
            "ds": ds,
            "formset": formset,
            "helper": helper,
            "form": form,
            "user": request.user,
            "updated": updated,
            "err": err,
            "target": target,
            "manpage": manpage,
            "help_texts": help_texts,
            "next_url": next_url,
        },
    )


"""
@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_detail(request, ds_uuid):
    logger.debug("HERE 1")
    updated = False
    target = "Dataset description"
    ds = get_object_or_404(DataSet, uuid=ds_uuid)

    ds_uuid = ds.uuid
    categories_all = Category.objects.all().order_by("name")


    if request.method == "POST":
        logger.debug("HERE 2")
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            return response403("Access denied")

        if ds.dataset_type == "DR":
            # data review
            form = DataReviewEditForm(request.POST, request.FILES, instance=ds)
            logger.debug("REVIEW")
        else:
            # data set
            form = DataSetEditForm(request.POST, request.FILES, instance=ds)

        if form.is_valid():
            ds = form.save(commit=False)
            ds.uuid = ds_uuid
            ds.save()
            form.save_m2m()
            main_category_slug = form.cleaned_data.get("main_category")
            categories = form.cleaned_data.get("categories")
            main_category = Category.objects.get(slug=main_category_slug)
            ds.dataset_management_object.main_category = main_category
            ds.dataset_management_object.categories.set(categories)
            ds.dataset_management_object.save()
            ds.save()

            err = None
            updated = True
            # send an action
            action.send(
                request.user, verb="edited description of", target=ds, public=False
            )
        else:
            err = form.errors.as_data()

    else:
        # user = request.user
        err = None
        if ds.dataset_type == "DR":
            # data review
            form = DataReviewEditForm(instance=ds)
        else:
            # data set
            form = DataSetEditForm(instance=ds)

    return render(
        request,
        "core/_edit_form.html",
        {
            "ds": ds,
            "form": form,
            "user": request.user,
            "updated": updated,
            "err": err,
            "target": target,
            "categories_all": categories_all,
            "btn_text": _("Save Description"),
        },
    )
"""


""" @login_required
@dd_tou_accepted
def add(request, dataset_type=None):
    if request.method == "POST":
        ds = DataSet()
        logger.debug(f"POST dataset_type: {dataset_type}")
        if dataset_type == "datareview":
            # data review
            form = DataReviewAddForm(request.POST, request.FILES, instance=ds)
        else:
            # data set
            form = DataSetEditForm(request.POST, request.FILES, instance=ds)
        if form.is_valid():
            ds = form.save(commit=False)
            ds.owner = request.user
            if dataset_type == "datareview":
                link_url = form.cleaned_data["link"]
                ds.dataset_type = "DR"
                ds.save()
                ds.data_access = "OA"
                # set license to ODbL TODO: move to settings
                license_name = 'Attribution and Share Alike: Open Data Commons Open Database License (ODbL) ("share-alike")'
                ds.license = License.objects.get(license_name=license_name)
                standard_license = LicensePage.objects.get(name=license_name)
                ds.license.license_type = License.STANDARD
                ds.license.standard_license = standard_license
                ds.license.save()

                # set request.user as review author
                creators = list()
                dscreator = DataSetCreator(
                    dataset=ds,
                    name=request.user.last_name,
                    first_name=request.user.first_name,
                )
                dscreator.save()
                if "doi.org" in link_url:
                    # extract metadata if link_url points to doi.org
                    data = get_data_from_doi_url(link_url)
                    short_creators = list()
                    for entry in data["creators"]:
                        short_creators.append(entry.get("name"))
                        if entry.get("first_name"):
                            creators.append(
                                f"{entry.get('name')}, {entry.get('first_name')}"
                            )
                        else:
                            creators.append(entry.get("name"))
                    ds.title = f"Review of {'; '.join(creators)} ({data['year']}): {data['title']}"
                    ds.shorttitle = (
                        f"Review: {'; '.join(short_creators)} ({data['year']})"[:50]
                    )
            try:
                ds.save()
            except LicensePage.DoesNotExist as e:
                messages.error(request, e)
                logger.error(e)
                raise Http404(e) from e
            form.save_m2m()
            main_category_slug = form.cleaned_data.get("main_category")
            try:
                main_category = Category.objects.get(slug=main_category_slug)
                ds.dataset_management_object.main_category = main_category
                ds.dataset_management_object.save()
                return redirect("dddatasets:prep_edit", ds_uuid=ds.uuid)
            except:
                messages.warning(request, _("Please add a main category"))
        else:
            messages.warning(request, form.errors.as_data())
    else:
        # GET
        if dataset_type == "datareview":
            logger.debug("GET datareview")
            # data review
            form = DataReviewAddForm()
        else:
            # data set
            form = DataSetEditForm()
    if dataset_type == "datareview":
        btn_text = _("Add Data Review")
        heading = _("Add new data review")
    else:
        btn_text = _("Add Dataset")
        heading = _("Add new dataset")
    return render(
        request,
        "dddatasets/prep/dataset_add.html",
        {
            "manpage": get_man_page("data-upload"),
            "user": request.user,
            "form": form,
            "categories_all": Category.objects.all().order_by("name"),
            "btn_text": btn_text,
            "heading": heading,
        },
    ) """

@login_required
@dd_tou_accepted
@dd_user_verified
def retry_zenodo(request, ds_uuid):
    """
    Function to retry publish a published dataset on zenodo
    in case of prior fails
    """
    if request.method == "POST":
        ds = DataSet.objects.get(uuid=ds_uuid)
        logger.debug(ds.title)
        logger.debug(ds.version)
        logger.debug(ds.uuid)
        pub_pro = ds.datasetpublishingprocess
        if pub_pro:
            success, message = pub_pro.publish_zenodo()
            logger.debug(success)
            logger.debug(message)
            if success:
                messages.success(
                    request,
                    message,
                )
                logger.debug("Zenodo pubblishing success")
                pub_pro.delete()
            else:
                messages.success(
                    request,
                    message,
                )
                logger.debug("Zenodo pubblishing error")
    return redirect("dddatasets:prep_edit_versions", ds_uuid=ds.uuid)



@login_required
@dd_tou_accepted
@dd_user_verified
def remove(request):
    """
    Function to delete a dataset which uses ic-delete-from and
    the DELETE http-method (POST, but translated by django-intercooler-helpers)
    and form data to send the uuid
    """
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("dddatasets:prep_listing")
    if request.method == "DELETE":
        ds_uuid = request.DELETE["objectid"]
        ds = get_object_or_404(DataSet, uuid=ds_uuid)
        if ds.owner != request.user:
            raise PermissionDenied()

        if ds.published:
            messages.error(
                request,
                _("Deletion of published datasets is not possible"),
            )
            return redirect("dddatasets:prep_edit_versions", ds_uuid=ds.uuid)

        dsmo = ds.dataset_management_object
        ds.delete()
        # if ds is last dataset delete dsmo
        if dsmo.get_all_datasets().count() == 0:
            messages.success(request, _("Dataset deleted."))
            dsmo.delete()
            return redirect("dddatasets:prep_listing")
        else:
            messages.success(request, _("Dataset version deleted."))
            topversion = dsmo.get_top_version_dataset()
            return redirect("dddatasets:prep_edit_versions", ds_uuid=topversion.uuid)
    else:
        # redirect Requests other than DELETE
        return redirect("dddatasets:prep_listing")


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def refresh_header(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
        return response403("Access denied")
    return render(
        request,
        "dddatasets/prep/_dataset_refresh_header.html",
        {"ds": ds},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit(request, ds_uuid):
    """
    Prep area tab to edit dataset description
    TODO: can probably be merged with edit_detail
    """
    ds = get_object_or_404(DataSet, uuid=ds_uuid)

    context = dict()
    context["help_texts"] = get_help_texts("edit-dataset-description-add-tags")
    context["ds"] = ds

    return render(request, "dddatasets/prep/edit.html", context)


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_image(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    err = None

    if request.method == "POST":
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            return response403("Access denied")
        form = DataSetImageForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data["image"]:
                ds.image = form.cleaned_data["image"]
            ds.save()
            action.send(request.user, verb="edited image of", target=ds, public=False)
            return HttpResponse("upload successful")
        else:
            err = form.errors.as_data()

    response = render(
        request,
        "dddatasets/prep/_dataset_image.html",
        {"ds": ds, "err": err},
    )
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def country_tags_list(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    all_countries = Country.objects.filter(public=True)
    countries = list()
    for ac in all_countries:
        if ac in ds.get_countries():
            countries.append((ac, True))
        else:
            countries.append((ac, False))

    return render(
        request,
        "dddatasets/prep/_dataset_country_tags_list.html",
        {"ds": ds, "countries": countries},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def object_tags_list(request, object_type):
    if object_type == "language":
        qs = LanguageTagged.objects.all()
    if object_type == "keyword":
        qs = KeywordTagged.objects.all()
    if object_type == "disciplines":
        qs = DisciplinesTagged.objects.all()
    if object_type == "methods_of_data_collection":
        qs = CollectionMethodsTagged.objects.all()
    if object_type == "methods_of_data_analysis":
        qs = AnalysisMethodsTagged.objects.all()
    query = None

    if query:
        return qs.filter(name__istartswith=query)
    else:
        return render(request, "dddatasets/prep/_object_tags_list.html", {"qs": qs})


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def country_tags_edit(request, ds_uuid):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("dddatasets:prep_edit", ds_uuid)
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
        return response403("Access denied")

    if request.method == "POST":
        slug = request.POST.get("country")
        country = Country.objects.get(slug=slug)
        if country in ds.get_countries():
            ds.countries.remove(country)
        else:
            ds.countries.add(country)
        ds.save()

        return country_tags_list(request, ds_uuid=ds_uuid)


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def tag_add(request, ds_uuid, object_type):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("dddatasets:prep_edit", ds_uuid)
    ds = get_object_or_404(DataSet, uuid=ds_uuid)

    if request.method == "POST":
        if check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            tag = request.POST.get("%s-tag" % (object_type,), "").title()
            if len(tag) > 0:
                if object_type == "language":
                    ds.languages_of_data.add(tag)
                elif object_type == "keyword":
                    if tag.find(",") > -1:
                        for elem in tag.split(","):
                            if elem != "":
                                ds.keywords.add(elem.strip())
                    elif tag.find(";") > -1:
                        for elem in tag.split(";"):
                            if elem != "":
                                ds.keywords.add(elem.strip())
                    else:
                        if tag != "":
                            ds.keywords.add(tag.strip())
                elif object_type == "disciplines":
                    ds.disciplines.add(tag)
                elif object_type == "methods_of_data_collection":
                    ds.methods_of_data_collection.add(tag)
                elif object_type == "methods_of_data_analysis":
                    ds.methods_of_data_analysis.add(tag)

    if object_type == "language":
        tags = ds.get_language_tags()
    elif object_type == "keyword":
        tags = ds.get_keyword_tags()
    elif object_type == "disciplines":
        tags = ds.get_disciplines_tags()
    elif object_type == "methods_of_data_collection":
        tags = ds.get_methods_of_data_collection_tags()
    elif object_type == "methods_of_data_analysis":
        tags = ds.get_methods_of_data_analysis_tags()
    else:
        tags = None

    return render(
        request,
        "dddatasets/prep/_tag_add_control.html",
        {"user": request.user, "ds": ds, "tags": tags, "object": object_type},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def tag_remove(request, ds_uuid, object_type):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("dddatasets:prep_edit", ds_uuid)
    ds = get_object_or_404(DataSet, uuid=ds_uuid)

    if request.method == "POST":
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            return response403("Access denied")
        tag_pk = request.POST.get("%s-tag-remove" % (object_type,), "")
        if len(tag_pk) > 0:
            if object_type == "language":
                tag = LanguageTagged.objects.get(id=tag_pk)
                if check_perms("edit_dsmo", request.user, ds.dataset_management_object):
                    ds.languages_of_data.remove(tag)
                tags = ds.get_language_tags()
            if object_type == "keyword":
                tag = KeywordTagged.objects.get(id=tag_pk)
                if check_perms("edit_dsmo", request.user, ds.dataset_management_object):
                    ds.keywords.remove(tag)
                tags = ds.get_keyword_tags()
            if object_type == "disciplines":
                tag = DisciplinesTagged.objects.get(id=tag_pk)
                if check_perms("edit_dsmo", request.user, ds.dataset_management_object):
                    ds.disciplines.remove(tag)
                tags = ds.get_disciplines_tags()
            if object_type == "methods_of_data_collection":
                tag = CollectionMethodsTagged.objects.get(id=tag_pk)
                if check_perms("edit_dsmo", request.user, ds.dataset_management_object):
                    ds.methods_of_data_collection.remove(tag)
                tags = ds.get_methods_of_data_collection_tags()
            if object_type == "methods_of_data_analysis":
                tag = AnalysisMethodsTagged.objects.get(id=tag_pk)
                if check_perms("edit_dsmo", request.user, ds.dataset_management_object):
                    ds.methods_of_data_analysis.remove(tag)
                tags = ds.get_methods_of_data_analysis_tags()

    return render(
        request,
        "dddatasets/prep/_tag_add_control.html",
        {"user": request.user, "ds": ds, "tags": tags, "object": object_type},
    )




@login_required
@dd_tou_accepted
@dd_user_verified
def edit_collaboration(request, ds_uuid):
    """
    Prep area tab to edit collaborators
    """
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        raise PermissionDenied
    dsmo = ds.dataset_management_object
    manpage = get_man_page("edit-collaboration")

    user_perms = dsmo.get_dsmo_users_with_perms()

    control_args = {
        "user_all": dsmo.get_dsmo_users(),
        "user_admin": user_perms["admin"],
        "user_edit": user_perms["edit"],
        "user_view": user_perms["view"],
        "groups": dsmo.groups.all(),
        "div_id": "dataset-users",
        "input_id": "dataset-user",
        "url_object_search": "ddusers:user_search",
        "text": "Dataset Users",
    }

    return render(
        request,
        "dddatasets/prep/dataset_edit_collaboration.html",
        {"ds": ds, "manpage": manpage, "control_args": control_args},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_access_user_search(request, ds_uuid):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("dddatasets:prep_edit_publish_access", ds_uuid)
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))

    term = request.GET.get("q")
    qs = user_search(term)

    search_type = "pubaccess_search"

    return render(
        request,
        "ddusers/_search_results.html",
        {"object_list": qs, "ds": ds, "search_type": search_type},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_access_user(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, dsmo)

    if request.method == "POST":
        if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
            return HttpResponse(_("Access denied"))
        userid = request.POST["userid"]
        user = User.objects.get(uuid=userid)
        perm_code = request.POST["perm"]
        feed = request.POST.get("requestsrc")

        if perm_code == "clr":
            logger.debug("clearing perms for %s", user)
            ds.clear_user_permissions(user)
            ds.save()

            action.send(
                request.user,
                verb="revoked access",
                action_object=ds,
                target=user,
                public=False,
            )

            notification = Notification()
            notification.text = "Access to datasaet has been revoked"
            notification.owner = request.user
            notification.recipient = user
            notification.notification_type = Notification.ACCESS_REQUEST
            notification.content_type = ContentType.objects.get_for_model(ds)
            notification.object_id = ds.id
            notification.save()
            message = _(
                'Your access to the dataset "{}" at {} has been revoked by the data owner.'.format(
                    ds, ds.get_absolute_url()
                )
            )

        elif perm_code == "acc":
            access_request = ds.get_access_request(user)
            logger.debug("assigning perms for %s", user)
            ds.assign_user_permissions(user, "ra_view_dataset")
            ds.save()
            action.send(
                request.user,
                verb="granted access",
                action_object=ds,
                target=user,
                public=False,
            )

            notification = Notification()
            notification.text = "Access to dataset has been granted"
            notification.owner = request.user
            notification.recipient = user
            notification.notification_type = Notification.ACCESS_REQUEST
            notification.content_type = ContentType.objects.get_for_model(ds)
            notification.object_id = ds.id
            if access_request:
                notification.parent = access_request.notification
            notification.save()
            message = _(
                'You have been granted access to the dataset "{}" at {}.'.format(
                    ds, ds.get_absolute_url()
                )
            )

            if access_request:
                access_request.delete()

        else:
            logger.error("requested non existing perm_code: %s", perm_code)

        text = _("Access to dataset {}".format(ds))
        subject = text

        # send email to owner and user
        email_to = [user.email]
        email_to.append(ds.owner.get_email())
        send_update_email(subject, message, email_to)
    if feed:
        return user_notifications(request)
    else:
        return edit_publish_access(request, ds_uuid=ds_uuid)


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_collaboration_user_search(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))

    term = request.GET.get("q")
    qs = user_search(term)
    search_type = "collaboration_search"

    return render(
        request,
        "ddusers/_search_results.html",
        {"object_list": qs, "ds": ds, "search_type": search_type},
    )


def collab_notification(ds, sender, target, permission):
    if permission == "clear":
        verb = _(f"revoked access")
        email_subject = _(f'Access revoked for dataset "{ds}"')
        message = _(
            f'Your access to the dataset "{ds}" at {ds.get_absolute_url_prep()} has been revoked.'
        )
    else:
        verb = _(f"granted {permission} access")
        email_subject = _(f'Access granted for dataset "{ds}"')
        message = _(
            f'You have been granted {permission} access to the dataset "{ds}" at {ds.get_absolute_url_prep()}.'
        )
    action.send(
        sender,
        verb=verb,
        action_object=ds,
        target=target,
        public=False,
    )
    notification = Notification(
        owner=sender,
        content_type=ContentType.objects.get_for_model(ds),
        object_id=ds.id,
        text=message,
        notification_type=Notification.PRIVATE,
        recipient=target,
    )
    notification.save()
    # send email to user
    email_to = [target.get_email()]
    send_update_email(email_subject, message, email_to)


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_collaboration_user(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    dsmo = ds.dataset_management_object
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, dsmo)

    if request.method == "POST":
        userid = request.POST["userid"]
        user = User.objects.get(uuid=userid)
        perm_code = request.POST["perm"]

        if perm_code == "clr":
            dsmo.clear_user_permissions(user)
            collab_notification(ds, request.user, user, "clear")
        elif perm_code == "v":
            dsmo.assign_user_permissions(user, "view")
            collab_notification(ds, request.user, user, "view")
        elif perm_code == "e":
            dsmo.assign_user_permissions(user, "edit")
            collab_notification(ds, request.user, user, "edit")
        elif perm_code == "a":
            dsmo.assign_user_permissions(user, "admin")
            collab_notification(ds, request.user, user, "admin")
        else:
            logger.error("requested non existing perm_code: %s", perm_code)

    return edit_collaboration(request, ds_uuid=ds_uuid)


def ownership_transfer_user_search_helper(request, ds):
    term = request.GET.get("q")
    qs = user_search(term)
    search_type = "ownership_search"

    return render(
        request,
        "ddusers/_search_results.html",
        {"object_list": qs, "ds": ds, "search_type": search_type},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_ownership_transfer(request, ds_uuid, action):
    owner_actions = ["search", "request", "cancel"]
    new_owner_actions = ["accept", "reject"]
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    # action for prev owner
    if action in owner_actions:
        if (
            not check_perms("admin_dsmo", request.user, ds.dataset_management_object)
            and not request.user == ds.owner
        ):
            return HttpResponse(_("Access denied"))

        if action == "search":
            return ownership_transfer_user_search_helper(request, ds)
        elif action == "request":
            if request.method == "POST":
                newuserid = request.POST["newuserid"]
                newuser = User.objects.get(uuid=newuserid)
                dsot = DataSetOwnerTransfer(
                    dataset=ds, new_owner=newuser, prev_owner_request=True
                )
                dsot.save()
        elif action == "cancel":
            if request.user == ds.owner:
                dsot = ds.dataset_owner_transfer_dataset
                dsot.send_notification(
                    f'The Ownership transfer request has been canceled for dataset "{ds}" by {ds.owner}',
                    dsot.new_owner,
                    ds.owner,
                )
                dsot.delete()
        else:
            return HttpResponse(_("Access denied"))
        return edit_collaboration(
            request, ds_uuid=ds_uuid
        )  # keyword arguments needed for direct call of views because of decorators
    # actions for new owner
    elif action in new_owner_actions:
        dsot = ds.dataset_owner_transfer_dataset
        if not request.user == dsot.new_owner:
            return HttpResponse(_("Access denied"))
        if action == "accept":
            # saving dsot model with new_owner_accept set to True sets
            # dataset to new owner and deletes the dsot
            dsot.new_owner_accept = True
            dsot.save()
            return redirect(
                "dddatasets:prep_edit_collaboration",
                ds_uuid=ds.uuid,
            )
        elif action == "reject":
            dsot.send_notification(
                f'The Ownership transfer request for dataset "{ds}" has been rejected by {dsot.new_owner}',
                ds.owner,
                dsot.new_owner,
            )
            dsot.delete()
            return user_notifications(request)
        else:
            return HttpResponse(_("Access denied"))

    else:
        return HttpResponse(_("Access denied"))


@login_required
@dd_tou_accepted
@dd_user_verified
def comments(request, ds_uuid):
    """
    calls dataset_comments for "prep" comments
    """
    response = dataset_comments(request, ds_uuid, "prep")
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_comment(request, ds_uuid, co_uuid=None):
    """
    calls dataset_edit_comment for a "prep" comment
    """
    response = dataset_edit_comment(request, ds_uuid, "prep", co_uuid)
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_comment_reply(request, ds_uuid, co_uuid):
    """
    calls dataset_edit_comment_reply for a "prep" comment
    """
    response = dataset_edit_comment(request, ds_uuid, "prep", co_uuid, reply=True)
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publication(request, ds_uuid, ds_pub_uuid=None):
    """
    Add, edit, remove publications
    """
    ds = get_object_or_404(DataSet, uuid=ds_uuid)

    # DELETE, remove DatasetPublication object, not Publication
    if request.method == "DELETE":
        check_perms_403("edit_dsmo", request.user, ds.dataset_management_object)
        ds_pub_uuid = request.DELETE["objectid"]
        ds_pub = DataSetPublication.objects.get(uuid=ds_pub_uuid)
        ds_pub.delete()
        return redirect("dddatasets:prep_edit_publications", ds_uuid=ds.uuid)

    if ds_pub_uuid:
        ds_pub = DataSetPublication.objects.get(uuid=ds_pub_uuid)
        publication = ds_pub.publication
    else:
        ds_pub = DataSetPublication()
        publication = Publication()
        ds_pub.dataset = ds

    if request.method == "POST":
        check_perms_403("edit_dsmo", request.user, ds.dataset_management_object)
        if ds_pub_uuid:
            # Edit
            form = PublicationForm(
                request.POST, instance=publication, prefix="publication"
            )
            sub_form = DataSetPublicationForm(
                request.POST, instance=ds_pub, prefix="ds_pub"
            )
            if form.is_valid() and sub_form.is_valid():
                publication = form.save()
                ds_pub = sub_form.save()
                return edit_publications(request, ds_uuid=ds_uuid)
        else:
            # Add
            form = PublicationForm(
                request.POST, instance=publication, prefix="publication"
            )
            sub_form = DataSetPublicationForm(
                request.POST, instance=ds_pub, prefix="ds_pub"
            )
            if form.is_valid() and sub_form.is_valid():
                ds_pub = sub_form.save(commit=False)
                publication = form.save()
                ds_pub.publication = publication
                ds_pub.save()
                updated = True
                form_err = None
                return edit_publications(request, ds_uuid=ds_uuid)
            else:
                updated = False
                form_err = form.errors.as_data()
    else:
        # Get
        if ds_pub_uuid:
            # Edit
            form = PublicationForm(instance=publication, prefix="publication")
            sub_form = DataSetPublicationForm(instance=ds_pub, prefix="ds_pub")
        else:
            form = PublicationForm(prefix="publication")
            sub_form = DataSetPublicationForm(prefix="ds_pub")
        updated = False
        form_err = None
    return render(
        request,
        "core/_edit_form.html",
        {
            "target": "publication",
            "ictarget": "#ds-content",
            "form": form,
            "sub_form": sub_form,
            "user": request.user,
            "form_err": form_err,
            "updated": updated,
            "next_url": request.session.get(str(ds_uuid) + "wp"),
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publications(request, ds_uuid):
    """Display a list of :class:`discuss_data.dddatasets.Publications` connected to a :class:`discuss_data.dddatasets.DataSet`.

    **Context**

    ``ds``
        An instance of :class:`discuss_data.dddatasets.DataSet`.

    **Template:**

    :file:`dddatasets/prep/dataset_edit_publications.html`
    """
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    # in review only curators can access
    if ds.pub_request_pending():
        if request.user not in ds.get_curators():
            raise PermissionDenied
    else:
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            raise PermissionDenied
    manpage = get_man_page("edit-related-publications")
    control_args = {
        "title": "Publications related to this Data Set",
        "type": "publication",
        "edit_url": "dddatasets:prep_edit_publication",
        "add_url": "dddatasets:prep_edit_publication",
        "list_url": "dddatasets:prep_edit_publications",
        "delete_url": "dddatasets:prep_edit_publication",
        "outer_panel": True,
        "add_text": "Add new publication related to this data set...",
    }

    return render(
        request,
        "dddatasets/prep/dataset_edit_publications.html",
        {
            "ds": ds,
            "control_args": control_args,
            "manpage": manpage,
            "next_url": request.session.get(str(ds_uuid) + "wp"),
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_archive_search(request, ds_uuid, as_uuid=None):
    logger.debug("EDIT ARCHIVE SEARCH AS: %s, DS: %s", as_uuid, ds_uuid)
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        raise PermissionDenied
    try:
        archivalsignature = ArchivalSignature.objects.get(uuid=as_uuid)
    except ArchivalSignature.DoesNotExist:
        archivalsignature = None
    term = request.GET.get("q")
    logger.debug("Archive Search Term: %s", term)
    qs = Archive.objects.filter(Q(short_name__icontains=term) | Q(name__icontains=term))
    if qs:
        notfound = None
    else:
        notfound = f'No archive with matches the query "{ term }" has been found. Search again or add a new archive to the database'

    response = render(
        request,
        "dddatasets/prep/_archive_search_list.html",
        {
            "object_list": qs,
            "ds": ds,
            "archivalsignature": archivalsignature,
            "card_type": "search",
            "notfound": notfound,
        },
    )
    if qs.count() == 0:
        response["X-IC-Trigger"] = "archive-not-found"
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_archive_add(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        raise PermissionDenied
    new = True
    if request.method == "POST":
        archive_id = request.POST["archive_id"]
        archive = Archive.objects.get(id=archive_id)
        form = ArchivalSignatureForm(
            initial={
                "archive-id": archive_id,
                "archive_id": archive.id,
                "archive_name": archive.short_name,
            }
        )
        logger.debug("form %s", form)
        archive_form = ArchiveForm()
    return render(
        request,
        "dddatasets/prep/_archive_add.html",
        {
            "archive": archive,
            "ds": ds,
            "new": new,
            "form": form,
            "archive_form": archive_form,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def new_archive_add(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    check_perms_403("edit_dsmo", request.user, ds.dataset_management_object)
    if request.method == "POST":
        form = ArchiveForm(request.POST)
        form_err = None
        if form.is_valid():
            archive = form.save()
            return redirect("dddatasets:prep_edit_archivalsignatures", ds_uuid=ds.uuid)
        else:
            form_err = form.errors.as_data()
    else:
        # get
        form = ArchiveForm()
    return render(
        request,
        "dddatasets/prep/_archive_add.html",
        {"archive": archive, "form": form, "form_err": form_err},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_archivalsignatures(request, ds_uuid):
    """Display a list of :class:`discuss_data.dddatasets.ArchivalSignature` connected to a :class:`discuss_data.dddatasets.DataSet`.

    **Context**

    ``ds``
        An instance of :class:`discuss_data.dddatasets.DataSet`.

    **Template:**

    :file:`dddatasets/prep/dataset_edit_archivalsignature.html`
    """
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    archive_form = ArchiveForm()

    manpage = get_man_page("edit-related-archivalsignatures")
    control_args = {
        "title": "Archival Record Identifiers related to this Data Set",
        "type": "archivalsignature",
        "edit_url": "dddatasets:prep_edit_archivalsignature",
        "add_url": "dddatasets:prep_edit_archivalsignature",
        "list_url": "dddatasets:prep_edit_archivalsignatures",
        "delete_url": "dddatasets:prep_edit_archivalsignature",
        "outer_panel": True,
        "add_text": "Add new archival signature related to this data set...",
    }

    return render(
        request,
        "dddatasets/prep/dataset_edit_archivalsignatures.html",
        {
            "ds": ds,
            "archive_form": archive_form,
            "control_args": control_args,
            "manpage": manpage,
            "next_url": request.session.get(str(ds_uuid) + "wp"),
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_archivalsignature_archive(request, ds_uuid, as_uuid=None):
    logger.debug("Calling AF")
    return edit_archivalsignature(request, ds_uuid, as_uuid=None, archive_form=True)


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_archivalsignature(request, ds_uuid, as_uuid=None, archive_form=None):
    """
    Add, edit, remove archivalsignatures
    """
    logger.debug("AF %s", archive_form)
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    logger.debug("Dataset: %s, %s", ds, ds.id)

    form_err = None
    updated = False
    # DELETE
    if request.method == "DELETE":
        check_perms_403("edit_dsmo", request.user, ds.dataset_management_object)
        as_uuid = request.DELETE["objectid"]
        archivalsignature = ArchivalSignature.objects.get(uuid=as_uuid)
        archivalsignature.delete()
        return redirect("dddatasets:prep_edit_archivalsignatures", ds_uuid=ds.uuid)

    if as_uuid:
        logger.debug("AS found")
        archivalsignature = ArchivalSignature.objects.get(uuid=as_uuid)
    else:
        archivalsignature = ArchivalSignature()
        archivalsignature.dataset = ds
    add_archive_form_err = None

    if request.method == "POST":
        # Add
        check_perms_403("edit_dsmo", request.user, ds.dataset_management_object)
        form = ArchivalSignatureForm(request.POST)
        if form.is_valid():
            logger.debug("Add valid")
            logger.debug("form data: %s", form.cleaned_data)
            archive_id = form.cleaned_data.get("archive_id")
            archive = Archive.objects.get(id=archive_id)
            archivalsignature = form.save(commit=False)
            archivalsignature.archive = archive
            archivalsignature.dataset = ds
            archivalsignature.save()
            logger.debug("AS %s", archivalsignature.__dict__)
            updated = True
            form_err = None
            return edit_archivalsignatures(request, ds_uuid=ds_uuid)
        else:
            updated = False
            form_err = form.errors.as_data()
            logger.debug("Not valid %s"), form_err

    else:
        # Get
        form = ArchivalSignatureForm()
        updated = False
        form_err = None
    return render(
        request,
        "dddatasets/prep/_archivalsignature_edit.html",
        {
            "target": "archivalsignature",
            "ictarget": "#ds-content",
            "form": form,
            "user": request.user,
            "form_err": form_err,
            "updated": updated,
            "next_url": request.session.get(str(ds_uuid) + "wp"),
            "ds": ds,
            "archivalsignature": archivalsignature,
            "archive_form": archive_form,
            "add_archive_form_err": add_archive_form_err,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def dsa_pdf(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    check_perms_403("edit_dsmo", request.user, ds.dataset_management_object)
    # Creating http response
    response = FileResponse(ds.dsa.pdf)
    response["Content-Disposition"] = 'inline; filename="{}"'.format(ds.dsa.pdf.name)
    response["Content-Transfer-Encoding"] = "binary"
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_dsa(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    check_perms_403("admin_dsmo", request.user, ds.dataset_management_object)

    if not ds.dsa:
        ds.dsa = DataSubmissionAgreement()

    if request.method == "POST":
        form = DataSubmissionAgreementForm(request.POST, instance=ds.dsa)
        if form.is_valid():
            ds.dsa = form.save()
            ds.save()
            updated = True
            form_err = None
            ds.generate_dsa()
            return edit_versions(request, ds_uuid=ds_uuid)
        else:
            updated = False
            form_err = form.errors.as_data()
    else:
        # GET
        form = DataSubmissionAgreementForm(instance=ds.dsa)
        updated = False
        form_err = None

    return render(
        request,
        "core/_edit_form.html",
        {
            "target": "data submission agreement",
            "ictarget": "#ds-content",
            "form": form,
            "user": request.user,
            "form_err": form_err,
            "updated": updated,
            "next_url": request.session.get(str(ds_uuid) + "wp"),
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_versions(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    dsmo = ds.dataset_management_object

    if ds.pub_request_pending() and request.user not in ds.get_curators():
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            return handler403(request)

    if request.user == dsmo.owner:
        publish_right = True
    else:
        publish_right = False

    manpage = get_man_page("edit-dataset-versions")
    manpage_dsa = get_man_page("data-submission-agreement")

    control_args = {
        "publish_right": publish_right,
        "title": "Files",
        "type": "datafile",
        "outer_panel": False,
        "edit_url": "dddatasets:prep_edit",
        "view_url": "dddatasets:detail",
        "add_url": "dddatasets:prep_edit_versions_add",
        "list_url": "dddatasets:prep_edit_versions",
        "delete_url": "dddatasets:prep_remove",
        "retry_zenodo_url": "dddatasets:prep_retry_zenodo",
        "object_id": ds.uuid,
        "add_text": "Add new version...",
        "object_type": "version",
        "object_text": "Add version",
        "objects": ds.get_versions(),
    }

    return render(
        request,
        "dddatasets/prep/dataset_edit_versions.html",
        {
            "ds": ds,
            "control_args": control_args,
            "manpage": manpage,
            "manpage_dsa": manpage_dsa,
            "next_url": request.session.get(str(ds_uuid) + "wp"),
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_versions_add(request, ds_uuid):
    # TODO: this should be done via POST
    # user = User.objects.get(id=request.user.id)
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    logger.debug("creating new dataset version, checking permissions")
    if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
        raise PermissionDenied

    logger.debug("creating new dataset version")
    dsnv = ds.create_new_version()
    dsnv.uuid = uuidlib.uuid4()
    dsnv.save()
    messages.success(request, "New version added.")
    # send an action
    action.send(
        request.user,
        verb="created new version {} of".format(dsnv.version),
        target=ds,
        public=False,
    )
    return edit_versions(request, ds_uuid=dsnv.uuid)


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_overview(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-overview")

    metadata = get_dataset_fields_from_form(ds, DataSetForm(instance=ds))
    detail = get_dataset_fields_from_form(ds, DataSetEditForm(instance=ds))
    fullpage = True

    return render(
        request,
        "dddatasets/prep/dataset_edit_preview.html",
        {
            "ds": ds,
            "manpage": manpage,
            "metadata": metadata,
            "detail": detail,
            "fullpage": fullpage,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_publish_start(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-publish")
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    if ds.is_frozen():
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_start.html",
        {"ds": ds, "manpage": manpage},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_publish_check(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-publish-check")
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    if ds.is_frozen():
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    request.session[str(ds_uuid) + "wp"] = request.path

    # validate dataset fields
    errors = list()
    try:
        ds.full_clean()
    except ValidationError as error:
        errors.append(error)

    # check if creator(s) are attached to dataset
    creators = DataSetCreator.objects.filter(dataset=ds)
    if creators.count() < 1:
        try:
            raise ValidationError({"creators": ["This field cannot be blank."]})
        except ValidationError as error:
            errors.append(error)

    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_check.html",
        {"ds": ds, "manpage": manpage, "errors": errors},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_publish_datafiles(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-publish-datafiles")
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    if ds.is_frozen():
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    request.session[str(ds_uuid) + "wp"] = request.path

    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_datafiles.html",
        {"ds": ds, "manpage": manpage},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_publish_license(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-publish-license")
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)

    request.session[str(ds_uuid) + "wp"] = request.path

    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_license.html",
        {"ds": ds, "manpage": manpage},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_publish_copyright(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-publish-copyright")
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    if ds.is_frozen():
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    request.session[str(ds_uuid) + "wp"] = request.path
    copyright_declaration_page = get_man_page("dataset-copyright-declaration")
    privacy_rights_declaration_page = get_man_page("privacy-rights-declaration")

    target = _("Dataset Copyright Declaration")
    err = None
    updated = False

    if request.method == "POST":
        sub_form_copyright = DataSetCopyrightForm(request.POST, instance=ds)
        sub_form_privacy_rights = DataSetPrivacyRightsForm(request.POST, instance=ds)
        form = DataSubmissionAgreementAcceptForm(request.POST, instance=ds.dsa)
        if (
            form.is_valid()
            and sub_form_copyright.is_valid()
            and sub_form_privacy_rights.is_valid()
        ):
            form.save()
            sub_form_copyright.save()
            sub_form_privacy_rights.save()
            ds.dsa.dsa_accepted_date = date.today()
            updated = True
        else:
            err = form.errors.as_data()
            err += sub_form_copyright.errors.as_data()
            err += sub_form_privacy_rights.errors.as_data()
    else:
        sub_form_copyright = DataSetCopyrightForm(instance=ds)
        sub_form_privacy_rights = DataSetPrivacyRightsForm(instance=ds)
        form = DataSubmissionAgreementAcceptForm(instance=ds.dsa)

    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_copyright.html",
        {
            "ds": ds,
            "manpage": manpage,
            "form": form,
            "sub_form_copyright": sub_form_copyright,
            "sub_form_privacy_rights": sub_form_privacy_rights,
            "target": target,
            "err": err,
            "updated": updated,
            "copyright_declaration_page": copyright_declaration_page,
            "privacy_rights_declaration_page": privacy_rights_declaration_page,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publish_final_fetch(request, ds_uuid, task):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)

    # check if ds is published NOT frozen
    if ds.published and not ds.republish_as_oa:
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    request.session[str(ds_uuid) + "wp"] = request.path
    collectionCreated = fetch(task).success

    response = render(
        request,
        "dddatasets/prep/_dataset_edit_publish_final_fetch.html",
        {"isFetched": collectionCreated, "ds_uuid": str(ds_uuid)},
    )
    if collectionCreated:
        messages.success(request, "Collection created successfully.")
        response["X-IC-CancelPolling"] = "true"
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publish_final_upload(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)

    request.session[str(ds_uuid) + "wp"] = request.path
    # check if ds is published NOT frozen
    if ds.published and not ds.republish_as_oa:
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )
    # Get token for DARIAH upload
    th = TokenHelper()
    t = th.get_token_from_session(request)
    publisher = Publisher(t, ds)
    # get collection from session
    collection = request.session.get("collection", None)
    status = publisher.upload_status(collection)
    logger.debug("UPLOAD_STATUS: %s", status)
    upload_complete = True
    task_exists = True
    for file in status.keys():
        if not status[file]:
            upload_complete = False

    files = collection.get("files")
    for file in files:
        task_exists = collection["files"][file].get("task_id", False)

    if not upload_complete and not task_exists:
        tasks = publisher.upload(collection)
        # put task id into collection
        for file in files:
            task_id = tasks[files[file]["df_uuid"]]
            collection["files"][file]["task_id"] = task_id
            # save collection in session
            request.session["collection"] = collection

    response = render(
        request,
        "dddatasets/prep/_dataset_edit_publish_final_upload.html",
        {"status": status, "upload_complete": upload_complete, "ds_uuid": str(ds_uuid)},
    )
    if upload_complete:
        response["X-IC-CancelPolling"] = "true"
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publish_final_pubdd(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)

    # check if ds is published NOT frozen
    if ds.published:
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    # publish dataset in discuss data (non-dariah)
    try:
        ds.publish(request.user)
    except ImproperlyConfigured:
        messages.error(
            request,
            "This instance of Discuss Data is not properly configured to register DataCite DOIs.",
        )
    except MetaDataError:
        messages.error(request, "Missing or malformed metadata.")
    # TODO: remove success message from template; messages are supposed to be delivered by messages interface only!!!
    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_final_pubdd.html",
        {"ds": ds},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publish_final_publish(request, ds_uuid):
    logger.debug("in edit_publish_final_publish")
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        logger.debug("[DHREP PUBLISHER] access denied")
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    # check if ds is published NOT frozen
    if ds.published and not ds.republish_as_oa:
        logger.debug("redirect")
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )
    logger.debug("start token helper")
    th = TokenHelper()
    t = th.get_token_from_session(request)
    logger.debug("start publisher")
    publisher = Publisher(t, ds)
    # get collection from session
    logger.debug("get collection from session")
    collection = request.session.get("collection", {})
    logger.debug("[DHREP PUBLISHER] collection: %s", str(collection))
    try:
        logger.debug("getting publisher status:")
        status = publisher.publish_status(collection)
    except Exception as e:
        logger.debug("[DHREP PUBLISHER EXCEPTION]: %s", e)
        status = None

    if status:
        logger.debug("[DHREP PUBLISHER] status: %s", status)
    else:
        logger.debug("[DHREP PUBLISHER] no status!")

    # an API implementation flaw makes the "processStatus" entry missing sometimes; i suppose this is in the state of QUEUING
    try:
        process_status = status["publishStatus"]["processStatus"]
    except Exception as e:  # KeyError as e:
        logger.exception(e)
        if status:
            process_status = "QUEUED"
        else:
            process_status = "FAILED"

    try:
        progress = status["publishStatus"]["progress"]
    except Exception as e:
        logger.exception("[DHREP PUBLISHER EXCEPTION 2] status: %s", e)
        progress = None

    logger.debug("process_status: %s", process_status)
    logger.debug("progress: %s", progress)

    # create response here to cancel polling on success and failed
    response = render(
        request,
        "dddatasets/prep/_dataset_edit_publish_final_publish.html",
        {
            "ds_uuid": str(ds_uuid),
            "status": process_status,
            "progress": progress,
            "ds": ds,
        },
    )
    if process_status == "FAILED":
        response["X-IC-CancelPolling"] = "true"
        logger.error("[DHREP PUBLISHER] FAILED: %s", status)
        messages.error(
            request, _("An Error occured. The dataset has not been published.")
        )
        return response

    # problem: it may take more than 5 seconds for a publishjob to begin to run after being triggered
    if process_status == "NOT_QUEUED":
        try:
            resp = publisher.publish(collection)
        except PublisherError as e:
            logger.error(e)
        else:
            logger.debug(resp.text)
    elif process_status == "FINISHED":
        response["X-IC-CancelPolling"] = "true"
        # the status dictionary contains the DOIs after the process has been finished;
        # on a new request, we can no longer get a status if the collection is popped from the session
        # move this to a later point!
        # request.session.pop("collection")
        publisher.update_dataset(collection, status)
        # also, make DD publication when finished
        logger.debug("[DHREP PUBLISHER] finished, starting ds.publish: %s", status)
        if ds.republish_as_oa:
            ds.data_access = "OA"
        try:
            ds.publish(request.user)
        except Exception as e:
            logger.exception("[DD PUBLISH] FAILED: %s", e)
            messages.error(
                request, _("An Error occured. The dataset has not been published.")
            )
            return redirect(
                "dddatasets:prep_edit_versions",
                ds_uuid=ds.uuid,
            )

        request.session.pop("collection")
        messages.success(
            request,
            _(
                "Success! The dataset has been published to Discuss Data and the Dariah Repository."
            ),
        )
    logger.debug("Final response: %s", response)
    return response


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publish_final(request, ds_uuid):
    # TODO: check for valid ds_uuid
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-publish-final")
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    # check if ds is published NOT frozen

    if request.method == "POST":
        repub_oa = request.POST.get("repub_oa")
        logger.debug(f"REPUB: {repub_oa}")
        logger.debug(f"REPUB MODEL: {ds.republish_as_oa}")
        if not ds.republish_as_oa:
            logger.debug(f"REPUB MODEL SET")
            ds.republish_as_oa = True
            ds.save()

    if ds.published and not ds.republish_as_oa:
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )
    # define context variables
    requestUri = None
    isTokenValid = False
    form = None
    err = None
    updated = False
    target = _("DARIAH Terms of Use Declaration")
    turtle_task = None
    publish_message = None
    # run checks
    check_error, check_list = ds.perform_checks()
    if check_error:
        error_string = _("Errors ocurred, please fix: ") + " ".join(check_list)
        messages.error(request, error_string)
        publish_message = error_string
    else:
        # no token evaluation if not open access
        if ds.data_access == ds.OPEN_ACCESS or ds.republish_as_oa:
            # Get token only for DARIAH upload
            if request.user.get_socialaccount_provider == 'dariah':
                logger.debug(request.user)
                logger.debug(request.user.get_socialaccount_provider)
                th = TokenHelper()
                t = th.get_token_from_session(request)
                collection = {}

                if t:
                    if t.check():
                        isTokenValid = True

                        messages.success(
                            request, "Token " + t.access_token + " is " + t.info()
                        )
                        publisher = Publisher(t, ds)
                        # create a job for the dataset and return the collection as a dictionary
                        collection = publisher.create_collection()
                        # save collection in session
                        request.session["collection"] = collection
                    else:
                        isTokenValid = False
                        messages.error(
                            request, "Token " + t.access_token + " is " + t.info()
                        )
                        request.session.pop("token")
                        request.session["ds_uuid"] = str(ds_uuid)
                else:
                    isTokenValid = False
                    messages.warning(request, "No Token provided.")
                    request.session["ds_uuid"] = str(ds_uuid)
                logger.debug(isTokenValid)
                # return render(request, "dhrep/_getToken.html", {"requestUri": th.request_url})
                # wenn token nicht gültig, ist collection = {}
                turtle_task = collection.get("turtle_task", None)
                requestUri = th.request_url
                publish_message = None

        if request.method == "POST":
            form = DariahTermsOfUse(request.POST, instance=request.user)
            if form.is_valid():
                form.save()
                updated = True
            else:
                err = form.errors.as_data()
        else:
            form = DariahTermsOfUse(instance=request.user)

    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_final.html",
        {
            "requestUri": requestUri,
            "isTokenValid": isTokenValid,
            "ds": ds,
            "form": form,
            "updated": updated,
            "err": err,
            "target": target,
            "manpage": manpage,
            "publish_message": publish_message,
            "turtle_task": turtle_task,
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_publish_request(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    manpage = get_man_page("dataset-publish-request")
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        logger.error("access denied")
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    if ds.is_frozen():
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    if request.method == "POST":
        # run checks
        logger.debug("POST publish request")
        check_error, check_list = ds.perform_checks()
        logger.error(check_error)
        if check_error:
            error_string = _("Errors ocurred, please fix: ") + " ".join(check_list)
            publish_message = error_string
            logger.error(error_string)
            messages.error(request, error_string)
        else:
            logger.debug("start request_publication")
            publish_message = ds.request_publication(request.user)
            logger.debug("end request_publication")
            return edit_versions(request, ds_uuid=ds_uuid)
    else:
        publish_message = None

    return render(
        request,
        "dddatasets/prep/_dataset_edit_publish_request.html",
        {"ds": ds, "manpage": manpage, "publish_message": publish_message},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publish_access(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        logger.error("access denied")
    manpage = get_man_page("dataset_publish")
    search_type = "pubaccess"
    form = DataSetIndividualLicenseForm(instance=ds.license)
    publish_right = check_perms(
        "admin_dsmo", request.user, ds.dataset_management_object
    )
    return render(
        request,
        "dddatasets/prep/dataset_edit_publish_access.html",
        {
            "ds": ds,
            "publish_right": publish_right,
            "manpage": manpage,
            "search_type": search_type,
            "form": form,
            "next_url": request.session.get(str(ds_uuid) + "wp"),
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def edit_publish_access_select(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)

    if request.method == "POST":
        if not ds.is_frozen():
            pubaccess = request.POST["pubaccess"]
            ds.data_access = pubaccess
            ds.save()
            return edit_publish_access(request, ds_uuid=ds_uuid)
    else:
        return edit_publish_access(request, ds_uuid=ds_uuid)


@login_required
@dd_tou_accepted
@dd_user_verified
@dd_dataset_access
def edit_publish_license_select(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if not check_perms("admin_dsmo", request.user, ds.dataset_management_object):
        return HttpResponse(_("Access denied"))
    # check_perms_403("admin_dataset", request.user, ds.dataset_management_object)
    if ds.is_frozen():
        return redirect(
            "dddatasets:prep_edit_versions",
            ds_uuid=ds.uuid,
        )

    if request.method == "POST":
        license_name = request.POST["licensename"]
        if license_name == "individual":
            if not ds.license or ds.license.license_type != License.INDIVIDUAL:
                ds.license = License.objects.create()
            ds.license.license_type = License.INDIVIDUAL
            form = DataSetIndividualLicenseForm(request.POST, instance=ds.license)
            if form.is_valid():
                ds.license = form.save()
                ds.license.save()
            else:
                # if form is invalid: do not save, show error message
                logger.info("Individual License Form INVALID")
                messages.error(
                    request,
                    _("Error in editing Individual License: {}").format(
                        form.errors.as_text()
                    ),
                )
        elif license_name == "nolicense":
            ds.license = None
        else:
            ds.license = License.objects.get(license_name=license_name)
            standard_license = LicensePage.objects.get(name=license_name)
            ds.license.license_type = License.STANDARD
            ds.license.standard_license = standard_license
            ds.license.save()
        ds.save()
    return edit_publish_access(request, ds_uuid=ds_uuid)


@login_required
@dd_tou_accepted
@dd_user_verified
def curation(request, ds_uuid):
    ds = get_object_or_404(DataSet, uuid=ds_uuid)
    if ds.pub_request_pending() and request.user not in ds.get_curators():
        raise PermissionDenied

    manpage = get_man_page("dataset_curation")

    if request.method == "POST":
        form = DataSetPublishAccept(request.POST)
        if form.is_valid():
            decision = form.cleaned_data.get("decision")
            message = form.cleaned_data.get("message")
            if decision == "accept":
                ds.accept_publication(request.user, message)
                messages.success(request, _("Dataset publication request accepted"))
            else:
                ds.deny_publication(request.user, message)
                messages.success(request, _("Dataset publication request denied"))
        else:
            messages.warning(request, form.errors.as_data())

    else:
        pass
    return render(
        request,
        "dddatasets/prep/dataset_curation.html",
        {
            "ds": ds,
            "manpage": manpage,
            # "form": form,
        },
    )
