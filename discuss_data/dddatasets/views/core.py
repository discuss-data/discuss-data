import logging

from actstream import actions
from actstream.models import followers
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import gettext as _
from django_sendfile import sendfile
from django.views.generic.detail import DetailView, View
from hitcount.views import HitCountDetailView, HitCountMixin
from hitcount.models import HitCount

from guardian.shortcuts import get_perms

from discuss_data.core.decorators import dd_tou_accepted, dd_user_verified
from discuss_data.core.helpers import check_perms, is_curator_or_check_perms_for_ds
from discuss_data.core.views import core_search_view, get_search_params
from discuss_data.dddatasets.models import (
    Category,
    DataFile,
    DataSet,
    DataSetAccessRequest,
    DataSetManagementObject,
)
from discuss_data.dddatasets.utils import check_published

logger = logging.getLogger(__name__)


# ************
# public views
# ************


def search(request):
    return core_search_view(request, "dataset_index", 10)


# unused?
def listing(request):
    dsmos = DataSetManagementObject.objects.select_related("main_published_ds")
    datasets = list()
    for dsmo in dsmos:
        if dsmo.main_published_ds:
            datasets.append(dsmo.main_published_ds)

    return render(
        request,
        "dddatasets/listing.html",
        {"datasets": datasets, "pagetitle": _("Datasets")},
    )


def category_list(request):
    categories = Category.objects.all()
    return render(
        request,
        "dddatasets/category_list.html",
        {"categories": categories, "pagetitle": _("Categories")},
    )


def category_page(request, slug):
    logger.debug("category slug: {}".format(slug))
    category = get_object_or_404(Category, slug=slug)
    template = "dddatasets/search_results.html"
    datasets = category.get_published_datasets_category_all()
    search_params = get_search_params(request, None, category)
    # return core_search_view(request, "dataset_index", 10, category=category)
    return render(
        request,
        template,
        {
            "object_list": datasets,
            "search_params": search_params,
            "query": search_params["q"],
            "category": category,
        },
    )

class DataSetPublicHitCountView(HitCountDetailView):
    model = DataSet
    count_hit = True

    def get_object(self, **kwargs):
        return check_published(self.kwargs['uuid'])


class DataSetDetailView(DataSetPublicHitCountView):
    template_name = "dddatasets/detail.html"


class DataSetMetadataView(DataSetPublicHitCountView):
    template_name = "dddatasets/metadata.html"


class DataSetPublicationsView(DataSetPublicHitCountView):
    template_name = "dddatasets/publications.html"


class DataSetDiscussView(DataSetPublicHitCountView):
    template_name = "dddatasets/discuss.html"


class DataSetVersionsView(DataSetPublicHitCountView):
    template_name = "dddatasets/versions.html"


class DataSetFilesView(DataSetPublicHitCountView):
    template_name = "dddatasets/files.html"

    def get_datafiles(self, **kwargs):
        logger.debug("get datafiles")
        datafiles = list()
        ds = self.get_object(**kwargs)
        for datafile in ds.get_datafiles():
            try:
                filesize = datafile.file.size
            except FileNotFoundError:
                filesize = 10
            if datafile.data_file_type in (
                datafile.FILE_TYPE_DATA,
                datafile.FILE_TYPE_CONVERTED,
            ):
                # if dataset is RA check permissions
                if (
                    ds.data_access != ds.RESTRICTED_ACCESS
                    or is_curator_or_check_perms_for_ds(
                        "ra_view_dataset", self.request.user, datafile.dataset
                    )
                ):
                    datafiles.append(
                        (datafile, filesize, datafile.get_file_format(), datafile.hit_count.hits)
                    )
                else:
                    datafiles.append(("RA", filesize, datafile.get_file_format(), datafile.hit_count.hits))
            else:
                logger.debug(f"append {datafile}, type {type(datafile)}")
                datafiles.append((datafile, filesize, datafile.get_file_format(), datafile.hit_count.hits))
        return datafiles

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["datafiles"] = self.get_datafiles(**kwargs)
        return context


def files_pdf(request, uuid):
    ds = get_object_or_404(DataSet, uuid=uuid)
    try:
        pdf_file = DataFile.objects.filter(dataset=ds, data_file_type="PDF")[0]
    except DataFile.DoesNotExist as e:
        raise Http404 from e
    return download(request, pdf_file)


def files_zip(request, uuid):
    ds = get_object_or_404(DataSet, uuid=uuid)
    datafile = get_object_or_404(DataFile, dataset=ds, data_file_type="ZIP")

    if ds.data_access != ds.RESTRICTED_ACCESS or is_curator_or_check_perms_for_ds(
        "ra_view_dataset", request.user, datafile.dataset
    ):
        return download(request, datafile)
    return HttpResponseForbidden()


def files_download(request, uuid, df_uuid):
    logger.debug(f"File download")
    ds = get_object_or_404(DataSet, uuid=uuid)
    datafile = get_object_or_404(DataFile, uuid=df_uuid, dataset=ds)
    logger.info("datafile.file.path: %s", datafile.file.path, exc_info=1)
    logger.info(
        "datafile.get_download_file_name(): %s", datafile.get_download_file_name()
    )
    # check permissions for the dataset if it is of filetype DATA or CONVERTED
    if datafile.data_file_type in (
        datafile.FILE_TYPE_DATA,
        datafile.FILE_TYPE_CONVERTED,
    ):
        # if dataset is RA check permissions
        if (
            ds.data_access != ds.RESTRICTED_ACCESS
            or is_curator_or_check_perms_for_ds(
                "ra_view_dataset", request.user, datafile.dataset
            )
            or "view_dsmo"
            or "admin_dsmo"
            or "edit_dsmo" in get_perms(request.user, ds.dataset_management_object)
        ):
            return download(request, datafile)
        logger.debug(f"File download forbidden: {datafile}")
        return HttpResponseForbidden()
    return download(request, datafile)


def download(request, datafile):
    """sendfile download wrapper"""
    # count hit
    hit_count = HitCount.objects.get_for_object(datafile)
    hit_count_response = HitCountMixin.hit_count(request, hit_count)
    logger.debug(f"File download: {datafile}, {hit_count_response}")

    return sendfile(
        request,
        datafile.file.path,
        attachment=True,
        attachment_filename=datafile.get_download_file_name(),
    )








# ************
# views that require log-in
# ************
@login_required
@dd_tou_accepted
@dd_user_verified
def follow(request, uuid):
    # redirect if no intercooler request
    if not request.is_intercooler():
        return redirect("dddatasets:search")
    dataset = get_object_or_404(DataSet, uuid=uuid)

    if request.user in followers(dataset):
        actions.unfollow(request.user, dataset)
    else:
        actions.follow(request.user, dataset)
    return render(
        request,
        "ddusers/_follow_button.html",
        {
            "object": dataset,
            "follow_url": "dddatasets:follow",
            "follow_text": "Follow this Dataset",
        },
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def access_request(request, uuid):
    ds = check_published(uuid)
    # check restricted access view permissions for the dataset
    if check_perms("ra_view_dataset", request.user, ds):
        # user has already access
        return HttpResponse(
            '<button class="btn btn-success" disabled>{}</button>'.format(
                _("You have access to this Dataset")
            )
        )

    if request.method == "POST":
        ds_access_request = DataSetAccessRequest(dataset=ds, user=request.user)
        ds_access_request.save()

        return HttpResponse(
            '<button class="btn btn-secondary" disabled>{}</button>'.format(
                ds_access_request
            )
        )
    else:
        ds_access_request = ds.get_access_request(request.user)
        if ds_access_request:
            return HttpResponse(
                '<button class="btn btn-secondary" disabled>{}</button>'.format(
                    ds_access_request
                )
            )
    return render(
        request,
        "dddatasets/_access_request.html",
        {
            "add_url": "dddatasets:access_request",
            "object": ds,
            "btn_text": "Request Access",
        },
    )
