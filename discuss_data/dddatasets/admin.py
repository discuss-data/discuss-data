from django.contrib import admin

from discuss_data.dddatasets.models import (
    AnalysisMethodsTags,
    Category,
    CollectionMethodsTags,
    DataFile,
    DataList,
    DataSet,
    DataSetManagementObject,
    DataType,
    DisciplinesTags,
    LanguageTags,
    License,
    DataSetAccessRequest,
    DataSetPublicationRequest,
    Archive,
    ArchivalSignature,
    DataSetOwnerTransfer,
    DataSetPrevOwner,
    DataSetPublishingProcess,
)


class DataSetAdmin(admin.ModelAdmin):
    search_fields = [
        "title",
        "subtitle",
        "owner__last_name",
    ]
    list_display = (
        "title",
        "subtitle",
        "owner",
        "uuid",
    )


class LicenseAdmin(admin.ModelAdmin):
    list_display = (
        "license_type",
        "standard_license",
        "license_name",
        "license_slug",
        "get_datasets",
    )


class CategoryAdmin(admin.ModelAdmin):
    raw_id_fields = ("curators",)
    autocomplete_fields = [
        "curators",
    ]
    list_display = (
        "name",
        "slug",
        "description",
        "get_curators",
    )

    def get_curators(self, instance):
        return [curator.get_academic_name() for curator in instance.curators.all()]


class DataSetOwnerTransferAdmin(admin.ModelAdmin):
    list_display = (
        "dataset",
        "get_owner_name",
        "new_owner",
        "prev_owner_request",
        "new_owner_accept",
    )
    search_fields = [
        "dataset__title",
    ]
    raw_id_fields = (
        "dataset",
        "new_owner",
    )


admin.site.register(DataSetPrevOwner)
admin.site.register(DataFile)
admin.site.register(DataSet, DataSetAdmin)
admin.site.register(DataSetManagementObject)
admin.site.register(License, LicenseAdmin)
admin.site.register(DataType)
admin.site.register(Category, CategoryAdmin)
admin.site.register(DataList)
admin.site.register(LanguageTags)
admin.site.register(CollectionMethodsTags)
admin.site.register(AnalysisMethodsTags)
admin.site.register(DisciplinesTags)
admin.site.register(DataSetAccessRequest)
admin.site.register(DataSetPublicationRequest)
admin.site.register(Archive)
admin.site.register(ArchivalSignature)
admin.site.register(DataSetOwnerTransfer, DataSetOwnerTransferAdmin)
admin.site.register(DataSetPublishingProcess)
