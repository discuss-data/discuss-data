import logging

import factory
import pytest
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import MultipleObjectsReturned
from django.db import IntegrityError, transaction

from discuss_data.dddatasets.models import (
    Category,
    DataFile,
    DataSet,
    DataSetManagementObject,
    License,
)
from discuss_data.ddusers.models import User

logger = logging.getLogger(__name__)


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category


class DataFileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataFile


class DataSetFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataSet


class DataSetManagementObjectFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DataSetManagementObject


class LicenseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = License


def object_creator(app_label, model_name, object_dictionary):
    """ create objects from a dictionary

        :param app_label: name of of the app
        :type app_label: str
        :param model_name: name of the model
        :type model_name: str
        :param object_dictionary: a dictionary comprising the objects attributes and values
        :type object_dictionary: dict

    """
    # be careful: 'object' is a reserved keyword
    model_type = ContentType.objects.get(app_label=app_label, model=model_name)
    for obj in object_dictionary:
        # if not model_type.model_class()(**obj):
        #    print("OBJ", **obj)
        try:
            with transaction.atomic():
                # new_obj = model_type.model_class().objects.create(**obj)
                new_obj = model_type.model_class()(**obj)
                new_obj.save()
        except IntegrityError as e:
            print("[ERROR]", e, model_name, obj)
            logger.error(e)
        except MultipleObjectsReturned as e:
            print("[ERROR]", e, model_name, obj)
            logger.error(e)


@pytest.fixture
def f_views_core(transactional_db):
    # USERS
    users = [
        {
            "username": "marsilius",
            "first_name": "Marsilius",
            "last_name": "Inghen",
            "email": "marsilius@discuss-data.net",
            "is_staff": True,
            "is_active": True,
            "date_joined": "2020-08-03T06:25:31Z",
            "uuid": "7dd0691b-7df8-4167-9b32-81199e74e89e",
            "academic_title": "DR",
            "accepted_dariah_tou": False,
            "accepted_dd_tou": True,
            "show_first_steps": True,
        },
        {
            "is_superuser": False,
            "username": "heinrich",
            "first_name": "Heinrich",
            "last_name": "Totting",
            "email": "heinrich@discuss-data.net",
            "is_staff": True,
            "is_active": True,
            "uuid": "a8d1587c-6b51-4822-b69e-3a24e81b1f3f",
            "academic_title": "DR",
            "name_suffix": "von Oyta",
            "accepted_dariah_tou": False,
            "accepted_dd_tou": True,
            "show_first_steps": True,
        },
        {
            "is_superuser": False,
            "username": "konrad",
            "first_name": "Konrad",
            "last_name": "Megenberg",
            "email": "konrad@discuss-data.net",
            "is_staff": True,
            "is_active": True,
            "uuid": "b3144040-7ffb-48de-9fe4-5a7de3e600e6",
            "academic_title": "DR",
            "accepted_dariah_tou": False,
            "accepted_dd_tou": True,
            "show_first_steps": True,
        },
        {
            "is_superuser": False,
            "username": "bartolus",
            "first_name": "Bartolus",
            "last_name": "de Saxoferrato",
            "email": "bartolus@discuss-data.net",
            "is_staff": False,
            "is_active": True,
            "uuid": "96aa784a-1595-4854-9126-6e612cf1f800",
            "academic_title": "DR",
            "accepted_dariah_tou": False,
            "accepted_dd_tou": True,
            "show_first_steps": True,
        },
    ]

    def create_users(users):
        for user in users:
            try:
                User.objects.get(username=user["username"])
            except User.DoesNotExist:
                try:
                    # see https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
                    with transaction.atomic():
                        User.objects.create_user(**user)
                except IntegrityError as e:
                    logger.error(e)

    create_users(users)

    # CATEGORIES
    categories = [
        {"name": "Other", "slug": "other", "description": "default"},
        {
            "name": "Yet Another",
            "slug": "ya",
            "description": "yet another non-default category",
        },
    ]

    # for category in categories:
    #    CategoryFactory(**category)

    object_creator("dddatasets", "category", categories)

    # TODO: if needed later provide factory `create_curations(category, user)`
    c1 = Category.objects.get(slug="other")
    c1.curators.add(User.objects.get(username="konrad"))
    c1.save()
    c2 = Category.objects.get(slug="ya")
    c2.curators.add(User.objects.get(username="bartolus"))
    c2.save()

    # DSMOs
    dsmos = [
        {
            "uuid": "0e2eaf36-46e1-4c75-91d2-55294b0e15ce",
            "owner": User.objects.get(username="marsilius"),
            "created_at": "2020-08-03T07:00:31.202Z",
            "updated_at": "2020-08-03T07:10:35.036Z",
            "published": True,
            "main_category": Category.objects.get(slug="other"),
            # "main_published_ds":2,
        },
        {
            "uuid": "439a6646-fa34-4fbd-a6ba-c06d03641920",
            "owner": User.objects.get(username="heinrich"),
            "created_at": "2020-08-19T12:22:52.333Z",
            "updated_at": "2020-08-19T12:27:38.123Z",
            "published": False,
            "main_category": Category.objects.get(slug="other"),
            # "main_published_ds":3,
        },
        {
            "uuid": "d4c5d62d-23c1-4fc7-8889-6a70680c887f",
            "owner": User.objects.get(username="konrad"),
            "created_at": "2020-08-19T13:41:34.170Z",
            "updated_at": "2020-08-19T13:43:58.666Z",
            "published": True,
            "main_category": Category.objects.get(slug="other"),
            # "main_published_ds":4,
        },
        {
            "uuid": "ce6e815d-859f-4c5c-804e-8b3b0d9d852d",
            "owner": User.objects.get(username="konrad"),
            "created_at": "2020-08-19T13:57:07.085Z",
            "updated_at": "2020-08-19T13:57:07.191Z",
            "published": False,
            "main_category": Category.objects.get(slug="other"),
        },
    ]

    # for dsmo in dsmos:
    #    DataSetManagementObjectFactory(**dsmo)

    object_creator("dddatasets", "datasetmanagementobject", dsmos)

    # DATASETS
    datasets = [
        {
            "uuid": "41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62",
            "owner": User.objects.get(username="marsilius"),
            "data_access": "RA",
            "doi": "10.80135/41B3540B-8D1F-4B3B-A1A1-9B9AA02F6C62",
            "title": "Rationes cur Urbano Pontifici electo adhaerendum",
            "shorttitle": "Rationes",
            "publication_date": "2020-08-03",
            "version": 1.0,
            "description": "Aufz\u00e4hlung von Argumenten",
            "sources_of_data": "Gedanken",
            "dataset_management_object": DataSetManagementObject.objects.get(
                uuid="0e2eaf36-46e1-4c75-91d2-55294b0e15ce"
            ),
            "published": True,
            "published_main_category": Category.objects.get(slug="other"),
            "publication_accepted": True,
            "publication_accepted_by": User.objects.get(username="konrad"),
            "copyright_declaration": True,
            # "datatypes": [4],
        },
        {
            "uuid": "ad7f8188-412a-48ce-a8f0-070e3f51783d",
            "owner": User.objects.get(username="heinrich"),
            "data_access": "OA",
            "dhdoi": "ausgedacht",
            "title": "Quaestiones Praedicamentorum",
            "shorttitle": "Quaestiones",
            "publication_date": "2020-08-19",
            "version": 1.0,
            "description": "Qu\u00e4stionensammlung",
            "sources_of_data": "thoughts of mind",
            "dataset_management_object": DataSetManagementObject.objects.get(
                uuid="439a6646-fa34-4fbd-a6ba-c06d03641920"
            ),
            "published": True,
            "published_main_category": Category.objects.get(slug="other"),
            "publication_accepted": True,
            "publication_accepted_by": User.objects.get(username="konrad"),
            "copyright_declaration": True,
            # "groups": [2],
            # "datatypes": [4],
        },
        {
            "uuid": "39e36320-bae7-4246-af47-98fc4249ffb9",
            "owner": User.objects.get(username="konrad"),
            "data_access": "MO",
            "doi": "10.80135/39E36320-BAE7-4246-AF47-98FC4249FFB9",
            "title": "Canones poenitentiales",
            "shorttitle": "Canones",
            "publication_date": "2020-08-19",
            "version": 1.0,
            "description": "Abhandlung \u00fcber die Beichte",
            "sources_of_data": "own thoughts",
            "dataset_management_object": DataSetManagementObject.objects.get(
                uuid="d4c5d62d-23c1-4fc7-8889-6a70680c887f"
            ),
            "published": True,
            "published_main_category": Category.objects.get(slug="other"),
            "publication_accepted": True,
            "publication_accepted_by": User.objects.get(username="konrad"),
            "copyright_declaration": True,
            # "datatypes": [4],
        },
        {
            "uuid": "f8b9c9f0-677f-49c1-8f97-0496c44ddbb3",
            "owner": User.objects.get(username="konrad"),
            "data_access": "RA",
            "title": "Expositio super spheram",
            "shorttitle": "Expositio",
            "publication_date": "2020-08-19",
            "version": 1.0,
            "description": "Erkl\u00e4rungen",
            "dataset_management_object": DataSetManagementObject.objects.get(
                uuid="ce6e815d-859f-4c5c-804e-8b3b0d9d852d"
            ),
            "published": False,
            "publication_accepted": False,
            "copyright_declaration": False,
        },
    ]

    for dataset in datasets:
        DataSetFactory(**dataset)

    # object_creator("dddatasets", "dataset", datasets)

    # DATAFILES
    datafiles = [
        {
            "uuid": "127ee0d0-37ac-4dd9-923d-fb458c4d347d",
            "dataset": DataSet.objects.get(uuid="41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62"),
            "file": "datasets/f02d54ea-8abf-430a-b69f-60b3180a1cc1",
            "data_file_type": "DAT",
            "data_file_format": "unknown",
            "data_file_size": 2000000,
            "content_type": "application/octet-stream",
            "name": "rationes2.xls",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "b6a6138f-46b9-4c42-b9ce-135ce3217caa",
            "dataset": DataSet.objects.get(uuid="41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62"),
            "file": "datasets/a3d1d132-63ae-45fa-aa34-a73ad4309733",
            "data_file_type": "DAT",
            "data_file_format": "unknown",
            "data_file_size": 1048576,
            "content_type": "application/pdf",
            "name": "rationes1.pdf",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "9a141b5f-9da9-4358-a1b8-d5b317fee4ea",
            "dataset": DataSet.objects.get(uuid="41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62"),
            "file": "datasets/9a141b5f-9da9-4358-a1b8-d5b317fee4ea",
            "data_file_type": "MET",
            "data_file_format": "unknown",
            "data_file_size": 1048576,
            "content_type": "application/pdf",
            "name": "met_ra.txt",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "91c67808-af8a-43f6-a8fb-ceafa31e83dc",
            "dataset": DataSet.objects.get(uuid="41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62"),
            "file": "datasets/91c67808-af8a-43f6-a8fb-ceafa31e83dc",
            "data_file_type": "DOC",
            "data_file_format": "unknown",
            "data_file_size": 1048576,
            "content_type": "application/pdf",
            "name": "doc_ra.txt",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "b3852026-c14c-4bff-a7e7-bb4b138e964f",
            "dataset": DataSet.objects.get(uuid="ad7f8188-412a-48ce-a8f0-070e3f51783d"),
            "file": "datasets/b3852026-c14c-4bff-a7e7-bb4b138e964f",
            "data_file_type": "MET",
            "data_file_format": "unknown",
            "data_file_size": 1048576,
            "content_type": "application/pdf",
            "name": "met_oa.txt",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "2c32fca6-2964-4aa7-a939-0c0efecc9f85",
            "dataset": DataSet.objects.get(uuid="ad7f8188-412a-48ce-a8f0-070e3f51783d"),
            "file": "datasets/2c32fca6-2964-4aa7-a939-0c0efecc9f85",
            "data_file_type": "DOC",
            "data_file_format": "unknown",
            "data_file_size": 1048576,
            "content_type": "application/pdf",
            "name": "doc_oa.txt",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "859d6079-0a3c-4714-98e2-d45ed94ca8d0",
            "dataset": DataSet.objects.get(uuid="39e36320-bae7-4246-af47-98fc4249ffb9"),
            "file": "datasets/859d6079-0a3c-4714-98e2-d45ed94ca8d0",
            "data_file_type": "MET",
            "data_file_format": "unknown",
            "data_file_size": 1048576,
            "content_type": "application/pdf",
            "name": "met_mo.txt",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "df0a6580-9ad4-48e2-9da7-abb4ecdc6ffe",
            "dataset": DataSet.objects.get(uuid="39e36320-bae7-4246-af47-98fc4249ffb9"),
            "file": "datasets/df0a6580-9ad4-48e2-9da7-abb4ecdc6ffe",
            "data_file_type": "DOC",
            "data_file_format": "unknown",
            "data_file_size": 1048576,
            "content_type": "application/pdf",
            "name": "doc_mo.txt",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "088b2348-30eb-48e7-874d-ed47252d6ade",
            "dataset": DataSet.objects.get(uuid="41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62"),
            "file": "/app/data/datasets/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62-description.pdf",
            "data_file_type": "PDF",
            "data_file_format": "unknown",
            "data_file_size": 1,
            "content_type": "text/plain",
            "name": "DiscussData-rationes-cur-urbano-pontifici-electo-adhaerendum-description.pdf",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "21a17051-4334-4ef2-a450-225a724eb723",
            "dataset": DataSet.objects.get(uuid="ad7f8188-412a-48ce-a8f0-070e3f51783d"),
            "file": "/app/data/datasets/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62-description.pdf",
            "data_file_type": "PDF",
            "data_file_format": "unknown",
            "data_file_size": 1,
            "content_type": "text/plain",
            "name": "DiscussData-rationes-cur-urbano-pontifici-electo-adhaerendum-description.pdf",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "a70fe21c-a473-4d75-b6ea-e91e33841428",
            "dataset": DataSet.objects.get(uuid="39e36320-bae7-4246-af47-98fc4249ffb9"),
            "file": "/app/data/datasets/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62-description.pdf",
            "data_file_type": "PDF",
            "data_file_format": "unknown",
            "data_file_size": 1,
            "content_type": "text/plain",
            "name": "DiscussData-rationes-cur-urbano-pontifici-electo-adhaerendum-description.pdf",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
        {
            "uuid": "6a2d32b8-0d0d-4a2c-bca5-ebd150a13eec",
            "dataset": DataSet.objects.get(uuid="41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62"),
            "file": "/app/data/datasets/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62-all-files.zip",
            "data_file_type": "ZIP",
            "data_file_format": "unknown",
            "data_file_size": 1,
            "content_type": "text/plain",
            "name": "DiscussData-rationes-cur-urbano-pontifici-electo-adhaerendum-all-files.zip",
            "repository_file_id": "not set",
            "repository": "dariah-repository",
            "dhdoi": "",
        },
    ]

    for datafile in datafiles:
        DataFileFactory(**datafile)

    # object_creator("dddatasets", "datafile", datafiles)
