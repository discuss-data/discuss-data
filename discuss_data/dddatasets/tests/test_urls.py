import uuid as uuidlib
from django.urls import reverse, resolve


def test_search():
    assert reverse("dddatasets:search") == "/dataset/search/"
    assert resolve("/dataset/search/").view_name == "dddatasets:search"


def test_files_zip():
    uuid = uuidlib.UUID("05d5d982-9239-4feb-be5d-abbf9fa15d1d")
    assert (
        reverse("dddatasets:files_zip", kwargs={"uuid": uuid})
        == "/dataset/" + str(uuid) + "/files/zip/"
    )
    assert (
        resolve("/dataset/" + str(uuid) + "/files/zip/").view_name
        == "dddatasets:files_zip"
    )
    assert resolve("/dataset/" + str(uuid) + "/files/zip/").kwargs == {"uuid": uuid}
