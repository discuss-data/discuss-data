import uuid as uuidlib

import pytest
from django.urls import reverse


@pytest.fixture
def unpublished_path():
    return "/dataset/f8b9c9f0-677f-49c1-8f97-0496c44ddbb3/"


@pytest.fixture
def published_mo_path():
    return "/dataset/39e36320-bae7-4246-af47-98fc4249ffb9/"


@pytest.fixture
def published_ra_path():
    return "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/"


@pytest.fixture
def published_oa_path():
    return "/dataset/ad7f8188-412a-48ce-a8f0-070e3f51783d/"


# # TODO: mark as a helper function
# def test_all_in_one_view_caller(all_in_one, views):
#     parameter = list()
#     for uuid in all_in_one.keys():
#         for view in views:
#             parameter.append([view, uuid, all_in_one[uuid]])
#     return parameter


# # cannot use fixtures with mark.parametrize
# @pytest.mark.parametrize(
#     "view, uuid, status_code",
#     test_all_in_one_view_caller(
#         # datasets with their intended accessibility
#         {
#             "f8b9c9f0-677f-49c1-8f97-0496c44ddbb3": 404,  # unpublished
#             "39e36320-bae7-4246-af47-98fc4249ffb9": 200,  # metadata only
#             "41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62": 200,  # restricted access
#             "ad7f8188-412a-48ce-a8f0-070e3f51783d": 200,  # open access
#         },
#         # views that must be accessible without login
#         (
#             "detail",
#             "metadata",
#             "access",
#             "files",
#             "files_pdf",
#             "publications",
#             "versions",
#         ),
#     ),
# )
# def test_all_in_one_view_runner(client, view, uuid, status_code, f_views_core):
#     """ test all views that must be accessible without login
#     """
#     assert (
#         client.get(
#             reverse("dddatasets:" + view, kwargs={"uuid": uuidlib.UUID(uuid)})
#         ).status_code
#         == status_code
#     )


# ********
# what anonymous users can view
# ********

# does not work without any dataset because the index must be built before this page is accessible
# def test_search(client):
#     """ the datasets search page is supposed to be accessible by outside users
#     """
#     assert client.get("/dataset/search/").status_code == 200


def test_detail_unpublished(client, unpublished_path, f_views_core):
    """ the dataset detail page is supposed to be accessible by outside users
        if published, 404 is to be returned otherwise
    """
    assert client.get(unpublished_path).status_code == 404


def test_detail_mo(client, published_mo_path, f_views_core):
    """ the dataset detail page is supposed to be accessible by outside users
        if published, 404 is to be returned otherwise
    """
    assert client.get(published_mo_path).status_code == 200


def test_detail_ra(client, published_ra_path, f_views_core):
    """ the dataset detail page is supposed to be accessible by outside users
        only if published, 404 is to be returned otherwise
    """
    assert client.get(published_ra_path).status_code == 200


def test_detail_oa(client, published_oa_path, f_views_core):
    """ the dataset detail page is supposed to be accessible by outside users
        if published OA
    """
    assert client.get(published_oa_path).status_code == 200


def test_metadata_unpublished(client, unpublished_path, f_views_core):
    """ the dataset metadata page is supposed to be accessible by outside users

    """
    assert client.get(unpublished_path + "metadata/").status_code == 404


def test_metadata_mo(client, published_mo_path, f_views_core):
    """ the dataset metadata page is supposed to be accessible by outside users(???)
        only if published OA, 404 is to be returned otherwise
    """
    assert client.get(published_mo_path + "metadata/").status_code == 200


def test_metadata_ra(client, published_ra_path, f_views_core):
    """ the dataset metadata page is supposed to be accessible by outside users(???)
        only if published OA, 404 is to be returned otherwise
    """
    assert client.get(published_ra_path + "metadata/").status_code == 200


def test_metadata_oa(client, published_oa_path, f_views_core):
    """ the dataset metadata page is supposed to be accessible by outside users
        if published OA
    """
    assert client.get(published_oa_path + "metadata/").status_code == 200


# ********
# for the following tests, see https://gitlab.gwdg.de/discuss-data/discuss-data/-/issues/214
# ********
def test_access_ds_with_oa(client, published_oa_path, f_views_core) -> None:
    """ this tests the access of an anonymous user to the file view of a dataset
        with open access and that no files are listed as restricted.
    """
    assert client.get(published_oa_path + "files/").status_code == 200
    assert "Request Access" not in str(client.get(published_oa_path + "files/").content)


def test_access_ds_with_ra(client, published_ra_path, f_views_core) -> None:
    """ this tests the access of an anonymous user to the file view of a dataset
        with restricted access and that files are listed as restricted
    """
    assert client.get(published_ra_path + "files/").status_code == 200
    assert "Request Access" in str(client.get(published_ra_path + "files/").content)


def test_access_own_ds_with_ra(
    client, django_user_model, published_ra_path, f_views_core
) -> None:
    """ this tests the access of a user to the file view of a dataset
        with restricted access owned by him
    """
    client.force_login(django_user_model.objects.get(username="marsilius"))
    assert client.get(published_ra_path + "files/").status_code == 200


def test_access_own_ds_with_ra_file1(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a user to the file view of a dataset
        with restricted access owned by him
    """
    client.force_login(django_user_model.objects.get(username="marsilius"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/b6a6138f-46b9-4c42-b9ce-135ce3217caa/"
        ).status_code
        == 200
    )


def test_access_own_ds_with_ra_file2(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a user to the file view of a dataset
        with restricted access owned by him
    """
    client.force_login(django_user_model.objects.get(username="marsilius"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/127ee0d0-37ac-4dd9-923d-fb458c4d347d/"
        ).status_code
        == 200
    )


def test_access_ds_with_ra_file1_user(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a user to the file view of a dataset
        with restricted access
    """
    client.force_login(django_user_model.objects.get(username="heinrich"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/b6a6138f-46b9-4c42-b9ce-135ce3217caa/"
        ).status_code
        == 403
    )


def test_access_ds_with_ra_file2_curator(
    client, django_user_model, f_views_core
) -> None:
    """ this tests the access of a curator to the file view of a dataset
        with restricted access published in the curators category
    """
    client.force_login(django_user_model.objects.get(username="konrad"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/127ee0d0-37ac-4dd9-923d-fb458c4d347d/"
        ).status_code
        == 200
    )


def test_access_ds_with_ra_file2_curator_other_cat(
    client, django_user_model, f_views_core
) -> None:
    """ this tests the access of a curator to the file view of a dataset
        with restricted access published in an other than the curators category
    """
    client.force_login(django_user_model.objects.get(username="bartolus"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/127ee0d0-37ac-4dd9-923d-fb458c4d347d/"
        ).status_code
        == 403
    )


# ********
# test access to datasets zip files
# ********
def test_access_own_ds_with_ra_zip(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a user to the file view of a dataset
        with restricted access owned by him
    """
    client.force_login(django_user_model.objects.get(username="marsilius"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/zip/"
        ).status_code
        == 200
    )


def test_access_ds_with_ra_zip_user(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a user to the file view of a datasets
        zipped contents with restricted access
    """
    client.force_login(django_user_model.objects.get(username="heinrich"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/zip/"
        ).status_code
        == 403
    )


def test_access_ds_with_ra_zip_curator(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a curator to the file view of a datasets
        zipped contents with restricted access published in the curators category
    """
    client.force_login(django_user_model.objects.get(username="konrad"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/zip/"
        ).status_code
        == 200
    )


def test_access_ds_with_ra_zip_curator_other_cat(
    client, django_user_model, f_views_core
) -> None:
    """ this tests the access of a curator to the file view of a datasets
        zipped contents with restricted access published in an other than
        the curators category
    """
    client.force_login(django_user_model.objects.get(username="bartolus"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/zip/"
        ).status_code
        == 403
    )


# ********
# test access on datasets description pdf files
# ********
def test_access_own_ds_with_ra_pdf(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a user to the description PDF of a dataset
        with restricted access owned by him
    """
    client.force_login(django_user_model.objects.get(username="marsilius"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/pdf/"
        ).status_code
        == 200
    )


def test_access_anonymous_ra_pdf(client, django_user_model, f_views_core) -> None:
    """ this tests the access of an anonymous user to the description PDF of a dataset
        with restricted access
    """
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/pdf/"
        ).status_code
        == 200
    )


def test_access_anonymous_oa_pdf(client, django_user_model, f_views_core) -> None:
    """ this tests the access of an anonymous user to the description PDF of a dataset
        with open access
    """
    assert (
        client.get(
            "/dataset/ad7f8188-412a-48ce-a8f0-070e3f51783d/files/pdf/"
        ).status_code
        == 200
    )


def test_access_anonymous_mo_pdf(client, django_user_model, f_views_core) -> None:
    """ this tests the access of an anonymous user to the description PDF of a dataset
        with metadata only
    """
    assert (
        client.get(
            "/dataset/39e36320-bae7-4246-af47-98fc4249ffb9/files/pdf/"
        ).status_code
        == 200
    )


def test_access_ds_with_ra_pdf_user(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a user to the description PDF of a dataset with restricted access
    """
    client.force_login(django_user_model.objects.get(username="heinrich"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/pdf/"
        ).status_code
        == 200
    )


def test_access_ds_with_ra_pdf_curator(client, django_user_model, f_views_core) -> None:
    """ this tests the access of a curator to the description PDF of a dataset with restricted access published in the curators category
    """
    client.force_login(django_user_model.objects.get(username="konrad"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/pdf/"
        ).status_code
        == 200
    )


def test_access_ds_with_ra_pdf_curator_other_cat(
    client, django_user_model, f_views_core
) -> None:
    """ this tests the access of a curator to the description PDF of a dataset
        with restricted access published in an other than the curators category
    """
    client.force_login(django_user_model.objects.get(username="bartolus"))
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/pdf/"
        ).status_code
        == 200
    )


# ********
# datasets DOC and MET files should be always accessible openly
# *******


def test_access_ds_ra_doc_file(client, django_user_model, f_views_core) -> None:
    """ this tests the anonymous access to a DOC file of an RA dataset
    """
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/91c67808-af8a-43f6-a8fb-ceafa31e83dc/"
        ).status_code
        == 200
    )


def test_access_ds_ra_met_file(client, django_user_model, f_views_core) -> None:
    """ this tests the anonymous access to a MET file of an RA dataset
    """
    assert (
        client.get(
            "/dataset/41b3540b-8d1f-4b3b-a1a1-9b9aa02f6c62/files/9a141b5f-9da9-4358-a1b8-d5b317fee4ea/"
        ).status_code
        == 200
    )


def test_access_ds_oa_doc_file(client, django_user_model, f_views_core) -> None:
    """ this tests the anonymous access to a DOC file of an OA dataset
    """
    assert (
        client.get(
            "/dataset/ad7f8188-412a-48ce-a8f0-070e3f51783d/files/2c32fca6-2964-4aa7-a939-0c0efecc9f85/"
        ).status_code
        == 200
    )


def test_access_ds_oa_met_file(client, django_user_model, f_views_core) -> None:
    """ this tests the anonymous access to a MET file of an OA dataset
    """
    assert (
        client.get(
            "/dataset/ad7f8188-412a-48ce-a8f0-070e3f51783d/files/b3852026-c14c-4bff-a7e7-bb4b138e964f/"
        ).status_code
        == 200
    )


def test_access_ds_mo_doc_file(client, django_user_model, f_views_core) -> None:
    """ this tests the anonymous access to a DOC file of an MO dataset
    """
    assert (
        client.get(
            "/dataset/39e36320-bae7-4246-af47-98fc4249ffb9/files/df0a6580-9ad4-48e2-9da7-abb4ecdc6ffe/"
        ).status_code
        == 200
    )


def test_access_ds_mo_met_file(client, django_user_model, f_views_core) -> None:
    """ this tests the anonymous access to a MET file of an MO dataset
    """
    assert (
        client.get(
            "/dataset/39e36320-bae7-4246-af47-98fc4249ffb9/files/859d6079-0a3c-4714-98e2-d45ed94ca8d0/"
        ).status_code
        == 200
    )
