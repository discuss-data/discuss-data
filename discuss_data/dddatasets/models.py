import logging
import os
import uuid
from zipfile import ZipFile
import re
import json
import requests
import html
from decimal import Decimal

import filetype
from actstream import action
from datacite.errors import DataCiteNotFoundError, DataCiteUnauthorizedError
from dcxml import simpledc
from django.conf import settings
from django.utils.html import strip_tags
from django.utils.timezone import localtime
from django.contrib.auth.models import Group
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import (
    ImproperlyConfigured,
    ObjectDoesNotExist,
    PermissionDenied,
)
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.forms import ModelForm
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.html import format_html
from django.utils.text import slugify
from django.utils.translation import gettext as _
from django_bleach.models import BleachField
from django.utils import timezone
from django.db.models import Q
from guardian.shortcuts import assign_perm, get_perms, get_users_with_perms, remove_perm
from PIL import Image
from taggit.managers import TaggableManager
from taggit.models import GenericTaggedItemBase, TagBase
from hitcount.models import HitCountMixin, HitCount

from discuss_data.core.zenodo import ZenodoDatasetPublisher
from discuss_data.core.errors import MetaDataError
from discuss_data.core.models import KeywordTags, LanguageTags, PDFTemplate
from discuss_data.core.utils import generate_discuss_data_doi, qs_to_str
from discuss_data.ddcomments.models import Comment, Notification
from discuss_data.ddusers.models import User
from discuss_data.pages.models import LicensePage, ManualPage
from discuss_data.utils.cropped_thumbnail import cropped_thumbnail
from discuss_data.core.utils import send_update_email

from discuss_data.core.utils import create_pdf

from wagtail.admin.panels import FieldPanel

logger = logging.getLogger(__name__)

# provide a storage independent of 'media' file storage
datafile_storage = FileSystemStorage(location=settings.DATA_ROOT)


class DataList(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=400)
    description = BleachField(max_length=2000)
    owner = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    datasets = models.ManyToManyField(
        "DataSet", blank=True, related_name="datalist_datasets"
    )
    public = models.BooleanField(default="False")

    class Meta:
        ordering = ["name"]

    def get_status(self):
        if self.public:
            return _("Public")
        else:
            return _("Private")

    def __str__(self):
        return self.name

    def class_name(self):
        return self.__class__.__name__


class CategoryManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class Category(models.Model):
    class Meta:
        ordering = ["name"]
        constraints = [
            models.UniqueConstraint(
                fields=["name"],
                name="name",
            ),
        ]

    name = models.CharField(max_length=400)
    slug = models.SlugField()
    image = models.ImageField(blank=True, null=True)
    description = BleachField(max_length=2000)
    sponsors = models.ManyToManyField(
        "Sponsor", blank=True, related_name="category_sponsors_sponsor"
    )
    curators = models.ManyToManyField(
        User, blank=True, related_name="category_curators_user"
    )

    # panels for wagtail ModelAdmin integration of django model
    # https://docs.wagtail.io/en/stable/reference/contrib/modeladmin/index.html
    panels = [
        FieldPanel("name"),
        FieldPanel("slug"),
        FieldPanel("image"),
        FieldPanel("description"),
        FieldPanel("sponsors"),
        FieldPanel("curators"),
    ]

    def natural_key(self):
        return self.name

    def get_curators_emails(self):
        curators_emails = list()
        for curator in self.curators.all():
            curators_emails.append(curator.get_email())
        return curators_emails

    def get_published_datasets_category(self):
        return DataSet.objects.filter(published=True, published_categories__id=self.id)

    def get_published_datasets_category_main(self):
        return DataSet.objects.filter(
            published=True, published_main_category__id=self.id
        )

    def get_published_datasets_category_all(self):
        return DataSet.objects.filter(
            Q(published_categories__id=self.id) | Q(published_main_category__id=self.id)
            ).filter(published=True).order_by("-publication_date").distinct()

    def get_published_datasets_category_all_count(self):
        return len(self.get_published_datasets_category_all())

    def __str__(self):
        return self.name


class Sponsor(models.Model):
    name = models.CharField(max_length=400)
    institution = models.ForeignKey(
        "ddusers.Institution",
        blank=True,
        null=True,
        related_name="sponsor_institution",
        on_delete=models.CASCADE,
    )
    logo = models.ImageField(blank=True, null=True)
    url = models.URLField(blank=True)

    def __str__(self):
        return self.name


def datafile_file_path(instance, filename):
    return "datasets/" + str(instance.uuid)


class DataFile(models.Model, HitCountMixin):
    FILE_FORMATS = {
        "png": "image",
        "jpeg": "image",
        "jpg": "image",
    }

    FILE_TYPE_DATA = "DAT"
    FILE_TYPE_METADATA = "MET"
    FILE_TYPE_DOC = "DOC"
    FILE_TYPE_CONVERTED = "CFF"
    FILE_TYPE_GENERATED_PDF = "PDF"
    FILE_TYPE_GENERATED_ZIP = "ZIP"
    FILE_TYPE_DATA_REVIEW = "REV"

    DATA_FILE_TYPES = (
        (FILE_TYPE_DATA, _("data")),
        (FILE_TYPE_METADATA, _("metadata")),
        (FILE_TYPE_DOC, _("data documentation")),
        (FILE_TYPE_CONVERTED, _("converted file format")),
        (FILE_TYPE_DATA_REVIEW, _("data review")),
    )
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.ForeignKey(
        "DataSet",
        related_name="datafile_dataset_dataset",
        on_delete=models.CASCADE,
    )
    file = models.FileField(
        upload_to=datafile_file_path, storage=datafile_storage, max_length=1000
    )
    data_file_type = models.CharField(
        max_length=3,
        choices=DATA_FILE_TYPES,
        default=FILE_TYPE_DATA,
    )
    data_file_format = models.CharField(max_length=200, default="unknown")
    data_file_size = models.IntegerField(default=1)
    content_type = models.CharField(max_length=200, default="text/plain")
    name = models.CharField(
        max_length=200,
    )
    repository_file_id = models.CharField(max_length=200, default="not set")
    repository = models.CharField(max_length=200, default="dariah-repository")
    dhdoi = models.CharField(max_length=200, blank=True)

    hit_count_generic = GenericRelation(
        HitCount,
        object_id_field="object_pk",
        related_query_name="hit_count_generic_relation",
    )


    def get_pdf_file_name(self):
        # max length for DataFile.name db field set to 200
        # length of file name template is aprox. 35 chars
        title_max_length = 200 - 35
        # slice the title-slug to title_max_length
        title_slug = slugify(self.shorttitle)[:title_max_length]
        if self.dataset_type == "DR":
            return "DiscussData-{}-{}.pdf".format(self.owner.last_name, title_slug)

    def get_download_file_name(self):
        # filenames longer than 225 chars break Windows Fireforx
        # use dataset.shorttitle and restrict datafile.filename to max 55 chars
        filename, ext = os.path.splitext(self.name)
        title_max_length = 200 - 35
        title_slug = slugify(self.dataset.shorttitle)[:title_max_length]
        if self.data_file_type == "REV":
            # filename for datareview pdfs
            fn = "DiscussData-{}-{}.pdf".format(self.dataset.owner.last_name, title_slug)
            logger.debug(f"FILENAME 1: {fn}")
            return fn

        # filename for other files
        fn = "DiscussData-{}-{}{}".format(
            self.dataset.shorttitle.replace(" ", "_").replace('"', ""),
            filename.replace(" ", "_").replace('"', "")[:55],
            ext,
        )
        return fn

    def extension(self):
        extension = os.path.splitext(self.file.name)
        return extension

    def get_file_format(self):
        try:
            file_format = filetype.guess(self.file)
            if file_format is None:
                return "other"
        except FileNotFoundError:
            logger.error("file {} not found".format(self.file))
            return _("file not found")
        return file_format.mime

    def get_user(self):
        return self.dataset.owner

    def get_group(self):
        return self.dataset.group

    def __str__(self):
        return self.name

    def clone(self, new_ds):
        self.save()
        new_file = ContentFile(self.file.read())
        new_file.name = self.file.name
        df = self
        df.pk = None
        df.uuid = uuid.uuid4()
        df.file = new_file
        df.dataset = new_ds
        df.save()

    class Meta:
        permissions = (("view_dddatafile", "View Datafile"),)


class DataRepository(models.Model):
    name = models.CharField(max_length=400)
    logo = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.name


class DataType(models.Model):
    name = models.CharField(max_length=400)
    logo = models.ImageField(blank=True, null=True)

    # panels for wagtail ModelAdmin integration of django model
    # https://docs.wagtail.io/en/stable/reference/contrib/modeladmin/index.html
    panels = [
        FieldPanel("name"),
        FieldPanel("logo"),
    ]

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name


class License(models.Model):
    INDIVIDUAL = "IND"
    STANDARD = "STD"
    LICENSE_TYPE_CHOICES = (
        (INDIVIDUAL, _("individual")),
        (STANDARD, _("standard")),
    )

    license_type = models.CharField(
        max_length=4,
        choices=LICENSE_TYPE_CHOICES,
        default=STANDARD,
    )
    standard_license = models.ForeignKey(
        "pages.LicensePage",
        blank=True,
        null=True,
        related_name="license_license_page",
        on_delete=models.PROTECT,
    )
    license_text = BleachField(max_length=12000)
    license_name = models.CharField(max_length=400, blank=True)
    # TODO: license_slug field only unique for standard licenses, but not unique for individual licenses
    license_slug = models.CharField(max_length=200, blank=True)

    def get_datasets(self):
        return self.dataset_license.all()

    def get_license_name(self):
        if self.license_type == "STD":
            try:
                return self.standard_license.name
            except AttributeError:
                return "Standard license name not set"
        else:
            return "individual license"

    def get_license_title(self):
        if self.license_type == "STD":
            try:
                return self.standard_license.title
            except AttributeError:
                return "Standard license title not set"
        else:
            return "individual license"

    def get_license_slug(self):
        if self.license_type == "STD":
            try:
                return self.standard_license.slug
            except AttributeError:
                return "Standard license slug not set"
        else:
            return "license-individual"

    def get_zenodo_license_slug(self):
        if self.license_type == "STD":
            try:
                logger.debug(f"get_zenodo_license_slug: {self.standard_license.slug}")
                if self.standard_license.slug == "license-odc-by-v1-0":
                    return "odc-by-1.0"
                if self.standard_license.slug == "license-odbl":
                    return "odbl-1.0"
                return "License slug not found"
            except AttributeError:
                return "Standard license slug not set"
        else:
            return "license-individual"

    def get_license_text(self):
        if self.license_type == "STD":
            try:
                return self.standard_license.text
            except AttributeError:
                return "Standard license text not set"
        else:
            return self.license_text

    def list_licenses(self):
        return LicensePage.objects.all()

    def natural_key(self):
        return self.get_zenodo_license_slug()

    def __str__(self):
        return self.get_license_name()


class CollectionMethodsTagged(TagBase):
    class Meta:
        verbose_name = "Collection Methods Tag"
        verbose_name_plural = "Collection Methods Tags"

    panels = [
        FieldPanel("name"),
    ]


class CollectionMethodsTags(GenericTaggedItemBase):
    tag = models.ForeignKey(
        "CollectionMethodsTagged",
        null=True,
        related_name="collectionmethodstags_collectionmethodstagged",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.tag.name


class AnalysisMethodsTagged(TagBase):
    class Meta:
        verbose_name = "Analysis Methods Tag"
        verbose_name_plural = "Analysis Methods Tags"

    panels = [
        FieldPanel("name"),
    ]


class AnalysisMethodsTags(GenericTaggedItemBase):
    tag = models.ForeignKey(
        "AnalysisMethodsTagged",
        related_name="analysismethodstags_analysismethodstagged",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.tag.name


class DisciplinesTagged(TagBase):
    class Meta:
        verbose_name = "Disciplines Tag"
        verbose_name_plural = "Disciplines Tags"

    panels = [
        FieldPanel("name"),
    ]


class DisciplinesTags(GenericTaggedItemBase):
    tag = models.ForeignKey(
        "DisciplinesTagged",
        null=True,
        related_name="disciplinestags_disciplinestagged",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.tag.name


class Documentation(models.Model):
    DOCU_TYPES = (
        ("GEN", _("general documentation")),
        ("LEG", _("legal documents")),
        ("BPR", _("best practices")),
        ("USE", _("user documentation")),
    )
    title = models.CharField(max_length=200)
    slug = models.SlugField(
        max_length=200,
        help_text=_("Text used for URL generation, please use '_' between words"),
    )
    subtitle = models.CharField(max_length=200, blank=True)
    docu_type = models.CharField(
        max_length=3,
        choices=DOCU_TYPES,
        default="GEN",
    )
    file = models.FileField(blank=True, null=True)  # upload_to='datasets/')
    description = BleachField(max_length=2000, blank=True)
    text = BleachField(blank=True, max_length=12000)
    public = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class DataSetPublication(models.Model):
    PUB_TYPES = (
        ("DIS", _("discusses dataset")),
        ("USE", _("uses dataset")),
        ("DES", _("describes dataset")),
        # ('MET', 'similiar methodology'),
        # ('SIM', 'similiar approach'),
    )
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.ForeignKey("dddatasets.DataSet", on_delete=models.CASCADE)
    publication = models.ForeignKey(
        "ddpublications.Publication", on_delete=models.CASCADE, null=True
    )
    pub_type = models.CharField(
        max_length=3,
        choices=PUB_TYPES,
        default="DIS",
        verbose_name="Type of publication",
        help_text="Relation between dataset and publication",
    )

    def clone(self, new_ds):
        dspub = self
        dspub.id = None
        dspub.uuid = uuid.uuid4()
        dspub.dataset = new_ds
        dspub.save()

    def __str__(self):
        return format_html("{} ({})", self.publication, self.get_pub_type_display())


class DataSetExternalLink(models.Model):
    dataset = models.ForeignKey("DataSet", on_delete=models.CASCADE)
    link = models.URLField(help_text=_("URL to external Dataset"))
    site_title = models.CharField(
        max_length=200,
        help_text=_("Title of the external repository or web site"),
    )
    site_description = BleachField(
        max_length=400,
        blank=True,
        help_text=_("Brief description of the external repository or web site"),
    )
    site_text = BleachField(
        blank=True,
        max_length=2000,
        help_text=_("In-depth description of the external repository or web site"),
    )

    def __str__(self):
        return self.site_title


class DataSetAccessRequest(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.ForeignKey("DataSet", on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    notification = models.ForeignKey(
        "ddcomments.Notification", on_delete=models.PROTECT, null=True
    )

    def save(self, *args, **kwargs):
        # send notification on save
        ct = ContentType.objects.get_for_model(self.dataset)
        text = _("Dataset access request for dataset {}".format(self.dataset))
        notification = Notification(
            owner=self.user,
            content_type=ct,
            object_id=self.dataset.id,
            text=text,
            notification_type=Notification.ACCESS_REQUEST,
            recipient=self.dataset.owner,
        )
        notification.save()
        self.notification = notification
        logger.debug("[DataSetAccessRequest] send mail")
        # send email to dataset owner
        subject = text
        message = _(
            'A user requested access to your dataset "{}". Please grant or deny access on the dataset\'s collaboration tab in the data preperation area {}.'.format(
                self.dataset, self.dataset.get_absolute_url_prep_access()
            )
        )
        # send email to owner, curators also
        email_to = self.dataset.get_main_category().get_curators_emails()
        email_to.append(self.dataset.owner.get_email())
        send_update_email(subject, message, email_to)
        logger.debug("[DataSetAccessRequest] save")
        models.Model.save(self, *args, **kwargs)
        logger.debug("[DataSetAccessRequest] saved")
        # send email to user
        message_user = _(
            'Your request to access the dataset "{}" ({}) has been sent to the dataset owner.'.format(
                self.dataset, self.dataset.get_absolute_url()
            )
        )
        send_update_email(subject, message_user, [self.user.email])

    def __str__(self):
        ar_string = "Access request pending since {}".format(
            self.created_at.strftime("%d.%m.%Y, %H:%M:%S")
        )
        return _(ar_string)


class DataSetPublicationRequest(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.ForeignKey("DataSet", on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey("Category", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    notification = models.ForeignKey(
        "ddcomments.Notification", on_delete=models.PROTECT, null=True
    )

    def save(self, *args, **kwargs):
        logger.debug("[DataSetPublicationRequest] start save")
        # send notification on save
        ct = ContentType.objects.get_for_model(self.dataset)
        text = _("Publication request for category {}".format(self.category))
        notification = Notification(
            owner=self.user,
            content_type=ct,
            object_id=self.dataset.id,
            text=text,
            notification_type=Notification.PUB_REQUEST,
        )
        logger.debug("[DataSetPublicationRequest] notification save")
        notification.save()
        logger.debug("[DataSetPublicationRequest] notification saved")
        # send email to data owner
        owner_subject = "[Data Submission] {}".format(
            text,
        )
        owner_message = _(
            "Your dataset {} has been submitted for publication in the category {}:\n\n{}\n\nThe category's curators will conduct now a technical review of the uploaded data and metadata.".format(
                self.dataset, self.category, self.dataset.get_absolute_url_curation()
            )
        )
        send_update_email(owner_subject, owner_message, (self.user.get_email(),))

        self.notification = notification
        # send email to all curators
        subject = "[Curation] {}".format(
            text,
        )
        message = _(
            "The dataset {} has been submitted for publication in the category {}:\n\n{}\n\nPlease conduct a technical review of the uploaded data and metadata prior to your decision about the publication.".format(
                self.dataset, self.category, self.dataset.get_absolute_url_curation()
            )
        )
        logger.debug(self.category.get_curators_emails())
        email_to = self.category.get_curators_emails()
        logger.debug("[DataSetPublicationRequest] send mail")
        send_update_email(subject, message, email_to)
        logger.debug("[DataSetPublicationRequest] save")
        models.Model.save(self, *args, **kwargs)
        logger.debug("[DataSetPublicationRequest] saved")

    def __str__(self):
        ar_string = "Publication request for {} by {} pending since {}".format(
            self.dataset, self.user, self.created_at.strftime("%d.%m.%Y, %H:%M:%S")
        )
        return _(ar_string)


class DataSetCreator(models.Model):
    class Meta:
        ordering = ["list_position", "name"]

    PERSON = "PER"
    INSTITUTION = "INS"

    CREATOR_TYPE_CHOICES = [
        (PERSON, _("Person")),
        (INSTITUTION, _("Institution")),
    ]
    name = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200, blank=True)
    url = models.URLField(blank=True)
    list_position = models.IntegerField(null=True, blank=True)

    creator_type = models.CharField(
        max_length=3,
        choices=CREATOR_TYPE_CHOICES,
        default="PER",
    )
    dataset = models.ForeignKey(
        "DataSet",
        on_delete=models.CASCADE,
        related_name="dataset_creator",
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name

    def clone(self, new_ds):
        logger.debug(f"Clone creator {self} and attach to {new_ds}")
        dsc = self
        dsc.id = None
        dsc.dataset = new_ds
        dsc.save()


def dsa_file_path(instance, filename):
    return "datasets/dsa/" + str(uuid.uuid4())


class DataSubmissionAgreement(models.Model):
    text = models.TextField(default=_("Not generated yet"))
    birthdate = models.DateField()
    postal_address = BleachField(max_length=1200)
    project_type = BleachField(max_length=1200)
    project_title = BleachField(max_length=1200)
    dsa_accepted = models.BooleanField(
        default=False, verbose_name=_("I do accept the Data Submission Agreement.")
    )
    dsa_accepted_date = models.DateField(blank=True, null=True)
    pdf = models.FileField(
        upload_to=dsa_file_path, storage=datafile_storage, blank=True
    )


class Archive(models.Model):
    class Meta:
        ordering = ["short_name", "name"]

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    short_name = models.CharField(max_length=20)
    name = models.CharField(max_length=200)
    url = models.URLField(blank=True)
    country = models.CharField(max_length=200)

    def __str__(self):
        return self.short_name


class ArchivalSignature(models.Model):
    class Meta:
        ordering = ["archive", "fond", "opis", "delo", "list"]

    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.ForeignKey(
        "DataSet", on_delete=models.CASCADE, related_name="archival_signature_dataset"
    )
    archive = models.ForeignKey(
        Archive, on_delete=models.CASCADE, related_name="archive_archival_signature"
    )
    fond = models.CharField(max_length=200)
    opis = models.CharField(blank=True, null=True, max_length=20)
    delo = models.CharField(blank=True, null=True, max_length=20)
    list = models.CharField(blank=True, null=True, max_length=20)
    url = models.URLField(blank=True)

    def clone(self, new_ds):
        archival_signature = self
        archival_signature.id = None
        archival_signature.dataset = new_ds
        archival_signature.save()

    def __str__(self):
        return f"{self.id}"

    def html(self):
        logger.debug("AS public %s", self.__dict__)

        html_string = render_to_string(
            "dddatasets/_archival_signature.html", {"elem": self}
        )
        return html_string


class DataSetPrevOwner(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.ForeignKey(
        "DataSet", on_delete=models.CASCADE, related_name="prev_owner_dataset"
    )
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="prev_owner_user"
    )
    until = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user} - {self.until}"


class DataSet(models.Model):
    OPEN_ACCESS = "OA"
    METADATA_ONLY = "MO"
    RESTRICTED_ACCESS = "RA"
    DATA_ACCESS_CHOICES = [
        (OPEN_ACCESS, _("Open Access")),
        (METADATA_ONLY, _("Metadata only")),
        (RESTRICTED_ACCESS, _("Restricted access")),
    ]
    # Dataset / Data Review
    DATASET = "DS"
    DATAREVIEW = "DR"
    EXTERNAL = "EX"
    DATASET_TYPE_CHOICES = [
        (DATASET, _("Data Set")),
        (DATAREVIEW, _("Data Review")),
        (EXTERNAL, _("External Data Set")),
    ]

    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False
    )  # uuid _not_ as pk as this disturbs django 3rd party apps
    owner = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    dsa = models.OneToOneField(
        "DataSubmissionAgreement",
        blank=True,
        null=True,
        related_name="dataset_dsa",
        on_delete=models.CASCADE,
    )
    data_access = models.CharField(
        max_length=2,
        choices=DATA_ACCESS_CHOICES,
        default="RA",
    )
    dataset_type = models.CharField(
        max_length=2,
        choices=DATASET_TYPE_CHOICES,
        default="DS",
    )
    doi = models.CharField(max_length=200, blank=True)
    dhdoi = models.CharField(max_length=200, blank=True)
    zenodo_doi = models.CharField(max_length=200, blank=True)
    institution = models.ForeignKey(
        "ddusers.Institution",
        blank=True,
        null=True,
        related_name="dataset_institution",
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=200)
    subtitle = models.CharField(max_length=200, blank=True)
    shorttitle = models.CharField(max_length=50)
    image = models.ImageField(blank=True, null=True)
    countries = models.ManyToManyField(
        "ddusers.Country", related_name="dataset_country", blank=True
    )
    # access = models.CharField(max_length=3, choices=ACCESS_RULES_CHOICES, default='NET')
    link = models.URLField(
        blank=True,
        null=True,
        help_text=_("Please provide an URL starting with http, https, or ftp."),
        verbose_name=_("Link to the reviewed or external dataset"),
    )
    external_publisher = models.CharField(
        max_length=400,
        help_text=_(
            'Name of the repository which is holding external data'
        ),
        blank="True",
    )
    external_license = BleachField(
        help_text=_(
            'License under which external data is licensed'
        ),
        blank="True",
    )
    external_version = models.CharField(
        max_length=20,
        help_text=_(
            'Version of the reviewed dataset'
        ),
        blank="True",
    )
    data_repository = models.ForeignKey(
        "DataRepository",
        blank=True,
        null=True,
        related_name="dataset_datarepository_data_repository",
        on_delete=models.CASCADE,
    )
    date_of_data_creation_from = models.DateField(
        blank=True,
        null=True,
        help_text=_("Date format YYYY-MM-DD"),
        verbose_name=_("Date/period of data creation (from)"),
    )
    date_of_data_creation_to = models.DateField(
        blank=True,
        null=True,
        help_text=_("Date format YYYY-MM-DD"),
        verbose_name=_("Date/period of data creation (to)"),
    )
    date_of_data_creation_text = models.CharField(
        max_length=400,
        help_text=_(
            'If you cannot specify the exact date please indicate the approximate date here (eq. "October 2010")'
        ),
        blank="True",
    )

    major_version = models.IntegerField(default=1)
    minor_version = models.IntegerField(default=0)

    @property
    def version(self) -> str:
        return f"{self.major_version}.{self.minor_version}"

    datatypes = models.ManyToManyField(
        "DataType", verbose_name="Data types", related_name="dataset_datatypes_datatype"
    )
    datatype_text = models.CharField(max_length=400, blank=True)
    description = BleachField(verbose_name="Technical description", max_length=2400)
    keywords = TaggableManager("KeywordTags", blank=True, through=KeywordTags)
    sources_of_data = BleachField(max_length=6000)
    languages_of_data = TaggableManager(
        "LanguageTags", blank=True, through=LanguageTags
    )
    methods_of_data_collection = TaggableManager(
        "CollectionMethodsTags", blank=True, through=CollectionMethodsTags
    )
    methods_of_data_analysis = TaggableManager(
        "AnalysisMethodsTags", blank=True, through=AnalysisMethodsTags
    )
    disciplines = TaggableManager(
        "DisciplinesTags", blank=True, through=DisciplinesTags
    )
    time_period_text = models.CharField(
        max_length=400,
        blank=True,
        help_text=_(
            'If you cannot specify the exact date please indicate the approximate date here (eq. "October 2010")'
        ),
        verbose_name="Time period covered (text)",
    )
    time_period_from = models.DateField(
        blank=True,
        null=True,
        help_text=_("Date format YYYY-MM-DD"),
        verbose_name=_("Time period covered in data collection (from)"),
    )
    time_period_to = models.DateField(
        blank=True,
        null=True,
        help_text=_("Date format YYYY-MM-DD"),
        verbose_name=_("Time period covered in data collection (to)"),
    )
    license = models.ForeignKey(
        "License",
        related_name="dataset_license",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )

    related_dataset_text = BleachField(blank=True, max_length=1200)
    related_dataset = models.ManyToManyField(
        "DataSet",
        related_name="dataset_related_dataset_dataset",
        blank=True,
        help_text=_("Undo choice with CTRL–Mouse Click"),
    )
    related_projects = BleachField(blank=True, max_length=1200)
    institutional_affiliation = BleachField(blank=True, max_length=1200)
    sponsors = models.ManyToManyField(
        "Sponsor", blank=True, related_name="dataset_sponsors"
    )
    funding = BleachField(blank=True, max_length=1200)
    publications = models.ManyToManyField(
        "ddpublications.Publication",
        blank=True,
        through="dddatasets.DataSetPublication",
    )
    comments = GenericRelation("ddcomments.Comment", related_query_name="comments")
    dataset_management_object = models.ForeignKey(
        "DataSetManagementObject",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    published = models.BooleanField(default=False)
    published_main_category = models.ForeignKey(
        "Category",
        related_name="dataset_published_main_category",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    published_categories = models.ManyToManyField(
        "Category", related_name="dataset_published_categories", blank=True
    )
    created_at = models.DateField(default=timezone.now)
    publication_date = models.DateField(null=True, blank=True)
    publication_accepted = models.BooleanField(default=False)
    publication_accepted_by = models.ForeignKey(
        User,
        related_name="dataset_publication_accepted_by",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    COPYRIGHT_DECLARATION_STRING = _("I declare, that no copyrights are violated.")
    copyright_declaration = models.BooleanField(
        default=False, verbose_name=COPYRIGHT_DECLARATION_STRING
    )
    copyright_declaration_text = BleachField(
        default="I declare, that no copyrights are violated.",
        blank=True,
        max_length=400,
    )

    PRIVACY_RIGHTS_DECLARATION_STRING = _(
        "I declare, that no privacy and data protection regulations are violated."
    )
    privacy_rights_declaration = models.BooleanField(
        default=False,
        verbose_name=PRIVACY_RIGHTS_DECLARATION_STRING,
    )
    privacy_rights_declaration_text = BleachField(
        blank=True,
        max_length=400,
    )

    republish_as_oa = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ("ra_view_dataset", _("View data set in restricted access mode")),
        )
        ordering = ["-publication_date"]

    def get_zenodo_doi_url(self):
        return f"https://doi.org/{self.zenodo_doi}"

    def get_dhrep_doi_url(self):
        return f"https://doi.org/{self.dhdoi}"

    def get_archival_signatures(self):
        return ArchivalSignature.objects.filter(dataset=self)

    def get_license_name_from_ds(self):
        if not self.license:
            return "No license"
        else:
            return self.license.get_license_name()

    def get_license_text_from_ds(self):
        if not self.license:
            return "No license"
        else:
            return self.license.get_license_text()

    def license_is_valid(self):
        """use the DD_LICENSE_MATRIX map to check if a license
        (given by slug) is in the list of a data access model
        """
        license_valid = False
        if not self.license:
            # license = None is only valid in "nolicense" exists in matrix list
            if "nolicense" in settings.DD_LICENSE_MATRIX[self.data_access]:
                license_valid = True
        elif (
            # if license is set check validity
            self.license.get_license_slug()
            in settings.DD_LICENSE_MATRIX[self.data_access]
        ):
            license_valid = True

        return license_valid

    def generate_dsa(self):
        if self.dataset_type == "DR":
            dsa_page = PDFTemplate.objects.get(slug="dsa-review-pdf-template")
        else:
            dsa_page = PDFTemplate.objects.get(slug="dsa-pdf-template")

        if self.data_access == self.OPEN_ACCESS:
            access_text_slug = "access-open-access"
        elif self.data_access == self.RESTRICTED_ACCESS:
            access_text_slug = "access-restricted-access"
        elif self.data_access == self.METADATA_ONLY:
            access_text_slug = "access-metadata-only"
        else:
            access_text_slug = ""

        try:
            access_text_page = ManualPage.objects.get(slug=access_text_slug)
            access_text = str(access_text_page.body)
        except ManualPage.DoesNotExist:
            access_text = ""

        files_info = ""
        for datafile in self.get_datafiles():
            size = datafile.file.size
            name = datafile.name
            files_info += "- {}, {} bytes\n\n".format(name, size)
        self.dsa.dsa_accepted_date = localtime().date()
        self.dsa.text = f"Data Submission Agreement has been generated on {self.dsa.dsa_accepted_date}"
        self.dsa.save()

        description = re.sub(
            r"([\w.]+\@[\w.]+)", '#link("mailto:\g<1>")', self.description
        )
        vars_dict = {
            "[[NAME]]": self.owner.get_academic_name(),
            "[[EMAIL]]": self.owner.get_email(),
            "[[BIRTHDATE]]": str(self.dsa.birthdate),
            "[[ADDRESS]]": self.dsa.postal_address,
            "[[PROJECT_TYPE]]": self.dsa.project_type,
            "[[PROJECT_TITLE]]": self.dsa.project_title,
            "[[DESCRIPTION]]": description,
            "[[FILES]]": files_info,
            "[[AFFILIATION]]": self.institutional_affiliation,
            "[[SPONSORS]]": self.funding,
            "[[ACCESS_MODEL]]": self.get_data_access_display(),
            "[[ACCESS_TEXT]]": strip_tags(access_text),
            "[[LICENSE]]": html.unescape(strip_tags(self.get_license_name_from_ds())),
            "[[LICENSE_TEXT]]": html.unescape(
                strip_tags(self.get_license_text_from_ds())
            ),
            "[[DS_VERSION]]": self.version,
            "[[DS_URL]]": self.get_absolute_url(),
            "[[DS_TITLE]]": self.title,
            "[[DS_SUBTITLE]]": self.subtitle,
            "[[DSA_DATE]]": str(self.dsa.dsa_accepted_date),
        }
        dsa_text = str(dsa_page.content)

        for key, value in vars_dict.items():
            dsa_text = dsa_text.replace(key, value)
        # self.dsa.text = dsa_text
        data = dict()

        data["text"] = dsa_text
        data["title"] = "Discuss Data: Data Submission Agreement"

        url = "http://pdf_service:5150/pdfgen"
        headers = {"Content-type": "application/json"}

        pdf_response = requests.post(url, headers=headers, data=json.dumps(data))
        file_name = "{}-data-submission-agreement.pdf".format(self.uuid)
        path = os.path.join(settings.DATA_ROOT, "datasets", file_name)
        f = open(path, "wb")
        f.write(pdf_response.content)
        f.close()
        self.dsa.pdf = path

        self.dsa.save()

    def pub_request_pending(self):
        try:
            pub_request = DataSetPublicationRequest.objects.get(dataset=self)
            if pub_request:
                return True
        except ObjectDoesNotExist:
            pass
        return False

    def is_top_pub_version(self):

        return (
            self.version
            == self.dataset_management_object.get_top_version_published_dataset().version
        )

    def get_top_pub_version(self):
        return self.dataset_management_object.get_top_version_published_dataset()

    def is_frozen(self):
        if self.published:
            return True
        if self.publication_accepted:
            return True
        if self.pub_request_pending():
            return True
        return False

    def get_dsa(self):
        return DataSubmissionAgreement.objects.get(dataset=self)

    def get_escaped_description(self):
        description = self.description.replace("\n", "<br>")
        return description

    def get_creators(self):
        return DataSetCreator.objects.filter(dataset=self)

    def creators_str(self):
        creators_list = list()
        for creator in self.get_creators():
            if creator.creator_type == creator.PERSON:
                if creator.first_name:
                    creators_list.append(
                        "{} {}".format(creator.first_name, creator.name)
                    )
                else:
                    creators_list.append(creator.name)
            else:
                creators_list.append(creator.name)
        return ", ".join(creators_list)

    def get_all_access_requests(self):
        return DataSetAccessRequest.objects.filter(dataset=self)

    def get_access_request(self, user):
        return DataSetAccessRequest.objects.filter(dataset=self, user=user).first()

    def clear_user_permissions(self, user):
        for perm in get_perms(user, self):
            remove_perm(perm, user, self)

    def assign_user_permissions(self, user, perm):
        # assign new permission
        assign_perm(perm, user, self)

    def get_ra_with_perms(self):
        # get all users with permissions, but exclude owner
        return get_users_with_perms(self).exclude(uuid=self.owner.uuid)

    def user_has_ra_view_right(self, user):
        return user.has_perm("ra_view_dataset", self)

    def user_has_admin_right(self, user):
        if user == self.dataset_management_object.owner:
            return True
        return user.has_perm("admin_dsmo", self.dataset_management_object)

    def class_name(self):
        return self.__class__.__name__

    def is_main_published(self):
        """check if a dataset is the main published ds of a dsmo"""
        return bool(self.dataset_management_object.main_published_ds == self)

    def generate_typst_table(self):
        output = render_to_string("dddatasets/_metadata_table_typst.typ", {"ds": self})
        return output

    def generate_html_string(self):
        class PDFForm(ModelForm):
            class Meta:
                model = DataSet
                exclude = (
                    "html_string",
                    # "owner",
                    # "group",
                    # "institution",
                    # "title",
                    # "title_en",
                    # "title_ru",
                    # "subtitle",
                    # "subtitle_en",
                    # "subtitle_ru",
                    # "image",
                    # "categories",
                    # "countries",
                    # "description",
                )

        form = PDFForm(instance=self)
        html_string = render_to_string(
            "dddatasets/pdf_dataset.html", {"fields": form, "ds": self}
        )

        return html_string

    # commented out because of mypy error
    # def get_users(self):
    #    # strange import to avoid getting caught in circular import triggerd by guardian; conflict because guardian uses User Model defined in this file
    #    # TODO: define User model in seperate file
    #    from .views import dataset_get_users
    #
    #    return dataset_get_users(self)

    def get_prep_comments_all(self):
        # return all prep comments in reverse date order
        ct = ContentType.objects.get_for_model(DataSet)
        return Comment.objects.filter(
            content_type=ct,
            object_id=self.id,
        ).order_by("-date_added")

    def get_prep_comments_root(self):
        # return prep comments root nodes in reverse date order
        ct = ContentType.objects.get_for_model(DataSet)
        return (
            Comment.objects.root_nodes()
            .filter(
                content_type=ct,
                object_id=self.id,
            )
            .order_by("-date_added")
        )

    def get_public_comments_all(self):
        # return all public comments in reverse date order
        ct = ContentType.objects.get_for_model(DataSet)
        return Comment.objects.filter(
            content_type=ct,
            object_id=self.id,
            comment_type__in=(Comment.PUBLIC, Comment.PERMANENT),
        ).order_by("-date_added")

    def get_public_comments_root(self):
        # return public comments root nodes in reverse date order
        ct = ContentType.objects.get_for_model(DataSet)
        return (
            Comment.objects.root_nodes()
            .filter(
                content_type=ct,
                object_id=self.id,
                comment_type__in=(Comment.PUBLIC, Comment.PERMANENT),
            )
            .order_by("-date_added")
        )

    def get_pdf_file_object(self):
        if self.dataset_type == "DR":
            return self.get_review_file()
        return DataFile.objects.filter(dataset=self, data_file_type="PDF")[0]

    def get_pdf_file_name(self):
        # max length for DataFile.name db field set to 200
        # length of file name template is aprox. 35 chars
        title_max_length = 200 - 35
        # slice the title-slug to title_max_length
        title_slug = slugify(self.shorttitle)[:title_max_length]
        if self.dataset_type == "DR":
            return "DiscussData-{}-{}.pdf".format(self.owner.last_name, title_slug)
        return "DiscussData-{}-v{}-description.pdf".format(title_slug, self.version)

    def get_zip_file_name(self):
        return "DiscussData-{}-v{}-all-files.zip".format(
            slugify(self.title), self.version
        )

    # def generate_pdf(self):
    #    html = HTML(string=self.html_string, base_url=request.build_absolute_uri())
    #    file_name = "/tmp/dataset_%s.pdf" % (self.id, )
    #    return html.write_pdf()

    def get_absolute_url(self):
        return settings.DISCUSS_DATA_HOST + reverse(
            "dddatasets:detail", args=[str(self.uuid)]
        )

    def get_absolute_url_prep(self):
        return settings.DISCUSS_DATA_HOST + reverse(
            "dddatasets:prep_edit", args=[str(self.uuid)]
        )

    def get_absolute_url_prep_versions(self):
        return settings.DISCUSS_DATA_HOST + reverse(
            "dddatasets:prep_edit_versions", args=[str(self.uuid)]
        )

    def get_absolute_url_prep_access(self):
        return (
            settings.DISCUSS_DATA_HOST
            + reverse("dddatasets:prep_edit_publish_access", args=[str(self.uuid)])
            + "#accessrequests"
        )

    def get_absolute_url_prep_collaboration(self):
        return settings.DISCUSS_DATA_HOST + reverse(
            "dddatasets:prep_edit_collaboration", args=[str(self.uuid)]
        )

    def get_absolute_url_curation(self):
        return settings.DISCUSS_DATA_HOST + reverse(
            "dddatasets:prep_curation", args=[str(self.uuid)]
        )

    def get_fulltitle(self):
        if self.subtitle:
            fulltitle = "%s – %s" % (self.title, self.subtitle)
        else:
            fulltitle = self.title
        return fulltitle

    def create_new_version(self):
        logger.debug("Create new dataset version of %s", self)
        # orig_id = self.id
        datafiles = self.get_datafiles()
        datatypes = self.get_datatypes()
        countries = self.get_countries()
        keyword_tags = self.get_keyword_tags()
        language_tags = self.get_language_tags()
        disciplines_tags = self.get_disciplines_tags()
        methods_of_data_collection = self.get_methods_of_data_collection_tags()
        methods_of_data_analysis = self.get_methods_of_data_analysis_tags()
        data_creators = self.get_creators()
        dspublications = self.get_dataset_publications()
        archival_signatures = self.get_archival_signatures()

        # create new version
        ds = self
        ds.pk = None
        ds.published = False
        ds.minor_version = self.dataset_management_object.get_top_version() + 1
        ds.publication_accepted = False
        ds.publication_accepted_by = None
        ds.copyright_declaration = False
        ds.publication_date = None
        ds.created_at = timezone.now()
        ds.doi = ""
        ds.dhdoi = ""
        ds.dsa = None
        ds.published_main_category = None
        ds.privacy_rights_declaration = False
        ds.save()
        ds.published_categories.set([])

        for datafile in datafiles:
            datafile.clone(ds)

        for datatype in datatypes:
            ds.datatypes.add(datatype)

        for country in countries:
            ds.countries.add(country)

        for tag in keyword_tags:
            ds.keywords.add(tag)

        for tag in language_tags:
            ds.languages_of_data.add(tag)

        for tag in disciplines_tags:
            ds.disciplines.add(tag)

        for tag in methods_of_data_collection:
            ds.methods_of_data_collection.add(tag)

        for tag in methods_of_data_analysis:
            ds.methods_of_data_analysis.add(tag)

        logger.debug(f"data creators: {data_creators}")
        for creator in data_creators:
            logger.debug(f"cloning creator {creator}")
            creator.clone(ds)

        for archival_signature in archival_signatures:
            archival_signature.clone(ds)

        for pub in dspublications:
            logger.debug("PUB to be cloned %s", pub)
            pub.clone(ds)
        return ds

    def get_publications(self):
        return self.publications.all()

    def get_external_links(self):
        return DataSetExternalLink.objects.filter(dataset=self)

    def get_dataset_publications(self):
        return DataSetPublication.objects.filter(dataset=self)

    # deprecated begin
    def get_countries_list(self):
        return qs_to_str(self.get_countries(), "name")

    def get_keyword_tags_list(self):
        return qs_to_str(self.get_keyword_tags(), "name")

    def get_disciplines_tags_list(self):
        return qs_to_str(self.get_disciplines_tags(), "name")

    # deprecated end

    def get_countries(self):
        return self.countries.all().order_by("name")

    def get_keyword_tags(self):
        return self.keywords.all().order_by("name")

    def get_disciplines_tags(self):
        return self.disciplines.all().order_by("name")

    def get_language_tags(self):
        return self.languages_of_data.all().order_by("name")

    def get_methods_of_data_collection_tags(self):
        return self.methods_of_data_collection.all().order_by("name")

    def get_methods_of_data_analysis_tags(self):
        return self.methods_of_data_analysis.all().order_by("name")

    def get_datatypes(self):
        return self.datatypes.all()

    def get_data_citation(self):
        """
        This citation has seven components of which five are human readable:
        the author(s), title, year, data repository (or distributor), and version number.
        Two components are machine-readable (UNF = universal numerical fingerprint; DOI = Digital Object Identifier;
        ‘hdl’ refers to the international HANDLE.NET system).

        TODO: refactor to provide different citation styles,
        see https://gitlab.gwdg.de/discuss-data/discuss-data/-/issues/180
        """
        if self.doi:
            doi = self.doi
        else:
            doi = "00.00000/00000000-0000-0000-0000-000000000000"

        if self.publication_date:
            year = self.publication_date.year
        else:
            year = timezone.now().year

        if self.dataset_type == "DR":
            # owner is data review author
            creators = self.owner.get_academic_name()
            ex_version = ""
            if self.external_version:
                ex_version = f"v. {self.external_version}, "
            return format_html(
                f"{creators} ({year}): {self.get_fulltitle()} ({ex_version}published on {self.external_publisher}), v. {self.version}, Discuss Data, https://doi.org/{doi}"
            )

        creators = self.creators_str()
        return format_html(
                f"{creators} ({year}): {self.get_fulltitle()}, v. {self.version}, Discuss Data, https://doi.org/{doi}"
            )



    def get_data_citation_pdf(self):
        return self.get_data_citation()

    groups_dict = {
        "view": ("view_dataset",),
        "edit": ("view_dataset", "edit_dataset"),
        "admin": ("view_dataset", "edit_dataset", "admin_dataset"),
    }

    def create_groups(self, groups_dict):
        dsid = str(self.id)
        for gkey in groups_dict.keys():
            group_name = "%s_%s" % (dsid, gkey)
            logger.debug("create %s" % (group_name,))
            group = Group.objects.create(name=group_name, description=gkey)
            logger.debug("add %s to dataset" % (group_name,))
            self.groups.add(group)
            # assign permissions to group
            for perm in groups_dict[gkey]:
                logger.debug(
                    "assign %s to %s"
                    % (
                        perm,
                        group,
                    )
                )
                assign_perm(perm, group, self)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if not self.dataset_management_object:
            dsmo = DataSetManagementObject()
            dsmo.owner = self.owner
            dsmo.save()
            self.dataset_management_object = dsmo

        # if self.data_access != self.METADATA_ONLY and not self.license:
        #     # set odc-by as default license or individual if no odc-by licensepage exists
        #     try:
        #         license_page = LicensePage.objects.get(slug="license-odc-by-v1-0")
        #         license = License.objects.create(license_type=License.STANDARD, standard_license=license_page)
        #         license.save()
        #     except LicensePage.DoesNotExist():
        #         license = License.objects.create(license_type=License.INDIVIDUAL)
        #     self.license = license

        if self.image and hasattr(self.image, "url"):
            # in situ thumbnail generation at upload
            im = Image.open(self.image.path)
            thumbnail = cropped_thumbnail(im, [320, 160])
            thumbnail.save(self.image.path)

        # for using save a second time, force_insert may not be used
        # see https://code.djangoproject.com/ticket/28253#comment:4
        if kwargs.get("force_insert"):
            kwargs.pop("force_insert")
        super().save(*args, **kwargs)

    def get_all_files(self):
        if self.dataset_type == "DR":
            logger.debug(f"[DataSet] Datareview, exclude generated files: {self.get_datafiles()}")
            return self.get_datafiles()
        datafiles = DataFile.objects.filter(dataset=self).order_by("name")
        logger.debug(f"[DataSet] Dataset, get files: {datafiles}")
        return datafiles

    def get_review_file(self):
        return DataFile.objects.get(dataset=self, data_file_type=DataFile.FILE_TYPE_DATA_REVIEW)


    def get_datafiles(self):
        datafiles = (
            DataFile.objects.filter(dataset=self)
            .order_by("name")
            .exclude(data_file_type=DataFile.FILE_TYPE_GENERATED_PDF)
            .exclude(data_file_type=DataFile.FILE_TYPE_GENERATED_ZIP)
        )

        # no datafiles/converted files if access is "metadata only"
        if self.data_access == self.METADATA_ONLY:
            datafiles = datafiles.exclude(
                data_file_type=DataFile.FILE_TYPE_DATA
            ).exclude(data_file_type=DataFile.FILE_TYPE_CONVERTED)

        return datafiles

    def get_all_datafiles(self):
        return (
            DataFile.objects.filter(dataset=self)
            .order_by("name")
            .exclude(data_file_type=DataFile.FILE_TYPE_GENERATED_PDF)
            .exclude(data_file_type=DataFile.FILE_TYPE_GENERATED_ZIP)
        )

    def get_datafile(self, uuid):
        return DataFile.objects.filter(dataset=self, uuid=uuid).get()

    def get_datafiles_count(self):
        return self.get_datafiles().count()

    def get_owner_name(self):
        return f"{self.owner.get_academic_name}"

    def dublin_core(self):
        dc_dict = dict(
            titles=[self.title_en, self.subtitle_en],
            # creators = [self.get_owners()],
        )
        # dc_dict['titles'] =  "<dc:title>%s</dc:title>\n" % (self.title,)
        # dc_str += "<dc:subtitle>%s</dc:subtitle>\n" % (self.subtitle,)
        return simpledc.tostring(dc_dict)

    def get_dataset_publications_typst(self):
        output = ""
        if self.get_dataset_publications():
            for elem in self.get_dataset_publications():
                output += f"- {strip_tags(elem.publication.citation())}\n"
        else:
            return _("No publications were added to this Dataset.")
        return output

    def create_datareview_pdf(self):
        review_file = self.get_review_file().file.path
        logger.debug(f"[DATAREVIEW PDF] get review file: {self.get_review_file()}")
        descr_page = PDFTemplate.objects.get(slug="ds-review-pdf-template")
        version_history_list = [str(elem) for elem in self.get_published_versions()]
        description = re.sub(
            r"([\w.]+\@[\w.]+)", '#link("mailto:\g<1>")', self.description
        )
        vars_dict = {
            "[[CITATION]]": self.get_data_citation_pdf(),
            "[[DESCRIPTION]]": description,
            "[[LICENSE]]": html.unescape(strip_tags(self.get_license_name_from_ds())),
        }
        descr_text = str(descr_page.content)

        for key, value in vars_dict.items():
            descr_text = descr_text.replace(key, value)
        # self.dsa.text = dsa_text
        data = dict()

        data["text"] = descr_text
        data["title"] = "Discuss Data: Data Review" # TODO: not used?

        path = create_pdf(data, self.uuid, "datasets", "data-review")
        datafile, created = DataFile.objects.get_or_create(
            dataset=self, data_file_type=DataFile.FILE_TYPE_DATA_REVIEW
        )
        # merge PDFs
        from pypdf import PdfWriter
        merger = PdfWriter()

        for pdf in [path, review_file]:
            merger.append(pdf)
        merger.write(path)
        merger.close()

        datafile.file = path
        datafile.name = self.get_pdf_file_name()
        datafile.save()
        return datafile



    def create_description_pdf(self):
        descr_page = PDFTemplate.objects.get(slug="ds-description-pdf-template")

        if self.data_access == self.OPEN_ACCESS:
            access_text_slug = "access-open-access"
        elif self.data_access == self.RESTRICTED_ACCESS:
            access_text_slug = "access-restricted-access"
        elif self.data_access == self.METADATA_ONLY:
            access_text_slug = "access-metadata-only"
        else:
            access_text_slug = ""

        try:
            access_text_page = ManualPage.objects.get(slug=access_text_slug)
            access_text = str(access_text_page.body)
        except ManualPage.DoesNotExist:
            access_text = ""

        files_info = ""
        for datafile in self.get_datafiles():
            size = datafile.file.size
            name = datafile.name
            files_info += "- {}, {} bytes\n\n".format(name, size)

        version_history_list = [str(elem) for elem in self.get_published_versions()]

        description = re.sub(
            r"([\w.]+\@[\w.]+)", '#link("mailto:\g<1>")', self.description
        )
        vars_dict = {
            "[[DS_TITLE]]": self.title,
            "[[DS_SUBTITLE]]": self.subtitle,
            "[[CITATION]]": self.get_data_citation_pdf(),
            "[[DS_URL]]": self.get_absolute_url(),
            "[[DS_VERSION]]": f"Version {self.version}, published: {self.publication_date}",
            "[[DESCRIPTION]]": description,
            "[[FILES]]": files_info,
            "[[METADATA]]": strip_tags(self.generate_typst_table()),
            "[[VERSION_HISTORY]]": "\n– ".join(version_history_list),
            # "[[PUBLICATIONS]]": self.get_dataset_publications_typst(),
            "[[LICENSE]]": html.unescape(strip_tags(self.get_license_name_from_ds())),
            "[[LICENSE_TEXT]]": html.unescape(
                strip_tags(self.get_license_text_from_ds())
            ),
        }
        descr_text = str(descr_page.content)

        for key, value in vars_dict.items():
            descr_text = descr_text.replace(key, value)
        # self.dsa.text = dsa_text
        data = dict()

        data["text"] = descr_text
        data["title"] = "Discuss Data: Dataset Description"

        path = create_pdf(data, self.uuid, "datasets", "dataset-description")

        # create data file
        df, created = DataFile.objects.get_or_create(
            dataset=self, data_file_type=DataFile.FILE_TYPE_GENERATED_PDF
        )
        df.name = self.get_pdf_file_name()
        df.file = path
        df.save()
        return df

    def get_description_pdf_file(self):
        try:
            # TODO: no reliable lookup
            pdf_file = DataFile.objects.filter(dataset=self, data_file_type="PDF")[0]
        except Exception:
            pdf_file = None
        return pdf_file

    def create_files_zip(self):
        ds = self
        file_name = "{}-all-files.zip".format(ds.uuid)
        path = os.path.join(settings.DATA_ROOT, "datasets", file_name)
        # create zip file
        with ZipFile(path, "a") as zipfile:
            # add autogenerated pdf file
            pdf_file = self.get_description_pdf_file()
            zipfile.writestr(pdf_file.name, pdf_file.file.read())
            for file in ds.get_datafiles():
                zipfile.writestr(file.name, file.file.read())
            # fix for Linux zip files read in Windows
            for file in zipfile.filelist:
                file.create_system = 0

        # create data file
        df, created = DataFile.objects.get_or_create(
            dataset=ds, data_file_type=DataFile.FILE_TYPE_GENERATED_ZIP
        )
        df.name = self.get_zip_file_name()
        df.file = path
        df.save()

    def valid_licenses(self):
        return settings.DD_LICENSE_MATRIX[self.data_access]

    def request_publication(self, user):
        logger.debug("in request publication")
        pub_request = DataSetPublicationRequest.objects.filter(dataset=self, user=user)
        # TODO: add a check for compatibility between access model and chosen license
        logger.debug("not self published if")
        if not self.published:
            logger.debug("pub_request.count() < 1 if")
            if pub_request.count() < 1:
                logger.debug("user_has_admin_right if")
                if self.user_has_admin_right(user):
                    self.dataset_management_object.save()
                    self.published_main_category = self.get_main_category()
                    self.published_categories.set(self.get_categories())
                    self.copyright_declaration_text = self.COPYRIGHT_DECLARATION_STRING
                    self.privacy_rights_declaration_text = (
                        self.PRIVACY_RIGHTS_DECLARATION_STRING
                    )
                    logger.debug("save ds")
                    self.save()
                    logger.debug("saved ds")
                    # trigger publication request
                    pubreq = DataSetPublicationRequest()
                    pubreq.dataset = self
                    pubreq.user = user
                    pubreq.category = self.get_main_category()
                    logger.debug("save pubreq")
                    pubreq.save()
                    logger.debug("saved pubreq")

                    return _("Dataset publication requested")
                else:
                    logger.debug("user has no admin right")
                    return _("Operation not allowed")
            else:
                logger.debug("pub_request.count() < 1 false")
                return _("Dataset publication request pending")

        else:
            return _(
                "Dataset is already published. Please create new version and publish again"
            )

    def get_publication_request(self):
        return DataSetPublicationRequest.objects.filter(dataset=self)

    def deny_publication(self, user, message=None):
        if user in self.get_curators():
            pubreq = DataSetPublicationRequest.objects.get(dataset=self)
            ct = ContentType.objects.get_for_model(self)
            text = _(
                "The Publication request for category {} has been denied".format(
                    self.get_main_category()
                )
            )
            subject = text
            if message:
                text = "{}\n\n{}'s curators message:\n{}".format(text, user, message)

            notification = Notification(
                owner=user,
                content_type=ct,
                object_id=self.id,
                text=text,
                parent=pubreq.notification,
                notification_type=Notification.PUB_REQUEST,
            )
            notification.save()
            # send email to dataset owner
            mail_message = "Dataset {} at {}:\n{}".format(
                self, self.get_absolute_url_prep(), text
            )
            email_to = [self.owner.get_email()]
            send_update_email(subject, mail_message, email_to)
            # send emails to category curators
            curators_subject = "[Curation] {}".format(
                subject,
            )
            curators_to = self.get_main_category().get_curators_emails()
            send_update_email(curators_subject, mail_message, curators_to)
            pubreq.delete()
        else:
            raise PermissionDenied

    def perform_checks(self):
        error = False
        message_list = list()
        if not self.license_is_valid():
            message_list.append(_("License is not valid."))
            error = True
        return error, message_list

    def accept_publication(self, user, message=None):
        if user in self.get_curators():
            self.publication_accepted = True
            self.publication_accepted_by = user
            self.save()
            pubreq = DataSetPublicationRequest.objects.get(dataset=self)

            ct = ContentType.objects.get_for_model(self)
            text = _(
                "The Publication request for category {} has been accepted".format(
                    self.get_main_category()
                )
            )
            subject = text
            if message:
                text = "{}\n\n{}'s curators message:\n{}\n".format(text, user, message)

            notification = Notification(
                owner=user,
                content_type=ct,
                object_id=self.id,
                text=text,
                parent=pubreq.notification,
                notification_type=Notification.PUB_REQUEST,
            )
            notification.save()
            # send email to dataset owner
            mail_message = "Dataset {} at {}:\n{}".format(
                self, self.get_absolute_url_prep(), text
            )
            mail_message += "\nYou can now publish the dataset at {}.\n".format(
                self.get_absolute_url_prep_versions()
            )
            email_to = [self.owner.get_email()]
            send_update_email(subject, mail_message, email_to)
            # send emails to category curators
            curators_subject = "[Curation] {}".format(
                subject,
            )
            curators_to = self.get_main_category().get_curators_emails()
            send_update_email(curators_subject, mail_message, curators_to)
            pubreq.delete()
        else:
            raise PermissionDenied

    def publish(self, user):
        logger.debug("DS PUBLISH started")
        if not self.publication_accepted:
            return _("Dataset not accepted for publication")
        if self.user_has_admin_right(user):
            # if self.data_access == DataSet.OPEN_ACCESS:
            #    publish_to_dhrep(self, token)
            # run checks
            self.perform_checks()

            self.published_main_category = self.get_main_category()
            self.dataset_published_categories = self.get_categories()
            self.dataset_management_object.published = True
            self.dataset_management_object.main_published_ds = self
            self.dataset_management_object.save()
            self.publication_date = timezone.now()
            self.save()
            doi = None
            try:
                doi = generate_discuss_data_doi(self)
            except DataCiteUnauthorizedError as e:
                raise ImproperlyConfigured from e
            except DataCiteNotFoundError as e:
                raise MetaDataError from e
            except Exception as e:
                logger.exception(e)
            finally:
                self.doi = doi or ""

            if self.dataset_type == "DR":
                self.create_datareview_pdf()
            else:
                logger.debug("create_description_pdf")
                self.create_description_pdf()

            logger.debug("generate_dsa")
            self.generate_dsa()

            # TODO: do not create zip file as we will need
            # more than one to reflect multiple access models
            # self.create_files_zip()
            self.published = True

            # publish OA also to Zenodo
            if self.data_access == DataSet.OPEN_ACCESS:
                pub_process = DataSetPublishingProcess(dataset=self)
                pub_process.save()
                logger.debug(f"Pub Process started {pub_process.started}")
                pub_process.publish_zenodo()

            logger.debug("save")
            self.save()
            logger.debug("action.send")
            action.send(
                user,
                verb="published the dataset",
                target=self,
            )
            # send email to dataset owner
            subject = "Dataset {} has been published successfully!".format(
                self,
            )
            message = "{}\n\nIt is accessible via DOI https://doi.org/{}".format(
                subject, self.doi
            )
            if self.zenodo_doi:
                message += "\nZenodo-Repository entry: https://doi.org/{}".format(
                    self.zenodo_doi
                )
            if self.dhdoi:
                message += "\nDARIAH-Repository entry: https://doi.org/{}".format(
                    self.dhdoi
                )
            email_to = [self.owner.get_email()]
            logger.debug(send_update_email)
            send_update_email(subject, message, email_to)
            # send emails to category curators
            curators_subject = "[Curation] {}".format(
                subject,
            )
            curators_to = self.get_main_category().get_curators_emails()
            send_update_email(curators_subject, message, curators_to)
        else:
            raise PermissionDenied
        logger.debug("DS PUBLISH ended")

    def get_published_versions(self):
        return self.dataset_management_object.get_published_datasets().order_by(
            "-minor_version"
        )

    def get_versions(self):
        return self.dataset_management_object.get_all_datasets().order_by(
            "-minor_version"
        )

    def get_main_category(self):
        if self.published:
            return self.published_main_category
        else:
            return self.dataset_management_object.main_category

    def get_categories(self):
        if self.published:
            return self.published_categories.order_by("name")
        else:
            return self.dataset_management_object.categories.order_by("name")

    def get_curators(self):
        if self.published:
            return self.published_main_category.curators.order_by("last_name")
        else:
            return self.dataset_management_object.main_category.curators.order_by(
                "last_name"
            )

    def __str__(self):
        return "{}, v{}".format(self.title, self.version)


class DataSetPublishingProcess(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.OneToOneField(
        "DataSet",
        null=True,
        blank=True,
        on_delete=models.CASCADE,
    )
    started = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"pub process for {self.dataset.get_fulltitle()}, started {self.started}"

    def publish_zenodo(self):
        zp = ZenodoDatasetPublisher(self.dataset)
        rmd = zp.upload_metadata()
        rfiles = zp.upload_all_files()

        status_code, rpub = zp.publish()

        if status_code == 202:
            logger.debug(f"Zenodo publish successful")
            self.dataset.zenodo_doi = rmd["metadata"]["prereserve_doi"]["doi"]
            self.dataset.save()
            success = True
            message = "Successfully published the dataset to Zenodo"
        else:
            logger.error(
                f"Error in zenodo publish: {rpub['message']}, {rpub['errors']}"
            )
            success = False
            message = f"{rpub['message']}: "
            for error in rpub["errors"]:
                message += f"{error['field']}: {error['messages']}"

        return success, message


class DataSetManagementObject(models.Model):
    uuid = models.UUIDField(
        default=uuid.uuid4, editable=False
    )  # uuid _not_ as pk as this disturbs django 3rd party apps
    groups = models.ManyToManyField(Group, related_name="dsmo_groups_group")
    owner = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    published = models.BooleanField(default=False)
    doi = models.CharField(
        max_length=200,
        blank=True,
    )
    main_category = models.ForeignKey(
        "Category",
        related_name="dsmo_main_category",
        on_delete=models.PROTECT,
        default=1,
    )
    categories = models.ManyToManyField(
        "Category", related_name="dsmo_categories", blank=True
    )
    main_published_ds = models.OneToOneField(
        "DataSet",
        null=True,
        blank=True,
        on_delete=models.PROTECT,
    )

    # max_version = models.DecimalField(max_digits=5, decimal_places=1,default=1.0)
    # max_version_published = models.DecimalField(max_digits=5, decimal_places=1,default=1.0)

    class Meta:
        """
        Definition of individual dsmo permissions.
        DSMO permissions override DataSet permissions which are
        deprecated and will be removed in the near future
        """

        permissions = (
            ("view_dsmo", "View Data Set Management Object"),
            ("edit_dsmo", "Edit Data Set Management Object"),
            ("admin_dsmo", "Admin Data Set Management Object"),
        )

    # group individual permissions together
    perms_groups = {
        "view": ("view_dsmo",),
        "edit": ("view_dsmo", "edit_dsmo"),
        "admin": ("view_dsmo", "edit_dsmo", "admin_dsmo"),
    }

    def clear_user_permissions(self, user):
        for perm in get_perms(user, self):
            remove_perm(perm, user, self)

    def assign_user_permissions(self, user, perm):
        # clear all permissions
        self.clear_user_permissions(user)
        # assign new permissions
        for entry in self.perms_groups[perm]:
            assign_perm(entry, user, self)

    def save(self, *args, **kwargs):
        models.Model.save(self, *args, **kwargs)

        # set admin permissions for owner on first save
        if not get_perms(self.owner, self):
            self.assign_user_permissions(self.owner, "admin")

        super(DataSetManagementObject, self).save(*args, **kwargs)
        models.Model.save(self, *args, **kwargs)

        # Call the "real" save() method in the base class 'models.Model'.
        models.Model.save(self, *args, **kwargs)

    def user_has_admin_right(self, user):
        return user.has_perm("admin_dsmo", self)

    def get_dsmo_users(self):
        # get all users with permissions, but exclude owner
        return get_users_with_perms(self).exclude(uuid=self.owner.uuid)

    def get_dsmo_users_with_perms(self):
        user_admin = set()
        user_edit = set()
        user_view = set()
        user_groups = dict()

        for user, perms in get_users_with_perms(self, attach_perms=True).items():
            if "admin_dsmo" in perms:
                user_admin.add(user)
                continue
            if "edit_dsmo" in perms:
                user_edit.add(user)
                continue
            if "view_dsmo" in perms:
                user_view.add(user)

        user_groups = {
            "admin": user_admin,
            "edit": user_edit,
            "view": user_view,
        }
        return user_groups

    def get_all_datasets(self):
        return DataSet.objects.filter(dataset_management_object=self)

    def get_published_datasets(self):
        return DataSet.objects.filter(dataset_management_object=self).filter(
            published=True
        )

    def get_top_version(self):
        all_ds = self.get_all_datasets()
        max_version = all_ds.aggregate(models.Max("minor_version"))[
            "minor_version__max"
        ]
        return max_version

    def get_top_version_dataset(self):
        try:
            return self.get_all_datasets().order_by("-minor_version")[0]
        except IndexError:
            return None
        # max_version = self.get_top_version()
        # TODO: bug in returning 1.2 as top version, nonfix applied
        # return DataSet.objects.all()[0] #filter(dataset_management_object=self)[0]

    def get_top_version_published_dataset(self):
        try:
            return self.get_published_datasets().order_by("-minor_version")[0]
        except IndexError:
            return None

        # max_version = all_ds.aggregate(models.Max('version'))['version__max']
        # return all_ds.filter(dataset_management_object=self).filter(published=True).filter(version = max_version)[0]

    def get_absolute_url(self):
        return reverse(
            "dddatasets:detail", args=[str(self.get_top_version_dataset().uuid)]
        )

    def class_name(self):
        return self.__class__.__name__

    def owner_transfer(self, new_owner: User) -> None:
        prev_owner = self.owner
        self.owner = new_owner
        self.save()
        for ds in self.get_all_datasets():
            ds.owner = new_owner
            ds.save()
            dspo = DataSetPrevOwner()
            dspo.dataset = ds
            dspo.user = prev_owner
            dspo.save()

    def __str__(self):
        formatedDate = self.created_at.strftime("%Y-%m-%d %H:%M:%S")
        top_ds = self.get_top_version_dataset()
        return "{}, currently v{} – created at {}".format(
            top_ds.title, top_ds.version, formatedDate
        )


class DataSetOwnerTransfer(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    dataset = models.OneToOneField(
        "DataSet",
        on_delete=models.CASCADE,
        related_name="dataset_owner_transfer_dataset",
    )
    new_owner = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="new_owner_transfer_dataset"
    )
    prev_owner_request = models.BooleanField(default=False)
    new_owner_accept = models.BooleanField(default=False)

    def get_owner_name(self):
        return self.dataset.owner.get_academic_name()

    def send_notification(self, text, sender, recipient):
        notification = Notification(
            text=text,
            owner=sender,
            recipient=recipient,
            notification_type=Notification.TRANSFER_REQUEST,
            content_type=ContentType.objects.get_for_model(self.dataset),
            object_id=self.dataset.id,
        )
        notification.save()
        message = _(f"{text}")
        subject = _("Ownership transfer update")
        # send email to owner and user
        email_to = [self.new_owner.get_email()]
        email_to.append(self.dataset.owner.get_email())
        send_update_email(subject, message, email_to)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.prev_owner_request and self.new_owner_accept:
            logger.debug(
                f"TRANSFERRING OWNERSHIP for dataset {self.dataset} from {self.dataset.owner} to {self.new_owner}"
            )
            self.dataset.dataset_management_object.owner_transfer(self.new_owner)
            logger.debug("TRANSFERRING OWNERSHIP done.")
            super().save(*args, **kwargs)
            self.delete()
            self.send_notification(
                f"Ownership of dataset {self.dataset} has been transferred to {self.new_owner}",
                self.new_owner,
                self.dataset.owner,
            )
        else:
            self.send_notification(
                f"Ownership transfer request for dataset {self.dataset}",
                self.dataset.owner,
                self.new_owner,
            )
            logger.debug(
                f"SENDING NOTIFICATION for dataset {self.dataset} from {self.dataset.owner} to {self.new_owner}"
            )

    def __str__(self):
        return f"Transfer {self.dataset} from {self.dataset.owner} to {self.new_owner}"


# class PublishedDataSet(models.Model):
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#     doi = models.CharField(max_length=200, blank=True)
#     #version = models.FloatField(default=1.0)
#     #dataset = models.OneToOneField('DataSet', on_delete=models.PROTECT)
#     #dataset_uuid = models.UUIDField(editable=False, default=uuid.uuid4)

#     def get_top_version_dataset(self):
#         all_ds = self.get_all_datasets()
#         max_version = all_ds.aggregate(models.Max('version'))['version__max']

#         try:
#             return all_ds.filter(version = max_version)[0]
#         except:
#             return None


#     def get_all_datasets(self):
#         return DataSet.objects.filter(published_dataset=self).filter(published=True)


#     def __str__(self):
#         #return self.get_top_version_dataset().title
#         return "{}, v{}".format(self.get_top_version_dataset().title, self.id)
