import logging

from actstream import action
from django.contrib.contenttypes.models import ContentType
from django.http import Http404, HttpResponse
from django.shortcuts import render

from discuss_data.core.helpers import check_perms, check_perms_403
from discuss_data.ddcomments.forms import CommentForm
from discuss_data.ddcomments.models import Comment
from discuss_data.dddatasets.models import DataSet
from discuss_data.pages.models import ManualPage
from discuss_data.docupages.models import DocuPage

logger = logging.getLogger(__name__)


def get_dataset_fields_from_form(ds, form):
    dataset_fields = list()
    for field in form:
        try:
            qs = getattr(ds, field.name).all()
            entries = list()
            for entry in qs:
                entries.append(entry.name)
            entries_str = ", ".join(entries)
            dataset_fields.append((field.label, entries_str))
        except AttributeError:
            dataset_fields.append((field.label, getattr(ds, field.name)))
    return dataset_fields


def check_published(ds_uuid):
    """
    helper function to check if a dataset has been published
    and can thus be accessed openly
    """
    try:
        ds = DataSet.objects.get(uuid=ds_uuid)
    except DataSet.DoesNotExist:
        raise Http404("Dataset not found")
    if not ds.published:
        raise Http404("Dataset not found")

    return ds


def get_docu_page(slug):
    """
    helper function for docu pages
    """

    try:
        return DocuPage.objects.get(slug=slug)
    except Exception:
        return None


def get_man_page(slug):
    """
    helper function for manual pages
    """

    try:
        return ManualPage.objects.get(slug=slug)
    except Exception:
        return None


def edit_datafile(request, datafile, form):
    if form.is_valid():
        datafile.data_file_type = form.cleaned_data["data_file_type"]
        uploadedfile = form.cleaned_data["file"]
        datafile.file = uploadedfile
        try:
            datafile.name = uploadedfile._name
            datafile.content_type = uploadedfile.content_type
            datafile.data_file_size = uploadedfile.size
        except Exception as e:
            logger.error(e)

        datafile.save()
        updated = True
        form_err = None
    else:
        updated = False
        form_err = form.errors.as_data()
    return form, form_err, updated


# this is a view function
def dataset_comments(request, ds_uuid, view_type):
    """
        returns a list of either public or prep comments
        of a dataset depending on view_type param ("public"|"prep")
    """
    ds = DataSet.objects.get(uuid=ds_uuid)

    if view_type != "public":
        if not check_perms("edit_dsmo", request.user, ds.dataset_management_object):
            response = HttpResponse("no rights to private comments")
            response["X-IC-CancelPolling"] = "true"
            return response
        comments = ds.get_prep_comments_root()
        add_url = "dddatasets:prep_edit_comment"
        list_url = "dddatasets:prep_comments"
        reply_url = "dddatasets:prep_edit_comment_reply"
        root_count = comments.count()
        all_count = ds.get_prep_comments_all().count()
    else:
        comments = ds.get_public_comments_root()
        add_url = "dddatasets:edit_comment"
        list_url = "dddatasets:comments"
        reply_url = "dddatasets:edit_comment_reply"
        root_count = comments.count()
        all_count = ds.get_public_comments_all().count()
    # add http header to response to resume polling (which could have been canceled in prep_dataset_edit_comment)
    response = render(
        request,
        "ddcomments/_comments_list.html",
        {
            "ds": ds,
            "comments": comments,
            "add_url": add_url,
            "list_url": list_url,
            "reply_url": reply_url,
            "root_count": root_count,
            "all_count": all_count,
        },
    )
    response["X-IC-ResumePolling"] = "true"
    return response


# this is a view function
def dataset_edit_comment(request, ds_uuid, view_type, co_uuid, reply=None):
    """
        view to add, edit or delete public/prep comments
        to/from a dataset
    """

    if view_type == "public" or view_type == "prep":
        pass
    else:
        raise Exception("no view type")
    err = None
    ds = DataSet.objects.get(uuid=ds_uuid)
    if view_type == "public":
        add_url = "dddatasets:edit_comment"
        list_url = "dddatasets:comments"
        reply_url = "dddatasets:edit_comment_reply"
    else:
        add_url = "dddatasets:prep_edit_comment"
        list_url = "dddatasets:prep_comments"
        reply_url = "dddatasets:prep_edit_comment_reply"

    if view_type != "public":
        check_perms_403("edit_dsmo", request.user, ds.dataset_management_object)
    parent = None

    # DELETE: delete not the whole Comment object, but mark the text as "deleted"
    if request.method == "DELETE":
        co_uuid = request.DELETE["objectid"]
        comment = Comment.objects.get(uuid=co_uuid)
        check_perms_403("delete_comment", request.user, comment)
        comment.set_delete()
        comment.save()

        if view_type == "public":
            return dataset_comments(request, ds.uuid, "public")
        else:
            return dataset_comments(request, ds.uuid, "prep")

    if request.method == "POST":
        if co_uuid and not reply:
            # Edit
            comment = Comment.objects.get(uuid=co_uuid)
            check_perms_403("edit_comment", request.user, comment)
            form = CommentForm(request.POST, instance=comment)
            if form.is_valid():
                comment = form.save()
                if view_type == "public":
                    return dataset_comments(request, ds.uuid, "public")
                else:
                    return dataset_comments(request, ds.uuid, "prep")
            else:
                err = form.errors.as_data()
        else:
            # Add new and reply
            comment = Comment()
            form = CommentForm(request.POST, instance=comment)
            if form.is_valid():
                comment.content_type = ContentType.objects.get_for_model(ds)
                comment.object_id = ds.id
                comment.owner = request.user
                if view_type == "public":
                    comment.comment_type = comment.PUBLIC
                    act_public = True
                else:
                    comment.comment_type = comment.PRIVATE
                    act_public = False
                if reply:
                    comment.parent = Comment.objects.get(uuid=co_uuid)
                comment = form.save()
                # send action
                action.send(
                    request.user,
                    verb="added comment",
                    target=comment,
                    action_object=ds,
                    public=act_public,
                )
                return render(
                    request,
                    "ddcomments/_comment_block_load.html",
                    {"ds": ds, "add_url": add_url, "list_url": list_url},
                )
            else:
                err = form.errors.as_data()
    else:
        # Get
        if co_uuid and not reply:
            # Edit form
            comment = Comment.objects.get(uuid=co_uuid)
            check_perms_403("edit_comment", request.user, comment)
            form = CommentForm(instance=comment)
            # add http header to cancel polling while existing comment is being edited (resume header added in prep_dataset_comments() called by POST)
            response = render(
                request,
                "ddcomments/_comment_add.html",
                {
                    "ds": ds,
                    "comment": comment,
                    "comment_form": form,
                    "err": err,
                    "edit": True,
                    "add_url": add_url,
                    "list_url": list_url,
                    "reply_url": reply_url,
                    "form": form,
                    "ictarget": "#comments-list",
                },
            )
            response["X-IC-CancelPolling"] = "true"
            return response
        elif reply:
            # Reply form
            parent = Comment.objects.get(uuid=co_uuid)
            form = CommentForm()
        else:
            # Add form
            form = CommentForm()
        # updated = False
        # form_err = None
    response = render(
        request,
        "ddcomments/_comment_add.html",
        {
            "ds": ds,
            "parent": parent,
            "comment_form": form,
            "err": err,
            "add_url": add_url,
            "list_url": list_url,
            "reply_url": reply_url,
            "form": form,
            "reply": reply,
            "ictarget": "#discuss",
        },
    )
    if reply:
        response["X-IC-CancelPolling"] = "true"
    return response
