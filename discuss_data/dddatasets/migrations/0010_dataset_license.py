# Generated by Django 2.2.11 on 2020-05-10 11:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0019_licensepage'),
        ('dddatasets', '0009_remove_dataset_license'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataset',
            name='license',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dataset_license_page', to='pages.LicensePage'),
        ),
    ]
