# Generated by Django 2.2.13 on 2020-06-19 11:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dddatasets', '0024_auto_20200617_1105'),
    ]

    operations = [
        migrations.AddField(
            model_name='dataset',
            name='publication_accepted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='dataset',
            name='publication_accepted_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='dataset_publication_accepted_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
