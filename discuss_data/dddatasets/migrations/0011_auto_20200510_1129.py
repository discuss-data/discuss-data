# Generated by Django 2.2.11 on 2020-05-10 11:29

from django.db import migrations, models
import django.db.models.deletion
import django_bleach.models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0019_licensepage'),
        ('dddatasets', '0010_dataset_license'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='license',
            name='logo',
        ),
        migrations.RemoveField(
            model_name='license',
            name='name',
        ),
        migrations.AddField(
            model_name='license',
            name='individual_license',
            field=django_bleach.models.BleachField(blank=True),
        ),
        migrations.AddField(
            model_name='license',
            name='license_type',
            field=models.CharField(choices=[('IND', 'individual'), ('STD', 'standard')], default='STD', max_length=4),
        ),
        migrations.AddField(
            model_name='license',
            name='standard_license',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='license_license_page', to='pages.LicensePage'),
        ),
    ]
