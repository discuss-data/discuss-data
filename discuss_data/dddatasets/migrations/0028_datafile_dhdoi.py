# Generated by Django 2.2.13 on 2020-07-01 14:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dddatasets', '0027_dataset_dhdoi'),
    ]

    operations = [
        migrations.AddField(
            model_name='datafile',
            name='dhdoi',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
