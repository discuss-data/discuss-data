
from django_bleach.forms import BleachField

from crispy_forms.bootstrap import FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Div, Field, Layout, Submit
from django import forms
from django.forms import ModelForm
from django.utils.translation import gettext as _
from django.forms.widgets import HiddenInput

from discuss_data.core.helpers import get_help_texts
from discuss_data.dddatasets.models import (
    Category,
    DataFile,
    DataList,
    DataSet,
    DataSetCreator,
    DataSetPublication,
    DataSubmissionAgreement,
    License,
    ArchivalSignature,
    Archive,
)
from discuss_data.dddatasets.utils import get_man_page

UPDATED_MSG = """
<div class="pt-2">
    {% if updated %}<div id='updated-msg' class='alert alert-success alert-dismissible fade show' ic-trigger-on='timeout:10s' ic-action='delay:4500;fadeOut;remove' role='alert'><span class='spinner-sm'></span><strong>{{ target|title }} saved</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>{% endif %}
    {% if err %}<div id='updated-msg' class='alert alert-danger alert-dismissible fade show' ic-trigger-on='timeout:10s' ic-action='delay:4500;fadeOut;remove' role='alert'>
    <span class='spinner-sm'></span><strong>Error! {{ target|title }} could not be saved. Error encountered in fields {% for field in err %} {{ field }}, {% endfor %}</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div> {% endif %}
</div>
    """

ADD_EDIT_HEADING = (
    "<h2>{% if new %}Edit{% else %}Add{% endif %} {{ target|title }}</h2><hr>"
)

REQ_FIELD = "<p class='help-block'>[*] {}</p>".format(_("Required fields"))

EDIT_DATASET_BTN = "<button class='btn btn-primary' type='submit' ic-indicator='#form-spinner'>{% if btn_text %}{{ btn_text }}{% else %}Save{% endif %}</button> <span id='form-spinner' class='spinner-sm' style='display:none'></span>"


class DataSetPublicationForm(ModelForm):
    class Meta:
        model = DataSetPublication
        fields = [
            "pub_type",
        ]

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div(
                "pub_type",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
    )


class DataSetIndividualLicenseForm(ModelForm):
    class Meta:
        model = License
        fields = [
            "license_text",
        ]
        help_texts = get_help_texts("edit-license-access")
        manpage = get_man_page("edit-license-access")

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        HTML(
            "{% include '_help_text.html' with field=Meta.help_texts key='D-Customized-Data-Usage-Licence-Agreement' %}"
        ),
        HTML(
            "{% include 'dddatasets/_manual_page.html' with manpage=Meta.manpage key='D-Customized-Data-Usage-Licence-Agreement' %}"
        ),
        Div(
            Div(
                "license_text",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
    )


class DataSetValidateForm(ModelForm):
    class Meta:
        model = DataSet
        fields = "__all__"


class DataSetCopyrightForm(ModelForm):
    class Meta:
        model = DataSet
        fields = ("copyright_declaration",)

    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            Div(
                "copyright_declaration",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
    )


class DataSetPrivacyRightsForm(ModelForm):
    class Meta:
        model = DataSet
        fields = ("privacy_rights_declaration",)

    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            Div(
                "privacy_rights_declaration",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
    )


class DataSubmissionAgreementAcceptForm(ModelForm):
    class Meta:
        model = DataSubmissionAgreement
        fields = ("dsa_accepted",)

    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            HTML(
                "<div class='col-md-12 smallskip'>{} {}.</div>".format(
                    _("Finally, you need also to accept the"),
                    _("Data Submission Agreement"),
                )
            ),
            Div(
                "dsa_accepted",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        HTML(EDIT_DATASET_BTN),
        # FormActions(Submit("save", "Save {{ target }}"),),
        # Div(HTML(UPDATED_MSG),),
    )


class DataSubmissionAgreementForm(ModelForm):
    class Meta:
        model = DataSubmissionAgreement
        fields = (
            "birthdate",
            "postal_address",
            "project_type",
            "project_title",
            # "dsa_accepted",
            # "dsa_accepted_date",
        )
        help_texts = get_help_texts("data-submission-agreement-form")

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div(
                "postal_address",
                css_class="col-md-12",
            ),
            Div(
                "birthdate",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        Div(
            Div(
                "project_type",
                css_class="col-md-6",
            ),
            Div(
                "project_title",
                css_class="col-md-6",
            ),
            css_class="row",
        ),
        # Div(
        #     Div("dsa_accepted_date", readonly=True, css_class="col-md-6",),
        #     Div("dsa_accepted", css_class="col-md-6",),
        #     css_class="row",
        # ),
        HTML(REQ_FIELD),
        HTML(EDIT_DATASET_BTN),
    )


class DataSetCreatorFormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_method = "post"
        self.form_tag = False
        self.layout = Layout(
            Div(
                Div(
                    "name",
                    css_class="col-md-3",
                ),
                Div(
                    "first_name",
                    css_class="col-md-3",
                ),
                Div(
                    "creator_type",
                    css_class="col-md-2",
                ),
                Div(
                    "list_position",
                    css_class="col-md-2",
                ),
                Field("DELETE", template="core/_bool_button.html"),
                css_class="row crispyformset",
            ),
        )
        self.render_required_fields = True


class DataSetForm(ModelForm):
    class Meta:
        model = DataSet
        fields = (
            "date_of_data_creation_from",
            "date_of_data_creation_to",
            "date_of_data_creation_text",
            "time_period_from",
            "time_period_to",
            "time_period_text",
            "sources_of_data",
            "datatypes",
            "datatype_text",
            "institutional_affiliation",
            "funding",
            "related_dataset_text",
            # "related_dataset", TODO: commenting out as the field lists also unpublished datasets. We need a custom widget/control for this.
            "related_projects",
        )
        help_texts = get_help_texts("edit-metadata")
        widgets = {
            "datatypes": forms.CheckboxSelectMultiple,
        }

    helper = FormHelper()
    helper.form_tag = False

    helper.layout = Layout(
        Div(
            Div(
                HTML(
                    "{% load crispy_forms_tags i18n %}<h4>{% trans 'Data Creators' %}{% include '_help_text.html' with field=help_texts key='creators' %}</h4><p></p>{% crispy formset helper %}"
                ),
                css_class="col-md-12",
            ),
            HTML(
                "{% load i18n %}<div class='col-md-12 smallskip'><button type='button' class='btn btn-outline-primary' id='add-creator'>{% trans 'Add Creator' %}</button></div>",
            ),
            HTML("<hr>"),
            css_class="row",
        ),
        Div(
            Div(
                "date_of_data_creation_from",
                css_class="col-md-6",
            ),
            Div(
                "date_of_data_creation_to",
                css_class="col-md-6",
            ),
            Div(
                "date_of_data_creation_text",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        Div(
            Div(
                "time_period_from",
                css_class="col-md-6",
            ),
            Div(
                "time_period_to",
                css_class="col-md-6",
            ),
            Div(
                "time_period_text",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        Div(
            Div(
                "sources_of_data",
                css_class="col-md-6",
            ),
            Div(
                Div("datatypes"),
                Div("datatype_text"),
                css_class="col-md-6",
            ),
            css_class="row",
        ),
        Div(
            Div(
                "institutional_affiliation",
                css_class="col-md-3",
            ),
            Div("funding", css_class="col-md-3", wrapper_class="controls"),
            Div(
                "related_projects",
                css_class="col-md-3",
            ),
            Div(
                "related_dataset_text",
                css_class="col-md-3",
            ),
            css_class="row",
        ),
        # Div(
        #    Div("related_dataset_text", css_class="col-md-6",),
        #    Div("related_dataset", css_class="col-md-6",),
        #    css_class="row",
        # ),
        HTML(REQ_FIELD),
        HTML(EDIT_DATASET_BTN),
        # FormActions(
        #    Submit("save", "Save {{ target }}"),
        #    # Button('cancel', 'Cancel', data_toggle="collapse", data_target="#collapse-{{  object_type }}-{{ object.id }}"),
        # ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )
    # def __init__(self, *args, **kwargs):
    #    super().__init__(*args, **kwargs)
    #    self.fields['datatypes'].widget = forms.CheckboxSelectMultiple
    #    #self.fields['comment'].widget.attrs.update(size='40')


class DataSetCreatorForm(ModelForm):
    class Meta:
        model = DataSetCreator
        fields = ("name", "creator_type", "list_position")


class ArchiveForm(ModelForm):
    class Meta:
        model = Archive
        fields = ("short_name", "name", "country", "url")

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div(
                "short_name",
                css_class="col-md-12",
            ),
            Div(
                "name",
                css_class="col-md-12",
            ),
            Div(
                "country",
                css_class="col-md-12",
            ),
            Div(
                "url",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML(EDIT_DATASET_BTN),
    )


class ArchivalSignatureForm(ModelForm):
    archive_id = forms.IntegerField(required=True)
    archive_id.widget = archive_id.hidden_widget()
    archive_name = forms.CharField(required=False, disabled=True)

    class Meta:
        model = ArchivalSignature
        fields = ("archive_id", "archive_name", "fond", "opis", "delo", "list", "url")

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        # HTML(
        #     "{% load crispy_forms_tags i18n %}<h4>{% trans 'Keywords' %}{% include '_help_text.html' with field=help_texts key='keyword-tags' %}</h4>"
        # ),
        Div(
            HTML("<div class='row'>"),
            Div(
                Field(
                    "archive_name",
                    oninvalid="this.setCustomValidity('Add an archive using \"Archive Search\" field')",
                ),
                Field("archive_id"),
                css_class="col-md-2",
            ),
            HTML(
                """{% load crispy_forms_tags i18n %}
                <div class="col-md-3" class="form-group">
                    <label for="archive-search">Archive Search*</label>
                    <input class="w-100 form-control smallskip" type="text" placeholder="Search" name="q" ic-get-from="{% if archivalsignature %}{% url 'dddatasets:prep_edit_archive_search' ds.uuid archivalsignature.uuid %}{% else %}{% url 'dddatasets:prep_edit_archive_search' ds.uuid %}{% endif %}" ic-target="#search-list" aria-label="Search" {%if not archive %}required oninvalid="this.setCustomValidity('Type here to search for an archive or add an archive to the database using the button on the left'){%endif%}">
                </div>
                <div class="col-md-3" class="form-group">
                    <label for="add-archive">Add Archive</label>
                    <button type="button" class="btn btn-outline-primary form-control" data-toggle="modal" data-target="#add-archive-modal" ic-target="#add-archive-modal-target-span">{% trans "Add missing archive to database" %}</button>
                </div>
                </div>
                <div class="container"><div id="search-list" class="w-50 "col-md-12"></div></div>
            """
            ),
        ),
        Div(
            Div(
                Field(
                    "fond",
                ),
                css_class="col-md-2",
            ),
            Div(
                "opis",
                css_class="col-md-2",
            ),
            Div(
                "delo",
                css_class="col-md-2",
            ),
            Div(
                "list",
                css_class="col-md-2",
            ),
            Div(
                "url",
                css_class="col-md-8",
            ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML(EDIT_DATASET_BTN),
    )


class DataSetEditForm(ModelForm):
    main_category = forms.CharField(required=False)
    categories = forms.MultipleChoiceField(required=False, choices=[])

    class Meta:
        model = DataSet
        fields = (
            "title",
            "subtitle",
            "description",
            "main_category",
            "categories",
            "shorttitle",
        )
        # exclude = ("link",)
        help_texts = get_help_texts("edit-dataset-description")


    helper = FormHelper()
    helper.form_tag = False
    helper.help_text_inline = False
    helper.layout = Layout(
        Div(
            Div(
                "title",
                css_class="col-md-12",
            ),
            Div(
                "subtitle",
                css_class="col-md-12",
            ),
            Div(
                "shorttitle",
                css_class="col-md-12",
            ),
            Div(
                "description",
                css_class="col-md-12",
            ),
            HTML(
                "<span ic-get-from='{% url 'core:load' %}' ic-trigger-on='load'></span>"
            ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML(EDIT_DATASET_BTN),
        Div(
            HTML(UPDATED_MSG),
        ),
    )

    # init form at every request to update categories queried from db
    def __init__(self, *args, **kwargs):
        super(DataSetEditForm, self).__init__(*args, **kwargs)
        self.fields["categories"] = forms.MultipleChoiceField(
            required=False,
            choices=[
                (category.id, category.name)
                for category in Category.objects.all().order_by("name")
            ],
        )
        # forms.ChoiceField(choices=[(category.id, category.name) for color in Color.objects.all()])


class DataReviewEditForm(DataSetEditForm):
    link = forms.URLField(required=True, label=(_("Link / DOI")))
    external_license = forms.CharField(required=True)
    external_publisher = forms.CharField(required=True)

    class Meta:
        model = DataSet
        # fields = DataSetEditForm.Meta.fields + ("link",)
        fields = (
            "title",
            "subtitle",
            "description",
            "main_category",
            "categories",
            "shorttitle",
            "link",
            "external_publisher",
            "external_version",
            "external_license",
        )
        help_texts = get_help_texts("edit-datareview-description")

    layout = DataSetEditForm.helper.layout
    layout[0][2].append(
        Div(
            "link"
        )
    )
    layout[0][2].append(
        Div(
            Div(
                "external_publisher",
                css_class="col-md-8",
            ),
            Div(
                "external_version",
                css_class="col-md-4",
            ),
            Div(
                "external_license",
                css_class="col-md-12",
            ),
            css_class="row",
        )
    )

    def __init__(self, *args, **kwargs):
        super(DataReviewEditForm, self).__init__(*args, **kwargs)
        self.fields["description"].label = _("Abstract for the review")


class DataReviewAddForm(ModelForm):
    main_category = forms.CharField(required=False)
    categories = forms.MultipleChoiceField(required=False, choices=[])
    link = forms.URLField(required=True, label=(_("Link / DOI")))

    class Meta:
        model = DataSet
        fields = (
            "link",
            "main_category",
            "categories",
        )
        help_texts = get_help_texts("edit-datareview-description")

    helper = FormHelper()
    helper.form_tag = False
    helper.help_text_inline = False
    helper.layout = Layout(
        Div(
            Div(
                "link",
                css_class="col-md-10",
            ),
            # HTML(
            #     "<span ic-get-from='{% url 'core:load' %}' ic-trigger-on='load'></span>"
            # ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML(EDIT_DATASET_BTN),
        Div(
            HTML(UPDATED_MSG),
        ),
    )

    # init form at every request to update categories queried from db
    def __init__(self, *args, **kwargs):
        super(DataReviewAddForm, self).__init__(*args, **kwargs)
        self.fields["categories"] = forms.MultipleChoiceField(
            required=False,
            choices=[
                (category.id, category.name)
                for category in Category.objects.all().order_by("name")
            ],
        )


class DataSetImageForm(ModelForm):
    class Meta:
        model = DataSet
        fields = [
            "image",
        ]


class DataSetImageUploadForm(ModelForm):
    # TODO: updated image needs to be loaded at save
    remove_photo = forms.BooleanField(required=False)

    class Meta:
        model = DataSet
        fields = ("image", "remove_photo")

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        # HTML(ADD_EDIT_HEADING),
        Div(
            Div(
                "image",
                css_class="col-md-12",
            ),
            Div(
                "remove_photo",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        HTML(
            '<button type="submit">Save {{ target }}<span class="spinner-sm"></span></button>'
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )


class DataReviewFileUploadForm(ModelForm):
    class Meta:
        model = DataFile
        fields = ("file",)

    helper = FormHelper()
    helper.form_tag = False
    helper.use_custom_control = True
    helper.layout = Layout(
        Div(
            HTML(REQ_FIELD),
            css_class="col-md-12",
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )


class DataFileUploadForm(ModelForm):
    class Meta:
        model = DataFile
        fields = ("data_file_type", "file")

    helper = FormHelper()
    helper.form_tag = False
    helper.use_custom_control = True
    helper.layout = Layout(
        Div(
            "data_file_type",
            css_class="col-md-4",
        ),
        Div(
            HTML(REQ_FIELD),
            css_class="col-md-12",
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )

    def __init__(self, *args, **kwargs):
        super(DataFileUploadForm, self).__init__(*args, **kwargs)
        # remove Data Review as a data file type choice
        data_file_types = list()
        for elem in DataFile.DATA_FILE_TYPES:
            if elem[0] != "REV":
                data_file_types.append(elem)
        self.fields["data_file_type"].choices = data_file_types


class DataSetPublishAccept(forms.Form):
    choices = (
        ("accept", "accept"),
        ("decline", "decline"),
    )
    decision = forms.ChoiceField(choices=choices)
    message = BleachField(required=False)


class DataListForm(ModelForm):
    class Meta:
        model = DataList
        fields = [
            "name",
            "public",
            "description",
        ]

    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div(
                "name",
                css_class="col-md-11",
            ),
            Div(
                "public",
                css_class="col-md-1",
            ),
            Div(
                "description",
                css_class="col-md-12",
            ),
            css_class="row",
        ),
        HTML(REQ_FIELD),
        FormActions(
            Submit("save", "Save {{ target }}"),
        ),
        Div(
            HTML(UPDATED_MSG),
        ),
    )


class DataSetToListForm(forms.Form):
    ds_uuid = forms.UUIDField()
    dl_uuid = forms.UUIDField()
    form_type = forms.CharField()
