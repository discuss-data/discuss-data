from django.urls import path, include

from discuss_data.dddatasets.views import core, prep, public

app_name = "dddatasets"

# public URLs
urlpatterns = [
    path("search/", core.search, name="search"),
    path(
        "<uuid:uuid>/",  # public dataset views
        include(
            [
                path("", core.DataSetDetailView.as_view(), name="detail"),
                path("discuss/", core.DataSetDiscussView.as_view(), name="discuss"),
                path("follow/", core.follow, name="follow"),
                path("metadata/", core.DataSetMetadataView.as_view(), name="metadata"),
                # path("access/", core.DataSetAccessDetailView.as_view(), name="access"),
                path(
                    "access/request/",
                    core.access_request,
                    name="access_request",
                ),
                path("files/", core.DataSetFilesView.as_view(), name="files"),
                path("files/pdf/", core.files_pdf, name="files_pdf"),
                path("files/zip/", core.files_zip, name="files_zip"),
                path(
                    "files/<uuid:df_uuid>/",
                    core.files_download,
                    name="files_download",
                ),
                path(
                    "publications/",
                    core.DataSetPublicationsView.as_view(),
                    name="publications",
                ),
                path("versions/", core.DataSetVersionsView.as_view(), name="versions"),
            ]
        ),
    ),
    path(
        "<uuid:ds_uuid>/comments/",  # list comments view
        public.comments,
        name="comments",
    ),
    path(
        "<uuid:ds_uuid>/edit/comment/",  # edit comment views
        include(
            [
                path(
                    "",
                    public.edit_comment,
                    name="edit_comment",
                ),
                path(
                    "<uuid:co_uuid>/",
                    public.edit_comment,
                    name="edit_comment",
                ),
                path(
                    "<uuid:co_uuid>/reply/",
                    public.edit_comment_reply,
                    name="edit_comment_reply",
                ),
            ]
        ),
    ),
]

# private URLs
urlpatterns += [
    path("prep/", prep.listing, name="prep_listing"),
    path("prep/add/", prep.DataSetAddFormView.as_view(), name="prep_add"),
    path("prep/add/datareview/", prep.DataReviewAddFormView.as_view(), name="prep_datareview_add"),
    path("prep/add/externaldataset/", prep.ExternalDataSetAddFormView.as_view(), name="prep_externaldataset_add"),
    path("prep/remove", prep.remove, name="prep_remove"),
    path(
        "prep/<uuid:ds_uuid>/edit/",  # dataset prep edit
        include(
            [
                path("", prep.edit, name="prep_edit"),
                path("retry_zenodo/", prep.retry_zenodo, name="prep_retry_zenodo"),
                path(
                    "image/",
                    prep.edit_image,
                    name="prep_dataset_edit_image",
                ),
                path(
                    "detail/",
                    prep.DataSetEditFormView.as_view(),
                    name="prep_edit_detail",
                ),
                path(
                    "overview",
                    prep.edit_overview,
                    name="prep_edit_overview",
                ),
                path(
                    "detail/header",
                    prep.refresh_header,
                    name="prep_refresh_header",
                ),
                # tags
                path(
                    "tags/country/",
                    prep.country_tags_edit,
                    name="prep_country_tags_edit",
                ),
                path(
                    "tags/country/list/",
                    prep.country_tags_list,
                    name="prep_country_tags_list",
                ),
                path(
                    "tags/<str:object_type>/add/",
                    prep.tag_add,
                    name="prep_dataset_tag_add",
                ),
                path(
                    "tags/<str:object_type>/remove/",
                    prep.tag_remove,
                    name="prep_dataset_tag_remove",
                ),
                # metadata
                path(
                    "metadata/",
                    prep.edit_metadata,
                    name="prep_edit_metadata",
                ),
                # access
                path(
                    "access/user/search/",
                    prep.edit_access_user_search,
                    name="prep_edit_access_user_search",
                ),
                path(
                    "access/user/",
                    prep.edit_access_user,
                    name="prep_edit_access_user",
                ),
                # collaboration
                path(
                    "collaboration/",
                    prep.edit_collaboration,
                    name="prep_edit_collaboration",
                ),
                path(
                    "collaboration/ownershiptransfer/<str:action>/",
                    prep.edit_ownership_transfer,
                    name="prep_edit_ownership_transfer",
                ),
                path(
                    "collaboration/user/search/",
                    prep.edit_collaboration_user_search,
                    name="prep_edit_collaboration_user_search",
                ),
                path(
                    "collaboration/user/",
                    prep.edit_collaboration_user,
                    name="prep_edit_collaboration_user",
                ),
                # dataset files edit
                path(
                    "datafiles/",
                    prep.edit_datafiles,
                    name="prep_edit_datafiles",
                ),
                path(
                    "datafile/<uuid:df_uuid>/",
                    prep.EditDataFileView.as_view(),
                    name="prep_edit_datafile",
                ),
                path(
                    "datafile/",
                    prep.EditDataFileView.as_view(),
                    name="prep_edit_datafile",
                ),
                # dataset archival signatures edit
                path(
                    "archivalsignatures/",  # list
                    prep.edit_archivalsignatures,
                    name="prep_edit_archivalsignatures",
                ),
                # edit
                path(
                    "archivalsignature/<uuid:as_uuid>/",
                    prep.edit_archivalsignature,
                    name="prep_edit_archivalsignature",
                ),
                path(
                    "archivalsignature/",
                    prep.edit_archivalsignature,
                    name="prep_edit_archivalsignature",
                ),
                path(
                    "archivalsignature/archive/",
                    prep.edit_archivalsignature_archive,
                    name="prep_edit_archivalsignature_archive",
                ),
                path(
                    "archivalsignature/<uuid:as_uuid>/archives/",
                    prep.edit_archive_search,
                    name="prep_edit_archive_search",
                ),
                path(
                    "archivalsignature/archives/",
                    prep.edit_archive_search,
                    name="prep_edit_archive_search",
                ),
                path(
                    "archivalsignature/<uuid:as_uuid>/archive/add/",
                    prep.edit_archive_add,
                    name="prep_edit_archive_add",
                ),
                path(
                    "archivalsignature/archive/add/",
                    prep.edit_archive_add,
                    name="prep_edit_archive_add",
                ),
                path(
                    "archive/new/",
                    prep.new_archive_add,
                    name="prep_new_archive_add",
                ),
                # publications
                path(
                    "publications/",
                    prep.edit_publications,
                    name="prep_edit_publications",
                ),
                path(
                    "publication/<uuid:ds_pub_uuid>/",
                    prep.edit_publication,
                    name="prep_edit_publication",
                ),
                path(
                    "publication/",
                    prep.edit_publication,
                    name="prep_edit_publication",
                ),
                # versions
                path(
                    "versions/",
                    prep.edit_versions,
                    name="prep_edit_versions",
                ),
                path(
                    "versions/add",
                    prep.edit_versions_add,
                    name="prep_edit_versions_add",
                ),
                # publish
                path(
                    "publish/start",
                    prep.edit_publish_start,
                    name="prep_edit_publish_start",
                ),
                path(
                    "publish/check",
                    prep.edit_publish_check,
                    name="prep_edit_publish_check",
                ),
                path(
                    "publish/datafiles",
                    prep.edit_publish_datafiles,
                    name="prep_edit_publish_datafiles",
                ),
                path(
                    "publish/license",
                    prep.edit_publish_license,
                    name="prep_edit_publish_license",
                ),
                path(
                    "publish/copyright",
                    prep.edit_publish_copyright,
                    name="prep_edit_publish_copyright",
                ),
                path(
                    "publish/request",
                    prep.edit_publish_request,
                    name="prep_edit_publish_request",
                ),
                path(
                    "publish/final",
                    prep.edit_publish_final,
                    name="prep_edit_publish_final",
                ),
                path(
                    "publish/final/pubdd",
                    prep.edit_publish_final_pubdd,
                    name="prep_edit_publish_final_pubdd",
                ),
                path(
                    "publish/final/fetch/<str:task>",
                    prep.edit_publish_final_fetch,
                    name="prep_edit_publish_final_fetch",
                ),
                path(
                    "publish/final/upload",
                    prep.edit_publish_final_upload,
                    name="prep_edit_publish_final_upload",
                ),
                path(
                    "publish/final/publish",
                    prep.edit_publish_final_publish,
                    name="prep_edit_publish_final_publish",
                ),
                path(
                    "publish/access",
                    prep.edit_publish_access,
                    name="prep_edit_publish_access",
                ),
                path(
                    "publish/access/select",
                    prep.edit_publish_access_select,
                    name="prep_edit_publish_access_select",
                ),
                path(
                    "publish/license/select",
                    prep.edit_publish_license_select,
                    name="prep_edit_publish_license_select",
                ),
                path(
                    "comment/<uuid:co_uuid>/",
                    prep.edit_comment,
                    name="prep_edit_comment",
                ),
                path(
                    "comment/",
                    prep.edit_comment,
                    name="prep_edit_comment",
                ),
                path(
                    "comment/<uuid:co_uuid>/reply/",
                    prep.edit_comment_reply,
                    name="prep_edit_comment_reply",
                ),
            ]
        ),
    ),
    # curation
    path(
        "prep/<uuid:ds_uuid>/curation/",
        prep.curation,
        name="prep_curation",
    ),
    # comments
    path(
        "prep/<uuid:ds_uuid>/comments/",
        prep.comments,
        name="prep_comments",
    ),
    path(  # list tags
        "prep/tags/<str:object_type>/list/",
        prep.object_tags_list,
        name="prep_dataset_tags_list",
    ),
    # dsa
    path(
        "prep/<uuid:ds_uuid>/dsa/edit",
        prep.edit_dsa,
        name="prep_edit_dsa",
    ),
    path(
        "prep/<uuid:ds_uuid>/dsa/pdf",
        prep.dsa_pdf,
        name="prep_dsa_pdf",
    ),
]
