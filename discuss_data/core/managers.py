from datetime import datetime

from actstream.managers import ActionManager, stream
from actstream.registry import check
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q


class DiscussDataActionManager(ActionManager):
    @stream
    def mystream(self, obj, verb="posted", time=None):
        if time is None:
            time = datetime.now()
        return obj.actor_actions.filter(verb=verb, timestamp__lte=time)

    @stream
    def dataset_access_requests_sent(self, sender, recipient=None):
        notification_content_type = ContentType.objects.get(
            app_label="ddcomments", model="notification"
        )
        dataset_content_type = ContentType.objects.get(
            app_label="dddatasets", model="dataset"
        )
        filters = {"action_object_content_type": notification_content_type.id}
        filters["target_content_type"] = dataset_content_type.id
        if recipient:  # optional recipient
            filters["target"] = recipient
        # Actions where sender is the actor with extra filters applied
        return sender.actor_actions.filter(**filters)

    @stream
    def datasets_actions(self, datasets):

        ctype = ContentType.objects.get(app_label="dddatasets", model="dataset")
        return self.filter(
            action_object_content_type=ctype, action_object_object_id__in=datasets,
        )

    @stream
    def access_requests_received(self, datasets):
        notification_content_type = ContentType.objects.get(
            app_label="ddcomments", model="notification"
        )
        dataset_content_type = ContentType.objects.get(
            app_label="dddatasets", model="dataset"
        )

        return self.filter(
            action_object_content_type=notification_content_type.id,
            target_content_type=dataset_content_type.id,
            target_object_id__in=datasets,
        )

    def everything(self, *args, **kwargs):
        """
        Return all actions (public=True and False)
        """
        return self.filter(*args, **kwargs)

    @stream
    def any_everything(self, obj, **kwargs):
        """
        Stream of most recent actions where obj is the actor OR target OR action_object.
        Includes also Actions which are not public
        """
        check(obj)
        ctype = ContentType.objects.get_for_model(obj)
        return self.everything(
            Q(actor_content_type=ctype, actor_object_id=obj.pk,)
            | Q(target_content_type=ctype, target_object_id=obj.pk,)
            | Q(action_object_content_type=ctype, action_object_object_id=obj.pk,),
            **kwargs,
        )
