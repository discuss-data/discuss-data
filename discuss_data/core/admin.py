# from django.contrib import admin
# Register your models here.
from django.contrib import admin
from django.utils import timezone

from discuss_data.core.models import KeywordTagged, SystemEmailTemplate, SiteInfo, PDFTemplate, Email
from discuss_data.core import utils


import logging
logger = logging.getLogger(__name__)

class KeywordTaggedAdmin(admin.ModelAdmin):
    search_fields = ["name"]
    list_display = (
        "id",
        "name",
    )


class PDFTemplateAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "slug",
    )


class SystemEmailTemplateAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "slug",
    )


class SiteInfoAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "title",
    )

class EmailAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "subject",
        "group_to",
        "send",
        "date_sent"
    )
    readonly_fields = ("date_sent",)

    def save_model(self, request, obj, form, change):
        if obj.send:
            if obj.group_to == obj.USERS:
                emails = utils.get_users_emails()
            if obj.group_to == obj.CURATORS:
                emails = utils.get_curators_emails()
            if obj.group_to == obj.ADMINS:
                emails = utils.get_admins_emails()

            for email in emails:
                utils.dd_send_email(
                    subject=obj.subject,
                    message=obj.message,
                    email_to=[email,],
                    email_type=obj.email_type
                )
            obj.date_sent = timezone.now()
        super().save_model(request, obj, form, change)



admin.site.register(KeywordTagged, KeywordTaggedAdmin)
admin.site.register(SystemEmailTemplate, SystemEmailTemplateAdmin)
admin.site.register(PDFTemplate, PDFTemplateAdmin)
admin.site.register(SiteInfo, SiteInfoAdmin)
admin.site.register(Email, EmailAdmin)
