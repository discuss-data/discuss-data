from django.urls import path

from discuss_data.core import views

app_name = "core"

# public URLs
urlpatterns = [
    path("tags/keywords/", views.get_keyword_tags, name="tags"),
    path("polling/start/", views.polling_start, name="polling_start"),
    path("polling/stop/", views.polling_stop, name="polling_stop"),
    path("load/", views.load, name="load"),
    path("messages/", views.messages, name="messages"),
    path("search/", views.help_search, name="help_search"),
]
