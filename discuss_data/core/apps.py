from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = "discuss_data.core"
