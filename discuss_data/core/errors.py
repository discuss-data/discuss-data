"""Discuss Data Error Classes
"""


class DiscussDataError(Exception):
    """Base class for all Exceptions
    """


class MetaDataError(DiscussDataError):
    """Missing or malformed metadata"""
