from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from discuss_data.core.models import KeywordTagged, LanguageTagged
from discuss_data.dddatasets.models import (
    Category,
    DataSet,
    DataSetCreator,
    CollectionMethodsTagged,
    AnalysisMethodsTagged,
    DisciplinesTagged,
)

from discuss_data.ddusers.models import Affiliation, Country, User


@registry.register_document
class DataSetDocument(Document):
    countries = fields.ObjectField(properties={"name": fields.TextField()})
    published_categories = fields.ObjectField(properties={"name": fields.TextField()})
    published_main_category = fields.ObjectField(
        properties={"name": fields.TextField()}
    )
    owner = fields.ObjectField(
        properties={"first_name": fields.TextField(), "last_name": fields.TextField()}
    )
    dataset_creator = fields.ObjectField(properties={"name": fields.TextField()})
    keywords = fields.ObjectField(properties={"name": fields.TextField()})
    languages_of_data = fields.ObjectField(properties={"name": fields.TextField()})
    methods_of_data_collection = fields.ObjectField(
        properties={"name": fields.TextField()}
    )
    methods_of_data_analysis = fields.ObjectField(
        properties={"name": fields.TextField()}
    )
    disciplines = fields.ObjectField(properties={"name": fields.TextField()})
    # BleachField() instances are not recognized by django-elasticsearch-dsl
    # and need to be accessed as model attributes and added as a TextField()
    description = fields.TextField(attr="description")
    related_dataset_text = fields.TextField(attr="related_dataset_text")
    sources_of_data = fields.TextField(attr="sources_of_data")
    funding = fields.TextField(attr="funding")
    institutional_affiliation = fields.TextField(attr="institutional_affiliation")

    class Index:
        # Name of the Elasticsearch index
        name = "datasets"
        # See Elasticsearch Indices API reference for available settings
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = DataSet  # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            "title",
            "subtitle",
            "time_period_text",
            "time_period_from",
            "time_period_to",
        ]
        related_models = [
            Country,
            Category,
            User,
            DataSetCreator,
            KeywordTagged,
            CollectionMethodsTagged,
            AnalysisMethodsTagged,
            DisciplinesTagged,
        ]

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Country):
            instance = related_instance.dataset_country.all()
        elif isinstance(related_instance, User):
            instance = related_instance.get_published_datasets()
        elif isinstance(related_instance, Category):
            instance = related_instance.get_published_datasets_category_all()
        elif isinstance(related_instance, KeywordTagged):
            instance = DataSet.objects.filter(keywords__name=related_instance.name)
        elif isinstance(related_instance, CollectionMethodsTagged):
            instance = DataSet.objects.filter(
                methods_of_data_collection__name=related_instance.name
            )
        elif isinstance(related_instance, AnalysisMethodsTagged):
            instance = DataSet.objects.filter(
                methods_of_data_analysis__name=related_instance.name
            )
        elif isinstance(related_instance, DisciplinesTagged):
            instance = DataSet.objects.filter(disciplines__name=related_instance.name)
        elif isinstance(related_instance, LanguageTagged):
            instance = DataSet.objects.filter(
                languages_of_data__name=related_instance.name
            )
        elif isinstance(related_instance, DataSetCreator):
            instance = related_instance.dataset

        return instance

    def get_queryset(self):
        """Mandatory for excluding instances from index by filtering by attributes.
        Works only for build_index command.
        Also to improve performance we can select related in one sql request
        """
        return (
            super(DataSetDocument, self)
            .get_queryset()
            .filter(published="True")
            .select_related("owner")
        )

    @staticmethod
    def filter_instance(instance):
        """return instance only if published is True"""
        if isinstance(instance, DataSet):
            if instance.published:
                return [instance]
            else:
                return []
        else:
            return instance

    def update(self, instance, refresh=None, action="index", **kwargs):
        """Apply exclude from index filter also for index update by signal (eg instance changes)"""
        return super().update(self.filter_instance(instance), refresh, action, **kwargs)


@registry.register_document
class UserDocument(Document):
    # ObjectField works for ForeignKey relations using related_name
    # NestedField problematic in Search
    countries = fields.ObjectField(properties={"name": fields.TextField()})
    interests = fields.ObjectField(properties={"name": fields.TextField()})

    affiliation_user = fields.ObjectField(
        properties={
            "position": fields.TextField(),
            "name_of_institution": fields.TextField(),
            "place_of_institution": fields.TextField(),
            "country_of_institution": fields.TextField(),
        }
    )

    class Index:
        # Name of the Elasticsearch index
        name = "users"
        # See Elasticsearch Indices API reference for available settings
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = User  # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            "first_name",
            "last_name",
        ]
        related_models = [
            Country,
            KeywordTagged,
            Affiliation,
        ]

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Country):
            return related_instance.user_country.all()
        elif isinstance(related_instance, KeywordTagged):
            return User.objects.filter(interests__name=related_instance.name)
        elif isinstance(related_instance, Affiliation):
            return related_instance.user

    def get_queryset(self):
        """Mandatory for excluding instances from index by filtering by attributes.
        Works only for build_index command.
        Also to improve performance we can select related in one sql request
        """
        return (
            super(UserDocument, self)
            .get_queryset()
            .exclude(profile_accessibility="HID")
            .exclude(is_active=False)
            .exclude(first_name="")
            .exclude(last_name="")
            .exclude(username="AnonymousUser")
        )

    @staticmethod
    def filter_instance(instance):
        """filter hidden and internally used user accounts"""
        if isinstance(instance, User):
            if (
                instance.profile_accessibility == "HID"
                or not instance.is_active
                or instance.first_name == ""
                or instance.last_name == ""
                or instance.username == "AnonymousUser"
            ):
                return []
            else:
                return [instance]
        else:
            return instance

    def update(self, instance, refresh=None, action="index", **kwargs):
        """Apply exclude from index filter also for index update by signal (eg instance changes)"""
        return super().update(self.filter_instance(instance), refresh, action, **kwargs)
