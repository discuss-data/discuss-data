from discuss_data.core.utils import AttributeProcessor


def test_attribute_processor():
    """ test the callable AttributeProcessor to encode/decode correctly
    """
    ap = AttributeProcessor()

    name = "Ljubišas Gülçin Amélie Rømi"
    attribute_value = name.encode("utf-8").decode("ISO8859-1")
    converted_attribute_value = ap(attribute_value)

    # ensure that the chosen example is not the same in both encodings
    assert not attribute_value == converted_attribute_value
    assert name == converted_attribute_value
