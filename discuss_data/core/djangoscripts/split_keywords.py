import logging

from django.core.exceptions import ValidationError

from discuss_data.core.models import KeywordTagged

from discuss_data.dddatasets.models import DataSet

logger = logging.getLogger(__name__)


def find_keywords(split_char):
    keywords = KeywordTagged.objects.filter(name__contains=split_char)

    for tag in keywords:
        print("TAG:", tag)
        datasets = DataSet.objects.filter(keywords__name__in=[tag])

        new_tags = list()
        print("split into:")
        for elem in tag.name.split(split_char):
            new_tags.append(elem.strip())
            print("\t" + elem.strip())

        if datasets:
            print("affected Datasets:")
        else:
            print("no datasets affected")
        for ds in datasets:
            print("\t" + str(ds), ds.uuid)
            for elem in new_tags:
                pass
                # ds.keywords.add(elem)
                # ds.keywords.remove(tag)
        # tag.delete()
        print("====================")


def run():
    print("TAGS with ','")
    find_keywords(split_char=",")

    print("\n\n&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n\n")

    print("TAGS with ';'")

    find_keywords(split_char=";")
