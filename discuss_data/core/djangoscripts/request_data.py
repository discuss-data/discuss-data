import logging
import sys
from django.core.serializers import serialize
from request.models import Request

logger = logging.getLogger(__name__)


def run(*script_args):
    usage = """
    Django runscript to extract request data (all or for a specific year) for datasets and return as JSON.

        usage: docker-compose run --rm django python manage.py runscript request_data --script-args "<filter_for_year>"

        filter_for_year is used as a date filter for a specific year, empty string ("") will return data for all years
    """
    if len(script_args) < 1:
        print(usage)
        sys.exit("Error: not enough args given. Exiting")

    qs = Request.objects.filter(path__contains="dataset")
    if not script_args[0] == "":
        qs = qs.filter(time__year=script_args[0])
    print(serialize("json", qs))
