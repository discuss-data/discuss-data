import arrow
from django_q.models import Schedule


"""
    Definition of the task schedules to generate at startup
    https://django-q.readthedocs.io/en/latest/schedules.html#reference
"""
SCHEDULES = [
    # Schedule for the DAILY send_status_email task
    # for user mailings. Called daily at 20:00
    {
        "name": "statusmails_daily",
        "func": "discuss_data.ddusers.tasks.send_status_emails",
        "schedule_type": Schedule.CRON,
        "repeats": -1,
        "cron": "0 20 * * *",
    },
    # Schedule for the WEEKLY send_status_email task
    # or user mailings. Called every Saturday at 22:00
    {
        "name": "statusmails_weekly",
        "func": "discuss_data.ddusers.tasks.send_status_emails",
        "schedule_type": Schedule.CRON,
        "repeats": -1,
        "cron": "0 22 * * 6",
    },
]


def run():
    """
        Initialize all schedules defined in SCHEDULES
    """
    for elem in SCHEDULES:
        sched, created = Schedule.objects.get_or_create(name=elem.get("name"))
        sched.func = elem.get("func")
        sched.schedule_type = elem.get("schedule_type")
        sched.minutes = elem.get("minutes")
        sched.repeats = elem.get("repeats")
        sched.cron = elem.get("cron")
        sched.save()
    print("Schedules defined ")
