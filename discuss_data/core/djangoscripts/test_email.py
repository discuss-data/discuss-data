import logging
from django.core.mail import send_mail
from django.conf import settings

logger = logging.getLogger(__name__)

def send_email():
    email_from = settings.DEFAULT_FROM_EMAIL
    subject_prefixed = f"{settings.EMAIL_SUBJECT_PREFIX}Test Email"
    message_text = "This is a test email"
    email_to = ["edvfso@uni-bremen.de",]
    send_mail(
        subject_prefixed,
        message_text,
        email_from,
        email_to,
        fail_silently=False,
    )


def run():
    print("sending test email")
    send_email()
