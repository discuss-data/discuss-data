from django.contrib.messages import get_messages


"""
    Middleware class to update django messages via ajax
    using intercoolerjs custom headers.
    Add to middleware section in settings:
    "discuss_data.core.middleware.IntercoolerMessageMiddleware"
"""


class IntercoolerMessageMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # get messages stored in session
        storage = get_messages(request)

        # if messages are queued send event
        if storage._queued_messages:
            # t rigger javascipt event using intercooler header
            response["X-IC-Trigger"] = "messages-trigger"

        # Code to be executed for each request/response after
        # the view is called.

        return response
