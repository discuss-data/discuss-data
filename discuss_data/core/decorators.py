import logging

from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings

from discuss_data.ddusers.models import User
from discuss_data.dddatasets.models import DataSet

logger = logging.getLogger(__name__)


def dd_user_verified(function=None):
    """
    Decorator for views that checks that the user has been verified,
    redirecting to the respective form if not.

    Implementation follows `@login_required`
    (https://docs.djangoproject.com/en/2.2/_modules/django/contrib/auth/decorators/#login_required).

    Usage:
    ::
        @dd_user_verified
        def myview(request):
            ...
    """
    def test_func(user):
        verified_providers = settings.VERIFIED_PROVIDERS
        if user.is_superuser:
            logger.debug("User verified: superuser")
            return True
        if user.get_socialaccount_provider() in verified_providers:
            logger.debug(f"User verified: {user.get_socialaccount_provider()}")
            return True
        if user.verified:
            logger.debug(f"User verified {user.username}")
            return True
        else:
            logger.debug(f"User not verified {user.username}")
            return False

    actual_decorator = user_passes_test(
        #lambda u: getattr(u, "verified", False),
        test_func,
        login_url="/users/verification/",
        redirect_field_name="next",
    )
    return actual_decorator(function)


def dd_tou_accepted(function=None):
    """
    Decorator for views that checks that the user has accepted the Discuss Data TOU,
    redirecting to the first-log-in page if not.

    Implementation follows `@login_required`
    (https://docs.djangoproject.com/en/2.2/_modules/django/contrib/auth/decorators/#login_required).

    Usage:
    ::
        @dd_tou_accepted
@dd_user_verified
        def myview(request):
            ...
    """
    actual_decorator = user_passes_test(
        lambda u: getattr(u, "accepted_dd_tou", False),
        login_url="/users/terms-of-use/",
        redirect_field_name="next",
    )
    if function:
        return actual_decorator(function)
    return actual_decorator


def dd_profile_accessible(function):
    def wrap(request, *args, **kwargs):
        user = get_object_or_404(User, uuid=kwargs["us_uuid"])
        if (user.profile_accessibility == "PUB") or (
            user.profile_accessibility == "NET" and request.user.is_authenticated
        ):
            return function(request, *args, **kwargs)
        else:
            messages.error(
                request, "The user has chosen not to share his profile details."
            )
            return HttpResponseRedirect(
                "/users/search/"
            )  # better: redirect to last page

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def dd_dataset_access(function):
    def wrap(request, *args, **kwargs):
        if request.user.is_superuser:
            return function(request, *args, **kwargs)

        ds = get_object_or_404(DataSet, uuid=kwargs["ds_uuid"])
        dsmo = ds.dataset_management_object
        # default: not access
        access = False
        # access if dataset is frozen
        if ds.is_frozen():
            if ds.pub_request_pending() and request.user in ds.get_curators():
                access = True
            else:
                # special case versions tab
                if request.resolver_match.url_name == "prep_edit_versions":
                    access = False
                    if request.user == ds.owner or request.user.has_perm(
                        "admin_dsmo", dsmo
                    ):
                        access = True
                else:
                    return redirect(
                        "dddatasets:prep_edit_versions",
                        ds_uuid=ds.uuid,
                    )
        else:
            # access if dataset is not frozen
            if request.user == ds.owner:
                access = True
            # dataset collaborators
            elif (
                request.user.has_perm("view_dsmo", dsmo)
                or request.user.has_perm("edit_dsmo", dsmo)
                or request.user.has_perm("admin_dsmo", dsmo)
            ):
                access = True
            else:
                access = False
        if not access:
            raise PermissionDenied
        return function(request, *args, **kwargs)

    return wrap
