import logging

from django.core.exceptions import PermissionDenied
from guardian.core import ObjectPermissionChecker
from elasticsearch_dsl import Q
from discuss_data.core.documents import DataSetDocument, UserDocument
from discuss_data.pages.models import ManualPage


logger = logging.getLogger(__name__)


SEARCH_FIELDS_USER = [
    "first_name",
    "last_name^3",
    "countries.name",
    "interests.name",
    "affiliation_user.place_of_institution",
    "affiliation_user.country_of_institution",
    "affiliation_user.name_of_institution",
    "affiliation_user.position",
]
SEARCH_FIELDS_DATASET = [
    "title^3",
    "subtitle^2",
    "dataset_creator.name",
    "countries.name",
    "keywords.name",
    "published_categories.name",
    "published_main_category.name",
    "owner.first_name",
    "owner.last_name^2",
    "description",
    "related_dataset_text",
    "sources_of_data",
    "funding",
    "institutional_affiliation",
    "languages_of_data.name",
    "methods_of_data_collection.name",
    "analysis_of_data_collection.name",
    "disciplines.name",
]

# SEARCH_FILTERS = {
#     "countries": list(),
#     "categories": list(),
#     "keywords": list(),
#     "languages": list(),
#     "methods_of_data_collection": list(),
#     "methods_of_data_analysis": list(),
#     "disciplines": list(),
# }


# Commented out, as filters are not applied using this code.
# Switching back to older version below.
# def add_tag_filter(search, filter_name, filter_list, filter_field):
#     """
#     Add specific filters dynamically to search object using the alternative elasticsearch-dsl syntax
#     see: https://elasticsearch-dsl.readthedocs.io/en/latest/search_dsl.html#dotted-fields
#     """
#     if filter_list:
#         query_list = list()
#         # construct list of Q match clauses for filtering elements
#         for elem in filter_list:
#             if elem != "":
#                 query_list.append(Q("match", **{"{}.name".format(filter_name): elem}))
#         # add the list to the search object
#         return search.filter("terms", **{filter_name + "." + filter_field: query_list})
#     else:
#         # return original search object if filter_list is None
#         return search


def add_tag_filter(search, filter_name, filter_list):
    """
    Add specific filters dynamically using the alternative elasticsearch-dsl syntax
    see: https://elasticsearch-dsl.readthedocs.io/en/latest/search_dsl.html#dotted-fields
    """
    if filter_list:
        for elem in filter_list:
            if elem != "":
                search = search.query("match", **{"{}.name".format(filter_name): elem})
    return search


def index_search(index, term, **kwargs):
    """
    Using elastic simple_query_string with default wildcard attached, default AND
    operator and a limited set of weighted fields. Parallel use of wildcards and
    fuzziness is not possible. See fields and options:
    https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-simple-query-string-query.html
    """

    if term:
        # add fuzziness to each word in query for catching misspellings
        # term_list = term.split(" ")
        # wc_term = "~1 ".join(term_list)
        # wc_term = wc_term + "~1"
        # add wildcard to each word
        term_list = term.split(" ")
        wc_term = "* ".join(term_list)
        wc_term = wc_term + "*"
    else:
        # use wildcard search for returning all objects in index if no term is provided (default on list pages)
        wc_term = "*"

    countries = kwargs.get("countries", None)
    categories = kwargs.get("categories", None)
    keywords = kwargs.get("keywords", None)
    languages = kwargs.get("languages", None)
    methods_of_data_collection = kwargs.get("methods_of_data_collection", None)
    methods_of_data_analysis = kwargs.get("methods_of_data_analysis", None)
    disciplines = kwargs.get("disciplines", None)

    if index == "user_index":
        document = UserDocument
        search_fields = SEARCH_FIELDS_USER

    # do only index published datasets!!
    if index == "dataset_index":
        document = DataSetDocument
        search_fields = SEARCH_FIELDS_DATASET

    # create an elastic simple query clause
    q = Q(
        "simple_query_string",
        query=wc_term,
        fields=search_fields,
        default_operator="AND",
    )

    # "filter" does not work with categories as they come from 2 different fields.
    # Using "query" clause instead, but only AND concatenation with q produces reasonable results
    # THIS DID NOT WORK, query with category "Social policy" returned "Social media" as well
    # category filtering moved to bottom of this function, uses queryset filtering instead
    # if categories:
    #     for elem in categories:
    #         if elem != "":
    #             q_cat = Q("match", published_main_category__name=elem) | Q(
    #                 "match", published_categories__name=elem
    #             )
    #             # using & instead of | to add to query
    #             q = q & q_cat
    #             logger.debug("SEARCH CATEGORIES")
    #             logger.debug(q)

    # create elastic bool query and include q query
    filter_query = Q("bool", must=q)

    # create search object from document
    search = document.search().query(filter_query)

    # add filters for both dataset_index and user_index to search
    search = add_tag_filter(search, "countries", countries)

    # add filters for dataset_index
    if index == "dataset_index":
        search = add_tag_filter(search, "keywords", keywords)
        search = add_tag_filter(search, "languages_of_data", languages)
        search = add_tag_filter(search, "disciplines", disciplines)
        search = add_tag_filter(
            search, "methods_of_data_collection", methods_of_data_collection
        )
        search = add_tag_filter(
            search, "methods_of_data_analysis", methods_of_data_analysis
        )

    # add filters for user_index
    if index == "user_index":
        search = add_tag_filter(search, "interests", keywords)

    # return all results, not just 10 (elastic default)
    total = search.count()
    search = search[0:total]
    qs = search.to_queryset()
    if categories:
        for elem in categories:
            if elem != "":
                qs = qs.filter(published_main_category__name=elem) | qs.filter(
                    published_categories__name=elem
                )
    return qs.distinct()


def split_items_2(items):
    output = list()
    items_len = len(items)
    for i in range(0, items_len, 2):
        try:
            output.append((items[i], items[i + 1]))
        except IndexError:
            output.append((items[i], None))
    return output


def split_items_3(items):
    output = list()
    items_len = len(items)
    for i in range(0, items_len, 3):
        try:
            output.append((items[i], items[i + 1], items[i + 2]))
        except IndexError:
            try:
                output.append((items[i], items[i + 1], None))
            except IndexError:
                output.append((items[i], None))
    return output


def check_perms_403(permission, user, dd_obj):
    if not check_perms(permission, user, dd_obj):
        raise PermissionDenied


def is_curator_or_check_perms_for_ds(permission, user, ds):
    if user in ds.get_curators():
        return True
    else:
        check = check_perms(permission, user, ds)
        return check


def get_oep_parameters(dtype, outer_panel, add_text):
    return {
        "type": dtype,
        "title": "{}s".format(dtype.capitalize()),
        "edit_url": "{}_add".format(dtype),
        "list_url": "get_user_{}s".format(dtype),
        "outer_panel": outer_panel,
        "add_text": add_text,
    }


def check_perms(permission, user, dd_obj):
    try:
        if dd_obj.owner == user:
            logger.debug("is owner")
            return True
    except Exception:
        logger.debug("is not owner")

    checker = ObjectPermissionChecker(user)

    if checker.has_perm(permission, dd_obj):
        logger.debug("%s has perm %s on %s" % (user, permission, dd_obj))
        return True
    else:
        logger.debug("%s lacks perm %s on %s" % (user, permission, dd_obj))
        return False
    return False


# this is not a view function
def get_help_texts(slug):
    try:  # try block because this code part gets somehow called before migrations are executed.
        # Querying for a ManualPage thus triggers a db error in an empty db
        # ("django.db.utils.ProgrammingError: relation "pages_manualpage" does not exist")
        # as the db schema is not defined yet
        manpage = ManualPage.objects.get(slug=slug)
        help_text_dict = dict()
        help_text_dict["manpage"] = manpage
        for block in manpage.body:
            help_text_dict[block.value["field_slug"]] = block.value["paragraph"]
        return help_text_dict
    except Exception:
        return None
