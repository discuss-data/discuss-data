import logging

from django.db import models
from django_bleach.models import BleachField
from taggit.models import GenericTaggedItemBase, TagBase
from wagtail.admin.panels import FieldPanel
from django.contrib.sites.models import Site
from django.utils.translation import gettext as _

logger = logging.getLogger(__name__)


class Topic(models.Model):
    title = models.CharField(max_length=200)
    description = BleachField(blank=True)

    def __str__(self):
        return self.title


class Keyword(models.Model):
    title = models.CharField(max_length=200)
    description = BleachField(blank=True)

    def __str__(self):
        return self.title


class EnTagged(TagBase):
    class Meta:
        verbose_name = "English Tag"
        verbose_name_plural = "English Tags"


class EnglishTags(GenericTaggedItemBase):
    tag = models.ForeignKey("EnTagged", on_delete=models.CASCADE)


class AffiliationTagged(TagBase):
    class Meta:
        verbose_name = "Affiliation Tag"
        verbose_name_plural = "Affiliation Tags"


class AffiliationTags(GenericTaggedItemBase):
    tag = models.ForeignKey(
        "AffiliationTagged",
        related_name="affiliation_tags",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.tag.name


class KeywordTagged(TagBase):
    class Meta:
        verbose_name = "Keyword Tag"
        verbose_name_plural = "Keyword Tags"

    panels = [
        FieldPanel("name"),
    ]


class KeywordTags(GenericTaggedItemBase):
    tag = models.ForeignKey(
        "KeywordTagged",
        related_name="keywordtags_keywordtagged",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.tag.name


class LanguageTagged(TagBase):
    class Meta:
        verbose_name = "Language Tag"
        verbose_name_plural = "Language Tags"

    panels = [
        FieldPanel("name"),
    ]


class LanguageTags(GenericTaggedItemBase):
    tag = models.ForeignKey(
        "LanguageTagged",
        related_name="languagetags_languagetagged",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.tag.name


class Link(models.Model):
    url = models.URLField()
    doi = models.CharField(max_length=200, blank=True)
    description = models.CharField(max_length=400, blank=True)

    # def lookup_doi(self):
    #     service, doi_dict = lookup_doi(self.doi)
    #     print(doi_dict['author'])

    def __str__(self):
        return self.url


class SystemEmailTemplate(models.Model):
    content = models.TextField()
    slug = models.SlugField()

    def __str__(self):
        return self.slug


class SiteInfo(models.Model):
    site = models.OneToOneField(Site, on_delete=models.CASCADE)
    title = models.CharField(max_length=200, blank=True)
    subtitle = models.CharField(max_length=400, blank=True)
    text = models.TextField(blank=True)
    email_sys = models.EmailField(_("Email (System)"), max_length=254)
    email_contact = models.EmailField(_("Email (Contact)"), max_length=254)

    def __str__(self):
        return self.title


class PDFTemplate(models.Model):
    content = models.TextField()
    slug = models.SlugField()

    def __str__(self):
        return self.slug


class Email(models.Model):
    CURATORS = "CUR"
    USERS = "USR"
    ADMINS = "ADM"

    GROUP_TO_CHOICES = [
        (CURATORS, _("curators")),
        (USERS, _("users")),
        (ADMINS, _("administrators")),
    ]

    UPDATE = "UPD"
    POSTING = "PST"

    EMAIL_TYPE_CHOICES = [
        (UPDATE, _("update")),
        (POSTING, _("posting")),
    ]
    email_type = models.CharField(
        max_length=3,
        choices=EMAIL_TYPE_CHOICES,
        default=POSTING,
    )
    subject = models.CharField(
        max_length=400
    )
    message = BleachField()
    group_to = models.CharField(
        max_length=3,
        choices=GROUP_TO_CHOICES,
        default=USERS,
    )
    send = models.BooleanField(default=False)
    date_sent = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.subject
