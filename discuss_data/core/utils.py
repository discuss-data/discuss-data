import logging
import requests
import os
import json
from datetime import timedelta

from datacite import DataCiteMDSClient, schema41
from datacite.errors import DataCiteServerError
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils import timezone
from django.apps import apps

logger = logging.getLogger(__name__)


class AttributeProcessor:
    """Create a callable `AttributeProcessor` to process SHIB_HEADER attribute values.

    This processor is used in the Shibboleth attribute map configuration and then called by the
    ShibbolethRemoteUserMiddleware while parsing the attributes.

    Usage:
    ::
        SHIBBOLETH_ATTRIBUTE_MAP = {
            "HTTP_REMOTE_USER": (True, "username", AttributeProcessor()),
        }

    """

    def __call__(self, x):
        return x.encode("ISO8859-1").decode("utf-8")


def qs_to_str(qs, label_field):
    obj_list = list()
    for elem in qs:
        obj_list.append(getattr(elem, label_field))
    return ", ".join(obj_list)


def generate_discuss_data_doi(dataset) -> str:
    creators = list()
    for creator in dataset.get_creators():
        if creator.creator_type == creator.PERSON:
            creators.append(
                {
                    "nameType": "Personal",
                    "creatorName": "{}, {}".format(creator.name, creator.first_name),
                }
            )
        if creator.creator_type == creator.INSTITUTION:
            creators.append({"nameType": "Organizational", "creatorName": creator.name})

    logger.debug(creators)

    data = {
        "identifier": {
            "identifier": str(settings.DC_PREFIX) + "/" + str(dataset.uuid),
            "identifierType": "DOI",
        },
        "creators": creators,
        "titles": [{"title": str(dataset.title)}],
        "publisher": "Discuss Data", # this is the data repository, see https://datacite-metadata-schema.readthedocs.io/en/4.5/properties/publisher/
        "publicationYear": str(dataset.publication_date.year),
        "resourceType": {"resourceTypeGeneral": "Dataset"},
    }

    logger.debug(data)
    logger.debug(schema41.validate(data))

    if schema41.validate(data):
        doc = schema41.tostring(data)
    else:
        doc = None

    logger.debug(str(doc))
    # Initialize the MDS client.
    d = DataCiteMDSClient(
        username=str(settings.DC_USER),
        password=str(settings.DC_PWD),
        url=str(settings.DC_URL),
        prefix=str(settings.DC_PREFIX),
        timeout=10,
    )

    logger.debug("dataset.get_absolute_url(): %s", str(dataset.get_absolute_url()))
    response = d.metadata_post(doc)
    logger.debug("response: %s", response)
    if response.startswith("OK"):
        doi = response[response.find("(") + 1 : response.find(")")]
        try:
            d.doi_post(str(doi), str(dataset.get_absolute_url()))
        except DataCiteServerError as e:
            logger.exception(e)

    return doi


def weekly_td():
    # week timedelta
    startdate = timezone.now()
    enddate = startdate - timedelta(days=7)
    return startdate, enddate


def daily_td():
    # day timedelta
    startdate = timezone.now()
    enddate = startdate - timedelta(hours=24)
    return startdate, enddate

def send_user_verification_email():
    pass


def send_update_email(subject, message, email_to):
    dd_send_email(subject, message, email_to, email_type='UPD', email_from=settings.DEFAULT_FROM_EMAIL)


def dd_send_email(subject, message, email_to, email_type, email_from=settings.ALT_FROM_EMAIL):
    if email_type == "UPD":
        template = "core/update_email.html"
    template = "core/email.html"
    email_from = email_from
    subject_prefixed = f"{settings.EMAIL_SUBJECT_PREFIX}{subject}"
    # model = apps.get_model('discuss_data.core.SiteInfo')
    site_info = dict()
    site_info["title"] = settings.DD_TITLE
    site_info["subtitle"] = settings.DD_SUBTITLE
    site_info["url"] = settings.DISCUSS_DATA_HOST

    message_text = render_to_string(
        template,
        {
            "message": message, "subject": subject, "html": False, "site_info": site_info
        },
    )

    try:
        send_mail(
            subject_prefixed,
            message_text,
            email_from,
            email_to,
            fail_silently=False,
            # html_message=message_html
        )
    except Exception as e:
        logger.error(e)


def create_pdf(data, uuid, folder, name):
    url = "http://pdf_service:5150/pdfgen"
    headers = {"Content-type": "application/json"}

    pdf_response = requests.post(url, headers=headers, data=json.dumps(data))
    file_name = f"{uuid}-{name}.pdf"
    path = os.path.join(settings.DATA_ROOT, folder, file_name)
    f = open(path, "wb")
    f.write(pdf_response.content)
    f.close()
    return path


def get_users_emails():
    user_model = apps.get_model("ddusers.User")
    emails = set()
    for user in user_model.objects.all():
        emails.add(user.get_email())
    return emails


def get_curators_emails():
    category_model = apps.get_model("dddatasets.Category")
    emails = set()

    for category in category_model.objects.all():
        for user in category.curators.all():
            emails.add(user.get_email())
    return emails


def get_admins_emails():
    user_model = apps.get_model("ddusers.User")
    emails = set()
    for user in user_model.objects.filter(is_superuser=True):
        emails.add(user.get_email())
    return emails
