import logging

from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import F
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils.decorators import method_decorator
from django.utils.translation import gettext as _
from django.views.generic import View
from django.views.generic.list import ListView
from wagtail.models import Page

from discuss_data.core.decorators import dd_tou_accepted
from discuss_data.core.models import KeywordTagged
from discuss_data.core.helpers import (
    index_search,
    split_items_2,
    split_items_3,
)
from discuss_data.pages.models import LandingPage, TextPage


logger = logging.getLogger(__name__)

SLUG_LANDING = getattr(settings, "WAGTAIL_LANDING_PAGE", None)
SLUG_CONTACTS = getattr(settings, "WAGTAIL_CONTACTS_PAGE", None)
SLUG_SECURITY = getattr(settings, "WAGTAIL_SECURITY_PAGE", None)


class SearchUsersAndDatasetsView(ListView):
    paginate_by = 25
    template_name = "core/_search_results.html"

    def get_queryset(self):
        query = self.request.GET.get("q")
        countries = self.request.GET.getlist("countries")
        categories = self.request.GET.getlist("categories")

        queryset = index_search(
            "user_index", query, countries=countries, categories=categories
        )

        # hide hidden and inactive users
        filtered = queryset.exclude(profile_accessibility="HID").exclude(
            is_active=False
        )

        if self.request.user.is_authenticated:
            return filtered

        # also hide network users by default (when not logged in)
        return filtered.exclude(profile_accessibility="NET")


def help_search(request):
    return core_search_view(request, "help_index", 10)


@method_decorator(dd_tou_accepted, name="dispatch")
class ProtectedView(LoginRequiredMixin, View):
    """ base class for views that require the user to log in and to accept
        the terms of use
    """

    login_url = getattr(settings, "LOGIN_URL", None)
    redirect_field_name = getattr(settings, "LOGIN_REDIRECT_URL", None)


def get_search_params(request, order_by_filter=None, category=None):
    search_params = dict()
    search_params["q"] = request.GET.get("q")

    # extract country slugs from GET
    search_params["countries"] = request.GET.getlist("countries")

    # extract category slugs from GET
    categories = request.GET.getlist("categories")
    if category:
        categories.append(category.name)
    search_params["categories"] = categories

    # extract keyword slugs from GET
    search_params["keywords"] = request.GET.getlist("keywords")

    # extract language slugs from GET
    search_params["languages"] = request.GET.getlist("languages")

    # extract disciplines slugs from GET
    search_params["disciplines"] = request.GET.getlist("disciplines")

    # extract methods_of_data_collection slugs from GET
    search_params["methods_of_data_collection"] = request.GET.getlist(
        "methods_of_data_collection"
    )

    # extract methods_of_data_analysis slugs from GET
    search_params["methods_of_data_analysis"] = request.GET.getlist(
        "methods_of_data_analysis"
    )

    try:
        search_params["orderby"] = [order_by_filter]
    except UnboundLocalError:
        pass
    return search_params


def core_search_view(request, search_index, objects_on_page, category=None):
    # search result ordering
    order_by_filter = request.GET.get("orderby")
    if order_by_filter == "alpha":
        order_by = "title"
    else:
        order_by = "-publication_date"

    search_params = get_search_params(request, order_by_filter, category)
    if search_index == "dataset_index":
        template = "dddatasets/search_results.html"
        queryset = index_search(
            "dataset_index",
            search_params["q"],
            countries=search_params["countries"],
            categories=search_params["categories"],
            keywords=search_params["keywords"],
            languages=search_params["languages"],
            methods_of_data_analysis=search_params["methods_of_data_analysis"],
            methods_of_data_collection=search_params["methods_of_data_collection"],
            disciplines=search_params["disciplines"],
        )
        logger.debug("SEARCH PARAMS %s", search_params)
        # only the dataset with the highest published version per dsmo will be listed in search results
        # generate SQL join using Django F() expressions
        # https://docs.djangoproject.com/en/3.0/topics/db/queries/#filters-can-reference-fields-on-the-model
        queryset = queryset.filter(
            dataset_management_object__main_published_ds_id=F("id")
        ).order_by(order_by)

    elif search_index == "user_index":
        template = "ddusers/_search_results.html"
        queryset = (
            index_search(
                "user_index",
                search_params["q"],
                countries=search_params["countries"],
                keywords=search_params["keywords"],
            )
            .exclude(profile_accessibility="HID")  # exclude hidden users from search
            .order_by("last_name")
        )

    elif search_index == "help_index":
        template = "pages/index_page_search.html"
        queryset = Page.objects.live().search(search_params["q"])
    else:
        logger.debug("no search index given")

    pagination = False
    paginator_range = None
    paginator_last_page = None
    # deactivate paging by setting objects_on_page to an unrealistic high value
    objects_on_page = 1000

    # remove existing GET page key from querydict
    get_params_without_page = request.GET.copy()
    if get_params_without_page.get("page"):
        del get_params_without_page["page"]

    # if more then 24 users activate pagination
    if queryset.count() > objects_on_page:

        paginator = Paginator(queryset, objects_on_page)  # Show 25 contacts per page
        pagination = True
        page = request.GET.get("page")
        paginator_range = range(1, paginator.num_pages + 1)
        paginator_last_page = paginator.num_pages + 1
        try:
            objects = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            objects = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            objects = paginator.page(paginator.num_pages)
    else:
        objects = queryset

    return render(
        request,
        template,
        {
            "object_list": objects,
            "pagination": pagination,
            "paginator_range": paginator_range,
            "paginator_last_page": paginator_last_page,
            "search_params": search_params,
            "query": search_params["q"],
            "get_params": get_params_without_page,
            "category": category,
        },
    )


def landing_page(request):
    """
    Dashboard User Page
    """
    page = get_object_or_404(LandingPage, slug="discuss-data-landingpage")
    news_items = split_items_2(page.news_item.all())
    slogan_items = split_items_3(page.slogans_item.all())
    return render(
        request,
        "landingpage/landing_page.html",
        {"page": page, "news_items": news_items, "slogan_items": slogan_items},
    )


def about_page(request):
    """
    Platform about page
    """
    page = get_object_or_404(LandingPage, slug="discuss-data-landingpage")
    return render(request, "about.html", {"page": page},)


def contacts_page(request):
    """
    Platform contacts page
    """
    page = get_object_or_404(TextPage, slug=SLUG_CONTACTS)
    return render(request, "pages/text_page.html", {"page": page},)


def security_page(request):
    """
    Platform security page
    """
    page = get_object_or_404(TextPage, slug=SLUG_SECURITY)
    return render(request, "pages/text_page.html", {"page": page},)


def login_page(request: HttpRequest):
    """
    Platform login page
    """
    return render(request, "login.html")


def handler404(request):
    response = render(request, "404.html")
    response.status_code = 404
    return response


def handler403(request):
    response = render(request, "403.html")
    response.status_code = 403
    return response


def response403(message):
    response = HttpResponse(_(message))
    # response.status_code = 403
    return response


def handler500(request, *args, **argv):
    try:
        from sentry_sdk import last_event_id
    except ImportError:
        response = render(request, "500.html")
        response.status_code = 500
        return response
    else:
        return render(
            request, "500.html", {"sentry_event_id": last_event_id()}, status=500
        )


def get_keyword_tags(request):
    qs = KeywordTagged.objects.all()
    query = None
    if query:
        return qs.filter(name__istartswith=query)

    return render(request, "core/_tags.html", {"qs": qs})


def polling_start(request):
    response = HttpResponse()
    response["X-IC-ResumePolling"] = "true"
    return response


def polling_stop(request):
    response = HttpResponse()
    response["X-IC-CancelPolling"] = "true"
    return response


def load(request):
    # return an empty response when core:load url is called.
    # needed for js initialisation in _tag_add_control.html
    response = HttpResponse()
    return response


def messages(request):
    return render(request, "core/_messages.html", {})
