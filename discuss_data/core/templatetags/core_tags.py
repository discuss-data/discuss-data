import logging

from django import template
from django.utils.html import format_html
from wagtail.models import Page

from discuss_data.ddcomments.forms import CommentForm, NotificationForm
from discuss_data.ddcomments.models import Notification
from discuss_data.ddusers.models import User
from discuss_data.pages.models import ManualPage, TocTree
from discuss_data.dddatasets.models import License
from django.apps import apps

register = template.Library()
DEFAULT_USER_IMAGE = "/static/images/user_default.png"

logger = logging.getLogger(__name__)


@register.simple_tag
def nav_active(req_path, url_str, *args, **kwargs):
    """
    nav_active() will check the web request path and compare it
    to the given url string, setting the active class if they match.

    Needs clean URL naming scheme!

    Use in template: {% nav_active request.path "/url_str_here"%}
    """
    try:
        exclude = kwargs["exclude"]
    except KeyError:
        exclude = None

    if exclude:
        exlist = exclude.split(" ")

    try:
        if exclude:
            if req_path.startswith(url_str):
                for entry in exlist:
                    if req_path.startswith(entry):
                        return ""
                return "active"
        else:
            if req_path.startswith(url_str):
                return "active"
            return ""
    except Exception:
        return ""


@register.filter
def get_dict(some_object):
    """
    Returns the __dict__ for every object
    """
    return some_object.__dict__


@register.filter
def remove_underscores(string):
    """
    Returns the string with underscores replaces by spaces
    """
    return string.replace("_", " ")


@register.filter
def get_user_image(uuid):
    user = User.objects.get(uuid=uuid)
    try:
        image_url = user.photo.url
    except Exception:
        image_url = DEFAULT_USER_IMAGE
    return image_url


@register.filter
def get_value(dictionary, key):
    """
    Returns the value in dictionary
    """
    try:
        return dictionary[key]
    except Exception:
        return None


@register.filter
def get_file_icon(mime_str: str) -> str:
    """
    Returns the icon class string for the mime group
    """
    if mime_str == "application/zip":
        return "i-file-zip-md"
    if mime_str in ("archive/pdf", "application/pdf"):
        return "i-file-pdf-md"

    try:
        kind, ext = mime_str.split("/")
        return "i-file-" + kind + "-md"
    except Exception:
        return "i-file-md"


@register.filter
def user_has_restricted_access(dataset, user):
    return dataset.user_has_ra_view_right(user)


@register.filter
def user_has_admin_right(dataset, user):
    try:
        return dataset.user_has_admin_right(user)
    except AttributeError:
        logger.error(
            "Dataset mentioned in notification does not exist. Triggerd by user %s",
            format(user.username),
        )
        return False


@register.filter
def prepare_notification_form(notification, user):
    if notification.notification_type == Notification.ACCESS_REQUEST:
        recipient_uuid = notification.owner.uuid
    elif notification.notification_type == Notification.PUB_REQUEST:
        recipient_uuid = notification.owner.uuid
    else:
        if notification.owner.uuid == user.uuid:
            recipient_uuid = notification.recipient.uuid
        else:
            recipient_uuid = notification.owner.uuid

    parent_uuid = None
    if notification.level > 0:
        if not notification.get_next_sibling():
            if notification.parent:
                parent_uuid = notification.parent.uuid
            initial = {
                "recipient_uuid": recipient_uuid,
                "parent_uuid": parent_uuid,
            }
            return NotificationForm(initial=initial)
    else:
        if notification.is_leaf_node():
            parent_uuid = notification.uuid
            initial = {
                "recipient_uuid": recipient_uuid,
                "parent_uuid": parent_uuid,
            }
            return NotificationForm(initial=initial)
    return None


@register.filter
def prepare_comment_form(comment, user):
    parent_uuid = None
    if comment.level > 0:
        if not comment.get_next_sibling():
            if comment.parent:
                parent_uuid = comment.parent.uuid
            initial = {
                "parent_uuid": parent_uuid,
            }
            return CommentForm(initial=initial)
    else:
        if comment.is_leaf_node():
            parent_uuid = comment.uuid
            initial = {
                "parent_uuid": parent_uuid,
            }
            return CommentForm(initial=initial)
    return None


@register.filter
def get_parent_comment(comment):
    parent = None
    if comment.level > 0:
        if not comment.get_next_sibling():
            if comment.parent:
                parent = comment.parent
    else:
        if comment.is_leaf_node():
            parent = comment
    return parent


@register.filter
def subtract(value, arg):
    return value - arg


@register.filter
def divide(value, arg):
    return value / arg


@register.filter
def replace_vars(string, vars_dict):
    for key, value in vars_dict.items():
        string = str(string).replace(key, value)
    return string


@register.simple_tag(takes_context=True)
def get_all_pages(context):
    return Page.objects.live()


@register.simple_tag(takes_context=True)
def get_root_help_page(context):
    """get the root page of help"""
    try:
        root_help_page = Page.objects.live().get(slug="discuss-data-help")
    except Page.DoesNotExist:
        root_help_page = None
    return root_help_page


@register.simple_tag
def get_toc_tree(slug):
    """get the table of contents saved in a TocTree for navigation"""
    return format_html(TocTree.objects.get(slug=slug).html)


@register.simple_tag(takes_context=True)
def get_curators_page(context):
    """get the curators page from wagtail"""
    try:
        curators_page = ManualPage.objects.live().get(slug="who-are-curators")
    except ManualPage.DoesNotExist:
        curators_page = None
    return curators_page


@register.filter
def add_url_search_params(search_params_dict):
    """add search params from a dictionary of lists to an url"""
    params_string = "?q="
    query = search_params_dict["q"]
    if query:
        params_string += query

    for key, value in search_params_dict.items():
        if key != "q":
            if value:
                for entry in value:
                    params_string += "&{}={}".format(key, entry)
    return params_string


@register.simple_tag
def get_manpage(page_slug):
    """ get a man page from wagtail
    """
    try:
        man_page = ManualPage.objects.live().get(slug=page_slug)
    except ManualPage.DoesNotExist:
        man_page = None
    return man_page


@register.filter
def get_license(slug):
    """ get a license from a slug
    """
    try:
        return License.objects.get(license_slug=slug)
    except License.DoesNotExist:
        return None


@register.filter
def get_all_objects(model_name):
    """ get all objects of a given model
        call with: filters_all='core.LanguageTagged'|get_all_objects
    """
    Model = apps.get_model(model_name)
    qs = Model.objects.all()
    return qs
