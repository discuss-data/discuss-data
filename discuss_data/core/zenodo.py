import requests
import json
import logging
import copy
from django.conf import settings

logger = logging.getLogger(__name__)


class ZenodoPublisher:
    """
    Publish objects to Zenodo
    """

    def __init__(self, files, metadata):
        logger.debug("Init ZenodoPublisher")
        self.files = files
        self.metadata = metadata
        self.params = {}
        self.headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {settings.ZENODO_ACCESS_TOKEN}",
        }
        self.deposition_id, self.bucket_url = self.get_bucket_url()

    def get_deposition_url(self, deposition_id=None):
        """
        returns url of depositions or of specific id
        """
        if deposition_id:
            return f"{settings.ZENODO_URL}/deposit/depositions/{deposition_id}"
        return f"{settings.ZENODO_URL}/deposit/depositions"

    def get_bucket_url(self):
        """
        returns url of the bucket
        """
        deposition_url = self.get_deposition_url()
        r = requests.post(
            deposition_url, json={}, params=self.params, headers=self.headers
        )

        deposition_id = r.json()["id"]
        bucket_url = r.json()["links"]["bucket"]
        logger.debug(f"deposition_id = {deposition_id}, bucket_url = {bucket_url}")
        return deposition_id, bucket_url

    def upload_file(self, filename, filepath):
        """upload a file to a bucket, return response"""
        headers = copy.deepcopy(self.headers)
        headers["Content-Type"] = "application/octet-stream"
        with open(filepath, "rb") as fp:
            r = requests.put(
                f"{self.bucket_url}/{filename}",
                data=fp,
                params=self.params,
                headers=headers,
            )
        logger.debug(f"File Upload: {r.json()}")
        return r.json()

    def upload_all_files(self):
        """upload multiple files to a bucket, return list of responses"""
        logger.debug(f"[ZENODO PUBLISHER] files to upload: {self.files}")
        return [self.upload_file(file[0], file[1]) for file in self.files]

    def upload_metadata(self):
        """upload metadata to a record, return response"""
        deposition_url = self.get_deposition_url(self.deposition_id)
        r = requests.put(
            deposition_url,
            params=self.params,
            data=json.dumps(self.metadata),
            headers=self.headers,
        )
        # r.raise_for_status()
        logger.debug(f"Metadata Upload: {r.json()}")
        return r.json()

    def publish(self):
        publish_url = f"{self.get_deposition_url(self.deposition_id)}/actions/publish"
        r = requests.post(publish_url, params=self.params, headers=self.headers)
        logger.debug(f"Zenodo publish: {r.json()}")
        logger.debug(r.status_code)
        return r.status_code, r.json()


class ZenodoDatasetPublisher(ZenodoPublisher):
    """
    Publish Discuss Data Datasets to Zenodo
    """

    def __init__(self, dataset):
        logger.debug("Init ZenodoDatasetPublisher")
        self.dataset = dataset
        self.metadata = {"metadata": self.set_metadata(self.dataset)}
        self.files = self.get_files(self.dataset)
        ZenodoPublisher.__init__(self, self.files, self.metadata)

    def get_creators_list(self, queryset):
        """
        helper for set_metadata
        """
        creators = list()
        for entry in queryset:
            if entry.creator_type == "INS":
                creators.append({"name": entry.name})
            else:
                if entry.first_name:
                    name = f"{entry.name}, {entry.first_name}"
                else:
                    name = entry.name
                creators.append({"name": name})
        return creators

    def get_creation_date(self, dataset, metadata):
        """
        helper for set_metadata
        """
        if not dataset.date_of_data_creation_from:
            if dataset.date_of_data_creation_text:
                metadata[
                    "notes"
                ] += f"<p>Dataset creation: {dataset.date_of_data_creation_text}</p>"
            return None
        date_entry = dict()
        date_entry["type"] = "Collected"
        date_entry["start"] = dataset.date_of_data_creation_from.strftime("%Y-%m-%d")
        if not dataset.date_of_data_creation_to:
            date_entry["end"] = date_entry["start"]
        date_entry["end"] = dataset.date_of_data_creation_to.strftime("%Y-%m-%d")
        if dataset.date_of_data_creation_text:
            date_entry["description"] = dataset.date_of_data_creation_text
        return date_entry

    def get_all_keywords_as_list(self, dataset):
        """
        helper for set_metadata
        """
        keywords = list()
        tag_lists = [
            dataset.get_keyword_tags(),
            dataset.get_countries(),
            dataset.get_language_tags(),
            dataset.get_disciplines_tags(),
        ]
        for tags in tag_lists:
            for entry in tags:
                keywords.append(entry.name)
        return keywords

    def get_method(self, dataset):
        """
        helper for set_metadata
        """
        collection = ", ".join(
            [tag.name for tag in dataset.get_methods_of_data_collection_tags()]
        )
        analysis = ", ".join(
            [tag.name for tag in dataset.get_methods_of_data_analysis_tags()]
        )
        output = ""
        if collection:
            output += f"Method(s) of data collection: {collection}"
        if collection and analysis:
            output += f"<br>"
        if analysis:
            output += f"Method(s) of data analysis: {analysis}"
        return output

    def get_files(self, dataset):
        """
        get files from dataset as list of tuples [(file1_name, file1_path), (file2_name, file2:path)]
        """
        files = list()
        for df in dataset.get_all_files():
            files.append((df.name, df.file.path))
        logger.debug(f"Files: {files}")
        return files

    def set_metadata(self, dataset):
        """
        Set zenodo metadata fields for Discuss Data datasets
        """
        notes = f'Published on Discuss Data, <a href="{dataset.get_absolute_url()}">{dataset.get_absolute_url()}</a>'
        curator = dataset.publication_accepted_by
        metadata = {
            "title": dataset.get_fulltitle(),
            "description": dataset.description,
            "publication_date": dataset.publication_date.strftime("%Y-%m-%d"),
            "access_right": "open",
            "license": dataset.license.get_zenodo_license_slug(),
            "keywords": self.get_all_keywords_as_list(dataset),
            "notes": notes,
            "related_identifiers": [
                {"relation": "isAlternateIdentifier", "identifier": dataset.doi},
                {
                    "relation": "isIdenticalTo",
                    "identifier": dataset.doi,
                    "resource_type": "dataset",
                },
            ],
            "contributors": [
                {
                    "name": f"{curator.last_name}, {curator.first_name}",
                    "affiliation": "Discuss Data",
                    "type": "DataCurator",
                },
            ],
            "references": [dataset.get_data_citation()],
            "communities": [{'identifier':'discuss-data-eescca'}],
            "version": str(dataset.version),
            #'language': TODO: iso code of ONE main language needed, need to add custom through model to taggit languages
            "method": self.get_method(dataset),
        }

        if dataset.dataset_type == "DR":
            # Data Review
            metadata["upload_type"] = "publication"
            metadata["publication_type"] = "other"
            creator = dict()
            creator["name"] = dataset.owner.get_academic_name()
            creator["affiliation"] = dataset.owner.get_main_affiliation_text()
            metadata["creators"] = [creator] # dataset.owner.get_academic_name() # 'creators': [{'name': 'Ilko Kucheriv Democratic Initiatives Foundation', 'affiliation': None}
        else:
            # Data Set
            metadata["upload_type"] = "dataset"
            metadata["creators"] = self.get_creators_list(dataset.get_creators())


        if self.get_creation_date(dataset, metadata):
            metadata["dates"] = [self.get_creation_date(dataset, metadata)]
        logger.debug(f"Zenodo metadata: {metadata}")
        return metadata
