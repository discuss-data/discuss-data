import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

from discuss_data.dhrep.token import InvalidTokenException, TokenHelper
from discuss_data.core.decorators import dd_tou_accepted, dd_user_verified

logger = logging.getLogger(__name__)


@login_required
@dd_tou_accepted
@dd_user_verified
def get_token(request):
    """ checks for a valid token stored in the session and
    shows a link to acquire a new token if no valid token is found

    :param request: object that contains metadata about the request
    :type request: HttpRequest
    :return: renders template to show token info or redirects to dataset publish process
    :rtype: HttpResponse, HttpResponseRedirect

    """

    th = TokenHelper()
    t = th.get_token_from_session(request)

    logger.debug(t)

    if not t:
        messages.warning(request, "No Token provided.")
        return render(request, "dhrep/_getToken.html", {"requestUri": th.request_url})

    if not t.check():
        messages.error(request, "Token " + t.access_token + " is " + t.info())
        request.session.pop("token")
        return render(request, "dhrep/_getToken.html", {"requestUri": th.request_url})

    messages.success(request, "Token " + t.access_token + " is " + t.info())
    ds_uuid = request.session.pop("ds_uuid", False)
    logger.debug(ds_uuid)
    if ds_uuid:
        return redirect("dddatasets:prep_edit_publish_final", ds_uuid)

    return render(
        request, "dhrep/_showToken.html", {"token": t.access_token, "info": t.info()},
    )


@login_required
@dd_tou_accepted
@dd_user_verified
def show_token(request, token):
    """[summary]

    :param request: object that contains metadata about the request
    :type request: HttpRequest
    :param token: token uri fragment
    :type token: str
    :return: renders template to show token info or redirects to dataset publish process
    :rtype: HttpResponse, HttpResponseRedirect

    Returns:
        [type] -- [description]
    """
    th = TokenHelper()
    t = th.get_token_from_session(request)
    if t:
        if t.check():
            messages.success(request, "Token " + t.access_token + " is " + t.info())
            ds_uuid = request.session.pop("ds_uuid", False)
            logger.debug(ds_uuid)
            if ds_uuid:
                return redirect("dddatasets:prep_edit_publish_final", ds_uuid)
            else:
                return render(
                    request,
                    "dhrep/_showToken.html",
                    {"token": t.access_token, "info": t.info()},
                )

        messages.error(request, "Token " + t.access_token + " is " + t.info())
        request.session.pop("token")

    try:
        t = th.create_token_object(token)
    except InvalidTokenException:
        messages.error(request, "Token " + token + " is malformed.")
        return render(request, "dhrep/_getToken.html", {"requestUri": th.request_url})

    if t.check():
        request.session["token"] = t.to_dict()
        messages.success(request, "Token " + t.access_token + " is " + t.info())
        ds_uuid = request.session.pop("ds_uuid", False)
        logger.debug(ds_uuid)
        if ds_uuid:
            return redirect("dddatasets:prep_edit_publish_final", ds_uuid)

        return render(
            request,
            "dhrep/_showToken.html",
            {"token": t.access_token, "info": t.info()},
        )

    messages.error(request, "Token " + t.access_token + " is " + t.info())
    return render(request, "dhrep/_getToken.html", {"requestUri": th.request_url},)
