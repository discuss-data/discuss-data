from django.apps import AppConfig


class DhrepConfig(AppConfig):
    name = "discuss_data.dhrep"
    label = "dddhrep"
    verbose_name = "Wrapper module to connect to the DARIAH Repository APIs"
