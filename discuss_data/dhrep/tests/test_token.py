import logging

import pytest
from django.core.exceptions import ImproperlyConfigured
from django.test import override_settings, modify_settings

from discuss_data.dhrep.token import InvalidTokenException, Token, TokenHelper

logger = logging.getLogger(__name__)

# testing the settings does not work properly
#
# def test_request_url(settings):
#     pdp_url = "https://pdpdev.de.dariah.eu"
#     client_id = "discussdata"
#     redirect_uri = "https://discuss-data.net/dhrep/token"
#     settings.DARIAH_PDP_URL = pdp_url
#     settings.DARIAH_PDP_CLIENT_ID = client_id
#     settings.DARIAH_PDP_REDIRECT_URI = redirect_uri
#     th = TokenHelper()
#     assert (
#         th.request_url
#         == pdp_url
#         + "/"
#         + "oauth2/oauth2/authorize?response_type=token"
#         + "&client_id="
#         + client_id
#         + "&scope=read,write"
#         + "&redirect_uri="
#         + redirect_uri
#     )


# @modify_settings(PDP_URL="http://pdpdev.de.dariah.eu")
# def test_pdp_settings():
#     with pytest.raises(ImproperlyConfigured):
#        TokenHelper()


def test_create_token_object():
    token = "access_token=0d0#344e5-96c0-441d-bc2c-8230f0cea2a9&token_type=bearer&expires_in=86400&scope=read,write"
    th: TokenHelper = TokenHelper()
    t: Token = th.create_token_object(token)
    assert t.access_token == "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9"
    assert t.token_type == "bearer"
    assert t.expires_in == "86400"
    assert t.scope == "read,write"


def test_create_invalid_token_object():
    token = "aksess_token=0d0#344e5-96c0-441d-bc2c-8230f0cea2a9&token_type=bearer&expires_in=86400&scope=read,write"
    th: TokenHelper = TokenHelper()
    with pytest.raises(InvalidTokenException):
        th.create_token_object(token)


def test_invalid_token():
    t = Token("0d0#344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write")
    assert not t.check()


def test_create_token_object_from_dict():
    token = {
        "access_token": "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9",
        "token_type": "bearer",
        "expires_in": "86400",
        "scope": "read,write",
    }
    th = TokenHelper()
    t = th.create_token_object_from_dict(token)
    assert t.access_token == "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9"
    assert t.token_type == "bearer"
    assert t.expires_in == "86400"
    assert t.scope == "read,write"


def test_create_invalid_key_token_object_from_dict():
    token = {
        "aksess_token": "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9",
        "token_type": "bearer",
        "expires_in": "86400",
        "scope": "read,write",
    }
    th = TokenHelper()
    with pytest.raises(InvalidTokenException):
        th.create_token_object_from_dict(token)


def test_to_dict():
    token = Token(
        "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
    )
    t = token.to_dict()
    assert t["access_token"] == "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9"
    assert t["token_type"] == "bearer"
    assert t["expires_in"] == "86400"
    assert t["scope"] == "read,write"


def test_str():
    token = Token(
        "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
    )
    assert token.__str__() == "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9"


def test_mock(requests_mock_token_not_found):

    token = Token(
        "0d0#344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
    )
    assert token.info() == "Unauthorized"
