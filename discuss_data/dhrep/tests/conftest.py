import logging

import pytest
from django.contrib.auth import get_user_model
from django.db import IntegrityError, transaction

User = get_user_model()

logger = logging.getLogger(__name__)


def create_user(user_dict):
    try:
        user = User.objects.get(username=user_dict["username"])
    except User.DoesNotExist:
        try:
            # see https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
            with transaction.atomic():
                user = User.objects.create_user(**user_dict)
        except IntegrityError as e:
            logger.error(e)
    return user


@pytest.fixture
def dariah_user(transactional_db):
    user = {
        "username": "marsilius",
        "first_name": "Marsilius",
        "last_name": "Inghen",
        "email": "marsilius@discuss-data.net",
        "is_staff": True,
        "is_active": True,
        "uuid": "7dd0691b-7df8-4167-9b32-81199e74e89e",
        "academic_title": "DR",
        "accepted_dariah_tou": True,
        "accepted_dd_tou": True,
        "show_first_steps": True,
    }
    return create_user(user)


@pytest.fixture
def requests_mock_create(requests_mock, settings):
    """mock create posts to dariahstorage, returning 3 different dariah storage id's.

    return id's (in order):
          * EAEA0-8AAB-4F0A-C67E-0
          * EAEA0-C314-FE32-9B64-0
          * EAEA0-9F76-BBFD-16AF-0

    https://requests-mock.readthedocs.io/en/latest/response.html#response-lists
    """

    requests_mock.post(
        settings.DARIAH_STORAGE_LOCATION,
        [
            {
                "headers": {
                    "Location": "http://cdstar.de.dariah.eu/dariah/EAEA0-8AAB-4F0A-C67E-0"
                },
                "status_code": 201,
            },
            {
                "headers": {
                    "Location": "http://cdstar.de.dariah.eu/dariah/EAEA0-C314-FE32-9B64-0"
                },
                "status_code": 201,
            },
            {
                "headers": {
                    "Location": "http://cdstar.de.dariah.eu/dariah/EAEA0-9F76-BBFD-16AF-0"
                },
                "status_code": 201,
            },
        ],
    )


@pytest.fixture
def requests_mock_publish(requests_mock, settings):
    storage_id = (
        "EAEA0-8AAB-4F0A-C67E-0"  # the first id from requests_mock_create-fixture
    )
    # the storage update with ttl mock
    requests_mock.put(settings.DARIAH_STORAGE_LOCATION + storage_id, status_code=201)
    # the dhpublish mock
    requests_mock.post(
        settings.DARIAH_PUBLISH_URL + storage_id + "/publish", status_code=200
    )


@pytest.fixture
def requests_mock_token_ok(requests_mock, settings):
    requests_mock.get(
        settings.DARIAH_STORAGE_LOCATION + "auth/info", status_code=200, reason="OK"
    )


@pytest.fixture
def requests_mock_token_not_found(requests_mock, settings):
    # check: status code really 200???
    requests_mock.get(
        settings.DARIAH_STORAGE_LOCATION + "auth/info",
        status_code=200,
        reason="Unauthorized",
    )
