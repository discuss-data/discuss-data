import pytest
from django.urls import reverse


def test_get_token_no_login(client):
    response = client.get(reverse("dhrep:get_token"))
    assert response.status_code == 302
    assert response.url == "/shib/login/?next=" + reverse("dhrep:get_token")


def test_show_token_no_login(client):
    token: str = "access_token=0d03344e5-96c0-441d-bc2c-8230f0cea2a9&token_type=bearer&expires_in=86400&scope=read,write"
    response = client.get(reverse("dhrep:show_token", kwargs={"token": token}))
    assert response.status_code == 302


def test_get_token(client, dariah_user):
    client.force_login(dariah_user)
    response = client.get(reverse("dhrep:get_token"))
    assert response.status_code == 200


def test_show_invalid_token(client, dariah_user, requests_mock_token_not_found):
    client.force_login(dariah_user)
    token: str = "access_token=0d03344e5-96c0-441d-bc2c-8230f0cea2a9&token_type=bearer&expires_in=86400&scope=read,write"
    response = client.get(reverse("dhrep:show_token", kwargs={"token": token}))
    assert response.status_code == 200


def test_show_valid_token(client, dariah_user, requests_mock_token_ok):
    client.force_login(dariah_user)
    token: str = "access_token=0d03344e5-96c0-441d-bc2c-8230f0cea2a9&token_type=bearer&expires_in=86400&scope=read,write"
    response = client.get(reverse("dhrep:show_token", kwargs={"token": token}))
    assert response.status_code == 200
