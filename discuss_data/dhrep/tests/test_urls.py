from django.urls import reverse, resolve


def test_get_token():
    assert reverse("dhrep:get_token") == "/dhrep/token/"
    assert resolve("/dhrep/token/").view_name == "dhrep:get_token"


def test_show_token():
    token: str = "access_token=0d03344e5-96c0-441d-bc2c-8230f0cea2a9&token_type=bearer&expires_in=86400&scope=read,write"
    assert (
        reverse("dhrep:show_token", kwargs={"token": token}) == "/dhrep/token/" + token
    )
    assert resolve("/dhrep/token/" + token).view_name == "dhrep:show_token"
    assert resolve("/dhrep/token/" + token).kwargs == {"token": token}
