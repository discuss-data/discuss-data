import pytest

from django.core.files.base import ContentFile

from discuss_data.dhrep.storage import Storage, StorageException
from discuss_data.dhrep.token import Token


# this does not really test the correct setting because setting override does not work that way!
def test_storage_url_no_trailing_slash(settings):
    """ DARIAH_STORAGE_LOCATION should always have a trailing slash
    """
    settings.DARIAH_STORAGE_LOCATION = "https://cdstar.de.dariah.eu/test/dariah"
    storage = Storage()
    assert storage.storage_url == "https://cdstar.de.dariah.eu/test/dariah/"


def test_create_good_token(requests_mock_create):
    """ Create object without content with valid token
    """
    token = Token(
        "0d0x344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
    )
    storage = Storage()
    assert isinstance(storage.create_object(token), str)


def test_create_invalid_token(settings, requests_mock):
    """ Create object without content with invalid token
    """
    with pytest.raises(StorageException, match="Error creating new cdstar object"):
        requests_mock.post(
            settings.DARIAH_STORAGE_LOCATION,
            json={"error": "pdp-error", "reason": "Token not found"},
            status_code=401,
        )
        token = Token(
            "0d0x344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
        )
        storage = Storage()
        storage.create_object(token)


def test_create_storage_not_reachable(settings, requests_mock):
    """ Test behaviour on connection problems
    """
    with pytest.raises(ConnectionError):
        requests_mock.post(settings.DARIAH_STORAGE_LOCATION, exc=ConnectionError)
        token = Token(
            "0d0x344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
        )
        storage = Storage()
        storage.create_object(token)


def test_update(settings, requests_mock):
    """ Update object with given id
    """
    storageid = "EAEA0-9F76-BBFD-16AF-0"
    requests_mock.put(settings.DARIAH_STORAGE_LOCATION + storageid, status_code=201)
    token = Token(
        "0d0x344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
    )
    storageid = "EAEA0-9F76-BBFD-16AF-0"
    tfile = ContentFile("some text here")
    tfile.file.content_type = "text/plain"
    storage = Storage()
    storage.update(token, storageid, tfile)


def test_update_invalid_token(settings, requests_mock):
    """ Update object with given id and invalid token
    """
    with pytest.raises(StorageException, match="Error updating cdstar object"):
        storageid = "EAEA0-9F76-BBFD-16AF-0"
        requests_mock.put(
            settings.DARIAH_STORAGE_LOCATION + storageid,
            json={
                "error": "pdp-error",
                "reason": "The access token you provided is invalid or has expired.",
            },
            status_code=401,
        )
        token = Token(
            "0d0x344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write"
        )
        cfile = ContentFile("some text content")
        cfile.file.content_type = "text/plain"
        storage = Storage()
        storage.update(token, storageid, cfile)
