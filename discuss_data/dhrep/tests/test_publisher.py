import os
import json
import time
import py
import pytest
from rdflib import Graph, Namespace
from discuss_data.dhrep.token import Token
from discuss_data.dddatasets.models import DataSet
from discuss_data.dhrep.publisher import Publisher, PublisherError
from discuss_data.dhrep.storage import StorageException

DCTERMS = Namespace("http://purl.org/dc/terms/")

# _dir = os.path.dirname(os.path.realpath(__file__))
# FIXTURE_DIR = py.path.local(_dir) / "../fixtures"

token = Token("0d0x344e5-96c0-441d-bc2c-8230f0cea2a9", "bearer", "86400", "read,write")
dataset = DataSet("")

storage_id = "EAEA0-8AAB-4F0A-C67E-0"  # the first id from requests_mock_create-fixture
collection: dict = {"collection_id": storage_id}

# testing the settings does not work properly

# def test_publish_url_no_trailing_slash(settings):
#     """ DARIAH_PUBLISH_URL should always have a trailing slash
#     """
#     settings.DARIAH_PUBLISH_URL = "https://trep.de.dariah.eu/1.0/dhpublish"

#     publisher = Publisher(token, dataset)
#     assert publisher.publish_url == "https://trep.de.dariah.eu/1.0/dhpublish/"


def test_publish():
    """ test publish process, all the server responses are ingested, compare conftest.py
    """

    publisher = Publisher(token, dataset)

    assert isinstance(publisher.token, Token)
    assert isinstance(publisher.dataset, DataSet)


def test_publish_invalid_token(settings, django_db_setup, requests_mock):
    """ invalid tokens should raise a StorageException
    """
    publisher = Publisher(token, dataset)
    requests_mock.post(
        settings.DARIAH_PUBLISH_URL + storage_id + "/publish",
        json={"error": "pdp-error", "reason": "Token not found"},
        status_code=401,
    )
    with pytest.raises(PublisherError, match="Error starting publication process"):
        publisher.publish(collection)


def test_publish_status(
    settings,
    django_db_setup,
    requests_mock,
    requests_mock_create,
    requests_mock_publish,
):
    """ a publish job should have a publish status, which changes in the process time and
        should have .status['finished'] = true when done
    """
    pass


def test_create_collection_rdf(requests_mock_create):
    """ test if generated RDF (turtle) for collection is valid RDF
    """
    pass
