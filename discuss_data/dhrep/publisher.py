import json
import logging

import requests
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.core.files.base import ContentFile
from django.core.validators import URLValidator
from django.template import loader
from django_q.tasks import async_task

from discuss_data.dhrep import settings
from discuss_data.dhrep.storage import Storage, StorageException
from discuss_data.dhrep.token import Token

logger = logging.getLogger(__name__)
dariah_storage = Storage()


class Publisher:
    """ Publish to the DARIAH-DE Repository

    Repository Frontend docs:
        https://repository.de.dariah.eu/doc/services/submodules/publikator/docs/

    Usage:
    ::
        # create a Publisher from a Token and a DataSet
        publisher = Publisher(t, ds)
        # create a collection in ownstorage
        collection = publisher.create_collection()
        # upload files from DataSet into collection
        publisher.upload(collection)
        # retrieve status of file upload
        status = publisher.upload_status(collection)
        # update the dataset and its files
        publisher.update_dataset(collection, status)

"""

    def __init__(self, token: Token, dataset) -> None:
        self._token = token
        self._dataset = dataset
        # in python3.8, we can use a TypedDict class to further specify the types of the dict entries

        validator = URLValidator(["https"])
        try:
            validator(settings.PUBLISH_URL)
        except ValidationError as e:
            raise ImproperlyConfigured from e

        publish_url = settings.PUBLISH_URL
        if not publish_url.endswith("/"):
            publish_url += "/"
        self._publish_url = publish_url

    @property
    def publish_url(self) -> str:
        return self._publish_url

    @property
    def token(self) -> Token:
        return self._token

    @property
    def dataset(self):
        return self._dataset

    def create_collection(self) -> dict:
        """creates all objects of a dataset in ownstorage
           and returns a collection dictionary

        :return: a collection dictionary
        :rtype: dict
        """
        token = self.token
        dataset = self.dataset
        # create collection
        collection_id = dariah_storage.create_object(token)

        datafiles = dataset.get_datafiles()

        logger.debug("datafiles: %s", datafiles)
        files = {}
        for datafile in datafiles:
            datafile_storage_id = dariah_storage.create_object(token)
            logger.debug("datafile_storage_id: %s", datafile_storage_id)

            # TODO: dict->dc-template
            files[datafile_storage_id] = {
                "id": str(datafile.id),
                "df_uuid": str(
                    datafile.uuid
                ),  # change to file-uuid or even better: link to the uuid or as for the dataset the doi (datacite)
                "name": str(datafile.name),
                # this is not necessarily the files content_type(!) and defaults to "text/plain" as for the model
                "content_type": str(datafile.content_type),
                "storage_id": str(datafile_storage_id),
            }

        logger.debug("files: %s", files)

        turtle = self.create_collection_rdf(collection_id, dataset, files)
        # ContentFile cannot handle UTF-8 properly -> encode
        tfile = ContentFile(turtle.encode("UTF-8"))
        task = ""
        # Upload turtlefile to the storage
        try:
            task = async_task(
                "discuss_data.dhrep.services.update", token, collection_id, tfile,
            )
        except StorageException as e:
            logger.error(e)

        collection = {
            "collection_id": collection_id,
            "files": files,
            "ds_uuid": str(dataset.uuid),
            "turtle_task": task,
        }

        logger.debug("collection: %s", collection)

        return collection

    @staticmethod
    def create_collection_rdf(storage_id: str, dataset, datafiles) -> str:
        """Create dariahrep collection rdf for a dataset

        https://repository.de.dariah.eu/doc/services/submodules/kolibri/kolibri-dhpublish-service/docs/index.html

        :param storage_id: dariahstorage id for the collection file, for self-reference
        :type storage_id: str
        :param dataset: a DataSet to generate rdf for
        :type dataset: DataSet
        :param datafiles: Array of DataFile contained in collection extended with their storage_id
        :type datafiles: Dict[str, Dict[str, DataFile]]
        :return: RDF (turtle) to represent the given dataset
        :rtype: str

        """

        turtle = loader.render_to_string(
            "collection.ttl",
            {"storage_id": storage_id, "dataset": dataset, "datafiles": datafiles},
        )
        logger.debug("[COLLECTION_RDF]: %s", turtle)

        return turtle

    def publish(self, collection):
        """Publish a collection

        :param collection:
        :type collection: dict
        :raises PublisherError: An error from publicator if HTTP-Status != 200

        """
        token = self.token
        storage_id = collection.get("collection_id", None)
        response = requests.post(
            self.publish_url + storage_id + "/publish",
            headers={
                "X-Storage-Token": token.access_token,
                "X-Transaction-ID": storage_id,  # set to a value that is unique per transaction!
            },
        )

        if response.status_code != 200:
            raise PublisherError(
                "Error starting publication process: "
                + response.text
                + " - "
                + str(response.status_code)
            )
        return response

    def publish_status(self, collection):
        """get status from publish service for a given collection

        :param collection: the collection dictionary as returned by create_collection
        :type collection: dict
        :raises PublisherError: An error from publicator if HTTP-Status != 200
        :return: publish status response
        :rtype: dict

        """
        token = self.token
        storage_id = collection.get("collection_id", None)

        if not storage_id:
            return {}

        response = requests.get(
            self.publish_url + storage_id + "/status",
            headers={
                "X-Storage-Token": token.access_token,
                "Accept": "application/json",
            },
        )

        logger.debug("status-text: %s", response.text)
        logger.debug("status-status: %s", response.status_code)

        if response.status_code != 200:
            raise PublisherError(
                "Error with publish status: "
                + response.text
                + " - "
                + str(response.status_code)
            )

        return json.loads(response.text)

    def update_dataset(self, collection, status):
        """Write DOIs from publishing into `DataSet` and its `DataFile`s

        :param collection: the collection dictionary as returned by create_collection
        :type collection: dict
        :param status: status response as returned by publish_status
        :type status: dict

        """
        dataset = self.dataset

        for file in status["publishObjects"]:
            uri = file.get("uri")
            storage_id = uri[uri.rfind("/") + 1 :]
            pid = file.get("pid")
            if collection["collection_id"] == storage_id:
                dataset.dhdoi = pid
                dataset.save()
            else:
                try:
                    uuid = collection["files"][storage_id]["df_uuid"]
                except KeyError as e:
                    logger.error(e)
                else:
                    datafile = dataset.get_datafile(uuid)
                    datafile.dhdoi = pid
                    datafile.save()

    def upload(self, collection):
        """starts file upload as async service

        :param collection: collection dictionary (to be typed)
        :type collection: dict

        :return: dict of files and tasks uuids
        :rtype: Dict[str, UUID]
        """

        token = self.token
        dataset = self.dataset

        files = collection.get("files")
        logger.debug("files: %s", str(files))
        datafiles = dataset.get_datafiles()
        logger.debug("datafiles: %s", str(datafiles))
        tasks = {}

        for datafile in datafiles:
            for file_id, file_properties in files.items():
                logger.debug(
                    "df_uuid: %s \t df_uuid: %s",
                    str(datafile.uuid),
                    str(file_properties["df_uuid"]),
                )
                if str(datafile.uuid) == str(file_properties["df_uuid"]):
                    logger.debug("file_object_type: %s", type(datafile.file))
                    logger.debug("file_attributes: %s", dir(datafile.file))
                    tasks[str(datafile.uuid)] = async_task(
                        "discuss_data.dhrep.services.update", token, file_id, datafile,
                    )

        return tasks

    def upload_status(self, collection):
        """get status from storage service for a given collection

        :param collection: the collection dictionary as returned by create_collection
        :type collection: dict
        :return:
        :rtype: dict
        """
        files = collection["files"]
        status = {}
        for file in files.keys():
            try:
                dariah_storage.get(self.token, file)
                logger.debug("DARIAH UPLOAD STATUS: %s finished", format(file))
            except StorageException as e:
                status[file] = False
                # do not show complete traceback in debug logging
                logger.debug("[[DARIAH UPLOAD STATUS]]: %s ", format(e.args[0]))
            else:
                status[file] = True

        return status


class PublisherError(Exception):
    """Thrown in case of problems with the publish service"""
