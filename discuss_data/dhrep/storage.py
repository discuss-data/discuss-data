""" Connection to DARIAH-DE storage

    Storage API docs: http://hdl.handle.net/11858/00-1734-0000-0009-FEA1-D
"""
import logging

import requests
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.core.validators import URLValidator

from discuss_data.dhrep import settings
from discuss_data.dhrep.token import Token

logger = logging.getLogger(__name__)


class Storage:
    """
    Methods to create and update data objects in DARIAH-DE storage
    """

    def __init__(self) -> None:
        validator = URLValidator(["https"])
        try:
            validator(settings.STORAGE_URL)
        except ValidationError as e:
            raise ImproperlyConfigured from e

        storage_url = settings.STORAGE_URL
        if not storage_url.endswith("/"):
            storage_url += "/"
        self._storage_url = storage_url

    @property
    def storage_url(self) -> str:
        """DARIAH-DE storage API endpoint
        """
        return self._storage_url

    def get(self, token: Token, storage_id: str):
        response = requests.get(
            self._storage_url + storage_id,
            headers={"Authorization": token.token_type + " " + token.access_token},
        )

        if response.status_code != 200:
            raise StorageException(
                "Not yet uploaded "
                + storage_id
                + ". "
                + str(response.status_code)
                + ": "
                + response.reason
            )
        return response

    def create_object(self, token: Token) -> str:
        """Create a new object in dariahstorage

        :param token: authentication token
        :type token: Token
        :raises StorageException: if response from storage had no 201 status code
        :return: the storage id of created object
        :rtype: str

        """
        logger.debug(token)

        response = requests.post(
            self._storage_url,
            headers={
                "Authorization": token.token_type + " " + token.access_token,
                "Content-type": "text/plain",
            },
        )

        if response.status_code != 201:
            raise StorageException(
                "Error creating new cdstar object: "
                + response.text
                + " - "
                + str(response.status_code)
            )

        storage_id = response.headers["location"].rsplit("/", 1)[-1]
        return storage_id

    def update(self, token: Token, storage_id: str, content) -> None:
        """Update an object in dariahstorage (DEPRECATED)

        :param token: authentication token
        :type token: Token
        :param storageid: the storage id
        :type storageid: str
        :param content: the data to store
        :type content: ContentFile
        :raises StorageException: if response from storage had no 201 status code

        """

        response = requests.put(
            self._storage_url + storage_id,
            headers={
                "Authorization": token.token_type + " " + token.access_token,
                "Content-type": content.file.content_type,
            },
            data=content.read(),
        )

        if response.status_code != 201:
            raise StorageException(
                "Error updating cdstar object "
                + storage_id
                + ": "
                + response.text
                + " - "
                + str(response.status_code)
            )


class StorageException(Exception):
    """Thrown in case of problems with the storage"""


# an instance of storage
# deprecated
storage = Storage()
