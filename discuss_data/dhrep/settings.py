from django.conf import settings

CRUD_URL = getattr(settings, "DARIAH_CRUD_URL", None)
DOI_PREFIX = getattr(settings, "DARIAH_DOI_PREFIX", None)
HDL_PREFIX = getattr(settings, "DARIAH_HDL_PREFIX", None)
PDP_CLIENT_ID = getattr(settings, "DARIAH_PDP_CLIENT_ID", None)
PDP_REDIRECT_URI = getattr(settings, "DARIAH_PDP_REDIRECT_URI", None)
PDP_URL = getattr(settings, "DARIAH_PDP_URL", None)
PUBLISH_URL = getattr(settings, "DARIAH_PUBLISH_URL", None)
STORAGE_URL = getattr(settings, "DARIAH_STORAGE_LOCATION", None)
