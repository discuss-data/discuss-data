from django.urls import path

from discuss_data.dhrep import views

app_name = "dhrep"
urlpatterns = [
    path("token/<token>", views.show_token, name="show_token"),
    path("token/", views.get_token, name="get_token"),
]
