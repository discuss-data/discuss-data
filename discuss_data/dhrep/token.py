import logging
from typing import Dict

import requests
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.core.validators import URLValidator

from discuss_data.dhrep import settings

logger = logging.getLogger(__name__)


class Token:
    """Manage DARIAH-DE Repository token

    API Documentation:
        https://repository.de.dariah.eu/doc/services/submodules/kolibri/kolibri-dhpublish-service/docs/index.html#api-documentation

    Usage:

    """

    def __init__(
        self, access_token: str, token_type: str, expires_in: str, scope: str
    ) -> None:
        self._access_token = access_token
        self._token_type = token_type
        self._expires_in = expires_in
        self._scope = scope

    @property
    def access_token(self) -> str:
        return str(self._access_token)

    @property
    def token_type(self) -> str:
        return str(self._token_type)

    @property
    def expires_in(self) -> str:
        return str(self._expires_in)

    @property
    def scope(self) -> str:
        return str(self._scope)

    def check(self) -> bool:
        """check token validity

            :return: true if token is valid, false otherwise
            :rtype: bool

        """
        response = requests.get(
            settings.STORAGE_URL + "auth/info",
            headers={"Authorization": self.token_type + " " + self.access_token},
        ).ok

        return response

    def info(self) -> str:
        """inform about token validity

            :return: the status of the token: "OK", "Gone"
            :rtype: str
        """
        response = requests.get(
            settings.STORAGE_URL + "auth/info",
            headers={"Authorization": self.token_type + " " + self.access_token},
        ).reason

        return response

    def to_dict(self) -> Dict[str, str]:
        """convert a token object to a dictionary

            :return: the token as a dictionary
            :rtype: dict
        """
        return {
            "access_token": self.access_token,
            "token_type": self.token_type,
            "expires_in": self.expires_in,
            "scope": self.scope,
        }

    def __str__(self) -> str:
        return self.access_token


class TokenHelper:
    """Help managing DARIAH-DE Repository token

    objects of this class provide access to the token configuration and provide helper
    functions to convert from and to Token objects

    """

    def __init__(self):

        validator = URLValidator(["https"])
        try:
            validator(settings.PDP_URL)
            validator(settings.PDP_REDIRECT_URI)
        except ValidationError as e:
            logger.error(e)
            raise ImproperlyConfigured

        pdp_url = settings.PDP_URL
        if not pdp_url.endswith("/"):
            pdp_url += "/"

        self._request_url = (
            pdp_url
            + "oauth2/oauth2/authorize?response_type=token"
            + "&client_id="
            + settings.PDP_CLIENT_ID
            + "&scope=read,write"
            + "&redirect_uri="
            + settings.PDP_REDIRECT_URI
        )

    @property
    def request_url(self) -> str:
        return str(self._request_url)

    @staticmethod
    def create_token_object(token: str) -> Token:
        """takes a token uri fragment and returns a token object

            :param token: token uri fragment
            :type token: str
            :raises InvalidTokenException: the given token fragment can not be converted
             into a Token object
            :return: the resulting token object
            :rtype: Token

        """

        d = dict(x.split("=") for x in token.split("&"))
        try:
            t = Token(d["access_token"], d["token_type"], d["expires_in"], d["scope"])
        except (ValueError, KeyError) as e:
            logger.error(e)
            raise InvalidTokenException("Invalid Token: " + token)

        return t

    @staticmethod
    def create_token_object_from_dict(token: Dict[str, str]) -> Token:
        """takes a token in dict format, e.g. from the session dictionary,
        and converts it to a token object

            :param token: token dictionary
            :type token: str
            :return: the resulting Token object
            :rtype: Token

        """

        try:
            t = Token(
                token["access_token"],
                token["token_type"],
                token["expires_in"],
                token["scope"],
            )
        except (TypeError, KeyError) as e:
            logger.error(e)
            raise InvalidTokenException(e)
        else:
            return t

    def get_token_from_session(self, request):
        """tries to get the token from the session dictionary

        :param request: the view request
        :type request: HttpRequest

        :return: a token object if there is one stored in the session
        :rtype: Token Only, if there is a token stored, None otherwise.

        """
        if request.session.get("token", None):
            try:
                t = self.create_token_object_from_dict(request.session.get("token"))

            except InvalidTokenException:
                return None
            else:
                return t

        return None


class InvalidTokenException(Exception):
    """Thrown when there is something wrong with the token"""
