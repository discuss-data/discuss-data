"""Provide an asynchronous publish service

"""

import logging

import requests

from discuss_data.dddatasets.models import DataFile
from discuss_data.dhrep import settings
from discuss_data.dhrep.storage import StorageException
from discuss_data.dhrep.token import Token

logger = logging.getLogger(__name__)


def update(token: Token, storage_id: str, file, content_type=None) -> None:
    """Update an object in dariahstorage

        :param token: authentication token
        :type token: Token
        :param storageid: the storage id
        :type storageid: str
        :param file: the data to store
        :type file: ContentFile or DataFile
        :raises StorageException: if response from storage had no 201 status code
    """

    if isinstance(file, DataFile):
        logger.debug("file.file_object_type: %s", type(file.file))
        logger.debug("file.file_attributes: %s", dir(file.file))
        content = file.file
        content_type = "application"
    else:
        content = file  # https://docs.djangoproject.com/en/2.2/ref/models/fields/#django.db.models.fields.files.FieldFile
        content_type = "text/plain; charset=utf-8"

    with content.open(mode="rb") as stream:
        response = requests.put(
            settings.STORAGE_URL + storage_id,
            headers={
                "Authorization": token.token_type + " " + token.access_token,
                "Content-Type": content_type,
            },
            data=stream,
        )

    if response.status_code != 201:
        raise StorageException(
            "Error updating cdstar object "
            + storage_id
            + ": "
            + response.text
            + " - "
            + str(response.status_code)
        )
