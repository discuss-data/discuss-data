from django.apps import AppConfig


class UtilsConfig(AppConfig):
    name = "discuss_data.utils"
