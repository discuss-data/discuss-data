<!--
Before your file a feature request, please read

https://discuss-data.pages.gwdg.de/discuss-data/docs/contribution.html

The amount of developer time Discuss Data has available is very small.
Requesting a feature is no guarantee that it will get implemented. Someone
(maybe you) needs to step up to do the work.
-->

## Summary
<!-- Summarize the requested feature in a few sentences.  -->

## Feature details
<!-- A step-by-step list of what the feature should achieve (where applicable) -->

## Implementation in Other Systems (optional)
<!-- Does this feature already exist elsewhere? How does it work there? Try
to provide as many details as possible -->


<!-- Adapted from https://gitlab.freedesktop.org/libinput/libinput/-/blob/master/.gitlab/issue_templates/FeatureRequest.md -->

/label ~"type::Feature" ~"priority::None"


NOTES
 describe the need and use case. Include answers to such questions as “What problem it will solve?” and “How will it benefit the users?”