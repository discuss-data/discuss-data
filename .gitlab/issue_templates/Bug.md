<!--- Provide a general summary of the issue in the Title above -->

## Expected Behavior
<!--- Tell us what should happen -->

## Current Behavior
<!--- Tell us what happens instead of the expected behavior -->

## Steps to Reproduce
<!--- Provide a link to a live example, or an unambiguous set of steps to -->
<!--- reproduce this bug. Include code to reproduce, if relevant -->

1.
2.
3.
4.

## Additional Information (optional)

### Possible Solution
<!--- Not obligatory, but suggest a fix/reason for the bug, -->

### Context
<!--- How has this issue affected you? What are you trying to accomplish? -->

### Detailed Description
<!--- Provide a detailed description of the change or addition you are proposing -->

### Possible Implementation
<!--- Not obligatory, but suggest an idea for implementing addition or change -->


<!-- Adapted from https://raw.githubusercontent.com/stevemao/github-issue-templates/master/bugs-only/ISSUE_TEMPLATE.md -->

/label ~"type::Bug" ~"priority::None"

NOTES

For bugs, create a reproducible test case. To effectively address, the bug must be repeatable on demand.